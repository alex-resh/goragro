<?php

use yii\db\Migration;

/**
 * Class m200409_080739_categories_fix
 */
class m200409_080739_categories_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%category}}', 'lft');
        $this->dropColumn('{{%category}}', 'rgt');
        $this->dropColumn('{{%category}}', 'depth');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%category}}', 'lft', $this->integer());
        $this->addColumn('{{%category}}', 'rgt', $this->integer());
        $this->addColumn('{{%category}}', 'depth', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200409_080739_categories_fix cannot be reverted.\n";

        return false;
    }
    */
}
