<?php

use yii\db\Migration;

/**
 * Class m200323_123156_add_city_lat_lon_to_address
 */
class m200323_123156_add_city_lat_lon_to_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%organisation_adress}}', 'lat', $this->float(8));
        $this->addColumn('{{%organisation_adress}}', 'lon', $this->float(8));
        $this->addColumn('{{%organisation_adress}}', 'city_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%organisation_adress}}', 'city_id');
        $this->dropColumn('{{%organisation_adress}}', 'lon');
        $this->dropColumn('{{%organisation_adress}}', 'lat');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200323_123156_add_city_lat_lon_to_address cannot be reverted.\n";

        return false;
    }
    */
}
