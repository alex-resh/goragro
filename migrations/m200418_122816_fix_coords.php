<?php

use yii\db\Migration;

/**
 * Class m200418_122816_fix_coords
 */
class m200418_122816_fix_coords extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%organisation_adress}}', 'lat', $this->string(16));
        $this->alterColumn('{{%organisation_adress}}', 'lon', $this->string(16));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%organisation_adress}}', 'lat', $this->float());
        $this->alterColumn('{{%organisation_adress}}', 'lon', $this->float());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200418_122816_fix_coords cannot be reverted.\n";

        return false;
    }
    */
}
