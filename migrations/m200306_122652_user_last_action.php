<?php

use yii\db\Migration;

/**
 * Class m200306_122652_user_last_action
 */
class m200306_122652_user_last_action extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'last_action_time', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'last_action_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200306_122652_user_last_action cannot be reverted.\n";

        return false;
    }
    */
}
