<?php

use yii\db\Migration;

/**
 * Class m200508_070359_fie_tags_count
 */
class m200508_070359_fie_tags_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%news_tag}}', 'views_count', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%news_tag}}', 'views_count', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200508_070359_fie_tags_count cannot be reverted.\n";

        return false;
    }
    */
}
