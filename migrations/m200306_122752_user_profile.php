<?php

use yii\db\Migration;

/**
 * Class m200306_122752_user_profile
 */
class m200306_122752_user_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'phone', $this->string());
        $this->addColumn('{{%profile}}', 'name_prefix', $this->string());
        $this->addColumn('{{%profile}}', 'about', $this->text());
        $this->addColumn('{{%profile}}', 'img', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'img');
        $this->dropColumn('{{%profile}}', 'about');
        $this->dropColumn('{{%profile}}', 'name_prefix');
        $this->dropColumn('{{%profile}}', 'phone');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200306_122752_user_profile cannot be reverted.\n";

        return false;
    }
    */
}
