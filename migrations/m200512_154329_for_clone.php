<?php

use yii\db\Migration;

/**
 * Class m200512_154329_for_clone
 */
class m200512_154329_for_clone extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%organisation}}', 'clone_id', $this->integer());
        $this->addColumn('{{%organisation_gallery}}', 'clone_id' , $this->integer());
        $this->addColumn('{{%action}}', 'clone_id', $this->integer());
        $this->addColumn('{{%additionally_data}}', 'clone_id', $this->integer());
        $this->addColumn('{{%product}}', 'clone_id', $this->integer());
        $this->addForeignKey('fk-action-organisation-organisation_id', '{{%action}}', 'organisation_id', '{{%organisation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-organisation-clone_id', '{{%organisation}}', 'clone_id', '{{%organisation}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organisation-clone_id', '{{%organisation}}');
        $this->dropForeignKey('fk-action-organisation-organisation_id', '{{%action}}');
        $this->dropColumn('{{%product}}', 'clone_id');
        $this->dropColumn('{{%additionally_data}}', 'clone_id');
        $this->dropColumn('{{%action}}', 'clone_id');
        $this->dropColumn('{{%organisation_gallery}}', 'clone_id');
        $this->dropColumn('{{%organisation}}', 'clone_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200512_154329_for_clone cannot be reverted.\n";

        return false;
    }
    */
}
