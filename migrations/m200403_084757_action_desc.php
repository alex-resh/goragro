<?php

use yii\db\Migration;

/**
 * Class m200403_084757_action_desc
 */
class m200403_084757_action_desc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%action}}', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%action}}', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200403_084757_action_desc cannot be reverted.\n";

        return false;
    }
    */
}
