<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additionally_category}}`.
 */
class m200312_053321_create_additionally_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additionally_category}}', [
            'id' => $this->primaryKey(),
            'additionally_id' => $this->integer(),
            'category_id' => $this->integer(),
            'pos_a_in_c' => $this->smallInteger(4),
            'pos_c_in_a' => $this->smallInteger(4),
        ]);

        $this->addForeignKey('fk-add_cat-add_id', '{{%additionally_category}}', 'additionally_id', '{{%additionally}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-add_cat-cat_id', '{{%additionally_category}}', 'category_id', '{{%category}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-add_cat-cat_id', '{{%additionally_category}}');
        $this->dropForeignKey('fk-add_cat-add_id', '{{%additionally_category}}');

        $this->dropTable('{{%additionally_category}}');
    }
}
