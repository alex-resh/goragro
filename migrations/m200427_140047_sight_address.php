<?php

use yii\db\Migration;

/**
 * Class m200427_140047_sight_address
 */
class m200427_140047_sight_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sight}}', 'address', $this->string());
        $this->addColumn('{{%sight}}', 'lat', $this->string());
        $this->addColumn('{{%sight}}', 'lon', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sight}}', 'address');
        $this->dropColumn('{{%sight}}', 'lat');
        $this->dropColumn('{{%sight}}', 'lon');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200427_140047_sight_address cannot be reverted.\n";

        return false;
    }
    */
}
