<?php

use yii\db\Migration;

/**
 * Class m200428_111422_likes
 */
class m200428_111422_likes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%likes}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(),
            'entity_id' => $this->integer(),
            'likes' => $this->integer(),
            'dislikes' => $this->integer()
        ]);

        $this->createTable('{{%likes_to_user}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'like_id' => $this->integer(),
            'value' => $this->integer()
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%likes}}');
        $this->dropTable('{{%likes_to_user}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_111422_likes cannot be reverted.\n";

        return false;
    }
    */
}
