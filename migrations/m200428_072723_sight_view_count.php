<?php

use yii\db\Migration;

/**
 * Class m200428_072723_sight_view_count
 */
class m200428_072723_sight_view_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sight}}', 'views_count', $this->integer()->defaultValue(0)->notNull());
        $this->addColumn('{{%sight}}', 'user_id', $this->integer());
        $this->addColumn('{{%sight}}', 'created_at', $this->integer());
        $this->addColumn('{{%sight}}', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sight}}', 'views_count');
        $this->dropColumn('{{%sight}}', 'user_id');
        $this->dropColumn('{{%sight}}', 'created_at');
        $this->dropColumn('{{%sight}}', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200428_072723_sight_view_count cannot be reverted.\n";

        return false;
    }
    */
}
