<?php

use yii\db\Migration;

/**
 * Class m200401_091357_add_user_id_to_organisation
 */
class m200401_091357_add_user_id_to_organisation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%organisation}}', 'user_id', $this->integer());
        $this->addForeignKey('fk-organisation_user-user_id', '{{%organisation}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organisation_user-user_id', '{{%organisation}}');
        $this->dropColumn('{{%organisation}}', 'user_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200401_091357_add_user_id_to_organisation cannot be reverted.\n";

        return false;
    }
    */
}
