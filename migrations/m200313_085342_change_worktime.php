<?php

use yii\db\Migration;

/**
 * Class m200313_085342_change_worktime
 */
class m200313_085342_change_worktime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%organisation_worktime}}');

        $this->createTable('{{%organisation_worktime}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'mo_start'      => $this->time(),
                'mo_end'        => $this->time(),
                'mo_br_start'   => $this->time(),
                'mo_br_end'     => $this->time(),
                'tu_start'      => $this->time(),
                'tu_end'        => $this->time(),
                'tu_br_start'   => $this->time(),
                'tu_br_end'     => $this->time(),
                'we_start'      => $this->time(),
                'we_end'        => $this->time(),
                'we_br_start'   => $this->time(),
                'we_br_end'     => $this->time(),
                'th_start'      => $this->time(),
                'th_end'        => $this->time(),
                'th_br_start'   => $this->time(),
                'th_br_end'     => $this->time(),
                'fr_start'      => $this->time(),
                'fr_end'        => $this->time(),
                'fr_br_start'   => $this->time(),
                'fr_br_end'     => $this->time(),
                'sa_start'      => $this->time(),
                'sa_end'        => $this->time(),
                'sa_br_start'   => $this->time(),
                'sa_br_end'     => $this->time(),
                'su_start'      => $this->time(),
                'su_end'        => $this->time(),
                'su_br_start'   => $this->time(),
                'su_br_end'     => $this->time(),
            ]
        );

        $this->addForeignKey('fk-worktime_organisation-organisation_id', '{{%organisation_worktime}}', 'organisation_id', '{{%organisation}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-worktime_organisation-organisation_id', '{{%organisation_worktime}}');
        $this->dropTable('{{%organisation_worktime}}');
        $this->createTable(
            '{{%organisation_worktime}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'day'=>$this->integer(),
                'start'=>$this->time(),
                'end'=>$this->time(),
            ]
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200313_085342_change_worktime cannot be reverted.\n";

        return false;
    }
    */
}
