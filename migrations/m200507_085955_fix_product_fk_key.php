<?php

use yii\db\Migration;

/**
 * Class m200507_085955_fix_product_fk_key
 */
class m200507_085955_fix_product_fk_key extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-product_cat-category-category_id', '{{%product_category}}');
        $this->addForeignKey('fk-product_cat-category-category_id', '{{%product_category}}', 'category_id', '{{%category}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200507_085955_fix_product_fk_key cannot be reverted.\n";

        return false;
    }
    */
}
