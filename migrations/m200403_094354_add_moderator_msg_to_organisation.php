<?php

use yii\db\Migration;

/**
 * Class m200403_094354_add_moderator_msg_to_organisation
 */
class m200403_094354_add_moderator_msg_to_organisation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%organisation}}', 'moderator_msg', $this->text());
        $this->addColumn('{{%organisation}}', 'status', $this->smallInteger(2)->defaultValue(0));
        $this->addColumn('{{%organisation}}', 'created_at', $this->integer());
        $this->addColumn('{{%organisation}}', 'updated_at', $this->integer());
        $this->addColumn('{{%organisation}}', 'views_count', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%organisation}}', 'views_count');
        $this->dropColumn('{{%organisation}}', 'updated_at');
        $this->dropColumn('{{%organisation}}', 'created_at');
        $this->dropColumn('{{%organisation}}', 'status');
        $this->dropColumn('{{%organisation}}', 'moderator_msg');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200403_094354_add_moderator_msg_to_organisation cannot be reverted.\n";

        return false;
    }
    */
}
