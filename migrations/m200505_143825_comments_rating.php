<?php

use yii\db\Migration;

/**
 * Class m200505_143825_comments_rating
 */
class m200505_143825_comments_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comment}}', 'rating', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comment}}', 'rating');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200505_143825_comments_rating cannot be reverted.\n";

        return false;
    }
    */
}
