<?php

use yii\db\Migration;

/**
 * Class m200512_062927_agent
 */
class m200512_062927_agent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%agent}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'organisation_id' => $this->integer(),
            'name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);


        $this->addForeignKey('agent_user', '{{%agent}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('agent_organisation', '{{%agent}}', 'organisation_id', '{{%organisation}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%agent}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200512_062927_agent cannot be reverted.\n";

        return false;
    }
    */
}
