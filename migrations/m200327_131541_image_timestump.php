<?php

use yii\db\Migration;

/**
 * Class m200327_131541_image_timestump
 */
class m200327_131541_image_timestump extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%image}}', 'created_at', $this->integer());
        $this->addColumn('{{%image}}', 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%image}}', 'created_at');
        $this->dropColumn('{{%image}}', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200327_131541_image_timestump cannot be reverted.\n";

        return false;
    }
    */
}
