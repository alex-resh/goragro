<?php

use yii\db\Migration;

/**
 * Class m200427_075436_action_fix_price
 */
class m200427_075436_action_fix_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%action}}', 'old_price', $this->float());
        $this->alterColumn('{{%action}}', 'new_price', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%action}}', 'old_price', $this->decimal(2,0));
        $this->alterColumn('{{%action}}', 'new_price', $this->decimal(2,0));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200427_075436_action_fix_price cannot be reverted.\n";

        return false;
    }
    */
}
