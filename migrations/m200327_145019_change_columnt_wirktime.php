<?php

use yii\db\Migration;

/**
 * Class m200327_145019_change_columnt_wirktime
 */
class m200327_145019_change_columnt_wirktime extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%organisation_worktime}}', 'mo_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'mo_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'mo_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'mo_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'tu_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'tu_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'tu_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'tu_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'we_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'we_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'we_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'we_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'th_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'th_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'th_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'th_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'fr_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'fr_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'fr_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'fr_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'sa_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'sa_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'sa_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'sa_br_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'su_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'su_end', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'su_br_start', $this->string(5));
        $this->alterColumn('{{%organisation_worktime}}', 'su_br_end', $this->string(5));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200327_145019_change_columnt_wirktime cannot be reverted.\n";

        return false;
    }
    */
}
