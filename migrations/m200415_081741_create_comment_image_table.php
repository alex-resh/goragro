<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment_image}}`.
 */
class m200415_081741_create_comment_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment_image}}', [
            'id' => $this->primaryKey(),
            'comment_id' => $this->integer(),
            'img' => $this->string(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-comment_image-comment-comment_id', '{{%comment_image}}', 'comment_id', '{{%comment}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-comment_image-comment-comment_id', '{{%comment_image}}');

        $this->dropTable('{{%comment_image}}');
    }
}
