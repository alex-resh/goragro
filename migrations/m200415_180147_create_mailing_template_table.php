<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mailing_template}}`.
 */
class m200415_180147_create_mailing_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mailing_template}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->string(),
            'from' => $this->string(),
            'subject' => $this->string(),
            'body' => $this->text(),
            'html' => $this->boolean(),
            'status' => $this->smallInteger(2)->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mailing_template}}');
    }
}
