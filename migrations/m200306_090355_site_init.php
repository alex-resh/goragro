<?php

use yii\db\Migration;

/**
 * Class m200306_090355_init
 */
class m200306_090355_site_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%category}}',
            [
                'id' => $this->primaryKey(),
                //'tree' => $this->integer()->notNull(),
                'lft' => $this->integer()->notNull(),
                'rgt' => $this->integer()->notNull(),
                'depth' => $this->integer()->notNull(),
                'name' => $this->string()->notNull(),
                'url' => $this->string()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]

        );
        $this->createTable(
            '{{%page}}',[
                'id' =>$this->primaryKey(),
                'text' =>$this->text(),
                'title'=>$this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'seo_url' =>$this->string(),
                'seo_title'=>$this->string(),
                'seo_keywords'=>$this->string(),
                'seo_desc'=>$this->text(),
            ]
        );
        $this->createTable(
            '{{%news}}',[
                'id' => $this->primaryKey(),
                'category_id' => $this->integer(),
                'news_category_id' => $this->integer(),
                'text' =>$this->text(),
                'title'=>$this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'status' => $this->integer(),
                'seo_url' =>$this->string(),
                'seo_title'=>$this->string(),
                'seo_keywords'=>$this->string(),
                'seo_desc'=>$this->text(),
                'img' => $this->string(),
            ]
        );
        $this->createTable(
            '{{%article}}',[
                'id' =>$this->primaryKey(),
                'text' =>$this->text(),
                'title'=>$this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
                'user_id'=>$this->integer(),
                'views_count'=>$this->integer(),
                'type'=>$this->integer(),
                //Blog,Recipe,Humor
            ]
        );
        $this->createTable(
            '{{%favorite}}',[
                'id'=>$this->primaryKey(),
                'entity'=>$this->string(),
                'entity_id'=>$this->integer(),
                'user_id'=>$this->integer(),
            ]
        );
        $this->createTable(
            '{{%settings}}',[
                'id'=>$this->primaryKey(),
                'key'=>$this->string(),
                'value'=>$this->text(),
            ]
        );
        $this->createTable(
            '{{%sight}}',[
                'id'=>$this->primaryKey(),
                'city_id'=>$this->integer(),
                'title'=>$this->string(),
                'text'=>$this->text(),
            ]
        );
        $this->createTable(
            '{{%sight_gallery}}',[
                'id'=>$this->primaryKey(),
                'sight_id'=>$this->integer(),
                'img'=>$this->string(),
            ]
        );
        //organisation begin
        $this->createTable(
            '{{%organisation}}',[
                'id'=>$this->primaryKey(),
                'name'=>$this->string(),
                'desc'=>$this->text(),
                'img'=>$this->string(),
            ]
        );
        $this->createTable(
            '{{%organisation_worktime}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'day'=>$this->integer(),
                'start'=>$this->time(),
                'end'=>$this->time(),
            ]
        );
        $this->createTable(
            '{{%organisation_category}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'category_id'=>$this->integer(),
            ]
        );
        $this->createTable(
            '{{%organisation_city}}',[
                'id'=>$this->primaryKey(),
                'organisation_city'=>$this->integer(),
                'city_id'=>$this->integer(),
            ]
        );
        $this->createTable(
            '{{%organisation_adress}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'postal_code'=>$this->string(),
                'street'=>$this->string(),
                'house'=>$this->string(),
                'flat'=>$this->string(),
                'flooar'=>$this->string(),
                'phones'=>$this->text(),
                'emails'=>$this->text(),
                'url'=>$this->string(),
                'social'=>$this->text(),
            ]
        );
        $this->createTable(
            '{{%organisation_gallery}}',[
                'id'=>$this->primaryKey(),
                'organisation_id'=>$this->integer(),
                'img'=>$this->string(),
                'lat' => $this->string(),
                'lon' => $this->string(),
            ]
        );
        $this->createTable(
            '{{%blog_city}}',[
                'id' => $this->primaryKey(),
                'blog_id' => $this->integer(),
                'city_id' => $this->integer(),
            ]
        );
        $this->createTable(
            '{{%news_category}}',[
                'id'=>$this->primaryKey(),
                'name'=>$this->string(),
            ]
        );
        //organisation end
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news_category}}');
        $this->dropTable('{{%blog_city}}');
        $this->dropTable('{{%organisation_gallery}}');
        $this->dropTable('{{%organisation_adress}}');
        $this->dropTable('{{%organisation_city}}');
        $this->dropTable('{{%organisation_category}}');
        $this->dropTable('{{%organisation_worktime}}');
        $this->dropTable('{{%organisation}}');
        $this->dropTable('{{%sight_gallery}}');
        $this->dropTable('{{%sight}}');
        $this->dropTable('{{%settings}}');
        $this->dropTable('{{%favorite}}');
        $this->dropTable('{{%article}}');
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%page}}');
        $this->dropTable('{{%category}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200214_071448_init cannot be reverted.\n";

        return false;
    }
    */
}

