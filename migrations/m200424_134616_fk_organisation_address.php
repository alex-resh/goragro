<?php

use yii\db\Migration;

/**
 * Class m200424_134616_fk_organisation_address
 */
class m200424_134616_fk_organisation_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('fk-organisation-address', '{{%organisation_adress}}', 'organisation_id', '{{organisation}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-organisation-org_cat', '{{%organisation_category}}', 'category_id', '{{category}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-organisation-org_cat-org', '{{%organisation_category}}', 'organisation_id', '{{organisation}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organisation-address', '{{%organisation_adress}}');
        $this->dropForeignKey('fk-organisation-org_cat', '{{%organisation_category}}');
        $this->dropForeignKey('fk-organisation-org_cat-org', '{{%organisation_category}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_134616_fk_organisation_address cannot be reverted.\n";

        return false;
    }
    */
}
