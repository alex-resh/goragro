<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mailing}}`.
 */
class m200415_182314_create_mailing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mailing}}', [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer(),
            'u_count' => $this->integer(),
            's_count' => $this->integer(),
            'start' => $this->integer(),
            'end' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-mailing-mailing_template-template_id', '{{%mailing}}', 'template_id', '{{%mailing_template}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-mailing-mailing_template-template_id', '{{%mailing}}');

        $this->dropTable('{{%mailing}}');
    }
}
