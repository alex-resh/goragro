<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m200505_121937_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%product}}');
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'organisation_id' => $this->integer(),
            'class_name' => $this->string(),
            'title' => $this->string(),
            'description' => $this->integer(),
            'img' => $this->string(),
            'price' => $this->integer(),
            'discount' => $this->integer(),
            'type' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->dropTable('{{%product_category}}');
        $this->createTable('{{%product_category}}', [
            'id' => $this->primaryKey(),
            'product_class_name' => $this->string(),
            'category_id' => $this->integer(),
            'status' => $this->smallInteger(2)->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-product_cat-category-category_id', '{{%product_category}}', 'category_id', '{{%organisation_category}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('i_product_cat-class_name', '{{%product_category}}', 'product_class_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-product_cat-category-category_id', '{{%product_category}}');
    }
}
