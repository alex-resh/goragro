<?php

use yii\db\Migration;

/**
 * Class m200505_064945_news_tags_count
 */
class m200505_064945_news_tags_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%news_tag}}', 'views_count', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{$news_tag}}', 'views_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200505_064945_news_tags_count cannot be reverted.\n";

        return false;
    }
    */
}
