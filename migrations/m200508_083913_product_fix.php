<?php

use yii\db\Migration;

/**
 * Class m200508_083913_product_fix
 */
class m200508_083913_product_fix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%product}}', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200508_083913_product_fix cannot be reverted.\n";

        return false;
    }
    */
}
