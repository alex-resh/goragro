<?php

use yii\db\Migration;

/**
 * Class m200416_085357_org_region
 */
class m200416_085357_org_region extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%organisation_adress}}', 'region', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%organisation_adress}}', 'region');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_085357_org_region cannot be reverted.\n";

        return false;
    }
    */
}
