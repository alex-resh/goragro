<?php

use yii\db\Migration;

/**
 * Class m200327_073307_actions
 */
class m200327_073307_actions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%action}}', [
            'id' => $this->primaryKey(),
            'organisation_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'old_price' => $this->money(2),
            'new_price' => $this->money(2),
            'discount' => $this->float(),
            'image_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createTable('{{%product_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'organisation_id' => $this->integer(),
            'product_category|_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'old_price' => $this->money(2),
            'new_price' => $this->money(2),
            'discount' => $this->float(),
            'image_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'img' => $this->string()
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%action}}');
        $this->dropTable('{{%actions_image}}');
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%image}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200327_073307_actions cannot be reverted.\n";

        return false;
    }
    */
}
