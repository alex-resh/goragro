<?php

use yii\db\Migration;

/**
 * Class m200306_153039_additionally
 */
class m200306_153039_additionally extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additionally}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->text(),
            'type' => $this->smallInteger(2),
            'data' => $this->json(),
            'status' => $this->smallInteger(2)->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%additionally}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200306_153039_additionally cannot be reverted.\n";

        return false;
    }
    */
}
