<?php

use yii\db\Migration;

/**
 * Class m200512_124021_moderation_comments
 */
class m200512_124021_moderation_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comment}}', 'moderated_by', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comment}}', 'moderated_by');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200512_124021_moderation_comments cannot be reverted.\n";

        return false;
    }
    */
}
