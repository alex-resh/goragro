<?php

use yii\db\Migration;

/**
 * Class m200311_083017_news_extend
 */
class m200311_083017_news_extend extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%news}}', 'news_category_id');
        $this->addColumn('{{%news}}', 'city_id', $this->integer());
        $this->addColumn('{{%news}}', 'user_id', $this->integer());

        $this->createTable('{{%news_tag}}', [
            'id' => $this->primaryKey(),
            'tag' => $this->string()
        ]);

        $this->createTable('{{%tags_to_news}}', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer(),
            'news_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200311_083017_news_extend cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200311_083017_news_extend cannot be reverted.\n";

        return false;
    }
    */
}
