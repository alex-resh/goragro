<?php

use yii\db\Migration;

/**
 * Class m200326_064808_gallery_change
 */
class m200326_064808_gallery_change extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%organisation_gallery}}', 'lat');
        $this->dropColumn('{{%organisation_gallery}}', 'lon');

        $this->addColumn('{{%organisation_gallery}}', 'desc', $this->string(255));
        $this->addColumn('{{%organisation_gallery}}', 'created_at', $this->integer());
        $this->addColumn('{{%organisation_gallery}}', 'updated_at', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%organisation_gallery}}', 'lat', $this->string(255));
        $this->addColumn('{{%organisation_gallery}}', 'lon', $this->string(255));

        $this->dropColumn('{{%organisation_gallery}}', 'desc');
        $this->dropColumn('{{%organisation_gallery}}', 'created_at');
        $this->dropColumn('{{%organisation_gallery}}', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200326_064808_gallery_change cannot be reverted.\n";

        return false;
    }
    */
}
