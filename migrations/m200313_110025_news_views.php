<?php

use yii\db\Migration;

/**
 * Class m200313_110025_news_views
 */
class m200313_110025_news_views extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%news}}', 'views_count', $this->integer()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%news}}', 'views_count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200313_110025_news_views cannot be reverted.\n";

        return false;
    }
    */
}
