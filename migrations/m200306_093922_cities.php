<?php

use yii\db\Migration;

/**
 * Class m200306_093922_cities
 */
class m200306_093922_cities extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/cities.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fs_region');
        $this->dropTable('fs_okrug');
        $this->dropTable('fs_city');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200214_120018_cities cannot be reverted.\n";

        return false;
    }
    */
}
