<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%additionally_data}}`.
 */
class m200312_110858_create_additionally_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%additionally_data}}', [
            'id' => $this->primaryKey(),
            'form_id' => $this->integer(),
            'organisation_id' => $this->integer(),
            'data' => $this->json(),
            'status' => $this->smallInteger(2)->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('fk-data_form-form_id', '{{%additionally_data}}', 'form_id', '{{%additionally}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-data_organisation-organisation_id', '{{%additionally_data}}', 'organisation_id', '{{%organisation}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-data_organisation-organisation_id', '{{%additionally_data}}');
        $this->dropForeignKey('fk-data_form-form_id', '{{%additionally_data}}');

        $this->dropTable('{{%additionally_data}}');
    }
}
