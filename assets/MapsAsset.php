<?php


namespace app\assets;


use app\models\Settings;
use yii\web\AssetBundle;

class MapsAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [];


    public function init()
    {
        parent::init();
        $this->js = [
            'https://api-maps.yandex.ru/2.1/?apikey='. Settings::get('ya_geocoder') .'&lang=ru_RU',
            //'js/placemark.js',
        ];
    }

}