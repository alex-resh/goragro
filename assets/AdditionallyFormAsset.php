<?php


namespace app\assets;


use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class AdditionallyFormAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/views/additionally/src';

    public $depends = [
        JqueryAsset::class,
    ];

    public $css = [
        'css/form.css',
    ];

    public $js = [
        'js/form.js',
    ];
}