"use strict";
$('#organisation-img').on('change', function () {
    if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $('.logo__picture').attr('src', e.target.result).css('opacity', 1);
        };

        reader.readAsDataURL(this.files[0]);
    }
});

$('body').on('change', '.product-img-file', function () {
    let t = $(this);
    if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            t.prevAll('.product-img').attr('src', e.target.result).css('opacity', 1);
        };

        reader.readAsDataURL(this.files[0]);
    }
});