"use strict";
(function($){
    $(function(){
        let product = $('.js-product');
        setTimeout(function () {
            $('.js-category').on('change', function(){
                let categories = $(this).val();
                $.ajax({
                    url: product.data('url'),
                    method: 'post',
                    data: {
                        id: product.data('id'),
                        categories: categories
                    }
                }).done(function(res) {
                    product.html(res);
                    product.trigger('changeContent');
                });
            });
        }, 0);
        product.on('changeContent', function () {
            let counter = $(this).data('counter')
            $('.el', product).each(function () {
                $(this).find('[name]').each(function(){
                    $(this).attr('name', $(this).attr('name').replace(/\[\](.+)/, '[' + counter + ']$1'));
                    console.log($(this).attr('name'));
                });
                counter --;
            });
            $(this).data('counter', counter);
            console.log($(this).data('counter'));
        });
        product.on('click', '.js-added-info-btn', function () {
            let t = $(this);
            t.closest('.el').remove();
            return false;
        });
        product.on('click', 'h3 .js-added-info-btn', function(){
            let t = $(this);
            $.ajax({
                url: product.data('url') + '/add',
                method: 'post',
                data: {
                    id: product.data('id'),
                    prod_class: t.data('class')
                }
            }).done(function (res) {
                t.closest('.added-info__item').append(res);
                product.trigger('changeContent');
            }).fail(function (res) {
                console.log(res.responseText);
            });
        });
        product.trigger('changeContent');
    });
})(jQuery);