<?php


namespace app\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class FilterAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/src/filter';

    public $depends = [
        JqueryAsset::class,
    ];

    public $js = [
        'js/filter.js',
    ];
}