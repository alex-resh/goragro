<?php


namespace app\assets;


use yii\web\AssetBundle;

class WeatherAsset extends AssetBundle
{
    public $css = [
        'weath/css/weather-icons.min.css',
    ];
}