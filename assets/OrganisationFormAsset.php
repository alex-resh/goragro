<?php


namespace app\assets;


use kartik\base\AssetBundle;
use yii\web\JqueryAsset;

class OrganisationFormAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/src/organisation_form';

    public $depends = [
        JqueryAsset::class,
    ];

    public $js = [
        'js/org-form.js',
    ];
}