<?php

namespace app\assets;

use kartik\base\AssetBundle;

class ProductFieldAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/src/product_field';

    public $js = [
        'js/product_field.js'
    ];
}