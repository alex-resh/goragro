<?php


namespace app\assets;


use yii\web\AssetBundle;

class AdditionallyFieldAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/src/additionally-field';

    public $css = [
        'css/additionally-field.css',
    ];
}