<?php

namespace app\controllers;

use app\models\OrganisationCategory;
use app\models\Product;
use app\models\ProductCategory;
use Yii;
use yii\web\Controller;

class ProductController extends Controller
{
    public function actionIndex()
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            $content = '';
            $id = $request->post('id');
            $categories = $request->post('categories');
            $classes = ProductCategory::find()
                ->distinct()
                ->select('product_class_name')
                ->andWhere(['category_id' => $categories])
                ->column();
            foreach ($classes as $class) {
                //Использовать andWere (В parent::find() уже усть where()
                $models = $class::find()->andWhere(['organisation_id' => $id])->all();
                $models = $models ? $models : [new $class(['organisation_id' => $id])];
                $content .= '<div class="added-info__item">';
                $content .= '<h3 class="section-subtitle">' . Product::productTypeList()[$class] . '<button class="added-info__button js-added-info-btn" title="Добавить" data-class="' . $class . '"></button></h3>';
                foreach ($models as $model) {
                    $content .= $this->renderPartial('_item_fields', [
                        'model' => $model,
                    ]);
                }
                $content .= '</div>';
            }
            return $content;
        }
    }

    public function actionAdd()
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            $class = $request->post('prod_class');
            $model = new $class([
                'organisation_id' => $request->post('id'),
            ]);
            return $this->renderPartial('_item_fields', [
                'model' => $model,
            ]);
        }
    }
}