<?php


namespace app\controllers;


use app\models\Organisation;
use Yii;
use yii\web\Controller;

class MapsController extends Controller
{
    public function actionIndex($category_id = null)
    {
        $this->layout = 'map';
        $city = Yii::$app->city->city;

        $query = Organisation::find()
            ->joinWith('address')
            ->where(['city_id' => $city->id, 'status' => Organisation::STATUS_PUBLISHED]);

        if($category_id){
            $query->joinWith('categories')
                ->andWhere(['category_id' => $category_id]);
        }

        $organisations = $query->all();
        return $this->render('index', ['city' => $city, 'orgsnisations' => $organisations]);
    }
}