<?php


namespace app\controllers;


use yii\web\Controller;

class WeatherController extends Controller
{
    public $layout = 'weather';

    public function actionIndex(){
        $weather = \Yii::$app->weather->getPrognos(\Yii::$app->city->city->name_en);
        $today = $weather[0];
        $weather = array_slice($weather, 1, 6);

        //print_r($weather[0]->parts->day->condition); die();
        return $this->render('index', ['weather' => $weather, 'today' => $today]);
    }
}