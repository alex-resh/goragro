<?php

namespace app\controllers;

use app\models\Profile;
use app\models\User;
use dektrium\user\models\Account;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction( $action ) {
        if ( parent::beforeAction ( $action ) ) {

            //change layout for error action after
            //checking for the error action name
            //so that the layout is set for errors only
            if ( $action->id == 'error' ) {
                $this->layout = 'error';
            }
            return true;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionEauth($service){
        $eauth = Yii::$app->get('eauth')->getIdentity($service);
        $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
        $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/eauth'));
        try{
            if($eauth->authenticate()){
                $attributes = (object)$eauth->getAttributes();
                $account = Account::findOne(['provider' => $service, 'client_id' => $attributes->id]);
                if ($account){
                    $identity = User::findOne($account->user_id);
                    Yii::$app->user->login($identity);
                } else {
                    $user = new User();
                    $user->username = $attributes->id;
                    //$user->email = $attributes->id;
                    $user->register();
                    //$user->save(false);
                    $id = $user->id;

                    $profile = $user->profile;
                    if(!$profile){
                        $profile = new Profile();
                        $profile->user_id = $id;
                    }
                    $profile->name = $attributes->name;
                    $profile->save();

                    $authManager = Yii::$app->authManager;
                    $role = $authManager->getRole('user');
                    $authManager->assign($role, $id);

                    $account = new Account();
                    $account->provider = $service;
                    $account->client_id = $attributes->id;
                    $account->user_id = $id;
                    $account->save(false);
                    Yii::$app->user->login($user);
                }
                return $this->redirect(['/']);
            }
        } catch (\nodge\eauth\ErrorException $e){

        }
        die();
    }


    public function actionOffline(){
        $this->layout = false;
        return $this->render('offline');
    }

    public function actionTest(){
       $result = Yii::$app->weather->getWeather('');
        $result = json_decode($result);
        print_r ($result);
        die();
    }
}
