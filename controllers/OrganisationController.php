<?php


namespace app\controllers;


use app\models\Action;
use app\models\Additionally;
use app\models\AdditionallyCategory;
use app\models\AdditionallyData;
use app\models\Agent;
use app\models\forms\AgentForm;
use app\models\forms\AskForm;
use app\models\forms\ErrorForm;
use app\models\Image;
use app\models\Organisation;
use app\models\OrganisationAdress;
use app\models\OrganisationCategory;
use app\models\OrganisationGallery;
use app\models\OrganisationWorktime;
use app\models\Product;
use app\models\ProductCategory;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use app\models\forms\OrganisationForm;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class OrganisationController extends Controller
{
    //public $layout = 'organisation';
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['ajax', 'agent', 'error', 'ask', 'index', 'view-action'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'index', 'update', 'get-additionally', 'gallery-upload', 'gallery-delete', 'image-upload'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($id)
    {
        $this->layout = 'organisation';
        $organisation = Organisation::findOne($id);
        if($organisation->status != Organisation::STATUS_PUBLISHED){
            if(!Yii::$app->user->can('admin') && $organisation->user_id != Yii::$app->user->id && !(($agent = $organisation->getAgent()) && $agent->status == Agent::STATUS_ACTIVE)){
                throw new ForbiddenHttpException();
            }
        }
        $agentForm = new AgentForm();
        $errorForm = new ErrorForm();
        $askForm = new AskForm();
        $organisation->updateCounters(['views_count' => 1]);
        return $this->render('index', [
            'organisation' => $organisation,
            'agentForm' => $agentForm,
            'errorForm' => $errorForm,
            'askForm' => $askForm,
        ]);
    }

    public function actionCreate()
    {
        $model = new Organisation([
            'user_id' => Yii::$app->user->id,
            'status' => Organisation::STATUS_NEW,
        ]);
        $organisationForm = new OrganisationForm();
        $address = new OrganisationAdress();
        $transaction = Yii::$app->db->beginTransaction();
        $post = Yii::$app->request->post();
        $needSave = false;
        if($model->load($post) && $model->save()){
            $address->organisation_id = $model->id;
            $needSave = true;
        }
        if($organisationForm->load($post) && $organisationForm->validate() && $needSave){
            $catIds = $organisationForm->categories;
            foreach ($catIds as $category) {
                $orgCatModel = new OrganisationCategory([
                    'organisation_id' => $model->id,
                    'category_id' => $category,
                ]);
                $needSave = ($needSave && $orgCatModel->save());
            }
        }
        if($address->load($post) && $address->validate() && $needSave){
            $address->save(false);
            $transaction->commit();

            /*Если действие вызвано из админки*/
            if(Yii::$app->controller->action->uniqueId == 'admin/organisation/create'){
                return $this->redirect(['/admin/organisation/update', 'id' => $model->id]);
            }
            return $this->redirect(['/organisation/update', 'id' => $model->id]);
        } else {
            $transaction->rollBack();
        }
        return $this->render('add', [
            'model' => $model,
            'address' => $address,
            'organisationForm' => $organisationForm,
        ]);
    }

    /**
     * $needSave показывает правильность сохранения всех моделей
     * Например:
     * $needSave = $needSave && $gallery_model->save();
     *
     * $organisationForm - модель для дополнительных атрибутов.
     * для Категорий
     *
     * @return string|Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */


    public function actionUpdate($id)
    {
        $isAdmin = Yii::$app->user->can('admin');
        $model = Organisation::findOne($id);
        if(!$model || (Yii::$app->user->id != $model->user_id && !$isAdmin && !((($agent = $model->agent) || ($agent = $model->origin->agent)) && $agent->status == Agent::STATUS_ACTIVE))){
            throw new NotFoundHttpException();
        }
        $model->status = Organisation::STATUS_MODERATED;
        $oldCatIds = OrganisationCategory::find()->select('category_id')->where(['organisation_id' => $id])->column();
        $organisationForm = new OrganisationForm([
            'categories' => $oldCatIds,
        ]);
        $worktime = $model->worktime ?? new OrganisationWorktime(['organisation_id' => $model->id]);
        $address = $model->address;
        $gallery = new OrganisationGallery();
        $gallery_models = $model->gallery;
        $image = new Image();
        $additionallyData = $model->additionallyDates;
        $action_models = $model->actions;
        if(empty($action_models)){
            $action_models = [New Action()];
        }

        $needSave = false;
        $post = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction();
        $model->status = Organisation::STATUS_MODERATED;
        if(!isset($_FILES[$model->formName()]['size']['img']) || $_FILES[$model->formName()]['size']['img'] == 0){
            $model->markAttributeDirty('img');
        }

        if($isAdmin){
            if(isset($post['publish'])){
                $model->status = Organisation::STATUS_PUBLISHED;
            } elseif(isset($post['rework'])){
                if($post[$model->formName()]['moderator_msg']){
                    $model->status = Organisation::STATUS_REWORK;
                    $model->scenario = 'rework';
                    $model->moderator_msg = $post[$model->formName()]['moderator_msg'];
                } else {
                    $needSave = false;
                    $model->addError('moderator_msg', 'При отправке на доработку необходимо заполнить поле "Сообщение модератора"');
                }

            }
        }
        if ($model->load($post)) {
            $needSave = $model->save();
        }

        if($organisationForm->load($post) && $organisationForm->validate() && $needSave){
            $catIds = $organisationForm->categories;
            foreach (array_diff($catIds, $oldCatIds) as $category) {
                $orgCatModel = new OrganisationCategory([
                    'organisation_id' => $model->id,
                    'category_id' => $category,
                ]);
                $needSave = ($needSave && $orgCatModel->save());
            }

            foreach (array_diff($oldCatIds, $catIds) as $catId) {
                $orgCatModel = OrganisationCategory::findOne(['category_id' => $catId, 'organisation_id' => $id]);
                $needSave = $needSave && $orgCatModel->delete();
            }
        }

        if(Yii::$app->request->post('work_time_data')){
            $data = Json::decode($post['work_time_data']);
            foreach ($data as $item) {
                foreach ($item['days'] as $day) {
                    $prefix = $worktime->days()[$day] . '_';
                    $worktime->{$prefix . 'start'} = $item['workStart'];
                    $worktime->{$prefix . 'end'} = $item['workEnd'];
                    $worktime->{$prefix . 'br_start'} = $item['breakStart'];
                    $worktime->{$prefix . 'br_end'} = $item['breakEnd'];
                }
            }
            $needSave = ($needSave && $worktime->save());
        }

        $needSave = $address->load($post) && $address->validate() && $needSave;


        $dataModel = new AdditionallyData([
            'organisation_id' => $model->id,
        ]);
        foreach (($post[$dataModel->formName()] ?? []) as $k => $item) {
            AdditionallyData::deleteAll(['organisation_id' => $id, 'form_id' => $k]);
            $dataModel->form_id = $k;
            $dataModel->data = Json::encode($item);
            $needSave = ($needSave && $dataModel->save());
            $dataModel = new AdditionallyData([
                'organisation_id' => $model->id,
            ]);
        }

        $gallery_data = Yii::$app->request->post('gallery');
        foreach ((array)$gallery_data['id'] as $gallery_id){
            if(isset($gallery_id)){
                $gallery_model = OrganisationGallery::findOne($gallery_id);
                if($gallery_model){
                    $gallery_model->organisation_id = $model->id;
                    $needSave = $needSave && $gallery_model->save();
                }
            }
        }

        if(Yii::$app->request->isPost) {
            $actionIds = [];
            $actionsTmp = [];
            foreach ($post[$action_models[0]->formName()] as $id => $item) {
                $action = $id > 0 ?
                    Action::findOne($id) :
                    new Action([
                        'organisation_id' => $model->id,
                    ]);
               $action->load($item, '') && $action->save();
                $actionIds[] = $action->id;
                $actionsTmp[] = $action;
            }
            $action_models = $actionsTmp;
            foreach ((array)Action::find()->andWhere(['and', ['organisation_id' => $model->id], ['not in', 'id', $actionIds]])->all() as $action){
                $action->delete();
            }
        }

        //Сохранение продуктов
        $prod_data = [];
        if($post){
            foreach (Product::productTypeList() as $class => $item) {
                $prod_data[$class] = [];
                $ids = [];
                $new_product = new $class(['organisation_id' => $model->id]);
                $arr = $_FILES[$new_product->formName()] ?? [];
                foreach ($post[$new_product->formName()] ?? [] as $prod_id => $attributes) {
                    if($prod_id > 0){
                        $product = $class::find()->andWhere(['id' => $prod_id])->one();
                        $product = $product ? $product : $new_product;
                    } else {
                        $product = $new_product;
                    }
                    $product->load($attributes, '');
                    if(isset($arr['name'][$prod_id]['img']) && $arr['size'][$prod_id]['img'] > 0){
                        $product->img = new UploadedFile([
                            'name' => $arr['name'][$prod_id]['img'],
                            'tempName' => $arr['tmp_name'][$prod_id]['img'],
                            'type' => $arr['type'][$prod_id]['img'],
                            'size' => $arr['size'][$prod_id]['img'],
                            'error' => $arr['error'][$prod_id]['img'],
                        ]);
                    }
                    $needSave = $product->save() && $needSave;
                    $ids[] = $product->id;
                    $prod_data[$class][] = $product;
                }
                $prod_for_delete = $class::find()->andWhere(['and', ['organisation_id' => $model->id], ['not in', 'id', $ids]])->all();
                foreach ($prod_for_delete as $item) {
                    $item->delete();
                }
            }
        } else {
            $classes = ProductCategory::find()
                ->distinct()
                ->select('product_class_name')
                ->andWhere(['category_id' => $oldCatIds])
                ->column();
            foreach ($classes as $class) {
                $models = $class::find()->andWhere(['organisation_id' => $model->id])->all();
                $prod_data[$class] = $models;
            }
        }

        if($needSave && $address->save()){
            if($model->status == Organisation::STATUS_PUBLISHED){
                $model = Organisation::findOne($model->id);
                $model->clone();
            }
            $transaction->commit();

            /*Если действие вызвано из админки*/
            if(Yii::$app->controller->action->uniqueId == 'admin/organisation/update'){
                return $this->redirect(['/admin/organisation']);
            }

            return $this->redirect(['index', 'id' => ($model->clone_id ? $model->id : $model->origin->id)]);
        } elseif($model->id) {
            $transaction->rollBack();
        }

        return $this->render('create', [
            'model' => $model,
            'organisationForm' => $organisationForm,
            'worktime' => $worktime,
            'address' => $address,
            'gallery' => $gallery,
            'gallery_models' => $gallery_models,
            'image' => $image,
            'additionallyData' => $additionallyData,
            'action_models' => $action_models,
            'isAdmin' => $isAdmin,
            'prod_data' => $prod_data,
        ]);
    }

    public function actionGetAdditionally()
    {
        $post = Yii::$app->request->post();
        $org_id = $post['org_id'];
        $formIds = $post['form_ids'] ?? [];
        $ids = AdditionallyCategory::find()->where(['category_id' => $post['cat_ids']])->select('additionally_id')->column();
        array_unique($ids);

        $models = Additionally::find()->where(['id' => array_diff($ids, $formIds)])->all();
        $needRemove = array_diff($formIds, $ids);
        AdditionallyData::deleteAll(['organisation_id' => $org_id, 'form_id' => $needRemove]);

        return $this->renderAjax('additionally_items', [
            'models' => $models,
            'needRemove' => $needRemove,
        ]);
    }

    public function actionGalleryUpload(){
        $id = Yii::$app->request->post('id');
        if(!$id) {
            $model = new OrganisationGallery();
        } else {
            $model = OrganisationGallery::findOne($id);
        }
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'id' => $model->id,
                'thumb' => $model->getThumbUploadUrl('img', 'preview')
            ];
        }
        //print_r($model->getErrors()); die('1');
    }

    public function actionImageUpload(){
        $id = Yii::$app->request->post('id');
        $model = Image::findOne($id);
        if(!$model) {
            $model = new Image();
        }
        if($model->load(Yii::$app->request->post()) && $model->save()){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'id' => $model->id,
                'thumb' => $model->getThumbUploadUrl('img', 'preview')
            ];
        }
        //print_r($model->getErrors()); die();
    }

    public function actionGalleryDelete(){
        $id = Yii::$app->request->post('id');
        $model = OrganisationGallery::findOne($id);
        if($model){
            $model->delete();
            return '1';
        }
    }

    public function actionGalleryDesc(){
        $id = Yii::$app->request->post('id');
        $desc = Yii::$app->request->post('desc');
        $model = OrganisationGallery::findOne($id);
        if($model){
            $model->desc = $desc;
            $model->save();
        }
    }

    public function actionAjax($id){
        $model = Organisation::findOne($id);
        return $this->renderPartial('ajax', ['model' => $model]);
    }

    public function actionAgent()
    {
        $model = new AgentForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->send();
            return $this->renderPartial('popups/agent_success');
        }
        return $this->render('popups/agent', [
            'model' => $model,
        ]);
    }

    public function actionAsk()
    {
        $model = new AskForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->send();
            return $this->renderPartial('popups/ask_success');
        }
        return $this->render('popups/ask', [
            'model' => $model,
        ]);
    }

    public function actionError()
    {
        $model = new ErrorForm();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->send();
            return $this->renderPartial('popups/error_success');
        }
        return $this->render('popups/error', [
            'model' => $model,
        ]);
    }

    public function actionViewAction($id){
        $model = Action::findOne($id);
        return $this->renderAjax('popups/view_action', ['model' => $model]);
    }

}