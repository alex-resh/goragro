<?php
namespace app\controllers\user;

use dektrium\user\controllers\RecoveryController;
use dektrium\user\models\LoginForm;
use dektrium\user\models\RegistrationForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SecurityController extends \dektrium\user\controllers\SecurityController
{

    public $layout = '/login';

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth', 'forgot-password', 'reset', 'register'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $model_reg = Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'model_reg'  => $model_reg,
            'module' => $this->module,
        ]);
    }

    public function actionForgotPassword()
    {
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        $controller = Yii::$app->createController('/user/recovery/forgot')[0];
        $controller->layout = false;
        $controller->viewPath = '@app/views/user/security/';
        $pasForm = $controller->actionRequest();
        return $this->render('password', [
            'model' => $model,
            'pasForm' => $pasForm,
        ]);
    }

    public function actionReset($id, $code)
    {
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        $controller = Yii::$app->createController('/user/recovery/reset')[0];
        $controller->layout = false;
        $controller->viewPath = '@app/views/user/security/';
        $pasForm = $controller->actionReset($id, $code);
        return $this->render('password', [
            'model' => $model,
            'pasForm' => $pasForm,
        ]);
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $model_reg = Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        $controller = Yii::$app->createController('/user/registration/register')[0];
        $controller->layout = false;
        $controller->viewPath = '@app/views/user/security/';
        $pasForm = $controller->actionRegister();
        return $this->render('password', [
            'model' => $model,
            'pasForm' => $pasForm,
        ]);

        return $this->render('login', [
            'model'  => $model,
            'model_reg'  => $model_reg,
            'module' => $this->module,
        ]);
    }
}