<?php


namespace app\controllers;


use app\models\Blog;
use app\models\NewsTag;
use app\models\search\BlogSearch;
use app\models\search\NewsSearch;
use app\models\TagsToNews;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class BlogController extends Controller
{

    public $layout = 'blog';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($cat_id = null, $tag = null)
    {
        $searchModel = new BlogSearch();

        $dataProvider = $searchModel->searchFront(\Yii::$app->request->queryParams, [], Blog::TYPE_BLOG);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
//        $Blog = Blog::find()->all();
//        return $this->render('index', ['Blog' => $Blog]);
    }
    public function actionView($id)
    {
        $Blog = Blog::findOne($id);

        if ($Blog === NULL)
            throw new HttpException(404, 'Document Does Not Exist');

        $Blog->updateCounters(['views_count' => 1]);

                return $this->render('_view', array(
            'model' => $Blog,
        ));
    }

    public function actionCreate(){
        $model = new Blog();
        $model->type = Blog::TYPE_BLOG;
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            return $this->render('success');
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id){
        $model = Blog::findOne(['id' => $id, 'type' => Blog::TYPE_BLOG, 'user_id' => \Yii::$app->user->id]);
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $model->tags = $model->getTagsList();
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            return $this->render('success');
        }
        return $this->render('update', ['model' => $model]);
    }

}