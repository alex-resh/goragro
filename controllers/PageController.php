<?php


namespace app\controllers;


use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{

    public function actionIndex($id)
    {
        if(isset(\Yii::$app->params['model'])){
            $model = \Yii::$app->params['model'];
        } else {
            $model = \app\models\Page::findOne($id);
        }
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $this->render('index', ['model' => $model]);
    }

}