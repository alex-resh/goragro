<?php


namespace app\controllers;


use app\models\Sight;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SightsController extends Controller
{

    public function actionIndex($id){
        $model = Sight::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $model->updateCounters(['views_count' => 1]);
        $this->layout = 'news_item';
        return $this->render('index', ['model' => $model]);
    }

}