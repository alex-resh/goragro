<?php


namespace app\controllers;


use app\models\News;
use app\models\NewsTag;
use app\models\search\NewsSearch;
use app\models\TagsToNews;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{

    public $layout = 'news';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($cat_id = null, $tag = null)
    {
        $searchModel = new NewsSearch();

        $query = News::find()->where(['status' => 1, 'city_id' => \Yii::$app->city->city->id, 'type' => News::TYPE_NEWS])->orderBy(['created_at' => SORT_DESC])->limit(5);
        if($cat_id){
            $query->andWhere(['category_id' => $cat_id]);
        }

        if($tag){
            $tag_model = NewsTag::findOne(['tag' => $tag]);
            if($tag_model){
                $query->innerJoin(TagsToNews::tableName(), TagsToNews::tableName().'.news_id=' . News::tableName().'.id')
                    ->where([TagsToNews::tableName().'.tag_id' => $tag_model->id]);
            }
        }


        $news = $query->all();
        $dataProvider = $searchModel->searchFront(\Yii::$app->request->queryParams, $news);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'news' => $news]);
//        $news = News::find()->all();
//        return $this->render('index', ['news' => $news]);
    }
    public function actionView($id)
    {
        $news = News::findOne($id);

        if ($news === NULL)
            throw new HttpException(404, 'Document Does Not Exist');

        $news->updateCounters(['views_count' => 1]);

        $query = News::find()
            ->where(['status' => 1, 'city_id' => \Yii::$app->city->city->id])
            ->andWhere(['!=', 'id', $id])
            ->andWhere(['category_id' => $news->category_id])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(4)
            ->all();

        return $this->render('_view', array(
            'model' => $news,
            'news' => $query
        ));
    }

    public function actionCreate(){
        $model = new News();
        $model->type = News::TYPE_NEWS;
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            return $this->render('success');
        }
        return $this->render('create', ['model' => $model]);
    }

}