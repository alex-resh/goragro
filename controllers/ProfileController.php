<?php


namespace app\controllers;


use app\models\Organisation;
use app\models\Profile;
use app\models\search\OrganisationSearch;
use app\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{
    public function actionIndex($id = null){
        $self = false;
        if(!$id){
            if(Yii::$app->user->isGuest){
                throw new NotFoundHttpException('User not found');
            }
            $id = Yii::$app->user->id;
            $self = true;
        }

        $user = User::findOne($id);
        if(!$user){
            throw new NotFoundHttpException('User not found');
        }

        if($id && $id == Yii::$app->user->id){
            $self = true;
        }

        $profile = Profile::findOne(['user_id' => $user->id]);
        if($self){
            if($user->load(Yii::$app->request->post()) & $profile->load(Yii::$app->request->post())){
                if($user->validate() & $profile->validate()){
                    $user->save();
                    $profile->save();
                    return $this->redirect('/profile');
                }
            }
        }

        return $this->render('index', ['user' => $user, 'self' => $self, 'profile' => $profile]);
    }

    public function actionOrganisation()
    {
        $searchModel = new OrganisationSearch();
        $dataProvider = $searchModel->searchForProfile(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['>', 'clone_id', 0]);
        return $this->render('organisation', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrganisationNew()
    {
        $searchModel = new OrganisationSearch();
        $dataProvider = $searchModel->searchForProfile(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_NEW]);
        return $this->render('organisation', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrganisationModerated()
    {
        $searchModel = new OrganisationSearch();
        $dataProvider = $searchModel->searchForProfile(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_MODERATED]);
        return $this->render('organisation', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrganisationReworked()
    {
        $searchModel = new OrganisationSearch();
        $dataProvider = $searchModel->searchForProfile(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_REWORK]);
        return $this->render('organisation', [
            'dataProvider' => $dataProvider,
        ]);
    }
}