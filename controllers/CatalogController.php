<?php


namespace app\controllers;


use app\models\Additionally;
use app\models\AdditionallyData;
use app\models\Category;
use app\models\Organisation;
use app\models\OrganisationAdress;
use app\models\search\OrganisationSearch;
use Yii;
use yii\base\Model;
use yii\web\Controller;

class CatalogController extends Controller
{
    public $layout = 'catalog';

    public function actionIndex($category_id = null)
    {
        $get = Yii::$app->request->get();
        $searchModel = new OrganisationSearch();
        $searchModel->load($get);
        $searchModel->category_id = $category_id;
        $dataProvider = $searchModel->searchFront(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 6;
        $dataProvider->query->orderBy('id DESC');
        $dataProvider->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_PUBLISHED])
            ->andWhere(['>', Organisation::tableName() . '.clone_id', 0]);
        $datas = [];
        if($category_id){
            $category = Category::findOne($category_id);
            $filters = $category->additionallies;
            $addDataForm = (new AdditionallyData())->formName();
            if(isset($get[$addDataForm])){
                foreach ($get[$addDataForm] as $key => $data) {
                    $dataModel = new AdditionallyData(['form_id' => $key]);
                    foreach ($data as $i => $item) {
                        $dataModel->{$i} = $item;
                    }
                    $datas[$key] = $dataModel;
                }
            }
        } else {
            $category = false;
            $filters = [];
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
            'filters' => $filters,
            'searchModel' => $searchModel,
            'datas' => $datas
        ]);
    }
}