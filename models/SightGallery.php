<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use Yii;

/**
 * This is the model class for table "sight_gallery".
 *
 * @property int $id
 * @property int|null $sight_id
 * @property string|null $img
 */
class SightGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sight_gallery}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/sight_gallery/{id}',
                'url' => '@web/upload/sight_gallery/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'big_img' => ['width' => 719],
                    'small_img' => ['width' => 262, 'height' => 180 ],
                ],
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sight_id'], 'integer', 'skipOnEmpty' => true],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sight_id' => 'Sight ID',
            'img' => 'Img',
        ];
    }
}
