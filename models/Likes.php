<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%likes}}".
 *
 * @property int $id
 * @property string|null $entity
 * @property int|null $entity_id
 * @property int|null $likes
 * @property int|null $dislikes
 * @property User[] $users
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%likes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'likes', 'dislikes'], 'integer'],
            [['entity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
            'likes' => 'Likes',
            'dislikes' => 'Dislikes',
        ];
    }

    public function getUsers($user_id = null){
        return $this->hasMany(LikesToUser::class, ['like_id' => 'id'])->andFilterWhere(['user_id' => $user_id]);
    }
}
