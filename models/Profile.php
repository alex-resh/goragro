<?php


namespace app\models;
use app\models\Settings;


class Profile extends \dektrium\user\models\Profile
{
    public function behaviors()
    {
        return [
            [
                'class' => \app\behaviors\UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/profile/{user_id}',
                'url' => '@web/upload/profile/{user_id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                    'small' => ['width' => 36, 'height' => 36],
                    'form_img' => ['width' => 59, 'height' => 59],
                ],
            ],
        ];
    }
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png'];
        $rules[] = ['phone', 'string'];
        $rules[] = ['name_prefix', 'safe'];
        $rules[] = ['about', 'safe'];
        return $rules;

    }
}