<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog_city".
 *
 * @property int $id
 * @property int|null $blog_id
 * @property int|null $city_id
 */
class BlogCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blog_id', 'city_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blog_id' => 'Blog ID',
            'city_id' => 'City ID',
        ];
    }
}
