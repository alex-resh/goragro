<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii2mod\comments\models\CommentModel;

/**
 * This is the model class for table "{{%comment_image}}".
 *
 * @property int $id
 * @property int|null $comment_id
 * @property string|null $img
 * @property string|null $name
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property CommentModel $comment
 */
class CommentImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%comment_image}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'upload' => [
                'class' => UploadImageBehavior::class,
                //'instanceByName' => true,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/comment/{id}',
                'url' => '@web/upload/comment/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 200, 'height' => 200],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comment_id', 'created_at', 'updated_at'], 'integer'],
            [['img', 'name'], 'string', 'max' => 255],
            [['comment_id'], 'exist', 'skipOnError' => true, 'targetClass' => CommentModel::class, 'targetAttribute' => ['comment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'comment_id' => 'Comment ID',
            'img' => 'Img',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Comment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(CommentModel::class, ['id' => 'comment_id']);
    }
}
