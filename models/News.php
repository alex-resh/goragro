<?php

namespace app\models;


use app\behaviors\UploadImageBehavior;
use app\modules\likes\traits\LikeTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;



/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $title
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $category_id
 * @property int|null $city_id
 * @property int|null $user_id
 * @property int $views_count
 * @property int $type
 * @property string|null $seo_url
 * @property string|null $seo_title
 * @property string|null $seo_keywords
 * @property string|null $seo_desc
 */
class News extends \yii\db\ActiveRecord
{
    use LikeTrait;
    const TYPE_NEWS = 0;
    const TYPE_BLOG = 1;

    public $tags;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class
            ],
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/news/{id}',
                'url' => '@web/upload/news/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'big_img' => ['width' => 524, 'height' => 460],
                    'middle_img' => ['width' => 262, 'height' => 460],
                    'small_img' => ['width' => 238, 'height' => 211 ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'seo_desc'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['seo_url', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
            ['title', 'required'],
            ['text', 'required'],
            ['category_id', 'required'],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png'],
            ['category_id', 'integer'],
            ['status', 'integer'],
            ['city_id', 'integer'],
            ['user_id', 'integer'],
            ['type', 'integer'],
            ['views_count', 'integer'],
            ['views_count', 'default', 'value' => 0],
            ['tags', 'safe'],
            ['status', 'default', 'value' => 0],
            ['city_id', 'default', 'value' => Yii::$app->city->city->id],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'title' => 'Заголовок',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'seo_url' => 'Seo Url',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_desc' => 'Seo Desc',
            'status' => 'Статус',
            'category_id' => 'Категория',
            'city' => 'Город',
            'img' => 'Изображение',
            'tags' => 'Теги',
        ];
    }

    public function getCategory(){
        return $this->hasOne(NewsCategory::class, ['id' => 'category_id']);
    }

    public function getCity(){
        return $this->hasOne(FsCity::class, ['id' => 'city_id']);
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getTagsList(){
        $query = new Query();
        $result = $query->select('tag')
            ->from('tags_to_news')
            ->innerJoin('news_tag', 'news_tag.id = tags_to_news.tag_id')
            ->innerJoin('news', 'news.id = tags_to_news.news_id')
            ->andWhere(['news.id' => $this->id])
            ->all();
        $out = [];
        foreach ($result as $tag){
            $out[] = $tag['tag'];
        }
        return $out;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        TagsToNews::deleteAll(['news_id' => $this->id]);
        if($this->tags) {
            foreach ($this->tags as $tag) {
                $model = NewsTag::findOne(['tag' => $tag]);
                if (!$model) {
                    $model = new NewsTag();
                    $model->tag = $tag;
                    $model->save();
                }
                $model2 = new TagsToNews();
                $model2->tag_id = $model->id;
                $model2->news_id = $this->id;
                $model2->save();
            }
        }
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){
            if($insert){
                $this->user_id = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }

    public function afterDelete()
    {
        TagsToNews::deleteAll(['news_id' => $this->id]);
    }

    public function getPrev($type = self::TYPE_NEWS){
        $prev = News::find()->where(['city_id' => $this->city_id, 'status' => 1])
            ->andWhere(['>', 'created_at', $this->created_at])
            ->andWhere(['news.type' => $type])
            ->orderBy(['created_at' => SORT_ASC])
            ->one();
        return $prev;
    }

    public function getNext($type = self::TYPE_NEWS){
        $next = News::find()->where(['city_id' => $this->city_id, 'status' => 1])
            ->andWhere(['<', 'created_at', $this->created_at])
            ->andWhere(['news.type' => $type])
            ->orderBy(['created_at' => SORT_DESC])
            ->one();
        return $next;
    }

    public static function getCountByCity($city_id = null, $type = self::TYPE_NEWS){
        if(!$city_id){
            $city_id = Yii::$app->city->city->id;
        }
        $count = News::find()->andWhere(['city_id' => $city_id])
            ->andWhere(['news.type' => $type])
            ->count();
        return $count;
    }

    public function getCommentsCount(){
        $count = Comments::find()->where(['relatedTo' => self::class . ':' . $this->id])->count();
        return $count;
    }

    public static function getTopAuthors(){
        $query = News::find()->select(['user_id', new Expression('count(*)')])
            ->andWhere(['type' => News::TYPE_BLOG])
            ->groupBy('user_id')
            ->orderBy('count(*) desc')
            ->all();
        $ids = [];
        foreach ($query as $item){
            $ids[] = $item->user_id;
        }
        return User::find()->where(['id' => $ids])->limit(10)->all();
    }

    public static function getTopTags(){
        $tags = NewsTag::find()->orderBy(['views_count' => SORT_DESC])->limit(20)->all();
        return $tags;
    }

}
