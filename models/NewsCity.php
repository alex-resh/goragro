<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_city".
 *
 * @property int $id
 * @property int|null $news_id
 * @property int|null $city_id
 */
class NewsCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news_city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['city_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'city_id' => 'City ID',
        ];
    }
}
