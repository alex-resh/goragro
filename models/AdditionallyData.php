<?php
namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%additionally_data}}".
 *
 * @property int $id
 * @property int|null $form_id
 * @property int|null $organisation_id
 * @property string|null $data
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Additionally $form
 * @property Organisation $organisation
 */
class AdditionallyData extends \yii\db\ActiveRecord
{

	public $id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally_data}}';
    }

    public function formName()
    {
        return 'df';
    }

    public static function instantiate($row)
    {
        $model = parent::instantiate($row);
        $model->id = $row['id'];
        return $model;
    }

    public function attributes()
    {
        try {
            if($this->form_id){
                return array_merge(parent::attributes(), $this->form->extraAttributes);
            }
        } catch (\Exception $e){
        }
        return parent::attributes();
    }

    public function loadDefaultValues($skipIfSet = true)
    {
        $data = Json::decode($this->form->data);
        foreach ($data['rows'] as $row) {
            foreach ($row['columns'] as $column) {
                foreach ($column['elements'] as $element) {
                    if($element['default_value'] !== null && (!$skipIfSet || $this->{$element['name']} === null)){
                        $this->{$element['name']} = $element['default_value'];
                    }
                }
            }
        }
        return parent::loadDefaultValues($skipIfSet);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['form_id', 'organisation_id', 'clone_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['data', 'id'], 'safe'],
            [['form_id'], 'exist', 'skipOnError' => true, 'targetClass' => Additionally::class, 'targetAttribute' => ['form_id' => 'id']],
            [['organisation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organisation::class, 'targetAttribute' => ['organisation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'form_id' => 'Form ID',
            'organisation_id' => 'Organisation ID',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Form]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getForm()
    {
        return $this->hasOne(Additionally::class, ['id' => 'form_id']);
    }

    /**
     * Gets query for [[Organisation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne(Organisation::class, ['id' => 'organisation_id']);
    }

    public function afterFind()
    {
        parent::afterFind();
        foreach ((array)Json::decode($this->data) as $key => $item) {
            $this->{$key} = $item;
        }
    }
}
