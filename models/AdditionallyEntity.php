<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additionally_entity".
 *
 * @property int $id
 * @property int|null $entity_id
 * @property string|null $name
 */
class AdditionallyEntity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally_entity}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_id' => 'Entity ID',
            'name' => 'Имя',
        ];
    }
}
