<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "agent".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $organisation_id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $phone
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Organisation $organisation
 * @property User $user
 */
class Agent extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%agent}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'organisation_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            ['email', 'email'],
            [['organisation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organisation::class, 'targetAttribute' => ['organisation_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'organisation_id' => 'Organisation ID',
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Organisation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne(Organisation::class, ['id' => 'organisation_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }


    public static function staticStatusList()
    {
        return [
            static::STATUS_NEW    => 'Модерация',
            static::STATUS_ACTIVE       => 'Одобрено',
        ];
    }

    public function getStatusClass()
    {
        $classes = [
            static::STATUS_NEW          => 'warning',
            static::STATUS_ACTIVE       => 'success',
        ];
        return $classes[$this->status];
    }

    public function getStatusName()
    {
        return $this->statusList()[$this->status];
    }

    public function statusList()
    {
        return static::staticStatusList();
    }

    public static function getCountNew(){
        return Agent::find()->where(['status' => Agent::STATUS_NEW])->count();
    }

}
