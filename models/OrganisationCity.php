<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_city".
 *
 * @property int $id
 * @property int|null $organisation_city
 * @property int|null $city_id
 */
class OrganisationCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation_city}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_city', 'city_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_city' => 'Organisation City',
            'city_id' => 'City ID',
        ];
    }
}
