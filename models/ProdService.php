<?php


namespace app\models;


class ProdService extends Product
{
    public function getPlaceholder()
    {
        return 'Введите название услуги...';
    }

    public function attributeLabels()
    {
        return parent::attributeLabels();
    }
}