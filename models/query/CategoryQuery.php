<?php


namespace app\models\query;


use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{

    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::class,
        ];
    }

}