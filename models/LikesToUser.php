<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%likes_to_user}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $like_id
 * @property int|null $value
 */
class LikesToUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%likes_to_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'like_id', 'value'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'like_id' => 'Like ID',
            'value' => 'Value',
        ];
    }
}
