<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use app\components\GeocoderBehavior;
use Yii;

/**
 * This is the model class for table "fs_city".
 *
 * @property int $id
 * @property int $id_region
 * @property string $name
 * @property string $name_en
 * @property string $name_ru_rod
 * @property string $name_ru_dat
 * @property string $name_ru_vin
 * @property string $name_ru_tvar
 * @property string $name_ru_pred
 * @property boolean $in_popup
 * @property boolean $bold
 * @property FsRegion $region
 */
class FsCity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%fs_city}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/background/main_bg.jpg',
                'path' => '@webroot/upload/intro_img/{id}',
                'url' => '@web/upload/intro_img/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'intro_img' => ['width' => 1400, 'height' => 662],
                ],
            ],
            'geocoder' => [
                'class' => GeocoderBehavior::class,
                'adress_field' => 'name'
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_region', 'name'], 'required'],
            [['id_region'], 'integer'],
            [['name'], 'string', 'max' => 250],
            [['name_en'], 'string', 'max' => 250],
            [['name_ru_rod'], 'string', 'max' => 250],
            [['name_ru_dat'], 'string', 'max' => 250],
            [['name_ru_vin'], 'string', 'max' => 250],
            [['name_ru_tvar'], 'string', 'max' => 250],
            [['name_ru_pred'], 'string', 'max' => 250],
            [['in_popup'], 'boolean'],
            [['bold'], 'boolean'],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png'],
            [['lat', 'lon'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_region' => 'Регион',
            'name' => 'Название',
            'name_en' => 'Название (на англиском)',
            'name_ru_rod' => 'Название (Кого? Чего?)',
            'name_ru_dat' => 'Название (Кому? Чему?)',
            'name_ru_vin' => 'Название (Кого? Что?)',
            'name_ru_tvar' => 'Название (Кем? Чем?)',
            'name_ru_pred' => 'Название (В ком? В чем?) - писать без предлога',
            'in_popup' => 'Показать в выборе городов',
            'bold' => 'Выделить жирным шрифтом',
            'img' => 'Изображение',
        ];
    }

    public function getRegion(){
        return $this->hasOne(FsRegion::class, ['id' => 'id_region']);
    }

}
