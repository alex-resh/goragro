<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tags_to_news}}".
 *
 * @property int $id
 * @property int|null $tag_id
 * @property int|null $news_id
 */
class TagsToNews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tags_to_news}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tag_id', 'news_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tag_id' => 'Tag ID',
            'news_id' => 'News ID',
        ];
    }
}
