<?php

namespace app\models;

use app\models\forms\ElForm;
use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%additionally}}".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $desc
 * @property int|null $type
 * @property string|null $data
 * @property int|null $status
 *
 * @property AdditionallyCategory[]|null $additionallyCategories
 * @property Category[]|null $categories
 * @property int[]|null $catIds
 * @property string[] $extraAttributes
 */
class Additionally extends \yii\db\ActiveRecord
{
    protected $_catIds = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'data'], 'required'],
            [['desc'], 'string'],
            [['type', 'status'], 'integer'],
            [['data'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['catIds'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'desc' => 'Описание',
            'type' => 'Тип',
            'data' => 'Данные',
            'status' => 'Статус',
        ];
    }

    public function getAdditionallyCategories()
    {
        return $this->hasMany(AdditionallyCategory::class, ['additionally_id' => 'id']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->via('additionallyCategories');
    }

    public function getCatIds()
    {
        return $this->getAdditionallyCategories()->select('category_id')->column();
    }

    public function setCatIds($ids)
    {
        $this->_catIds = $ids;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $links = $this->getAdditionallyCategories()->orderBy('pos_c_in_a')->all();
        $existsIds = [];
        $countDeleted = 0;
        foreach ($links as $link) {
            if(in_array($link->category_id, $this->_catIds)){
                $existsIds[] = $link->category_id;
                if($countDeleted){
                    $link->updateCounters('pos_c_in_a', $countDeleted);
                }
            } else {
                $link->delete();
                $countDeleted++;
            }
        }
        $pos = count($links) - $countDeleted;
        foreach (array_diff($this->_catIds, $existsIds) as $id){
            $model = new AdditionallyCategory([
                'additionally_id' => $this->id,
                'category_id' => $id,
                'pos_c_in_a' => ++$pos,
            ]);
            $model->save();
        }
    }

    public function field($data, $model = null)
    {
        if($data['el'] == 'widget'){
            $class = $data['widgetOptions']['class'];
            if($model){
                $options = array_merge($data['widgetOptions'], [
                    'model' => $model,
                    'attribute' => '[' . $model->form_id . ']' . $data['name'],
                ]);
                return $class::widget($options);
            }
            $options = array_merge((array)$data['widgetOptions'],  [
                'name' => '[' . ($model ? $model->form_id : 0) . ']' . $data['name'],
                'value' => $model ? $model->{$data['name']} : $data['default_value'],
            ]);
            return $class::widget($data['widgetOptions']);
        }
        if($model){
            $field = 'active' . ucfirst($data['el']);
        } else {
            $field =$data['el'];
        }
        if(in_array($data['el'], ElForm::listElements())){
            if($model){
                return Html::{$field}($model, '[' . ($model ? $model->form_id : 0) . ']' . $data['name'], (array)$data['listData'], (array)$data['options']);
            }
            return Html::{$field}($data['name'], (array)$data['default_value'], (array)$data['listData'], (array)$data['options']);
        }
        $options = $data['options'] ? $data['options'] : [];
        if($data['el'] != 'radio' && $data['el'] != 'checkBox'){
            Html::addCssClass($options, ['form__input', 'form__field']);
        } elseif(isset($data['label']) && $data['label']) {
            $options['label'] = $data['label'];
        }
        if($model){
            return Html::{$field}($model, '[' . $model->form_id . ']' . $data['name'], $options);
        }
        return Html::{$field}($data['name'], $data['default_value'], (array)$data['options']);
    }

    public function filter($data, $model = null)
    {
        if($data['filter'] == 'widget'){
            if(isset($data['filterWidgetOptions']['class'])){
                $class = $data['filterWidgetOptions']['class'];
                echo '<pre>';
                print_r($class);
                echo '</pre>';
            } else {
                return '<div class="alert alert-danger">Не задан класс виджета</div>';
            }

//            if($model){
//                $options = array_merge($data['widgetOptions'], [
//                    'model' => $model,
//                    'attribute' => '[' . $model->form_id . ']' . $data['name'],
//                ]);
//                return $class::widget($options);
//            }
//            $options = array_merge((array)$data['widgetOptions'],  [
//                'name' => '[' . ($model ? $model->form_id : 0) . ']' . $data['name'],
//                'value' => $model ? $model->{$data['name']} : $data['default_value'],
//            ]);
//            return $class::widget($data['widgetOptions']);
        }
//        if($model){
//            $field = 'active' . ucfirst($data['el']);
//        } else {
//            $field =$data['el'];
//        }
//        if(in_array($data['el'], ElForm::listElements())){
//            if($model){
//                return Html::{$field}($model, '[' . ($model ? $model->form_id : 0) . ']' . $data['name'], (array)$data['listData'], (array)$data['options']);
//            }
//            return Html::{$field}($data['name'], (array)$data['default_value'], (array)$data['listData'], (array)$data['options']);
//        }
//        $options = $data['options'] ? $data['options'] : [];
//        if($data['el'] != 'radio' && $data['el'] != 'checkBox'){
//            Html::addCssClass($options, ['form__input', 'form__field']);
//        } elseif(isset($data['label']) && $data['label']) {
//            $options['label'] = $data['label'];
//        }
//        if($model){
//            return Html::{$field}($model, '[' . $model->form_id . ']' . $data['name'], $options);
//        }
//        return Html::{$field}($data['name'], $data['default_value'], (array)$data['options']);
    }

    public function getExtraAttributes()
    {
        $res = [];
        foreach ((array)Json::decode($this->data)['rows'] as $row){
            foreach ($row['columns'] as $column) {
                foreach ($column['elements'] as $element) {
                    $res[] = $element['name'];
                }
            }
        }
        array_unique($res);
        return $res;
    }
}
