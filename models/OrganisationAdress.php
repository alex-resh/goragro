<?php

namespace app\models;

use app\components\GeocoderBehavior;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "organisation_adress".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property int|null $city_id
 * @property string|null $postal_code
 * @property string|null $street
 * @property string|null $house
 * @property string|null $flat
 * @property string|null $flooar
 * @property string|null $phones
 * @property string|null $emails
 * @property string|null $url
 * @property string|null $social
 * @property string|null $region
 */
class OrganisationAdress extends \yii\db\ActiveRecord
{
    public $phoneType = [];
    public $emailType = [];
    public $urlType = [];
    public $socialType = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation_adress}}';
    }

    public function behaviors()
    {
        return [

            'geocoder' => [
                'class' => GeocoderBehavior::class,
                'adress_field' => 'toString',
                'district_field' => 'region'
            ]
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'phoneType',
            'emailType',
            'urlType',
            'socialType',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'required', 'message' => 'Необходимо выбрать город'],
            [['phones', 'emails'], 'required', 'isEmpty' => function($value){return $value == [''];}],
            [['organisation_id', 'city_id'], 'integer'],
            ['phones', 'each', 'stopOnFirstError' => false, 'rule' => ['match', 'skipOnEmpty' => false, 'message' => 'Телефон "{value}" введён не верно', 'pattern' => '`\+7\(\d\d\d\)-\d\d\d-\d\d-\d\d.*`']],
            ['emails', 'each', 'rule' => ['email', 'message' => 'Значение «{value}» не является правильным email адресом', 'skipOnEmpty' => false]],
            ['url', 'each', 'rule' => ['url', 'message' => 'Значение "{value}" не является правильным url адресом', 'defaultScheme' => 'http']],
            [['phones', 'emails', 'social', 'url', 'phoneType', 'emailType', 'urlType', 'socialType'], 'each', 'rule' => ['string']],
            [['postal_code', 'street', 'house', 'flat', 'flooar'], 'string', 'max' => 255],
            ['region', 'safe'],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'address' => ['city_id', 'street', 'house', 'flat', 'flooar', 'postal_code'],
            'phones' => ['phones'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'city_id' => 'ID города',
            'postal_code' => 'Индекс',
            'street' => 'Улица',
            'house' => 'Дом',
            'flat' => 'Офис',
            'flooar' => 'Этаж',
            'phones' => 'Телефон',
            'emails' => 'Электроная почта',
            'url' => 'Адресс сайта',
            'social' => 'Соцсети',
            'region' => 'Район',
        ];
    }

    public function getCity(){
        return $this->hasOne(FsCity::class, ['id' => 'city_id']);
    }

    public function phoneTypeList()
    {
        $extra = [];
        foreach ((array)$this->phoneType as $item) {
            $extra[$item] = $item;
        }
        return array_merge([
            'Рабочий' => 'Рабочий',
            'Мобильный' => 'Мобильный',
            'Стационарный' => 'Стационарный',
        ], $extra);
    }

    public function emailTypeList()
    {
        $extra = [];
        foreach ((array)$this->emailType as $item) {
            $extra[$item] = $item;
        }
        return array_merge([
            'По вопросам рекламы' => 'По вопросам рекламы',
            'По всем вопросам' => 'По всем вопросам',
            'Рабочая' => 'Рабочая',
        ], $extra);
    }

    public function urlTypeList()
    {
        $extra = [];
        foreach ((array)$this->urlType as $item) {
            $extra[$item] = $item;
        }
        return array_merge([
            'Основной' => 'Основной',
        ], $extra);
    }

    public function socialTypeList()
    {
        $extra = [];
        foreach ((array)$this->socialType as $item) {
            $extra[$item] = $item;
        }
        return array_merge([
            'Instagram' => 'Instagram',
            'В контакте' => 'В контакте',
            'Facebook' => 'Facebook',
            'Twitter' => 'Twitter',
        ], $extra);
    }

    public function beforeSave($insert)
    {
        $arr = [];
        if(is_array($this->phones) && $this->phoneType){
            foreach ($this->phones as $k => $phone) {
                $arr[$k] = [
                    'value' => $phone,
                    'type' => $this->phoneType[$k],
                ];
            }
            $this->phones = Json::encode($arr);
            $arr = [];
        }

        if(is_array($this->emails) && $this->emailType){
            foreach ($this->emails as $k => $email) {
                $arr[$k] = [
                    'value' => $email,
                    'type' => $this->emailType[$k],
                ];
            }
            $this->emails = Json::encode($arr);
            $arr = [];
        }

        if(is_array($this->url) && $this->urlType){
            foreach ($this->url as $k => $url) {
                if(trim($url)){
                    $arr[$k] = [
                        'value' => $url,
                        'type' => $this->urlType[$k],
                    ];
                }
            }
            $this->url = Json::encode($arr);
            $arr = [];
        }

        if(is_array($this->social) && $this->socialType){
            foreach ($this->social as $k => $social) {
                if(trim($social)){
                    $arr[$k] = [
                        'value' => $social,
                        'type' => $this->socialType[$k],
                    ];
                }
            }
            $this->social = Json::encode($arr);
            $arr = [];
        }
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        parent::afterFind();
        $phones = [];
        $types = [];
        try {
            foreach (Json::decode($this->phones) as $i => $item) {
                $phones[$i] = $item['value'];
                $types[$i] = $item['type'];
            }
        } catch (\Exception $e){

        }
        $this->phones = $phones;
        $this->phoneType = $types;

        $emails = [];
        $types = [];
        try {
            foreach (Json::decode($this->emails) as $i => $item) {
                $emails[$i] = $item['value'];
                $types[$i] = $item['type'];
            }
        } catch (\Exception $e){

        }
        $this->emails = $emails;
        $this->emailType = $types;

        $url = [];
        $types = [];
        try {
            foreach (Json::decode($this->url) as $i => $item) {
                $url[$i] = $item['value'];
                $types[$i] = $item['type'];
            }
        } catch (\Exception $e){

        }
        $this->url = $url ? $url : [''];
        $list = $this->urlTypeList();
        $this->urlType = $types ? $types : [array_shift($list)];

        $social = [];
        $types = [];
        try {
            foreach (Json::decode($this->social) as $i => $item) {
                $social[$i] = $item['value'];
                $types[$i] = $item['type'];
            }
        } catch (\Exception $e){

        }
        $this->social = $social ? $social : [''];
        $list = $this->socialTypeList();
        $this->socialType = $types ? $types : [array_shift($list)];
    }

    public function getToString()
    {
        $arr = [
            $this->postal_code,
            $this->city->name ?? '',
            $this->street,
            $this->house
        ];
        foreach ($arr as $i => $k){
            if($k == ''){
                unset($arr[$i]);
            }
        }
        return implode(', ', $arr);
    }

    public static function regionList($city_id)
    {
        return static::find()->distinct()->select('region')->where(['!=', 'region', ''])->andWhere(['city_id' => Yii::$app->city->city->id])->indexBy('region')->column();
    }
}
