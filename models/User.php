<?php

namespace app\models;

use dektrium\user\helpers\Password;
use dektrium\user\models\Token;
use yii\base\Security;

/**
 * @property integer $last_action_time
 */
class User extends \dektrium\user\models\User
{

    public static function findByUsername($username){
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password){
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules['emailUnique'] = [
            'email',
            'unique',
            'message' => \Yii::t('user', 'This email address has already been taken'),
            'skipOnEmpty' => true
        ];
        $rules['lastAction'] = ['last_action_time', 'integer'];
        return $rules;
    }


    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = $this->module->enableConfirmation ? null : time();
            $this->password     = $this->module->enableGeneratingPassword ? Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_REGISTER);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($this->module->enableConfirmation) {
                /** @var Token $token */
                $token = \Yii::createObject(['class' => Token::className(), 'type' => Token::TYPE_CONFIRMATION]);
                $token->link('user', $this);
            }

            //$this->mailer->sendWelcomeMessage($this, isset($token) ? $token : null);
            $this->trigger(self::AFTER_REGISTER);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }

    public function getName(){
        if($this->profile->name != ''){
            return $this->profile->name;
        } else {
            return $this->username;
        }
    }

    public function isOnline(){
        return $this->last_action_time > time() - 60*5;
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    public function getAvatar(){
        return $this->profile->getThumbUploadUrl('img', 'thumb');
    }

    public function getFullname(){
        return $this->getName();
    }

}
