<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use app\modules\likes\traits\LikeTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "organisation".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $name
 * @property string|null $desc
 * @property string|null $img
 * @property string|null $moderator_msg
 * @property int $status
 * @property int $views_count
 * @property int $created_at
 * @property int $updated_at
 * @property Category[] $categories
 * @property OrganisationAdress $address
 * @property OrganisationGallery[] $gallery
 * @property OrganisationWorktime $worktime
 * @property FsCity $city
 * @property User $user
 * @property Action[] $actions
 * @property Agent[] $agents
 * @property int|null $clone_id
 * @property Organisation|null $clone
 * @property Organisation|null $origin
 */
class Organisation extends \yii\db\ActiveRecord
{
    use LikeTrait;

    const STATUS_NEW = 0;
    const STATUS_MODERATED = 1;
    const STATUS_REWORK = 2;
    const STATUS_PUBLISHED = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation}}';
    }

    public function scenarios()
    {
        $attributes = $this->attributes();
        unset($attributes['img']);
        return array_merge(parent::scenarios(), [
            'publish' => $attributes,
            'not_img' => $attributes,
            'clone',
        ]);
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/organisation/{id}',
                'url' => '@web/upload/organisation/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'org_img' => ['width' => 178, 'height' => 178],
                ],
            ],
            TimestampBehavior::class,
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'views_count', 'created_at', 'updated_at', 'clone_id'], 'integer'],
            [['desc'], 'string'],
            [['name', 'desc'], 'required'],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => 'default'],
            ['img', 'string', 'on' => 'clone'],
            ['moderator_msg', 'required', 'on' => 'rework'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'ID пользователя',
            'name'          => 'Заголовок',
            'desc'          => 'Описание',
            'img'           => 'Логотип',
            'moderator_msg' => 'Сообщение модератора',
            'status'        => 'Статус',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getOrganisationCategories()
    {
        return $this->hasMany(OrganisationCategory::class, ['organisation_id' => 'id']);
    }

    public function getCategories(){
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable(OrganisationCategory::tableName(), ['organisation_id' => 'id']);
    }

    public function getAddress(){
        return $this->hasOne(OrganisationAdress::class, ['organisation_id' => 'id']);
    }

    public function getGallery(){
        return $this->hasMany(OrganisationGallery::class, ['organisation_id' => 'id']);
    }

    public function getWorktime(){
        return $this->hasOne(OrganisationWorktime::class, ['organisation_id' => 'id']);
    }

    public function getCity(){
        return $this->hasOne(FsCity::class, ['id' => 'city_id'])
            ->viaTable(OrganisationCity::tableName(), ['organisation_city' => 'id']);
    }

    public function getActions(){
        return $this->hasMany(Action::class, ['organisation_id' => 'id']);
    }

    public function getAdditionallyDates()
    {
        return $this->hasMany(AdditionallyData::class, ['organisation_id' => 'id']);
    }

    public function getAgents(){
        return $this->hasMany(Agent::class, ['organisation_id' => 'id']);
    }

    public function getAgent(){
        return Agent::findOne(['organisation_id' => $this->id, 'user_id' => Yii::$app->user->id]);
    }

    public static function getCount($cat_id = null){
        $result = Organisation::find()->joinWith('address')
            ->andWhere(['city_id' => Yii::$app->city->city->id])
            ->andWhere(['status' => Organisation::STATUS_PUBLISHED]);
            if($cat_id){
                $result->joinWith('categories')
                    ->andWhere(['category_id' => $cat_id]);
            }

        return $result->count();
    }

    public static function staticStatusList()
    {
        return [
            static::STATUS_NEW          => 'Новая',
            static::STATUS_MODERATED    => 'Модерация',
            static::STATUS_REWORK       => 'Доработка',
            static::STATUS_PUBLISHED    => 'Опубликована',
        ];
    }

    public function statusList()
    {
        return static::staticStatusList();
    }

    public function getStatusName()
    {
        return $this->statusList()[$this->status];
    }

    public function getStatusClass()
    {
        $classes = [
            static::STATUS_NEW          => 'info',
            static::STATUS_MODERATED    => 'warning',
            static::STATUS_REWORK       => 'danger',
            static::STATUS_PUBLISHED    => 'success',
        ];
        return $classes[$this->status];
    }

    public function getCommentsCount(){
        $count = Comments::find()->where(['relatedTo' => self::class . ':' . $this->id])->count();
        return $count;
    }

    public function getClone()
    {
        return $this->hasOne(static::class, ['id' => 'clone_id']);
    }

    public function getOrigin()
    {
        return $this->hasOne(static::class, ['clone_id' => 'id']);
    }

    public function clone()
    {
        if($this->clone_id){
            return false;
        }

        // Клонируем организацию
        $transaction = Yii::$app->db->beginTransaction();
        $origin = $this->origin;
        $attributes = $this->attributes;
        $attributes['clone_id'] = $this->id;
        unset($attributes['id']);
        $origin = $origin ? $origin : new Organisation($attributes);
        $origin->clone_id = $this->id;
        $origin->scenario = 'clone';
        $origin->load($attributes, '') && $origin->save();
        $this->copyImg($this, $origin);

        // Клонируем адрес
        $attributes = OrganisationAdress::findOne(['organisation_id' => $this->id])->attributes;
        unset($attributes['id']);
        $attributes['organisation_id'] = $origin->id;
        $address = $origin->address;
        $address = $address ? $address : new OrganisationAdress();
        $address->load($attributes, '');
        $address->save();

        // Клонируем связи с категориями
        $catIds = OrganisationCategory::find()->select('category_id')->where(['organisation_id' => $this->id])->column();
        $oldCatIds = OrganisationCategory::find()->select('category_id')->where(['organisation_id' => $origin->id])->column();
        foreach (array_diff($oldCatIds, $catIds) as $id) {
            $model = OrganisationCategory::findOne(['organisation_id' => $origin->id, 'category_id' => $id]);
            $model->delete();
        }
        foreach (array_diff($catIds, $oldCatIds) as $id) {
            $model = new OrganisationCategory(['organisation_id' => $origin->id, 'category_id' => $id]);
            $model->save();
        }

        // Клонирование графика работы
        $attributes = $this->worktime->attributes;
        unset($attributes['id']);
        $attributes['organisation_id'] = $origin->id;
        $originWorktime = $origin->worktime;
        $originWorktime = $originWorktime ? $originWorktime : new OrganisationWorktime();
        $originWorktime->load($attributes, '');
        $originWorktime->save();

        // Клонирование галереи
        $models = $this->gallery;
        foreach ($models as $model) {
            $attributes = $model->attributes;
            unset($attributes['id']);
            $attributes['organisation_id'] = $origin->id;
            $attributes['clone_id'] = $model->id;
            $originModel = OrganisationGallery::findOne(['organisation_id' => $origin->id, 'clone_id' => $model->id]);
            $originModel = $originModel ? $originModel : new OrganisationGallery();
            $originModel->scenario = 'clone';
            $originModel->load($attributes, '');
            $originModel->save();
            $this->copyImg($model, $originModel);
        }
        foreach (OrganisationGallery::find()->andWhere(['and', ['organisation_id' => $origin->id], ['not in', 'clone_id', ArrayHelper::getColumn($models, 'id')]])->all() as $model) {
            $model->delete();
        }

        // Клонирование доп. данных
        $models = $this->additionallyDates;
        foreach ($models as $model) {
            $attributes = $model->attributes;
            unset($attributes['id']);
            $attributes['organisation_id'] = $origin->id;
            $attributes['clone_id'] = $model->id;
            $originModel = AdditionallyData::findOne(['organisation_id' => $origin->id, 'clone_id' => $model->id]);
            $originModel = $originModel ? $originModel : new AdditionallyData(['form_id' => $model->form_id]);
            $originModel->load($attributes, '');
            $originModel->save();
        }
        foreach (AdditionallyData::find()->andWhere(['and', ['organisation_id' => $origin->id], ['not in', 'clone_id', ArrayHelper::getColumn($models, 'id')]])->all() as $model) {
            $model->delete();
        }

        // Клонирование акций
        $models = $this->actions;
        foreach ($models as $model) {
            $attributes = $model->attributes;
            unset($attributes['id']);
            $attributes['organisation_id'] = $origin->id;
            $attributes['clone_id'] = $model->id;
            unset($attributes['image_id']);
            $originModel = Action::findOne(['organisation_id' => $origin->id, 'clone_id' => $model->id]);
            $originModel = $originModel ? $originModel : new Action();
            $originModel->load($attributes, '');
            // Клонирование изображения
            $img = $model->image;
            if(!$img){
                $originModel->image_id = null;
            } else {
                $originImage = $originModel->image_id ? $originModel->image : new Image();
                $originImage->scenario = 'clone';
                $imageAttributes = $img->attributes;
                unset($imageAttributes['id']);
                if($originImage->load($imageAttributes, '') && $originImage->save()){
                    $this->copyImg($img, $originImage);
                    $originModel->image_id = $originImage->id;
                }
            }
            $originModel->save();
        }
        foreach (Action::find()->andWhere(['and', ['organisation_id' => $origin->id], ['not in', 'clone_id', ArrayHelper::getColumn($models, 'id')]])->all() as $model) {
            $model->delete();
        }

        // Клонирование продуктов
        foreach (Product::productTypeList() as $class => $item) {
            $models = $class::find()->andWhere(['organisation_id' => $this->id])->all();
            foreach ($models as $model) {
                $attributes = $model->attributes;
                unset($attributes['id']);
                $attributes['organisation_id'] = $origin->id;
                $attributes['clone_id'] = $model->id;
                $originModel = $class::findOne(['organisation_id' => $origin->id, 'clone_id' => $model->id]);
                $originModel = $originModel ? $originModel : new $class();
                $originModel->scenario = 'clone';
                $originModel->load($attributes, '');
                $originModel->save();
                $this->copyImg($model, $originModel);
            }
            foreach ($class::find()->andWhere(['and', ['organisation_id' => $origin->id], ['not in', 'clone_id', ArrayHelper::getColumn($models, 'id')]])->all() as $model) {
                $model->delete();
            }
        }
        $transaction->commit();
        return true;
    }

    public static function getCountNew(){
        return Organisation::find()->where(['status' => Organisation::STATUS_NEW])->count();
    }

    public function copyImg($fromModel, $toModel){
        if(file_exists(Yii::getAlias(str_replace('{id}', $fromModel->id, $fromModel->path)))){
            FileHelper::copyDirectory(Yii::getAlias(str_replace('{id}', $fromModel->id, $fromModel->path)), Yii::getAlias(str_replace('{id}', $toModel->id, $toModel->path)));
        }
    }

    public static function getCountAdminMenu()
    {
        return Organisation::find()->where(['or', ['!=', 'status', Organisation::STATUS_PUBLISHED], ['>', 'clone_id', 0]])->count();
    }
}
