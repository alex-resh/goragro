<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%organisation_worktime}}".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property string|null $mo_start
 * @property string|null $mo_end
 * @property string|null $mo_br_start
 * @property string|null $mo_br_end
 * @property string|null $tu_start
 * @property string|null $tu_end
 * @property string|null $tu_br_start
 * @property string|null $tu_br_end
 * @property string|null $we_start
 * @property string|null $we_end
 * @property string|null $we_br_start
 * @property string|null $we_br_end
 * @property string|null $th_start
 * @property string|null $th_end
 * @property string|null $th_br_start
 * @property string|null $th_br_end
 * @property string|null $fr_start
 * @property string|null $fr_end
 * @property string|null $fr_br_start
 * @property string|null $fr_br_end
 * @property string|null $sa_start
 * @property string|null $sa_end
 * @property string|null $sa_br_start
 * @property string|null $sa_br_end
 * @property string|null $su_start
 * @property string|null $su_end
 * @property string|null $su_br_start
 * @property string|null $su_br_end
 *
 * @property Organisation $organisation
 */
class OrganisationWorktime extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation_worktime}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_id'], 'integer'],
            [['mo_start', 'mo_end', 'mo_br_start', 'mo_br_end', 'tu_start', 'tu_end', 'tu_br_start', 'tu_br_end', 'we_start', 'we_end', 'we_br_start', 'we_br_end', 'th_start', 'th_end', 'th_br_start', 'th_br_end', 'fr_start', 'fr_end', 'fr_br_start', 'fr_br_end', 'sa_start', 'sa_end', 'sa_br_start', 'sa_br_end', 'su_start', 'su_end', 'su_br_start', 'su_br_end'], 'safe'],
            [['organisation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organisation::class, 'targetAttribute' => ['organisation_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'mo_start' => 'Mo Start',
            'mo_end' => 'Mo End',
            'mo_br_start' => 'Mo Br Start',
            'mo_br_end' => 'Mo Br End',
            'tu_start' => 'Tu Start',
            'tu_end' => 'Tu End',
            'tu_br_start' => 'Tu Br Start',
            'tu_br_end' => 'Tu Br End',
            'we_start' => 'We Start',
            'we_end' => 'We End',
            'we_br_start' => 'We Br Start',
            'we_br_end' => 'We Br End',
            'th_start' => 'Th Start',
            'th_end' => 'Th End',
            'th_br_start' => 'Th Br Start',
            'th_br_end' => 'Th Br End',
            'fr_start' => 'Fr Start',
            'fr_end' => 'Fr End',
            'fr_br_start' => 'Fr Br Start',
            'fr_br_end' => 'Fr Br End',
            'sa_start' => 'Sa Start',
            'sa_end' => 'Sa End',
            'sa_br_start' => 'Sa Br Start',
            'sa_br_end' => 'Sa Br End',
            'su_start' => 'Su Start',
            'su_end' => 'Su End',
            'su_br_start' => 'Su Br Start',
            'su_br_end' => 'Su Br End',
        ];
    }

    /**
     * Gets query for [[Organisation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganisation()
    {
        return $this->hasOne(Organisation::class, ['id' => 'organisation_id']);
    }

    public function days()
    {
        return [
            'mo',
            'tu',
            'we',
            'th',
            'fr',
            'sa',
            'su',
        ];
    }

    public function getData()
    {
        $data = [];
        $days = [];
        $curr = [];
        foreach ($this->days() as $i => $day) {
            if($this->{$day . '_start'} && $this->{$day . '_end'}){
                $time = [
                    $this->{$day . '_start'},
                    $this->{$day . '_end'},
                    $this->{$day . '_br_start'},
                    $this->{$day . '_br_end'},
                ];
                if($time == $curr){
                    $days[] = $i;
                } elseif($days && $curr) {
                    $data[] = [
                        'days' => $days,
                        'workStart' => $curr[0],
                        'workEnd' => $curr[1],
                        'breakStart' => $curr[2],
                        'breakEnd' => $curr[3],
                    ];
                    $days = [$i];
                    $curr = $time;
                } else {
                    $days = [$i];
                    $curr = $time;
                }
            }
        }
        if($days && $curr) {
            $data[] = [
                'days' => $days,
                'workStart' => $curr[0],
                'workEnd' => $curr[1],
                'breakStart' => $curr[2],
                'breakEnd' => $curr[3],
            ];
        }
        if(empty($days)){
            $data[] = [
                'days' => [],
                'workStart' => '',
                'workEnd' => '',
                'breakStart' => '',
                'breakEnd' => '',
            ];
        }
        return $data;
    }

    public function getToday()
    {
        $prefix = lcfirst(substr(date('D'),0, 2)) . '_';
        $now = (int)date('H');
        $res = [
            $this->{$prefix . 'start'},
            $this->{$prefix . 'end'},
            $this->{$prefix . 'br_start'},
            $this->{$prefix . 'br_end'},
        ];

        if((int)$res[0] <= $now && $now < (int)$res[1]){
            $res[4] = 'Работает сейчас';
        }
        if(isset($res[4]) && (int)$res[2] <= $now && $now < $res[3]){
            $res[4] = 'Перерыв';
        }
        $res[4] = $res[4] ?? 'Закрыто';

        return $res;
    }

    public static function listForFilter()
    {
        return [
            '' => 'Не имеет значения',
            'around' => 'Круглосуточно',
            'now' => 'Работает сейчас',
        ];
    }
}
