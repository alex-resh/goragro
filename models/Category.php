<?php

namespace app\models;

use app\models\query\CategoryQuery;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property Organisation[] $organisations
 */
class Category extends \yii\db\ActiveRecord
{

    public $parent_id;
    public $c;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'url'], 'string', 'max' => 255],
            ['parent_id', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url' => 'Url',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата добавления',
        ];
    }

    public function getOrganisations(){
        return $this->hasMany(Organisation::class, ['id' => 'organisation_id'])
            ->viaTable(OrganisationCategory::tableName(), ['category_id' => 'id']);
    }

    public function getAdditionallies()
    {
        return $this->hasMany(Additionally::class, ['id' => 'additionally_id'])
            ->viaTable(AdditionallyCategory::tableName(), ['category_id' => 'id']);
    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public static function getOrgWithCount(){
        $result = Category::find()
            ->select([Category::tableName() . '.*', new Expression('count(*) as c')])
            ->innerJoin(OrganisationCategory::tableName(), 'organisation_category.category_id=category.id')
            ->innerJoin(Organisation::tableName(), 'organisation.id=organisation_category.organisation_id')
            ->innerJoin(OrganisationAdress::tableName(), 'organisation_adress.organisation_id=organisation_category.organisation_id')
            ->where([OrganisationAdress::tableName().'.city_id' => Yii::$app->city->city->id])
            ->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_PUBLISHED])
            ->andWhere(['>', Organisation::tableName() . '.clone_id', 0])
            ->groupBy([Category::tableName() . '.id'])
            ->all();
            return $result;
    }
}
