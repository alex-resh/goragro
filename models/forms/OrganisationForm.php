<?php


namespace app\models\forms;


use yii\base\Model;

class OrganisationForm extends Model
{
    public $categories;

    public function rules()
    {
        return [
            [['categories'], 'required'],
            [['categories'], 'each', 'rule' => ['integer']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'categories' => 'Категории',
        ];
    }
}