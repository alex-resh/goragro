<?php


namespace app\models\forms;


use yii\base\Model;

/**
 * Class CommentSettingsForm
 * @package app\models\forms
 *
 * @property int $number
 * @property int $width
 * @property int $height
 * @property bool $img_allowed
 */

class CommentSettingsForm extends Model
{
    public $number = 1;
    public $width = 200;
    public $height = 200;
    public $img_allowed = true;

    public function rules()
    {
        return [
            [['number', 'width', 'height', 'img_allowed'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number' => 'Количество',
            'width' => 'Ширина',
            'height' => 'Высота',
            'img_allowed' => 'Разрешить изображения',
        ];
    }
}