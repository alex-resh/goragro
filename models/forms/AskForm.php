<?php


namespace app\models\forms;


use app\models\Settings;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\base\Model;

class AskForm extends Model
{

    public $name;
    public $text;
    public $email;
    public $recapcha;

    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required'],
            ['text', 'string'],
            ['email', 'email'],
            ['recapcha', ReCaptchaValidator2::class, 'secret' => Settings::get('recaptcha_secret_key')]
        ];
    }

    public function send(){
        $body = 'Имя: ' . $this->name . "\nEmail: " . $this->email . "\nТекст: " . $this->text;
        \Yii::$app->mailer->compose()
            ->setTo('test@test.com')
            ->setFrom([\Yii::$app->params['senderEmail'] => \Yii::$app->params['senderName']])
            ->setReplyTo([$this->email => $this->name])
            ->setSubject('Заявка на представителя')
            ->setTextBody($body)
            ->send();
    }

}