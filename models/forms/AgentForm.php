<?php


namespace app\models\forms;


use app\models\Agent;
use app\models\Settings;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\base\Model;

class AgentForm extends Model
{

    public $name;
    public $phone;
    public $email;
    public $recapcha;
    public $organisation_id;

    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required'],
            ['phone', 'match', 'pattern' => '`\+7\(\d\d\d\)-\d\d\d-\d\d-\d\d.*`'],
            ['email', 'email'],
            ['organisation_id', 'integer'],
            ['recapcha', ReCaptchaValidator2::class, 'secret' => Settings::get('recaptcha_secret_key')]
        ];
    }

    public function send(){
        $this->save();
        $body = 'Имя: ' . $this->name . "\nТелефон: " . $this->phone . "\nEmail: " . $this->email;
        \Yii::$app->mailer->compose()
            ->setTo('test@test.com')
            ->setFrom([\Yii::$app->params['senderEmail'] => \Yii::$app->params['senderName']])
            ->setReplyTo([$this->email => $this->name])
            ->setSubject('Заявка на представителя')
            ->setTextBody($body)
            ->send();
    }

    public function save(){
        $agent = new Agent();
        $agent->user_id = \Yii::$app->user->id;
        $agent->organisation_id = $this->organisation_id;
        $agent->name = $this->name;
        $agent->email = $this->email;
        $agent->phone = $this->phone;
        $agent->status = Agent::STATUS_NEW;
        $agent->save();
    }

}