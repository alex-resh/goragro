<?php


namespace app\models\forms;


use app\models\Settings;
use himiklab\yii2\recaptcha\ReCaptchaValidator2;
use yii\base\Model;

class ErrorForm extends Model
{

    public $contacts;
    public $category;
    public $another;
    public $recapcha;

    public function rules()
    {
        return [
            [['contacts', 'category', 'another'], 'integer'],
            ['recapcha', ReCaptchaValidator2::class, 'secret' => Settings::get('recaptcha_secret_key')]
        ];
    }

    public function send(){
        $body = '';
        if($this->contacts){
            $body .= "Неверная контактная информация\n";
        }
        if($this->category){
            $body .= "Объект не соответствует рубрике\n";
        }
        if($this->another){
            $body .= "Другое\n";
        }
        if($body) {
            \Yii::$app->mailer->compose()
                ->setTo('test@test.com')
                ->setFrom([\Yii::$app->params['senderEmail'] => \Yii::$app->params['senderName']])
                ->setSubject('Ошибка в тексте')
                ->setTextBody($body)
                ->send();
        }
    }

}