<?php


namespace app\models\forms;


use yii\base\Model;
use yii\bootstrap\Html;
use yii\helpers\Json;

/**
 * @property string|null $label
 * @property string $name
 * @property string $icon
 * @property mixed $defaunt_value
 * @property string $el
 * @property string $listData
 * @property string $options
 * @property string $widgetOptions
 * @property string $filter
 * @property string $filterOptions
 * @property string $filterListData
 * @property string $filterWidgetOptions
 *
 * @property array $elList
 * @property array $listElements
 */
class ElForm extends Model
{
    public $label;
    public $name;
    public $icon;
    public $default_value;
    public $el;
    public $listData = [];
    public $options = [];
    public $widgetOptions = [];
    public $filter;
    public $filterOptions = [];
    public $filterListData = [];
    public $filterWidgetOptions = [];

    public function rules()
    {
        return [
            [['name', 'el'], 'required'],
            [['name', 'label', 'icon', 'default_value', 'options', 'listData', 'widgetOptions', 'filter', 'filterOptions', 'filterListData', 'filterWidgetOptions'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'icon' => 'Иконка',
            'default_value' => 'Значение поумолчанию',
            'el' => 'Элемент',
            'options' => 'Опции',
            'listData' => 'Список',
            'widgetOptions' => 'Опции виджета',
            'filter' => 'Фильтр',
            'filterOptions' => 'Опции фильтра',
            'filterWidgetOptions' => 'Опции виджета фильтра',
        ];
    }

    public static function elList()
    {
        return [
            'textInput' => 'Текстовое поле',
            'passwordInput' => 'Поле ввода пароля',
            'fileInput' => 'Поле загрузки файла',
            'textarea' => 'Текстэриа',
            'radio' => 'Радио',
            'checkBox' => 'Чекбокс',
            'dropDownList' => 'Выпадающий список',
            'listBox' => 'Список',
            'checkboxList' => 'Список чекбоксов',
            'radioList' => 'Радио список',
            'widget' => 'Виджет',
        ];
    }

    public function getElList()
    {
        return static::elList();
    }

    public static function listElements()
    {
        return [
            'dropDownList',
            'listBox',
            'checkboxList',
            'radioList',
        ];
    }

    public function getListElements()
    {
        return static::listElements();
    }

    public static function iconList()
    {
        $icons = [
            'ok',
            'flag',
            'headphones',
        ];
        return array_combine($icons, $icons);
    }

    public static function field($data, $model = null)
    {
        if($data['el'] == 'widget'){
            $class = $data['widgetOptions']['class'];
            if($model){
                $options = array_merge($data['widgetOptions'], [
                    'model' => $model,
                    'attribute' => '[' . $model->form_id . ']' . $data['name'],
                ]);
                return $class::widget($options);
            }
            $options = array_merge((array)$data['widgetOptions'],  [
                'name' => '[' . ($model ? $model->form_id : 0) . ']' . $data['name'],
                'value' => $model ? $model->{$data['name']} : $data['default_value'],
            ]);
            return $class::widget($data['widgetOptions']);
        }
        if($model){
            $field = 'active' . ucfirst($data['el']);
        } else {
            $field =$data['el'];
        }
        if(in_array($data['el'], ElForm::listElements())){
            if($model){
                return Html::{$field}($model, '[' . ($model ? $model->form_id : 0) . ']' . $data['name'], (array)$data['listData'], (array)$data['options']);
            }
            return Html::{$field}($data['name'], (array)$data['default_value'], (array)$data['listData'], (array)$data['options']);
        }
        $options = $data['options'] ? $data['options'] : [];
        if($data['el'] != 'radio' && $data['el'] != 'checkBox'){
            Html::addCssClass($options, ['form__input', 'form__field']);
        } elseif(isset($data['label']) && $data['label']) {
            $options['label'] = $data['label'];
        }
        if($model){
            return Html::{$field}($model, '[' . $model->form_id . ']' . $data['name'], $options);
        }
        return Html::{$field}($data['name'], $data['default_value'], (array)$data['options']);
    }

    public static function filter($data, $model = null)
    {
        if($data['filter'] == 'widget'){
            if(isset($data['filterWidgetOptions']['class'])){
                $class = $data['filterWidgetOptions']['class'];
            } else {
                return '<div class="alert alert-danger">Не задан класс виджета</div>';
            }
            if($model){
                $options = array_merge($data['filterWidgetOptions'], [
                    'model' => $model,
                    'attribute' => '[' . $model->form_id . ']' . $data['name'],
                ]);
                return $class::widget($options);
            }
            $options = array_merge((array)$data['filterWidgetOptions'],  [
                'name' => '[' . ($model ? $model->form_id : 0) . ']' . $data['name'],
            ]);
            return $class::widget($options);
        }
        if($model){
            $field = 'active' . ucfirst($data['filter']);
        } else {
            $field =$data['filter'];
        }
        if(in_array($data['filter'], ElForm::listElements())){
            if($model){
                return Html::{$field}($model, '[' . ($model ? $model->form_id : 0) . ']' . $data['name'], (array)$data['filterListData'], (array)$data['filterOptions']);
            }
            return Html::{$field}($data['name'], (array)$data['default_value'], (array)$data['filterListData'], (array)$data['filterOptions']);
        }
        $options = $data['filterOptions'] ? $data['filterOptions'] : [];
        if($data['filter'] != 'radio' && $data['filter'] != 'checkBox'){
            Html::addCssClass($options, ['form__input', 'form__field']);
        } elseif(isset($data['label']) && $data['label']) {
            $options['label'] = $data['label'];
        }
        if($model){
            return Html::{$field}($model, '[' . $model->form_id . ']' . $data['name'], $options);
        }
        return Html::{$field}($data['name'], $data['default_value'], (array)$data['filterOptions']);
    }
}