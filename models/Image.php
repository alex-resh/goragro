<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property int $id
 * @property string|null $img
 */
class Image extends \yii\db\ActiveRecord
{

    public $new;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                //'instanceByName' => true,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/images/{id}',
                'url' => '@web/upload/images/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'gallery_img' => ['width' => 179, 'height' => 155],
                ],
            ],
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img'], 'image', 'on' => 'default'],
            [['img'], 'string', 'on' => 'clone'],
            [['created_at', 'updated_at'], 'integer'],
            ['new', 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Img',
        ];
    }
}
