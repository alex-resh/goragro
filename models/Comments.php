<?php


namespace app\models;


use app\modules\likes\traits\LikeTrait;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii2mod\comments\models\CommentModel;
use yii2mod\moderation\enums\Status;

class Comments extends CommentModel
{
    use LikeTrait;


    public function rules()
    {
        return [
            [['entity', 'entityId'], 'required'],
            ['content', 'required', 'message' => \Yii::t('yii2mod.comments', 'Comment cannot be blank.')],
            [['content', 'entity', 'relatedTo', 'url'], 'string'],
            ['status', 'default', 'value' => Status::PENDING],
            ['status', 'in', 'range' => Status::getConstantsByName()],
            ['level', 'default', 'value' => 1],
            ['parentId', 'validateParentID', 'except' => static::SCENARIO_MODERATION],
            [['entityId', 'parentId', 'status', 'level'], 'integer'],
            ['rating', 'integer']
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['purify']);
        $behaviors['moderation']['moderatedByAttribute'] = 'moderated_by';
        $behaviors['moderation']['statusAttribute'] = 'status';
        return $behaviors;
    }

    public static function getBestCommentsForBlog(){
        $query = self::getBest();
        $comments = $query->andWhere(['like', 'relatedTo', 'Blog'])->limit(3)->all();
        return $comments;
//        $query
    }

    /**
     * Get comments tree.
     *
     * @param string $entity
     * @param string $entityId
     * @param null $maxLevel
     *
     * @return array|ActiveRecord[]
     */
    public static function getTree($entity, $entityId, $maxLevel = null)
    {
        $query = static::find()
            ->alias('c')
            //->approved()
            ->andWhere([
                'c.entityId' => $entityId,
                'c.entity' => $entity,
            ])
            ->orderBy(['c.parentId' => SORT_ASC, 'c.createdAt' => SORT_ASC])
            ->with(['author']);

        if ($maxLevel > 0) {
            $query->andWhere(['<=', 'c.level', $maxLevel]);
        }

        if(!\Yii::$app->user->can('admin')){
            if(\Yii::$app->user->isGuest){
                $query->approved();
            } else {
                $query->andWhere(['or' , ['status' => Status::APPROVED], ['createdBy' => \Yii::$app->user->id]]);
            }
        }

        $models = $query->all();

        if (!empty($models)) {
            $models = static::buildTree($models);
        }

        return $models;
    }

    public static function getRelatedTo(){
        $models = Comments::find()->select('relatedTo')->distinct()->all();
        return ArrayHelper::map($models, 'relatedTo', 'relatedTo');
    }

    public static function getCountNew(){
        return Comments::find()->where(['status' => \app\components\Status::PENDING])->count();
    }

}