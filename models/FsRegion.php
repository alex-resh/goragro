<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fs_region".
 *
 * @property int $id
 * @property int $id_country
 * @property int $id_okrug
 * @property string $name
 */
class FsRegion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%fs_region}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_country', 'id_okrug', 'name'], 'required'],
            [['id_country', 'id_okrug'], 'integer'],
            [['name'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_country' => 'Id Country',
            'id_okrug' => 'Id Okrug',
            'name' => 'Name',
        ];
    }
}
