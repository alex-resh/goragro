<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property int $id
 * @property string|null $name
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }

    public function countNewsByCity(){
        return $this->hasMany(News::class, ['category_id' => 'id'])
            ->where(['status' => 1, 'city_id' => Yii::$app->city->city->id, 'type' => News::TYPE_NEWS])
            ->count();
    }
}
