<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "organisation_gallery".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $img
 * @property string|null $desc
 */
class OrganisationGallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation_gallery}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                //'instanceByName' => true,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/organisation_gallery/{id}',
                'url' => '@web/upload/organisation_gallery/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                    'news_thumb' => ['width' => 200, 'height' => 200, 'bg_color' => '000'],
                    'gallery_img' => ['width' => 179, 'height' => 155],
                ],
            ],
            [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'clone',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_id', 'clone_id'], 'integer'],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => 'default'],
            ['img', 'string', 'on' => 'clone'],
            ['desc', 'safe'],
            ['created_at', 'integer'],
            ['updated_at', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'img' => 'Img',
        ];
    }
}
