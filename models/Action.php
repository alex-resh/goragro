<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%action}}".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property string $name
 * @property string $description
 * @property float|null $old_price
 * @property float|null $new_price
 * @property float|null $discount
 * @property int|null $image_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property Organisation $organisation
 */
class Action extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_id', 'clone_id', 'image_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            ['description', 'string'],
            [['old_price', 'new_price', 'discount'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'name' => 'Name',
            'old_price' => 'Old Price',
            'new_price' => 'New Price',
            'discount' => 'Discount',
            'image_id' => 'Image ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getImage(){
        return $this->hasOne(Image::class, ['id' => 'image_id']);
    }

    public function getImageUrl($thumb = 'preview'){
        if($this->image){
            return $this->image->getThumbUploadUrl('img', $thumb);
        }
    }

    public function getOrganisation(){
        return $this->hasOne(Organisation::class, ['id' => 'organisation_id']);
    }

}
