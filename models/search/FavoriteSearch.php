<?php


namespace app\models\search;

use app\models\Favorite;
use yii\data\ActiveDataProvider;

class FavoriteSearch extends Favorite
{
    public function search($params)
    {
        $query = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}