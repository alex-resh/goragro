<?php


namespace app\models\search;


use app\models\Additionally;
use yii\data\ActiveDataProvider;

class AdditionallySearch extends Additionally
{
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}