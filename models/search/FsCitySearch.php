<?php


namespace app\models\search;


use app\models\FsCity;
use yii\data\ActiveDataProvider;

class FsCitySearch extends FsCity
{

    public function rules()
    {
        return [
            ['name', 'safe'],
            ['name_en', 'safe'],
            ['id_region', 'safe'],
        ];
    }

    public function search($params){
        $query = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        if($this->name != ''){
            $query->andWhere(['like', 'name', $this->name]);
        }

        if($this->id_region != ''){
            $query->andWhere(['id_region' => $this->id_region]);
        }

        return $dataProvider;
    }

}