<?php

namespace app\models\search;

use app\models\Page;
use yii\data\ActiveDataProvider;

class PageSearch extends Page
{
    public function search($params)
    {
        $query = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}