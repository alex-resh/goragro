<?php


namespace app\models\search;


use yii\data\ActiveDataProvider;
use app\models\MailingTemplate;

class MailingTemplateSearch extends MailingTemplate
{
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}