<?php


namespace app\models\search;

use app\models\News;
use app\models\NewsTag;
use app\models\TagsToNews;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;


class NewsSearch extends News
{
    public function search($params){
        $query = $this->find()->where(['type' => News::TYPE_NEWS]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'pagination' => [
//                'pageSize' => 2,
//            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }

    public function searchFront($params, $first5 = [], $type = self::TYPE_NEWS){

        $ids = [];
        foreach ($first5 as $news){
            $ids[] = $news->id;
        }

        $query = $this->find()
            ->where(['status' => 1])
            ->andWhere(['city_id' => \Yii::$app->city->city->id])
            ->andWhere(['not in', News::tableName().'.id', $ids])
            ->andWhere(['type' => $type])
            ->orderBy(['created_at' => SORT_DESC])
            ->offset(5);

        if(\Yii::$app->request->get('cat_id')){
            $query->andWhere(['category_id' => \Yii::$app->request->get('cat_id')]);
        }

        if(\Yii::$app->request->get('tag')){
            $tag_model = NewsTag::findOne(['tag' => \Yii::$app->request->get('tag')]);
            if($tag_model){
                $tag_model->updateCounters(['views_count' => 1]);
                $query->innerJoin(TagsToNews::tableName(), TagsToNews::tableName().'.news_id=' . News::tableName().'.id')
                    ->andWhere([TagsToNews::tableName().'.tag_id' => $tag_model->id]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
//                '' => 5
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}