<?php


namespace app\models\search;


use yii\data\ActiveDataProvider;
use app\models\AfficheCategory;

class AfficheCategorySearch extends AfficheCategory
{
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}