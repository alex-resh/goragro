<?php

namespace app\models\search;

use app\models\Sight;
use yii\data\ActiveDataProvider;

class SightSearch extends Sight
{
    public function search($params)
    {
        $query = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}