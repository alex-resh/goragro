<?php


namespace app\models\search;


use yii\data\ActiveDataProvider;
use app\models\Mailing;

class MailingSearch extends Mailing
{
    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider;
    }
}