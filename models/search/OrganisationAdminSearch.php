<?php


namespace app\models\search;


use app\models\Organisation;
use yii\data\ActiveDataProvider;

class OrganisationAdminSearch extends Organisation
{
    public function search($params)
    {
        $this->load($params);
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}