<?php

namespace app\models\search;

use app\models\Article;
use yii\data\ActiveDataProvider;

class AticleSearch extends Article
{
    public function search($params){
        $query = $this->find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}