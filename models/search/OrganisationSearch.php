<?php


namespace app\models\search;

use app\models\AdditionallyData;
use app\models\Agent;
use app\models\Organisation;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Json;

class OrganisationSearch extends Organisation
{

    public $category_id;
    public $region = [];
    public $site_availability;
    public $wt_filter;

    public function formName()
    {
        return 'os';
    }

    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            ['region', 'each', 'rule' => ['string']],
            [['wt_filter'], 'string'],
            [['site_availability'], 'boolean'],
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'region',
            'site_availability',
            'wt_filter',
        ]);
    }


    public function search($params)
    {
        $query = static::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }

    public function searchFront($params)
    {
        $query = static::find()
            ->joinWith('address');

        $formName = (new AdditionallyData())->formName();
        if (isset($params[$formName]) && $params[$formName] != '') {
            foreach ($params[$formName] as $form_id => $data) {
                $alias = 'data' . $form_id;
                $needJoin = true;
                foreach ($data as $key => $value) {
                    if ($value != '' && is_string($value)) {
                        //Если форма не джойнилась, дожйним и ставим $needJoin = false;
                        $needJoin && $query->joinWith(['additionallyDates ' . $alias])->where([$alias . '.form_id' => $form_id]) && $needJoin = false;
                        $ops = explode('~', $value);
                        if($ops && $ops[0] === ''){
                            $query->andWhere([$ops[1], new Expression("JSON_EXTRACT(`" .$alias . "`.`data`->>'$[0]', '$." . $key . "')*1"), $ops[2], $ops[3]]);
                        } else {
                            $query->andWhere(new Expression("JSON_CONTAINS(`" . $alias . "`.`data`->>'$[0]', '" . Json::encode($value) . "', '$." . $key . "')"));
                        }
                    }
                }
            }
        }

        if($this->site_availability || $this->site_availability == '0'){
            $query->andFilterWhere($this->site_availability ? ['and', ['!=', 'url', null], ['!=', 'url', '[]']] : ['url' => '[]']);
        }

        $query->andFilterWhere(['region' => $this->region]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->category_id){
            $query->joinWith('organisationCategories')
                ->andWhere(['category_id' => $this->category_id]);
        }

        if($this->wt_filter){
            $prefix = lcfirst(substr(date('D'),0, 2)) . '_';
            if($this->wt_filter == 'around'){
                $query->andWhere([
                    'or',
                    new Expression('`' . $prefix . 'start`=`' . $prefix . 'end`'),
                    [$prefix . 'start' => '0:00', $prefix . 'end' => '24:00'],
                ]);
            } elseif($this->wt_filter == 'now') {
                $now = date('H')*1;
                $start = new Expression('`' . $prefix . 'start`*1');
                $end = new Expression('`' . $prefix . 'end`*1');
                $query->andWhere([
                    'or',
                    [
                        'and',
                        [
                            '<',
                            $start,
                            $end,
                        ],
                        [
                            '<=',
                            $start,
                            $now
                        ],
                        [
                            '<',
                            $now,
                            $end,
                        ]
                    ],
                    [
                        'and',
                        [
                            '>=',
                            $start,
                            $end,
                        ],
                        [
                            'or',
                            [
                                '<=',
                                $start,
                                $now
                            ],
                            [
                                '<',
                                $now,
                                $end,
                            ]
                        ]
                    ],
                ]);
            }
            $query->joinWith('worktime');
        }

        $query->andWhere(['city_id' => Yii::$app->city->city->id]);
        $query->andWhere([Organisation::tableName().'.status' => Organisation::STATUS_PUBLISHED]);
//        echo $query->createCommand()->rawSql;
        return $dataProvider;
    }

    public function searchForProfile($params, $user_id = null)
    {
        $user_id = $user_id ? $user_id : Yii::$app->user->id;

        $query = Organisation::find()
            ->joinWith('agents')
            ->andWhere([Organisation::tableName() . '.user_id' => $user_id])
            ->orWhere([
                'and',
                [Agent::tableName() . '.user_id' => $user_id],
                [Agent::tableName() . '.status' => Agent::STATUS_ACTIVE]
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}