<?php


namespace app\models\search;


use app\models\Agent;
use yii\data\ActiveDataProvider;

class AgentSearch extends Agent
{


    public function rules()
    {
        return [
            [['status', 'phone', 'email', 'name'], 'safe']
        ];
    }

    public function search($params){
        $query = Agent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$this->validate()){
            return $dataProvider;
        }

        if($this->status){
            $query->andWhere(['status' => $this->status]);
        }
        return $dataProvider;

    }

    public function statusList()
    {
        return static::staticStatusList();
    }

}