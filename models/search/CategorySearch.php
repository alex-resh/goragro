<?php

namespace app\models\search;

use app\models\Category;
use yii\data\ActiveDataProvider;

class CategorySearch extends Category
{
    public function search($params){
        $query = $this->find()
        ->orderBy(['id' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        return $dataProvider;
    }
}