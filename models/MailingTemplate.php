<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%mailing_template}}".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $from
 * @property string|null $subject
 * @property string|null $body
 * @property int|null $html
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Mailing[] $mailings
 */
class MailingTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%mailing_template}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['html', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description', 'from', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'from' => 'From',
            'subject' => 'Subject',
            'body' => 'Body',
            'html' => 'Html',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Mailings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMailings()
    {
        return $this->hasMany(Mailing::className(), ['template_id' => 'id']);
    }
}
