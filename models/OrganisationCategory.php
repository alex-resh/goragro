<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organisation_category".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property int|null $category_id
 */
class OrganisationCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%organisation_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'category_id' => 'Category ID',
        ];
    }
}
