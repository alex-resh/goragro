<?php

namespace app\models;

use app\behaviors\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property string|null $class_name
 * @property string|null $title
 * @property string|null $description
 * @property string|null $img
 * @property int|null $price
 * @property int|null $discount
 * @property string|null $type
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    public function init()
    {
        parent::init();
        $this->class_name = static::class;
    }

    public static function find()
    {
        return parent::find()->where(['class_name' => static::class]);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            'up' => [
                'class' => UploadImageBehavior::class,
                'attribute' => 'img',
                'scenarios' => ['default'],
                'placeholder' => '@webroot/images/test.png',
                'path' => '@webroot/upload/product/{id}',
                'url' => '@web/upload/product/{id}',
                'defaultThumbs' => [
                    'thumb' => ['width' => 120, 'height' => 120],
                    'big_img' => ['width' => 524, 'height' => 460],
                    'small_img' => ['width' => 238, 'height' => 211 ],
                ],
            ],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            'clone',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['organisation_id', 'clone_id', 'price', 'discount', 'status', 'created_at', 'updated_at'], 'integer'],
            [['class_name', 'title', 'description', 'type'], 'string', 'max' => 255],
            ['img', 'image', 'extensions' => 'jpg, jpeg, gif, png', 'on' => 'default'],
            ['img', 'string', 'on' => 'clone'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'class_name' => 'Class Name',
            'title' => 'Название',
            'description' => 'Описание',
            'img' => 'Изображение',
            'price' => 'Цена',
            'discount' => 'Скидка',
            'type' => 'Тип',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function productTypeList()
    {
        return [
            Product::class => 'Товары',
            ProdMeny::class => 'Меню',
            ProdService::class => 'Услуги',
        ];
    }

    public static function categoryIds($class_name)
    {
        return ProductCategory::find()->select('category_id')->where(['product_class_name' => $class_name])->column();
    }

    public function getPlaceholder()
    {
        return 'Введите название товара...';
    }

    public function typeList()
    {
        return [];
    }
}
