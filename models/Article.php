<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $title
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $user_id
 * @property int|null $views_count
 * @property int|null $type
 * @property User $user
 */
class Article extends \yii\db\ActiveRecord
{
    const ARTICLE_TYPE_BLOG = 1;
    const ARTICLE_TYPE_RECIPE = 2;
    const ARTICLE_TYPE_HUMOR = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['created_at', 'updated_at', 'user_id', 'views_count', 'type'], 'integer'],
            [['title'], 'required'],
            ['type', 'in', 'range' => [self::ARTICLE_TYPE_BLOG,self::ARTICLE_TYPE_RECIPE,self::ARTICLE_TYPE_HUMOR]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'title' => 'Заголовок',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'user_id' => 'User ID',
            'views_count' => 'Колличество просмотров',
            'type' => 'Тип',
        ];
    }
    public function getUser(){
        return $this->hasOne(User::class,['id'=>'user_id']);
    }
}
