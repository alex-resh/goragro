<?php

namespace app\models;

use Exception;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%additionally_category}}".
 *
 * @property int $id
 * @property int|null $additionally_id
 * @property int|null $category_id
 * @property int|null $pos_a_in_c
 * @property int|null $pos_c_in_a
 *
 * @property Additionally $additionally
 * @property Category $category
 */
class AdditionallyCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally_category}}';
    }

    public function attributes()
    {
        try {
            if($this->additionally_id){
                return array_merge(parent::attributes(), $this->additionally->extraAttributes);
            }
        } catch (Exception $e) {
        }
        return parent::attributes();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['additionally_id', 'category_id', 'pos_a_in_c', 'pos_c_in_a'], 'integer'],
            [['additionally_id'], 'exist', 'skipOnError' => true, 'targetClass' => Additionally::class, 'targetAttribute' => ['additionally_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'additionally_id' => 'Additionally ID',
            'category_id' => 'Category ID',
            'pos_a_in_c' => 'Pos A In C',
            'pos_c_in_a' => 'Pos C In A',
        ];
    }

    /**
     * Gets query for [[Additionally]].
     *
     * @return ActiveQuery
     */
    public function getAdditionally()
    {
        return $this->hasOne(Additionally::class, ['id' => 'additionally_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }
}
