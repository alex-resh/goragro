<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additionally_value".
 *
 * @property int $id
 * @property int|null $entity_id
 * @property string|null $value
 */
class AdditionallyValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally_value}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'integer'],
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_id' => 'Entity ID',
            'value' => 'Значение',
        ];
    }
}
