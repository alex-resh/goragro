<?php


namespace app\models;


class ProdMeny extends Product
{
    public function getPlaceholder()
    {
        return 'Введите название блюда...';
    }

    public function typeList()
    {
        return [
            'Основное меню',
            'Первое блюдо',
            'Второе блюдо',
            'Закуска',
        ];
    }
}