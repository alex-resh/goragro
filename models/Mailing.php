<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%mailing}}".
 *
 * @property int $id
 * @property int|null $template_id
 * @property int|null $u_count
 * @property int|null $s_count
 * @property int|null $start
 * @property int|null $end
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property MailingTemplate $template
 */
class Mailing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%mailing}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_id', 'u_count', 's_count', 'start', 'end', 'status', 'created_at', 'updated_at'], 'integer'],
            [['template_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailingTemplate::className(), 'targetAttribute' => ['template_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_id' => 'Template ID',
            'u_count' => 'U Count',
            's_count' => 'S Count',
            'start' => 'Start',
            'end' => 'End',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Template]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(MailingTemplate::className(), ['id' => 'template_id']);
    }
}
