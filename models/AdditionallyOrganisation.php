<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additionally_organisation".
 *
 * @property int $id
 * @property int|null $organisation_id
 * @property int|null $entity_id
 */
class AdditionallyOrganisation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%additionally_organisation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organisation_id'], 'integer'],
            [['entity_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organisation_id' => 'Organisation ID',
            'entity_id' => 'Entity ID',
        ];
    }
}
