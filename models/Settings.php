<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property string|null $key
 * @property string|null $value
 */
class Settings extends \yii\db\ActiveRecord
{
    const EVENT_CHANGE = 'change_settings';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }

    public static function get($key, $default = null){
        $setting = Settings::findOne(['key' => $key]);
        if(!$setting){
            return $default;
        }
        return $setting->value;
    }

    public function set($key, $value = null)
    {
        $model = Settings::findOne(['key' => $key]);
        if(!$model){
            $model = new Settings([
                'key' => $key,
            ]);
        }
        $model->value = $value;
        $model->save();
    }
}
