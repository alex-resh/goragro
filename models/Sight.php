<?php

namespace app\models;

use app\components\GeocoderBehavior;
use app\modules\likes\traits\LikeTrait;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sight".
 *
 * @property int $id
 * @property int|null $city_id
 * @property int|null $user_id
 * @property int|null $views_count
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $title
 * @property string|null $text
 * @property SightGallery[] $gallery
 * @property FsCity $city
 * @property string $address
 * @property string $lat
 * @property string $lon
 */
class Sight extends \yii\db\ActiveRecord
{
    use LikeTrait;

    public $images;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sight}}';
    }

    public function behaviors()
    {
        return [

            'geocoder' => [
                'class' => GeocoderBehavior::class,
                'adress_field' => 'addressStr',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'views_count','user_id'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
            [['text'], 'string'],
            [['address', 'lat','lon'], 'string'],
            [['title'], 'string', 'max' => 255],
            ['images', 'each', 'rule' => ['image']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'address' => 'Адрес',
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){
            if(!$insert && $this->oldAttributes['address'] != $this->address) {
                $this->lat = false;
                $this->lon = false;
            }
            return true;
        }
    }

    public function getGallery(){
        return $this->hasMany(SightGallery::class, ['sight_id' => 'id']);
    }

    public function getCity(){
        return $this->hasOne(FsCity::class, ['id' => 'city_id']);
    }

    public function getAddressStr(){
        return $this->city->name . ', ' .$this->address;
    }

    public function getUser(){
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getMainPhoto(){
        if(isset($this->gallery[0])){
            return $this->gallery[0]->getThumbUploadUrl('img', 'big_img');
        } else {
            return '/images/test.png';
        }
    }

    public function getCommentsCount(){
        $count = Comments::find()->where(['relatedTo' => self::class . ':' . $this->id])->count();
        return $count;
    }

    public function getPrev(){
        $prev = Sight::find()->andWhere(['city_id' => Yii::$app->city->city->id])
            ->andWhere(['<', 'id', $this->id])
            ->one();
        return $prev;
    }

    public function getNext(){
        $next = Sight::find()->andWhere(['city_id' => Yii::$app->city->city->id])
            ->andWhere(['>', 'id', $this->id])
            ->one();
        return $next;
    }

}
