<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $title
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $seo_url
 * @property string|null $seo_title
 * @property string|null $seo_keywords
 * @property string|null $seo_desc
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class
            ]
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['seo_desc','seo_url', 'seo_title', 'seo_keywords'], 'string', 'max' => 255],
            [['title'], 'required'],
            ['seo_url', 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'title' => 'Заголовок',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'seo_url' => 'Seo Url',
            'seo_title' => 'Seo Title',
            'seo_keywords' => 'Seo Keywords',
            'seo_desc' => 'Seo Desc',
        ];
    }
}
