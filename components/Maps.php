<?php


namespace app\components;

use linslin\yii2\curl;
use yii\base\Component;

class Maps extends Component
{
    public function getMaps($city)
    {
        $result = $this->getinfo($city);
        return $result;
    }

    public function getinfo($city)
    {
        $result = \Yii::$app->cache->get('maps_' . $city);
        if (empty($result)) {
            $curl = new curl\Curl();
            $response = $curl->get('https://geocode-maps.yandex.ru/1.x/', array_merge)([
                'apikey' => '4e405466-de50-447a-9017-87f7c9741a9b',
                'geocode' => '134.854, -25.828',
                'format' => 'json',
            ]);
            \Yii::$app->cache->set('maps_'.$city, $response,60*60*24);
        }
        return \Yii::$app->cache->get('maps_'.$city);

    }
}