<?php


namespace app\components;


use app\models\AfficheCategory;
use yii\base\Component;

class Affiche extends Component
{
    public function adminMenuItem()
    {
        $items = [];
        $categories = AfficheCategory::find()->select('name')->indexBy('id')->column();
        foreach ($categories as $id => $category) {
            $items[] = ['label' => $category, 'url' => ['/admin/affiche', 'id' => $id]];
        }
        $items[] = ['label' => 'Категории', 'url' => ['/admin/affiche/category']];

        return [
            'label' => 'Афиша',
            'items' => $items,
        ];
    }
}