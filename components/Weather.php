<?php

namespace app\components;

use app\models\FsCity;
use app\models\Settings;
use linslin\yii2\curl;
use yii\base\Component;
use yii\caching\DummyCache;
use yii\helpers\Json;

Class Weather extends Component
{
    public $condition = [
        'clear' => 'Ясно',
        'partly-cloudy' => 'Малооблачно',
        'cloudy' => 'Облачно с прояснениями',
        'overcast' => 'Пасмурно',
        'partly-cloudy-and-light-rain' => 'Небольшой дождь',
        'partly-cloudy-and-rain' => 'Дождь',
        'overcast-and-rain' => 'Сильный дождь',
        'overcast-thunderstorms-with-rain' => 'Сильный дождь, гроза',
        'cloudy-and-light-rain' => 'Небольшой дождь',
        'overcast-and-light-rain' => 'Небольшой дождь',
        'cloudy-and-rain' => 'Дождь',
        'overcast-and-wet-snow' => 'Дождь со снегом',
        'partly-cloudy-and-light-snow' => 'Небольшой снег',
        'partly-cloudy-and-snow' => 'Снег',
        'overcast-and-snow' => 'Снегопад',
        'cloudy-and-light-snow' => 'Небольшой снег',
        'overcast-and-light-snow' => 'Небольшой снег',
        'cloudy-and-snow' => 'Небольшой снег'
    ];
    public $wind_dir = [
        'nw' => 'СЗ',
        'n' => 'С',
        'ne' => 'СВ',
        'e' => ' В',
        'se' => 'ЮВ',
        's' => 'Ю',
        'sw' => 'ЮЗ',
        'w' => 'З',
        'c' => 'Ш',
    ];

    public function init()
    {

    }

    public function getWind($key){
        return $this->wind_dir[$key];
    }

    public function getCondition($key){
        return $this->condition[$key];
    }

    public function getPrognos($city)
    {
        try{
            $result = json_decode($this->getRequest($city));
            return $result->forecasts;
        } catch (\Exception $e){
            return 'simferopol';
        }
    }

    public function getWeather($city, $day = 0)
    {
        $result = json_decode($this->getRequest($city));
        if(isset($result->forecasts)){
            return $result->forecasts[$day]->parts->day->temp_max;
        } else {
            return 'Нет данных';
        }
    }

    public function getWater($city, $day = 0){
        $result = json_decode($this->getRequest($city));
        return $result->forecasts[$day]->parts->day->temp_water;
    }

    public function getSun($city){
        $result = json_decode($this->getRequest($city));
        return ['start' =>$result->forecasts[0]->sunrise, 'end' => $result->forecasts[0]->sunset ];
    }
    

    public function getWeatherToday($city){
        $result = json_decode($this->getRequest($city));
        return $result->forecasts[0]->parts->day;
    }

    public function getPrognosToday($city)
    {
        return $this->getPrognos($city)[0];
    }

    private function getRequest($city){
        $result = \Yii::$app->cache->get('weather_'.$city);
        if (empty($result) || $result == false){
            $model = FsCity::findOne(['name_en' => $city]);
            $curl = new curl\Curl();
            $response = $curl->setHeaders(
                [
                    'X-Yandex-API-Key' => Settings::get('ya_weather')
                ]
            )
                ->get('https://api.weather.yandex.ru/v1/forecast?lat='. $model->lat . '&lon=' . $model->lon);
            $tmp_response =Json::decode($response, false);
            if(is_object($tmp_response) && isset($tmp_response->forecasts)){
                foreach ((array)$tmp_response->forecasts as $forecast) {
                    foreach ($forecast->parts as $part) {
                        if(isset($part->temp_min)) {
                            $part->temp_min = $part->temp_min > 0 ? '+' . $part->temp_min : $part->temp_min;
                        }
                        if(isset($part->temp_max)) {
                            $part->temp_max = $part->temp_max > 0 ? '+' . $part->temp_max : $part->temp_max;
                        }
                        if(isset($part->temp_avg)) {
                            $part->temp_avg = $part->temp_avg > 0 ? '+' . $part->temp_avg : $part->temp_avg;
                        }
                        if(isset($part->temp_water)) {
                            $part->temp_water = $part->temp_water > 0 ? '+' . $part->temp_water : $part->temp_water;
                        }
                    }
                }
                $response = Json::encode($tmp_response);
                \Yii::$app->cache->set('weather_'.$city, $response,60*60);
            }
        }
        return \Yii::$app->cache->get('weather_'.$city);
    }

    public function getIconClass($day, $part)
    {
        if(isset($day->parts)){
            $key = $part . '-' . $day->parts->{$part}->condition;
//        echo $key;
            switch ($key){
                case 'morning-clear':
                case 'day-clear':
                    $class = 'day-sunny';
                    break;
                case 'evening-clear':
                case 'night-clear':
                    $class = 'night-clear';
                    break;
                case 'morning-partly-cloudy':
                case 'day-partly-cloudy':
                    $class = 'day-sunny-overcast';
                    break;
                case 'evening-partly-cloudy':
                case 'night-partly-cloudy':
                    $class = 'night-alt-partly-cloudy';
                    break;
                case 'morning-cloudy':
                case 'day-cloudy':
                    $class = 'day-cloudy';
                    break;
                case 'evening-cloudy':
                case 'night-cloudy':
                    $class = 'night-alt-cloudy';
                    break;
                case 'morning-overcast':
                case 'day-overcast':
                case 'evening-overcast':
                case 'night-overcast':
                    $class = 'cloudy';
                    break;
                case 'morning-partly-cloudy-and-light-rain':
                case 'day-partly-cloudy-and-light-rain':
                    $class = 'day-rain';
                    break;
                case 'evening-partly-cloudy-and-light-rain':
                case 'night-partly-cloudy-and-light-rain':
                    $class = 'night-alt-rain';
                    break;
                case 'morning-partly-cloudy-and-rain':
                case 'day-partly-cloudy-and-rain':
                case 'evening-partly-cloudy-and-rain':
                case 'night-partly-cloudy-and-rain':
                    $class = 'rain';
                    break;
                case 'morning-overcast-and-rain':
                case 'day-overcast-and-rain':
                case 'evening-overcast-and-rain':
                case 'night-overcast-and-rain':
                    $class = 'showers';
                    break;
                case 'morning-overcast-thunderstorms-with-rain':
                case 'day-overcast-thunderstorms-with-rain':
                    $class = 'day-thunderstorm';
                    break;
                case 'evening-overcast-thunderstorms-with-rain':
                case 'night-overcast-thunderstorms-with-rain':
                    $class = 'night-thunderstorm';
                    break;
                case 'morning-cloudy-and-light-rain':
                case 'day-cloudy-and-light-rain':
                    $class = 'day-rain';
                    break;
                case 'evening-cloudy-and-light-rain':
                case 'night-cloudy-and-light-rain':
                    $class = 'night-alt-rain';
                    break;
                case 'morning-overcast-and-light-rain':
                case 'day-overcast-and-light-rain':
                case 'evening-overcast-and-light-rain':
                case 'night-overcast-and-light-rain':
                case 'morning-cloudy-and-rain':
                case 'day-cloudy-and-rain':
                case 'evening-cloudy-and-rain':
                case 'night-cloudy-and-rain':
                    $class = 'rain';
                    break;
                case 'morning-overcast-and-wet-snow':
                case 'day-overcast-and-wet-snow':
                    $class = 'day-sleet';
                    break;
                case 'evening-overcast-and-wet-snow':
                case 'night-overcast-and-wet-snow':
                    $class = 'night-alt-sleet';
                    break;
                case 'morning-partly-cloudy-and-light-snow':
                case 'day-partly-cloudy-and-light-snow':
                    $class = 'day-snow';
                    break;
                case 'evening-partly-cloudy-and-light-snow':
                case 'night-partly-cloudy-and-light-snow':
                    $class = 'night-alt-snow';
                    break;
                case 'morning-partly-cloudy-and-snow':
                case 'day-partly-cloudy-and-snow':
                    $class = 'day-snow';
                    break;
                case 'evening-partly-cloudy-and-snow':
                case 'night-partly-cloudy-and-snow':
                    $class = 'night-alt-snow';
                    break;
                case 'morning-overcast-and-snow':
                case 'day-overcast-and-snow':
                case 'evening-overcast-and-snow':
                case 'night-overcast-and-snow':
                    $class = 'snow';
                    break;
                case 'morning-cloudy-and-light-snow':
                case 'day-cloudy-and-light-snow':
                    $class = 'day-snow';
                    break;
                case 'evening-cloudy-and-light-snow':
                case 'night-cloudy-and-light-snow':
                    $class = 'night-alt-snow';
                    break;
                case 'morning-overcast-and-light-snow':
                case 'day-overcast-and-light-snow':
                case 'evening-overcast-and-light-snow':
                case 'night-overcast-and-light-snow':
                    $class = 'snow';
                    break;
                case 'morning-cloudy-and-snow':
                case 'day-cloudy-and-snow':
                    $class = 'day-snow';
                    break;
                case 'evening-cloudy-and-snow':
                case 'night-cloudy-and-snow':
                    $class = 'night-alt-snow';
                    break;
                default:
                    $class = 'na';
                    break;
            }
            return 'wi wi-' . $class;
        } else {
            return 'wi wi-na';
        }

    }
}