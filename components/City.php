<?php


namespace app\components;


use app\models\FsCity;
use yii\base\Component;

/**
 * use Yii::$app->city->city->id
 */
class City extends Component
{

    public $city;

    public function init()
    {
        $name_en = \Yii::$app->request->get('city');
        if($name_en == ''){
            $name_en = 'simferopol';
        }
        $this->city = FsCity::findOne(['name_en' => $name_en]);
    }

}