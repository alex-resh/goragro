<?php


namespace app\components;


class Comment extends \yii2mod\comments\widgets\Comment
{

    public function run()
    {
        $commentClass = $this->getModule()->commentModelClass;
        $commentModel = \Yii::createObject([
            'class' => $commentClass,
            'entity' => $this->entity,
            'entityId' => $this->entityId,
        ]);
        $commentDataProvider = $this->getCommentDataProvider($commentClass);

        return $this->render($this->commentView, [
            'commentDataProvider' => $commentDataProvider,
            'commentModel' => $commentModel,
            'maxLevel' => $this->maxLevel,
            'encryptedEntity' => $this->encryptedEntity,
            'pjaxContainerId' => $this->pjaxContainerId,
            'formId' => $this->formId,
            'listViewConfig' => $this->listViewConfig,
            'commentWrapperId' => $this->commentWrapperId,
            'relatedTo' => $this->relatedTo,
        ]);
    }

}