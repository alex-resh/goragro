<?php


namespace app\components;


use yii\base\Behavior;
use yii\db\ActiveRecord;

class GeocoderBehavior extends Behavior
{

    public $adress_field;
    public $lat_field = 'lat';
    public $lon_field = 'lon';
    public $district_field = false;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
        ];
    }

    public function afterFind(){
        if(($this->owner->{$this->lat_field} == '' || $this->owner->{$this->lon_field} == '') && $this->owner->{$this->adress_field} != ''){
            $coords = \Yii::$app->geocoder->getCoords($this->owner->{$this->adress_field});
            $this->owner->{$this->lat_field} = $coords['lat'];
            $this->owner->{$this->lon_field} = $coords['lon'];
            $this->owner->save(false);
        }
        if($this->district_field && $this->owner->{$this->district_field} == ''){
            if(($this->owner->{$this->lat_field} != '' && $this->owner->{$this->lon_field} != '')){
                $district = \Yii::$app->geocoder->getDistrict($this->owner->{$this->lat_field}, $this->owner->{$this->lon_field});
                $this->owner->{$this->district_field} = $district;
                $this->owner->save(false);
            }
        }
    }

}