<?php


namespace app\components;


use app\models\User;
use yii\base\Component;

class UserLastAction extends Component
{

    public function init()
    {
        if(!\Yii::$app->user->isGuest){
            User::updateAll(['last_action_time' => time()], ['id' => \Yii::$app->user->id]);
        }
        parent::init();
    }

}