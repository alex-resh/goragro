<?php


namespace app\components;


class Status extends \yii2mod\moderation\enums\Status
{

    public static $list = [
        self::PENDING => 'Новый',
        self::APPROVED => 'Одобрен',
        self::REJECTED => 'Отклонен',
        self::POSTPONED => 'Отложен',
    ];

}