<?php


namespace app\components;


use app\models\Settings;
use yii\base\Component;

class Geocoder extends Component
{

    public $api_key;

    public function init()
    {
        parent::init();
        $this->api_key = Settings::get('ya_geocoder');
    }

    public function getCoords($address){
        $url = 'https://geocode-maps.yandex.ru/1.x/?apikey='. $this->api_key.'&format=json&geocode=' . urlencode($address);
        try {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
            $res = json_decode($content);
            if(isset($res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos)){
                list($lon, $lat) = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
                return [
                    'lon' => $lon,
                    'lat' => $lat
                ];
            }
        } catch (\Exception $e){
            return false;
        }
    }

    public function getDistrict($lat, $lon){
        $url = 'https://geocode-maps.yandex.ru/1.x/?apikey='. $this->api_key.'&format=json&kind=district&geocode=' . $lon . ',' . $lat;
        try {
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $content = file_get_contents($url, false, stream_context_create($arrContextOptions));
            $res = json_decode($content);
            if(isset($res->response->GeoObjectCollection->featureMember[0]->GeoObject->name)){
                return $res->response->GeoObjectCollection->featureMember[0]->GeoObject->name;
            }
        } catch (\Exception $e){
            return false;
        }
    }

}