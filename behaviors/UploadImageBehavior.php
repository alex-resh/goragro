<?php


namespace app\behaviors;

use app\events\SettingsEvent;
use app\models\Settings;
use yii\db\ActiveRecord;

class UploadImageBehavior extends \mohorev\file\UploadImageBehavior
{
    public $defaultThumbs = [];
    protected $_fieldNames = [];

    public function events()
    {
        $this->afterInit();
        return parent::events();
    }

    public function afterInit()
    {
        $class = $this->owner::className();
        foreach ($this->defaultThumbs as $key => $thumb) {
            $this->thumbs[$key] = $thumb;
            $field = $class . ':' . $key . ':width';
            $this->thumbs[$key]['width'] = $this->_fieldNames[$field] = Settings::get($field, $thumb['width'] ?? '');
            $field = $class . ':' . $key . ':height';
            $this->thumbs[$key]['height'] = $this->_fieldNames[$field] = Settings::get($field, $thumb['height'] ?? '');
        }
    }

    public function getFieldNames()
    {
        return $this->_fieldNames;
    }

    public function changeThumb($types)
    {
        $path = $this->getUploadPath($this->attribute);
        if(is_string($types) && is_file($path)){
            $types = [$types];
        }
        foreach ($types as $type) {
            $thumbPath = $this->getThumbUploadPath($this->attribute, $type);
            static::generateImageThumb($this->thumbs[$type], $path, $thumbPath);
        }
    }

    public function afterChangeSettings(SettingsEvent $event)
    {
        $changed = array_intersect_key($event->attributes, $this->getFieldNames());
        if(count($changed)){
            $class = $this->owner::className();
            $models = $class::find()->where(['!=', $this->attribute, ''])->all();
            $thumbs = array_unique(array_map(function ($item){
                return preg_replace('`(^.*?\:|\:.*?$)`', '', $item);
            }, array_keys($changed)));
            foreach ($models as $model) {
                $model->changeThumb($thumbs);
            }
        }
    }

    public function afterUpload()
    {
        parent::afterUpload();
    }
}