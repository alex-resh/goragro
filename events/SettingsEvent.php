<?php


namespace app\events;


use yii\base\Event;

/**
 * Class SettingsEvent
 * @package app\events
 *
 * @property array $attributes
 */
class SettingsEvent extends Event
{
    public $attributes;
}