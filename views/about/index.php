<div class="wysiwyg-content terms">

    <div class="wysiwyg wysiwyg--bg">
        <h3>Пользовательское соглашение</h3>
        <p>Garago.ru - интернет-ресурс созданный для общения пользователей, обмена информацией, мнениями,
            взаимопомощи. Мы ни когда не платили и не платим за отзывы, так как считаем, что отзыв должен
            быть
            написан по собственному желанию, без участия денежной или любой материальной мативации. Отзыв
            может
            быть: нейтральный, положительный или отрицательный. Ни при каких условиях, отзыв или
            комментарий, не
            должен быть написан за какое либо вознаграждение, пусть это: подарок, чья то просьба или
            денежная
            мотивация. Отзывы за вознаграждение являются заказными. Заказные отзывы не могут нести ни какой
            конструктивной критики, заказные отзывы ничто иное как ложь, выдумка, сказка, небылица и т.п.,
            заказные отзывы вводят людей в заблуждение, создают иллюзию и не дают сделать правильный выбор,
            а
            так же наносят ущерб деловой репутации.</p>

        <div class="img">
            <img src="\images/garago.jpg" alt="">
            <div class="img__ico">
                <img src="\images/logo.png" alt="">
            </div>
        </div>

        <p class="mt">В интернет-пространстве большинство сайтов отзовиков платит пользователю за добавления
            отзыва на сайт, что в свою очередь делает их абсолютно бессмысленными. Пользователь готовый
            писать
            отзывы за денежное вознаграждение может придумать сотни историй о том как он обращался в ту или
            иную
            организацию, фирму, компанию, что на самом деле будет являться ложью на 90%. Такие заказные
            отзывы
            вводят в заблуждение и могут навредить компании или пользователю. Будьте бдительны и
            внимательны!!!</p>

        <p>Мы, ни при каких условиях не пытаемся извлечь выгоду от написания положительных или отрицательных
            отзывов. Мнение администрации сайта Garago.ru может не совпадать с мнением пользователей нашего
            интернет-ресурса. Администрация сайта не несет ответственности за материалы опубликованные
            пользователями нашего сайта. Администрация Garago.ru регулярно проверяет опубликованные на сайте
            материалы, отзывы, но только по мере наших возможностей. Все поступившие (вменяемые) жалобы мы
            рассматриваем и даем ответ в течении 7 рабочих дней. </p>

        <blockquote class="post__blockquote">
            Кроме того часть отдыхающих начали самостоятельно организовывать поездки в Турцию: число
            бронирований авиабилетов в Анталью выросло на 20 процентов, констатирует «Коммерсантъ».
        </blockquote>

        <h4>Общие положения</h4>

        <p>Данный сайт создан для общения пользователей, обмена информацией, мнениями, взаимопомощи.</p>
        <p>Все сообщения, оставленные пользователями, отражают исключительно их мнение, которое может
            расходиться с мнением администрации сайта (далее Администрация).</p>

        <p>Администрация не дает гарантий, что информация, содержащаяся в сообщениях пользователей, правдива
            и
            отражает действительность, а также не несет ответственности за какой бы то ни было ущерб,
            причиненный вследствие использования информации, размещенной на данном сайте.</p>

        <p>Сайт является собственностью владельца домена, что дает ему возможность устанавливать данные
            Правила
            и следить за их исполнением, как самостоятельно, так и через уполномоченных модераторов.</p>

        <p>Администрация вправе без объяснения причин удалять сообщения и закрывать темы, которые на ее
            взгляд
            противоречат данным правилам или содержат информацию, нарушающую действующее законодательство
            РФ, а
            так же информацию, которая может быть использована с целью его нарушения. </p>

    </div><!-- /.wysiwyg -->

</div><!-- /.wysiwyg-content -->

<div class="post-navigate indent">

    <div class="soc-sety post-navigate__soc">
        <a href="<?php echo \yii\helpers\Url::to('https://vk.com/goragoru') ?>" class="soc-sety__item vk">
            <svg class="ico ico-vk">
                <use xlink:href="\images/svg/symbol/sprite.svg#vk"></use>
            </svg>
        </a>
        <a href="#" class="soc-sety__item tw">
            <svg class="ico ico-tw">
                <use xlink:href="\images/svg/symbol/sprite.svg#tw"></use>
            </svg>
        </a>
        <a href="#" class="soc-sety__item fc">
            <svg class="ico ico-fc">
                <use xlink:href="\images/svg/symbol/sprite.svg#fc"></use>
            </svg>
        </a>
        <a href="#" class="soc-sety__item ok">
            <svg class="ico ico-ok">
                <use xlink:href="\images/svg/symbol/sprite.svg#ok"></use>
            </svg>
        </a>
        <a href="#" class="soc-sety__item mail">
            <svg class="ico ico-mail">
                <use xlink:href="\images/svg/symbol/sprite.svg#mail"></use>
            </svg>
        </a>
        <a href="#" class="soc-sety__item google">
            <svg class="ico ico-google">
                <use xlink:href="\images/svg/symbol/sprite.svg#google"></use>
            </svg>
        </a>
    </div><!-- /.soc-sety -->

    <div class="post-nav">
        <div class="post-nav__arrows">
            <a href="#" class="post-nav__item prev">
                <svg class="ico ico-arrow-big">
                    <use xlink:href="\images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </a>
            <a href="#" class="post-nav__item next">
                <svg class="ico ico-arrow-big">
                    <use xlink:href="\images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </a>
        </div>
        <div class="post-nav__link">
            <span class="post-nav__label">далее: </span>
            <a href="#" class="post-nav__link-title">Мост в неизвестность...</a>
        </div>
    </div>

</div>

<div class="comment callback">
    <h3 class="comment__title title-inside">Форма обратной связи</h3>
    <div class="comment__add">
        <svg width="32px" height="32px">
            <path fill-rule="evenodd" fill="rgb(181, 181, 181)"
                  d="M32.000,19.000 L32.000,32.000 L-0.000,32.000 L-0.000,14.000 L-0.000,13.000 L4.000,10.750 L4.000,-0.000 L28.000,-0.000 L28.000,10.750 L32.000,13.000 L32.000,14.000 L32.000,19.000 L32.000,19.000 ZM31.007,31.010 L31.007,19.000 L31.000,19.000 L31.000,14.562 L16.000,23.000 L1.000,14.562 L0.993,31.010 L31.007,31.010 ZM28.000,11.750 L28.000,11.891 L28.000,11.891 L28.000,15.000 L27.000,15.000 L27.000,1.000 L5.000,1.000 L5.000,15.000 L4.000,15.000 L4.000,11.750 L1.000,13.437 L1.000,13.562 L16.000,22.000 L31.111,13.500 L28.000,11.750 ZM9.000,13.000 L23.000,13.000 L23.000,14.000 L9.000,14.000 L9.000,13.000 ZM9.000,9.000 L23.000,9.000 L23.000,10.000 L9.000,10.000 L9.000,9.000 ZM9.000,5.000 L23.000,5.000 L23.000,6.000 L9.000,6.000 L9.000,5.000 Z"/>
        </svg>
    </div>
    <form class="form form-comment form-callback" id="form-callback" name="form-callback"
          action="#" method="post">

        <div class="form__section">
            <div class="form__row">
                <div class="form__row-column">
                    <input type="text" id="form-comment_name" class="form__input"
                           name="form-comment_name"
                           placeholder="Имя" required/>
                </div>
                <div class="form__row-column">
                    <input type="email" id="form-comment_phone" class="form__input"
                           name="form-comment_phone"
                           placeholder="Email" required/>
                </div>
            </div>
            <div class="form__row">
                <div class="form-wysiwyg">
                                <textarea id="form-comment_message" class="form__textarea" name="form-comment_message"
                                          placeholder="Текст комментария"></textarea>
                </div>
            </div>

            <div class="form__select select">
                <div class="select__header">
                    <div class="select__label">
                        Выбрать тему сообщения
                    </div>
                    <div class="select__ico">
                        <svg class="ico ico-arrow-bottom">
                            <use xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom"></use>
                        </svg>
                    </div>
                </div>
                <div class="select__list">
                    <div class="select__item">Я пользователь</div>
                    <div class="select__item">Я администратор</div>
                    <div class="select__item">Я гость</div>
                    <div class="select__item">Я менеджер</div>
                </div>
            </div>
        </div><!-- /.form-callback__section -->

        <div class="form__bottom">

            <b>Ключевые слова</b></br></br>

            <div class="form__row form__row--checkeds">
                <div class="form__row-column">

                    <div class="radio">
                        <input class="radio__input" id="radio20" type="radio" name="radio">
                        <label class="radio__wrapper" for="radio20">
                            <span class="radio__custom"></span>
                            <span class="radio__label">
                                                    <span class="radio__text">Интересует стоимость</span>
                                                </span>
                        </label>
                    </div>
                    <div class="radio">
                        <input class="radio__input" id="radio21" type="radio" name="radio"
                               checked>
                        <label class="radio__wrapper" for="radio21">
                            <span class="radio__custom"></span>
                            <span class="radio__label">
                                                    <span class="radio__text">Интересует вакансия</span>
                                                </span>
                        </label>
                    </div>
                    <div class="radio">
                        <input class="radio__input" id="radio22" type="radio" name="radio">
                        <label class="radio__wrapper" for="radio22">
                            <span class="radio__custom"></span>
                            <span class="radio__label">
                                                    <span class="radio__text">Интересует журналистика</span>
                                                </span>
                        </label>
                    </div>
                    <div class="radio">
                        <input class="radio__input" id="radio23" type="radio" name="radio">
                        <label class="radio__wrapper" for="radio23">
                            <span class="radio__custom"></span>
                            <span class="radio__label">
                                                    <span class="radio__text">Интересует статья на сайте</span>
                                                </span>
                        </label>

                    </div>
                </div><!-- /.form__row-column -->

                <div class="form__row-column">
                    <div class="checkbox">
                        <input class="checkbox__input" id="check20" type="checkbox"
                               name="checkbox">
                        <label class="checkbox__wrapper" for="check20">
                            <span class="checkbox__custom"></span>
                            <span class="checkbox__label">
                                                    <span class="checkbox__text">Интересует стоимость</span>
                                                </span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <input class="checkbox__input" id="check21" type="checkbox"
                               name="checkbox"
                               checked>
                        <label class="checkbox__wrapper" for="check21">
                            <span class="checkbox__custom"></span>
                            <span class="checkbox__label">
                                                    <span class="checkbox__text">Интересует вакансия</span>
                                                </span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <input class="checkbox__input" id="check22" type="checkbox"
                               name="checkbox">
                        <label class="checkbox__wrapper" for="check22">
                            <span class="checkbox__custom"></span>
                            <span class="checkbox__label">
                                                    <span class="checkbox__text">Интересует журналистика</span>
                                                </span>
                        </label>
                    </div>
                    <div class="checkbox">
                        <input class="checkbox__input" id="check23" type="checkbox"
                               name="checkbox">
                        <label class="checkbox__wrapper" for="check23">
                            <span class="checkbox__custom"></span>
                            <span class="checkbox__label">
                                                    <span class="checkbox__text">Интересует статья на сайте</span>
                                                </span>
                        </label>
                    </div>

                </div><!-- /.form__row-column -->
            </div>

            <div class="form__row">
                <input type="submit" class="btn btn-accent" value="Отправить сообщение">
            </div>
        </div><!-- /.form-callback__bottom -->

    </form>
</div>