<?php
/* @var $model \app\models\News */

use yii\helpers\Url;
use app\modules\likes\widgets\LikesWidget;
?>
<div class="post indent">
    <div class="post__rate">
        <?php $likes = LikesWidget::begin(['model' => $model]) ?>
        <div class="rate-like">
            <?php echo $likes->like(true) ?>
            <?php echo $likes->dislike() ?>
        </div>
        <?php LikesWidget::end() ?>
    </div><!-- /.post__rate -->

    <div class="post__content">
        <a href="<?php echo Url::to(['blog/view', 'id' => $model->id]) ?>" class="post__title"><?php echo $model->title ?></a>

        <div class="post__actions post-actions">
            <div class="post-actions__left">
                <a href="#" class="post-actions__comments">
                                            <span class="post-actions__comments-ico">
                                                <svg class="ico ico-comments"><use
                                                        xlink:href="images/svg/symbol/sprite.svg#comments"></use></svg>
                                            </span>
                    <span class="post-actions__comments-count"><?php echo $model->getCommentsCount() ?></span>
                </a>
                <a href="#" class="post-actions__view">
                                            <span class="post-actions__view-ico">
                                                <svg class="ico ico-view"><use
                                                        xlink:href="images/svg/symbol/sprite.svg#view"></use></svg>
                                            </span>
                    <span class="post-actions__view-count"><?php echo $model->views_count ?></span>
                </a>
            </div>
            <div class="post-actions__right">
                <a href="#" class="favorites">
                    <i class="favorites-ico">
                        <svg class="ico ico-heart">
                            <use xlink:href="images/svg/symbol/sprite.svg#heart"></use>
                        </svg>
                    </i>
                    <span class="favorites-text">В избранное</span>
                </a>
            </div>
        </div><!-- /.post__actions -->

        <div class="post__img">
            <img src="<?php echo $model->getThumbUploadUrl('img', 'preview') ?>" alt="">
        </div>

        <div class="post__info">
            <p class="post__text"><?php echo mb_substr($model->text, 0, 1000) ?></p>
        </div>

        <div class="post__tag tag">
            <?php foreach ($model->getTagsList() as $tag): ?>
                <a href="<?php echo Url::toRoute(['/blog', 'tag' => $tag]) ?>" class="tag__item"><?php echo $tag; ?></a>
            <?php endforeach; ?>
        </div>
    </div><!-- /.post__content -->

    <div class="post__autor autor">

        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__avatar">
            <div class="autor__avatar-wrapper">
                <img src="<?php echo $model->user->profile->getThumbUploadUrl('img', 'small') ?>" alt="">
            </div>
        </a>
        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__name"><?php echo $model->user->profile->name?$model->user->profile->name:$model->user->username ?></a>
        <div class="autor__date">
            <div class="autor__date-day"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y') ?></div>
            <div class="autor__date-time"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:H:i') ?></div>
            <?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $model->user_id): ?>
            <br />
            <a href="<?php echo Url::to(['blog/update', 'id' => $model->id]); ?>" class="consent-company__btn consent-company__sumbit">Редактировать</a>
            <?php endif; ?>
        </div>
    </div>
</div><!-- /.post -->
