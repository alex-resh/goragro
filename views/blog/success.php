<?php
$this->title = 'Запись в блоге создана и отправлена на модерацию';
?>
<div class="public-company bg form-request">
    <div class="indent-lg vertical pt0">
        <h2><?php echo $this->title ?></h2>
        <p>После модерации запись появится на сайте</p>
    </div>
</div>