<?php
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \app\models\search\NewsSearch */

use yii\widgets\ListView;

?>
<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'posts-list'],
    'itemView' => '_item',
    'layout' => "{items}\n{pager}"
]);