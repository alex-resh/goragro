<?php
/**
 * Created by PhpStorm.
 * User: kozhevnikov
 * Date: 10/10/2017
 * Time: 13:59
 */

use onmotion\survey\models\SurveyUserAnswer;
use yii\helpers\Html;


/** @var $question \onmotion\survey\models\SurveyQuestion */
/** @var $form \yii\widgets\ActiveForm */

$userAnswers = $question->userAnswers;
$userAnswer = !empty(current($userAnswers)) ? current($userAnswers) : (new SurveyUserAnswer());
$radioList = [];

$totalVotesCount = $question->getTotalUserAnswersCount();

foreach ($question->answers as $i => $answer) {

    $radioList[$answer->survey_answer_id] = $answer->survey_answer_name;
    if ($answer->survey_answer_show_descr) {
        $radioList[$answer->survey_answer_id] .= $answer->survey_answer_descr;
    }
    //$radioList[$answer->survey_answer_id] .= '</div>';
}

if(!$question->getUserAnswers()->all()) {
    echo $form->field($userAnswer, "[$question->survey_question_id]survey_user_answer_value",
        ['template' => "{label}{input}\n{hint}\n{error}"])
        ->radioList($radioList, ['encode' => false, 'class' => 'poll__options',
            'item' => function ($index, $label, $name, $checked, $value) {
                $id = uniqid('radio-') . $value;
                return '<div class=\'poll__options-item \'>' . Html::radio($name, $checked, ['value' => $value, 'class' => 'radio__input', 'id' => $id]) . "<label class='radio__wrapper' for='$id'><span class=\"radio__custom\"></span>
                    <span class=\"radio__label\">
                                            <span class=\"radio__text\">" . $label . '</span>
                                        </span></label></div>';
            }
        ])->label(false);
} else {
    foreach ($question->answers as $i => $answer) {
        $count = $answer->getTotalUserAnswersCount();
        try {
            $percent = ($count / $totalVotesCount) * 100;
        }catch (\Exception $e){
            $percent = 0;
        }
        echo $answer->survey_answer_name;
        echo \yii\bootstrap\Progress::widget([
            'id' => 'progress-' . $answer->survey_answer_id,
            'percent' => $percent,
            'label' => $count,
            'barOptions' => ['class' => 'progress-bar-info init']
        ]);
        $this->registerJs(<<<JS
$(document).ready(function(e) {
    setTimeout(function() {
       $('.progress-bar').each(function(i, el) {
    if ($(el).hasClass('init')){
        $(el).removeClass('init');
    }
  })
    }, 1000);
});
JS
        );
    }
}
?>
