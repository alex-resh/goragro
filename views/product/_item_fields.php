<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Product
 */

use yii\helpers\Html; ?>
<div class="el added-info__element--added">
    <div class="added-info__wrapper">
        <?= Html::errorSummary($model, ['class' => 'alert alert-danger']);?>
        <div class="added-info__header">
            <?= Html::activeTextInput($model, '[' . $model->id . ']title', ['class' => 'added-info__title', 'placeholder' => $model->placeholder]);?>
            <button class="added-info__button js-added-info-btn"></button>
        </div><!-- /.added-info__header -->
        <div class="added-info__section">
            <div class="added-info__img js-added-load-img">
                <span>Загрузить фото</span>
                <?= Html::img($model->getThumbUploadUrl('img'), ['class' => 'added-info__picture product-img', 'style' => ['opacity' => 1]]);?>
                <?= Html::activeFileInput($model, '[' . $model->id . ']img', ['class' => 'added-info__input-file product-img-file']);?>
            </div>
            <div class="added-info__body">
                <?= Html::activeTextarea($model, '[' . $model->id . ']description', ['class' => 'added-info__descr form__input form__field', 'placeholder' => 'Описание']);?>

                <div class="added-info__bottom">
                    <div class="added-info__bottom-left">
                        <?= Html::activeTextInput($model, '[' . $model->id . ']discount', ['class' => 'added-info__input form__input form__field', 'placeholder' => 'Процент скидки']);?>

                        <?= Html::activeTextInput($model, '[' . $model->id . ']price', ['class' => 'added-info__input form__input form__field', 'placeholder' => 'Цена, руб.']);?>
                    </div>
                    <div class="added-info__bottom-right">
                        <div class="form__select select added-info__select">
                            <?= Html::activeDropDownList($model, '[' . $model->id . ']type', $model->typeList(), ['class' => 'form-control']);?>
                        </div><!-- /.select -->
                    </div>
                </div>
            </div>
        </div><!-- /.added-info__section -->
    </div>
</div>
