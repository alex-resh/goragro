<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\OrganisationAdress
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

$phoneUrl = Url::to(['/organisation/phone-field']);
$this->registerJs(<<<JS
    $('body').on('click', '.js-add-field', function() {
        var t = $(this);
        if(t.closest('.info-line').is('.info-line--added')){
            t.closest('.info-line').remove();
        } else {
            var line = t.closest('.info-line').find('select').select2('destroy').end().clone();
            var counter = (t.data('id-counter') ? t.data('id-counter') : t.closest('.info-line').siblings('.info-line').length) + 1;
            t.data('id-counter', counter);
            var id = t.closest('.info-line').find('select').attr('id').replace(/-0+$/, '-' + counter);
            var name = t.closest('.info-line').find('select').attr('name').replace(/\[0\]$/, '[' + counter + ']');
            var inputId = t.closest('.info-line').find(':text').attr('id').replace(/-0+$/, '-' + counter);
            var inputName = t.closest('.info-line').find(':text').attr('name').replace(/\[0\]$/, '[' + counter + ']');
            $(':text', line).attr('id', inputId).attr('name', inputName).val('');
            if($(':text', line).is('[data-plugin-inputmask]')){
                $(':text', line).inputmask('+7(999)-999-99-99');                
            }
            $('select', line).attr('id', id).attr('name', name).val($('option', line).eq(0).val());
            line.addClass('info-line--added');
            t.closest('.form__row').append(line);
            t.closest('.info-line').find('select').add('#' + id).each(function() {
                var id = $(this).attr('id');
                jQuery.when(jQuery('#' + id).select2(select2_e753e56a)).done(initS2Loading(id,'s2options_98abd855'));
            });            
        }
    });
JS
);
?>
<div class="address">
    <div class="form__row form__row--data">
        <div class="form__column">
            <?= Html::activeLabel($model, 'postal_code', ['class' => 'form__label']);?>
            <?= Html::activeTextInput($model, 'postal_code', ['class' => 'form__input form__field form__field--index']);?>
            <?= Html::error($model, 'post_code', ['class' => 'text-danger']);?>
        </div>
        <div class="form__column">
            <?= Html::activeLabel($model, 'street', ['class' => 'form__label']);?>
            <?= Html::activeTextInput($model, 'street', ['class' => 'form__input form__field form__field--street']);?>
            <?= Html::error($model, 'street', ['class' => 'text-danger']);?>
        </div>
        <div class="form__column">
            <?= Html::activeLabel($model, 'house', ['class' => 'form__label']);?>
            <?= Html::activeTextInput($model, 'house', ['class' => 'form__input form__field form__field']);?>
            <?= Html::error($model, 'house', ['class' => 'text-danger']);?>
        </div>
        <div class="form__column">
            <?= Html::activeLabel($model, 'flat', ['class' => 'form__label']);?>
            <?= Html::activeTextInput($model, 'flat', ['class' => 'form__input form__field form__field']);?>
            <?= Html::error($model, 'flat', ['class' => 'text-danger']);?>
        </div>
        <div class="form__column">
            <?= Html::activeLabel($model, 'flooar', ['class' => 'form__label']);?>
            <?= Html::activeTextInput($model, 'flooar', ['class' => 'form__input form__field form__field']);?>
            <?= Html::error($model, 'flooar', ['class' => 'text-danger']);?>
        </div>
    </div>
    <div class="form__row">
        <div class="form__row-col w100">
            <label class="form__label">
                                                <span class="form__label-title">
                                                    Телефон
                                                    <span class="form__star">*</span>
                                                </span>
            </label>
            <div class="form-question">
                <a href="#" class="form__question js-form-question">?</a>
                <div class="help-warning">
                    Это позволит вам получить именно то, что вам нужно!
                </div>
            </div>
        </div>
        <?= Html::error($model, 'phones[0]', ['class' => 'text-danger']);?>
        <div class="info-line">
            <?= MaskedInput::widget([
                'model' => $model,
                'attribute' => 'phones[0]',
                'mask' => '+7(999)-999-99-99',
                'options' => [
                    'class' => 'info-line__input form__input form__field',
                ],
            ]);?>
            <div class="info-line__select form__select select">

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'phoneType[0]',
                    'data' => $model->phoneTypeList(),
                    'size' => Select2::LARGE,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'class' => 'info-line__select form__select select js-phone',
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                    ],
                ]);?>
            </div>
            <button type="button" class="info-line__btn js-info-line-add js-add-field">
                <span></span>
            </button>
        </div>
        <?php foreach ((array)$model->phones as $i => $phone) : ?>
        <?php if($i) : ?>
                <div class="info-line info-line--added">
                    <?= MaskedInput::widget([
                        'model' => $model,
                        'attribute' => 'phones[' . $i . ']',
                        'mask' => '+7(999)-999-99-99',
                        'options' => [
                            'class' => 'info-line__input form__input form__field',
                        ],
                    ]);?>
                    <div class="info-line__select form__select select">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'phoneType[' . $i . ']',
                            'data' => $model->phoneTypeList(),
                            'size' => Select2::LARGE,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'class' => 'info-line__input form__input form__field'
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                        ]);?>
                    </div>
                    <button type="button" class="info-line__btn js-info-line-add js-add-field">
                        <span></span>
                    </button>
                </div>
        <?php endif;?>
        <?php endforeach;?>
    </div>
    <div class="form__row">
        <div class="form__row-col w100">
            <label class="form__label">
                                                <span class="form__label-title">
                                                    Электронная почта
                                                    <span class="form__star">*</span>
                                                </span>
            </label>
            <div class="form-question">
                <a href="#" class="form__question js-form-question">?</a>
                <div class="help-warning">
                    Это позволит вам получить именно то, что вам нужно!
                </div>
            </div>
        </div>
        <?= Html::error($model, 'emails[0]', ['class' => 'text-danger']);?>
        <div class="info-line">
            <?= Html::activeTextInput($model, 'emails[0]', ['class' => 'info-line__input form__input form__field']);?>
            <div class="info-line__select form__select select">

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'emailType[0]',
                    'data' => $model->emailTypeList(),
                    'size' => Select2::LARGE,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'class' => 'info-line__select form__select select js-email',
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                    ],
                ]);?>
            </div>
            <button type="button" class="info-line__btn js-info-line-add js-add-field">
                <span></span>
            </button>
        </div>
        <?php foreach ((array)$model->emails as $i => $email) : ?>
            <?php if($i) : ?>
                <div class="info-line info-line--added">
                    <?= Html::activeTextInput($model, 'emails[' . $i . ']', ['class' => 'info-line__input form__input form__field']);?>
                    <div class="info-line__select form__select select">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'emailType[' . $i . ']',
                            'data' => $model->emailTypeList(),
                            'size' => Select2::LARGE,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'class' => 'info-line__select form__select select js-email',
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                        ]);?>
                    </div>
                    <button type="button" class="info-line__btn js-info-line-add js-add-field">
                        <span></span>
                    </button>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    </div>

    <div class="form__row">
        <div class="form__row-col w100">
            <label class="form__label">
                                                <span class="form__label-title">
                                                    Адрес сайта
                                                    <span class="form__star">*</span>
                                                </span>
            </label>
            <div class="form-question">
                <a href="#" class="form__question js-form-question">?</a>
                <div class="help-warning">
                    Это позволит вам получить именно то, что вам нужно!
                </div>
            </div>
        </div>

        <?= Html::error($model, 'url[0]', ['class' => 'text-danger']);?>
        <div class="info-line">
            <?= Html::activeTextInput($model, 'url[0]', ['class' => 'info-line__input form__input form__field']);?>
            <div class="info-line__select form__select select">

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'urlType[0]',
                    'data' => $model->urlTypeList(),
                    'size' => Select2::LARGE,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'class' => 'info-line__select form__select select js-url',
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                    ],
                ]);?>
            </div>
            <button type="button" class="info-line__btn js-info-line-add js-add-field">
                <span></span>
            </button>
        </div>
        <?php foreach ((array)$model->url as $i => $url) : ?>
            <?php if($i) : ?>
                <div class="info-line info-line--added">
                    <?= Html::activeTextInput($model, 'url[' . $i . ']', ['class' => 'info-line__input form__input form__field']);?>
                    <div class="info-line__select form__select select">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'urlType[' . $i . ']',
                            'data' => $model->urlTypeList(),
                            'size' => Select2::LARGE,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'class' => 'info-line__select form__select select js-url',
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                        ]);?>
                    </div>
                    <button type="button" class="info-line__btn js-info-line-add js-add-field">
                        <span></span>
                    </button>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    </div><!-- /.form__row -->
    <div class="form__row">
        <div class="form__row-col w100">
            <label class="form__label">
                                                <span class="form__label-title">
                                                    Социальные сети
                                                    <span class="form__star">*</span>
                                                </span>
            </label>
            <div class="form-question">
                <a href="#" class="form__question js-form-question">?</a>
                <div class="help-warning">
                    Это позволит вам получить именно то, что вам нужно!
                </div>
            </div>
        </div>
        <?= Html::error($model, 'social[0]', ['class' => 'text-danger']);?>
        <div class="info-line">
            <?= Html::activeTextInput($model, 'social[0]', ['class' => 'info-line__input form__input form__field']);?>
            <div class="info-line__select form__select select">

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'socialType[0]',
                    'data' => $model->socialTypeList(),
                    'size' => Select2::LARGE,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'options' => [
                        'class' => 'info-line__select form__select select js-social',
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                    ],
                ]);?>
            </div>
            <button type="button" class="info-line__btn js-info-line-add js-add-field">
                <span></span>
            </button>
        </div>
        <?php foreach ((array)$model->social as $i => $social) : ?>
            <?php if($i) : ?>
                <div class="info-line info-line--added">
                    <?= Html::activeTextInput($model, 'social[' . $i . ']', ['class' => 'info-line__input form__input form__field']);?>
                    <div class="info-line__select form__select select">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'socialType[' . $i . ']',
                            'data' => $model->socialTypeList(),
                            'size' => Select2::LARGE,
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => [
                                'class' => 'info-line__select form__select select js-social',
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                        ]);?>
                    </div>
                    <button type="button" class="info-line__btn js-info-line-add js-add-field">
                        <span></span>
                    </button>
                </div>
            <?php endif;?>
        <?php endforeach;?>
    </div><!-- /.form__row -->
</div>

