<?php
/* @var $model \app\models\Organisation */
?>
<div class="event"> 

    <div class="event__info"> 
        <p class="event__title"> 
            <?php echo $model->name; ?>
            </p> 
        <div class="event__rate"> 
            <div class="rate"> 
                <i class="ico ico-star rate__ico"> 
                    <svg class="ico ico-star"> 
                        <use xlink:href="/images/svg/symbol/sprite.svg#star"></use> 
                        </svg> 
                    </i> 
                <i class="ico ico-star rate__ico"> 
                    <svg class="ico ico-star"> 
                        <use xlink:href="/images/svg/symbol/sprite.svg#star"></use> 
                        </svg> 
                    </i> 
                <i class="ico ico-star rate__ico"> 
                    <svg class="ico ico-star"> 
                        <use xlink:href="/images/svg/symbol/sprite.svg#star"></use> 
                        </svg> 
                    </i> 
                <i class="ico ico-star rate__ico"> 
                    <svg class="ico ico-star"> 
                        <use xlink:href="/images/svg/symbol/sprite.svg#star"></use> 
                        </svg> 
                    </i> 
                <i class="ico ico-star ico-star--empty rate__ico"> 
                    <svg class="ico ico-star"> 
                        <use xlink:href="/images/svg/symbol/sprite.svg#star"></use> 
                        </svg> 
                    </i> 
                </div> 
            <div class="event__rate-text">5 оценок</div> 
            </div> 
        <div class="event__descr"> 
            <p class="event__text"><?php echo $model->address->getToString() ?></p>
            <p class="event__text"><?php echo $model->address->phones[0] ?>, пн-вс: <?php echo $model->worktime->data[0]['workStart'] ?>-<?php echo $model->worktime->data[0]['workEnd'] ?></p>
            </div> 
        </div> 

    <div class="event__right"> 
        <div class="event__img"> 
            <img src="<?php echo $model->getThumbUploadUrl('img') ?>" alt="">
            </div> 
        <a href="#" class="event__link"><?php echo count($model->gallery) ?> фото</a>
        </div> 

       </div> 
</div>