<?php

/**
 * @var $this \yii\web\View
 * @var $models \app\models\Additionally[]
 * @var $needRemove array
 */

use app\assets\AdditionallyFieldAsset;
use app\models\AdditionallyData;
use yii\helpers\Json;

AdditionallyFieldAsset::register($this);
if($needRemove){
    $str = implode(', ', $needRemove);
    $this->registerJs(<<<JS
    $.fn.removeAdditionally([$str]);
JS
);
}
?>
<?php foreach ($models as $model) : ?>
    <?php
        $data = Json::decode($model->data);
        $dataModel = new AdditionallyData([
            'form_id' => $model->id,
        ]);
        $dataModel->loadDefaultValues();
    ?>
    <div class="additional-info" data-form_id="<?= $model->id;?>">
        <?php foreach ($data['rows'] as $row) : ?>
            <div class="additional-info__row">
                <?php foreach ($row['columns'] as $column) : ?>
                    <div class="additional-info__column">
                        <?php foreach ($column['elements'] as $element) : ?>
                            <div class="additional-info__section js-field">
                                <?php if(isset($element['label']) && $element['el'] != 'radio' && $element['el'] != 'checkBox') :?>
                                    <h5 class="additional__title"><?= $element['label'];?></h5>
                                <?php endif;?>
                                <?= $model->field($element, $dataModel);?>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endforeach;?>
    </div>
<?php endforeach;?>
