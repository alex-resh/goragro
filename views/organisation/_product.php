<?php

/**
 * @var $this \yii\web\View
 * @var $organisation_id int
 * @var $prod_data array
 */

use app\models\Product;
use yii\helpers\Url;
use app\assets\ProductFieldAsset;

ProductFieldAsset::register($this);
?>
<div class="js-product" data-url="<?= Url::to(['/product']);?>" data-id="<?= $organisation_id;?>" data-counter="-1">
<?php foreach ($prod_data as $class => $models) : ?>
    <div class="added-info__item">
        <h3 class="section-subtitle">
            <?= Product::productTypeList()[$class]; ?>
            <button class="added-info__button js-added-info-btn" title="Добавить" data-class="<?= $class;?>"></button>
        </h3>
        <?php foreach ($models as $model) {
            echo $this->render('../product/_item_fields', [
                'model' => $model,
            ]);
        } ?>
    </div>
<?php endforeach;?>
</div>


