<?php

/**
 * @var $this \yii\web\View
 * @var $organisationForm \app\models\forms\OrganisationForm
 * @var $formModel \app\models\Additionally
 * @var $model AdditionallyData
 * @var $additionallyData mixed
 */

use app\models\AdditionallyData;
use app\models\forms\ElForm;
use yii\bootstrap\Html;
use yii\helpers\Json;
use app\assets\AdditionallyFieldAsset;

AdditionallyFieldAsset::register($this);
if(!$additionallyData){
    $this->registerJs(<<<JS
        $('.js-category').trigger('change');
JS
);
}
?>
<?php foreach ($additionallyData as $i => $item) : ?>
    <?php
        $dataModel = new AdditionallyData(['form_id' => $i]);
        foreach ($item as $key => $value) {
            $dataModel->{$key} = $value;
        }
        $model = $dataModel->form;
        $data = Json::decode($model->data);
    ?>
    <div class="additional-info" data-form_id="<?= $model->id;?>">
        <h4><?= $model->title;?></h4>
        <?php foreach ($data['rows'] as $row) : ?>
            <div class="additional-info__row">
                <?php foreach ($row['columns'] as $column) : ?>
                    <div class="additional-info__column">
                        <?php foreach ($column['elements'] as $element) : ?>
                            <div class="additional-info__section js-field">
                                <?php if(isset($element['label']) && $element['el'] != 'radio' && $element['el'] != 'checkBox') :?>
                                    <h5 class="additional__title"><?= $element['label'];?></h5>
                                <?php endif;?>
                                <?= ElForm::field($element, $dataModel);?>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endforeach;?>
    </div>
<?php endforeach;?>