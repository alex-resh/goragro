<?php
/* @var $this \yii\web\View */
/* @var $form \yii\widgets\ActiveForm */
/* @var $image \app\models\Image */
/* @var $action_models \app\models\Action[] */

use yii\helpers\Html;
use yii\helpers\Url;

$field_name = Html::getInputName($image, 'img');
$field_new = Html::getInputName($image, 'new');
$uploadUrl = Url::to(['/organisation/image-upload']);
$this->registerJs(<<<JS
    var updateId = false;

    $('body').on('change', '.added-info__input-file', function(){
        
            var file = $(this)[0].files[0];
            var wrapper = $(this).parents('.added-info__element');
            
            var image_id = $(wrapper).find('.added-info__image-id').val();
            
            var fd = new FormData();
            fd.append('$field_name',file);
            fd.append('id',image_id);
            fd.append('$field_new',1);

            // if(updateId){
            //     var id = $(this).parents('.form-gallery__item-wrap').data('id');
            //     fd.append('id', updateId);
            //     var obj = $('.form-gallery__item-wrap[data-id='+updateId+']');
            // }
            
            // fd.append('$field_name',file);
            
            $.ajax({
                url: '$uploadUrl',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0){
                        
                        $(wrapper).find('.added-info__picture').attr('src', response.thumb);
                        $(wrapper).find('.added-info__image-id').val(response.id);
                        $(wrapper).find('.added-info__picture').css('opacity', 1);
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        
    });
    
    // Для моделей без id проставляем отрицательные id
    $('.js-actions').on('changeCounter', function(){
        let counter = $(this).data('counter')*1;
        console.log(counter);
        $('.js-actions .added-info__wrapper').each(function(){
            $(this).find('input[name], textarea[name]').each(function(){
                let name = $(this).attr('name').replace(/\[\](.+)/, '[' + counter + ']$1');
                $(this).attr('name', name);
                console.log(name);
            });
            counter --;
        });
        $(this).data('counter', counter);
    });
    
    $('.js-actions.added-info__item').on('addElement', function(){
        let counter = $(this).data('counter');
        $(this).find('.added-info__element').eq(-1).find('[name]').each(function(){
            let name = $(this).attr('name').replace(/(^.+?)\[\d+\]/, '$1[' + counter + ']');
            $(this).attr('name', name);
            console.log($(this).attr('name')); 
        });
        $(this).data('counter', counter -1);
    });
    
    $('.js-actions').trigger('changeCounter');
JS
);

?>
<div class="added-info__item js-actions" data-counter="-1">
    <h3 class="section-subtitle">Акции</h3>
    <?php foreach ($action_models as $index => $model): ?>
    <?php if($index > 0): ?>
    <div class='added-info__element added-info__element--added'>
    <?php endif; ?>
    <div class="added-info__element">
        <div class="added-info__wrapper">
            <div class="added-info__header">
                <?php echo Html::activeTextInput($model, '[' . $model->id . ']name', [
                    'class' => 'added-info__title',
                    'placeholder' => 'Введите название акции...'
                ]) ?>
                <button type="button"
                        class="added-info__button js-added-info-btn <?php echo $index == 0?'js-added-info-btn-add':''; ?>" data-count="<?php echo count($action_models) ?>"></button>
            </div><!-- /.added-info__header -->

            <div class="added-info__section">
                <div class="added-info__img js-added-load-img">
                    <span clas="no-loaded">Загрузить фото</span>
                    <img src="<?php echo $model->getImageUrl() ?>" class="added-info__picture" alt="" style="opacity: 1">
                    <?php echo Html::activeHiddenInput($model, '[' . $model->id . ']image_id', ['class' => 'added-info__image-id']) ?>
                    <input type="file" class="added-info__input-file"/>
                </div>
                <div class="added-info__body">
                    <?php echo Html::activeTextarea($model, '[' . $model->id . ']description', [
                            'class' => 'added-info__descr form__input form__field',
                            'placeholder' => 'Описание'
                    ]) ?>

                    <div class="added-info__bottom">
                        <div class="added-info__bottom-left">
                            <?php echo Html::activeTextInput($model, '[' . $model->id . ']discount', ['class' => 'added-info__input form__input form__field', 'placeholder' => 'Процент скидки']); ?>
                            <?php echo Html::activeTextInput($model, '[' . $model->id . ']old_price', ['class' => 'added-info__input form__input form__field', 'placeholder' => 'Цена, руб.']); ?>
                        </div>
                        <div class="added-info__bottom-right">
                            <?php echo Html::activeTextInput($model, '[' . $model->id . ']new_price', ['class' => 'added-info__input form__input form__field', 'placeholder' => 'Цена, руб.']); ?>
                        </div>
                    </div>
                </div>
            </div><!-- /.added-info__section -->
        </div><!-- /.added-info__wrapper -->
    </div><!-- /.added-info__element -->
        <?php if($index > 0): ?>
            </div>
            <?php endif; ?>
    <?php endforeach; ?>
