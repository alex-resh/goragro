<?php
/* @var $model \app\models\forms\AgentForm */
/* @var $form \yii\widgets\ActiveForm */
/* @var $this \yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

$this->registerJs(<<<JS
$('body').on('beforeSubmit','#form-question', function(e){
    var form = $(this);

    var formData = form.serialize();

    $.ajax({

        url: form.attr("action"),

        type: form.attr("method"),

        data: formData,

        success: function (data) {
            form.html(data);
        },

        error: function () {

            alert("Something went wrong");

        }

    });

}).on('submit', function(e){

    e.preventDefault();

});
JS
);

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-question',
    'options' => [
        'class' => 'form-pup white-popup-block mfp-hide',
    ],
    'method' => 'post',
    'action' => Url::to(['organisation/ask']),
]);

$field_options = [
    'template' =>"{input}\n{error}",
    'options' => [
        'class' => 'form__row'
    ],
    'inputOptions' => [
        'class' => 'form__input',
    ],
];

?>
    <h2 class="form__title title-inside">Задать вопрос</h2>
<?php echo $form->field($model, 'name', $field_options)->textInput(['placeholder' => 'Ваше имя']) ?>
<?php echo $form->field($model, 'email', $field_options)->textInput(['placeholder' => 'Email']) ?>
<?php echo $form->field($model, 'text', $field_options)->textarea(['placeholder' => 'Ваш вопрос', 'class' => 'form__input form__textarea']) ?>
<?php echo $form->field($model, 'recapcha')->widget(ReCaptcha2::class, [
    'siteKey' => \app\models\Settings::get('recaptcha_site_key')
])->label(false) ?>
<div class="form__row">
    <?php echo Html::submitInput('Отправить вопрос', ['class' => 'btn btn-accent']) ?>
</div>
<?php ActiveForm::end(); ?>
