<?php
/* @var $model \app\models\forms\AgentForm */
/* @var $form \yii\widgets\ActiveForm */
/* @var $this \yii\web\View */
/* @var $organisation \app\models\Organisation */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

$this->registerJs(<<<JS
$('body').on('beforeSubmit','#form-agent', function(e){
    var form = $(this);

    var formData = form.serialize();

    $.ajax({

        url: form.attr("action"),

        type: form.attr("method"),

        data: formData,

        success: function (data) {
            form.html(data);
        },

        error: function () {

            alert("Something went wrong");

        }

    });

}).on('submit', function(e){

    e.preventDefault();

});
JS
);

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-agent',
    'options' => [
        'class' => 'form-pup white-popup-block mfp-hide',
    ],
    'method' => 'post',
    'action' => Url::to(['organisation/agent']),
]);

$field_options = [
    'template' =>"{input}\n{error}",
    'options' => [
        'class' => 'form__row'
    ],
    'inputOptions' => [
        'class' => 'form__input',
    ],
];

?>
<h2 class="form__title title-inside">Я представитель</h2>
<?php if(!$agent = $organisation->getAgent()): ?>
    <p class="form__text text">Укажите ваши контактные данные и свяжемся с вами.</p>
<?php echo $form->field($model, 'name', $field_options)->textInput(['placeholder' => 'Ваше имя']) ?>
<?php echo $form->field($model, 'phone', $field_options)->widget(MaskedInput::class, [
    'mask' => '+7(999)-999-99-99',
    'options' => [
            'placeholder' => 'Телефон'
    ]
]) ?>
<?php echo $form->field($model, 'email', $field_options)->textInput(['placeholder' => 'Email']) ?>
<?php echo $form->field($model, 'recapcha')->widget(ReCaptcha2::class, [
    'siteKey' => \app\models\Settings::get('recaptcha_site_key')
])->label(false) ?>
<?php echo Html::activeHiddenInput($model, 'organisation_id', ['value' => $organisation->id]) ?>
<div class="form__row">
    <?php echo Html::submitInput('Отправить заявку', ['class' => 'btn btn-accent']) ?>
</div>
<?php else: ?>
    <?php if($agent->status == \app\models\Agent::STATUS_NEW): ?>
        <p class="form__text text">Ваша заявка на модерации.</p>
    <?php else: ?>
        <p class="form__text text">Вы уже представитель.</p>
    <?php endif; ?>
<?php endif; ?>
<?php ActiveForm::end(); ?>
