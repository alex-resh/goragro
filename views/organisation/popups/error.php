<?php
/* @var $model \app\models\forms\AgentForm */
/* @var $form \yii\widgets\ActiveForm */
/* @var $this \yii\web\View */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

$this->registerJs(<<<JS
$('body').on('beforeSubmit','#form-error', function(e){
    var form = $(this);

    var formData = form.serialize();

    $.ajax({

        url: form.attr("action"),

        type: form.attr("method"),

        data: formData,

        success: function (data) {
            form.html(data);
        },

        error: function () {

            alert("Something went wrong");

        }

    });

}).on('submit', function(e){

    e.preventDefault();

});
JS
);

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-error',
    'options' => [
        'class' => 'form-pup white-popup-block mfp-hide',
    ],
    'method' => 'post',
    'action' => Url::to(['organisation/error']),
]);

$field_options = [
    'template' =>"{input}\n{error}",
    'options' => [
        'class' => 'form__row'
    ],
    'inputOptions' => [
        'class' => 'form__input',
    ],
];

?>
<h2 class="form__title title-inside">Ошибка в тексте</h2>
<p class="form__text text">Укажите причины, по которым вы считаете описание объекта некорректными.</p>
<div class="form__row">

    <div class="checkbox">
        <?php echo Html::activeCheckbox($model, 'contacts', ['class' => 'checkbox__input', 'label' => false, 'id' => 'check30']) ?>
        <label class="checkbox__wrapper" for="check30">
            <span class="checkbox__custom"></span><!-- кругляшок с ::before при checked-->
            <span class="checkbox__label">
                        <span class="checkbox__text">Неверная контактная информация</span>
                    </span>
        </label>
    </div>

    <div class="checkbox">
        <?php echo Html::activeCheckbox($model, 'category', ['class' => 'checkbox__input', 'label' => false, 'id' => 'check31']) ?>
        <label class="checkbox__wrapper" for="check31">
            <span class="checkbox__custom"></span><!-- кругляшок с ::before при checked-->
            <span class="checkbox__label">
                        <span class="checkbox__text">Объект не соответствует рубрике</span>
                    </span>
        </label>
    </div>

    <div class="checkbox">
        <?php echo Html::activeCheckbox($model, 'another', ['class' => 'checkbox__input', 'label' => false, 'id' => 'check32']) ?>
        <label class="checkbox__wrapper" for="check32">
            <span class="checkbox__custom"></span><!-- кругляшок с ::before при checked-->
            <span class="checkbox__label">
                        <span class="checkbox__text">Другое</span>
                    </span>
        </label>
    </div>
</div>
<?php echo $form->field($model, 'recapcha')->widget(ReCaptcha2::class, [
    'siteKey' => \app\models\Settings::get('recaptcha_site_key')
])->label(false) ?>
<div class="form__row">
    <?php echo Html::submitInput('Отправить заявку', ['class' => 'btn btn-accent']) ?>
</div>
<?php ActiveForm::end(); ?>
