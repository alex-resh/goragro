<?php
/* @var $model \app\models\Action */
?>
<form class="form-pup form-pup--wide form-sale white-popup-block" id="form-sale" name="form-sale"
      action="#" method="post">

    <div class="form-sale__top" style="background-image: url('<?php echo $model->getImageUrl() ?>');">
        <span class="form-sale__label">Скидка - <?php echo $model->discount ?>%</span>
        <h2 class="form-sale__title"><?php echo $model->name ?></h2>
    </div><!-- /.form-sale__top -->
    <div class="form-sale__section">
        <div class="form-sale__info place-info">
            <p>
                <?php echo $model->description; ?>
            </p>
            <div class="form-sale__descr place-info__descr">
                <div class="place-info__line">
                        <span class="place-info__ico">
                            <svg class="ico ico-keyboard">
                                <use xlink:href="images/svg/symbol/sprite.svg#keyboard"></use>
                            </svg>
                        </span>
                    <p>
                        <a href="#" class="place-info__phone"><?php echo $model->organisation->address->phones[0] ?></a>
                    </p>
                </div>
                <div class="place-info__line">
                        <span class="place-info__ico">
                            <svg class="ico ico-location">
                                <use xlink:href="images/svg/symbol/sprite.svg#location"></use>
                            </svg>
                        </span>
                    <p class="place-info__text"><?php echo $model->organisation->address->getToString() ?><br>
                        пн-вс: 10:00-21:00
                    </p>

                </div>
            </div>
        </div>
    </div>
</form>