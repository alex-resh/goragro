<?php
/**
 * @var $this \yii\web\View
 * @var $gallery \app\models\OrganisationGallery
 * @var $gallery_models \app\models\OrganisationGallery[]
 */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

$field_name = Html::getInputName($gallery, 'img');
$field_org_name = Html::getInputName($gallery, 'organisation_id');
$id = $model->id;
$uploadUrl = Url::to(['/organisation/gallery-upload']);
$deleteUrl = Url::to(['/organisation/gallery-delete']);
$descUrl = Url::to(['/organisation/gallery-desc']);
$this->registerJs(<<<JS
    var updateId = false;

    $('body').on('change', '.js-file-gallery', function(){
        
        var files = $('.js-file-gallery')[0].files;
        for (file_id in files){
            var file = files[file_id];
            if(file.size == undefined) continue;
            
            console.log(file);
            
            var fd = new FormData();

            if(updateId){
                var id = $(this).parents('.form-gallery__item-wrap').data('id');
                fd.append('id', updateId);
                var obj = $('.form-gallery__item-wrap[data-id='+updateId+']');
            }
            
            fd.append('$field_name',file);
            fd.append('$field_org_name','0');
        
        
            $.ajax({
                url: '$uploadUrl',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0){
                        if(!updateId){
                            var gallery_item = $('.form-gallery__item-wrap:first').clone();
                            $(gallery_item).data('id', response.id);
                            $(gallery_item).attr('data-id', response.id);
                            $(gallery_item).find(".form-gallery__img img").attr("src",response.thumb);
                            $(gallery_item).find('.org-id').val(response.id);
                            $('.form-gallery__row').append(gallery_item);
                            gallery_item.show();
                        } else {
                            obj.find('.form-gallery__img img').attr('src', response.thumb);
                            updateId = false;
                            $('.js-file-gallery').attr('multiple', 'true');
                        }
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        }
    });

    $('body').on('click', '.js-gallery-menu__delete', function(e){
        e.preventDefault();
        var obj = $(this).parents('.form-gallery__item-wrap');
        var id = $(this).parents('.form-gallery__item-wrap').data('id');
        var fd = new FormData();
        fd.append('id', id);
        $.ajax({
            url: '$deleteUrl',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response){
                if(response != 0){
                    obj.remove();
                }else{
                    alert('file not uploaded');
                }
            },
        });
    });

    $('body').on('blur', '.org-desc', function(){
        var desc = $(this).val();
        var id = $(this).parents('.form-gallery__item-wrap').data('id');
        var fd = new FormData();
        fd.append('id', id);
        fd.append('desc', desc);
        $.ajax({
            url: '$descUrl',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
        });
    });
    
    $('body').on('click', '.js-gallery-menu__update', function(e){
        e.preventDefault();
        updateId = $(this).parents('.form-gallery__item-wrap').data('id');
        $(this).parents('.form-gallery__item-wrap').find('.js-gallery-button').click();
        $('.js-file-gallery').attr('multiple', false);
        $('.js-file-gallery').click();
    });
    
JS
);
?>
<div class="public-company__gallery vertical">
    <div class="indent-lg">
        <div class="form-gallery">
            <h3 class="section-subtitle">Фотогалерея</h3>

            <div class="form-gallery__wrapper">
                <div class="form-gallery__container">

                    <div class="form-gallery__row--video form__row">
                        <div class="form__row-col w100">
                            <label class="form__label">
                                                    <span class="form__label-title">
                                                        Вставить видео
                                                    </span>
                            </label>

                            <div class="form-question">
                                <a href="#" class="form__question js-form-question">?</a>
                                <div class="help-warning">
                                    Это позволит вам получить именно то, что вам нужно!
                                </div>
                            </div>
                        </div>

                        <input name="form__video-src" class="form__input form__field"
                               placeholder="http://video/videosrc"/>
                    </div>

                    <div class="form-gallery__row">
                        <div class="form-gallery__item-wrap" style="display: none">
                            <div class="form-gallery__item">
                                <div class="form-gallery__img">
                                    <img src="" alt="">
                                    <button type="button"
                                            class="form-gallery__button js-gallery-button">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                    <div class="form-gallery__menu gallery-menu">
                                        <a href="#" class="gallery-menu__link js-gallery-menu__update">Изменить фото</a>
                                        <a href="#" class="gallery-menu__link js-gallery-menu__delete">Удалить</a>

                                    </div>
                                </div>
                                <input type="hidden" name="gallery[id][]" class="org-id">
                                <input type="text" class="form-gallery__field"
                                       placeholder="Добавить описание..." name="gallery[desc][]"/>
                            </div>
                        </div><!-- /.form-gallery__item-wrap -->
                        <?php foreach ($gallery_models as $model): ?>
                            <div class="form-gallery__item-wrap" data-id="<?php echo $model->id; ?>">
                                <div class="form-gallery__item">
                                    <div class="form-gallery__img">
                                        <img src="<?php echo $model->getThumbUploadUrl('img', 'preview') ?>" alt="">
                                        <button type="button"
                                                class="form-gallery__button js-gallery-button">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </button>
                                        <div class="form-gallery__menu gallery-menu">
                                            <a href="#" class="gallery-menu__link js-gallery-menu__update">Изменить
                                                фото</a>
                                            <a href="#" class="gallery-menu__link js-gallery-menu__delete">Удалить</a>

                                        </div>
                                    </div>
                                    <input type="hidden" name="gallery[]id" class="org-id"
                                           value="<?php echo $model->id; ?>">
                                    <input type="text" class="form-gallery__field org-desc"
                                           placeholder="Добавить описание..." value="<?php echo $model->desc; ?>"/>
                                </div>
                            </div><!-- /.form-gallery__item-wrap -->
                        <?php endforeach; ?>
                    </div><!-- /.form-gallery__row -->
                </div><!-- /.form-gallery__container -->

            </div><!-- /.form-gallery__wrapper -->
        </div><!-- /.form-gallery -->
        <div class="form__row">

            <div class="file-upload">
                <label class="file-upload__btn">Выберите файл</label>
                <input type="file" name="file" class="js-file-gallery" multiple>
                <div class="form-upload__info">
                    <p class="file-upload__text"> ... или перетащите мышкой</p>
                    <p class="file-upload__formats">форматы jpg, gif, png</p>
                </div>
            </div>

        </div>
    </div>
</div>