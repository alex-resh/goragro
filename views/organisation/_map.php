<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Organisation */

$lat = $model->address->lat;
$lon = $model->address->lon;

$gallery_count = count($model->gallery);

$worktimeString = $model->worktime ? ', пн-вс: ' . $model->worktime->data[0]['workStart'] . '-' . $model->worktime->data[0]['workEnd'] : '';
$this->registerJs(
<<<JS
        ymaps.ready(init);
        var myMap,
            myPlacemark;
        function init() {
            myMap = new ymaps.Map("map-catalog", {
                center: [$lat, $lon],
                zoom: 17,
                controls: []
            });
            
            myPlacemark = new ymaps.Placemark(
                [$lat, $lon], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    '{$model->name}' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">{$model->address->getToString()}</p>' +
                    '<p class="event__text">{$model->address->phones[0]}{$worktimeString}</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="{$model->getThumbUploadUrl('img')}" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">{$gallery_count} фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: '/images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );
            
            
            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap.geoObjects.add(myPlacemark);

            myMap.behaviors.disable('scrollZoom');
            
        }
JS
);