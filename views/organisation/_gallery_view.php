<?php
/* @var $this \yii\web\View */
/* @var $models \app\models\OrganisationGallery[] */
?>
<?php if(count($models) > 0): ?>
<div class="post-anounce post-anounce--inside gallery-list">

    <div class="post-anounce__line">
        <div class="post-anounce__column post-anounce__column--big">
            <a href="<?php echo $models[0]->getUploadUrl('img'); ?>" savefrom_lm_index="0" savefrom_lm="1"
               class="post-anounce__item post-anounce__item--video"
               style="background-image: url('<?php echo $models[0]->getUploadUrl('img'); ?>');">
                            <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
            </a>
        </div>

        <div class="post-anounce__column">
            <?php if(isset($models[1])): ?>
            <a href="<?php echo $models[1]->getUploadUrl('img'); ?>" class="post-anounce__item post-anounce__item-gallery"
               style="background-image: url('<?php echo $models[1]->getUploadUrl('img'); ?>');">
                                <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
            </a>
            <?php endif; ?>

            <?php if(isset($models[2])): ?>
                <a href="<?php echo $models[2]->getUploadUrl('img'); ?>" class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('<?php echo $models[2]->getUploadUrl('img'); ?>');">
                                <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
                </a>
            <?php endif; ?>
        </div>

        <div class="post-anounce__column">
            <?php if(isset($models[3])): ?>
                <a href="<?php echo $models[3]->getUploadUrl('img'); ?>" class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('<?php echo $models[3]->getUploadUrl('img'); ?>');">
                                <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
                </a>
            <?php endif; ?>
    <?php if(isset($models[4])): ?>
            <a href="<?php echo $models[4]->getUploadUrl('img'); ?>" class="post-anounce__item post-anounce__item--<?php echo count($models) > 5?'more':'gallery' ?>"
               style="background-image: url('<?php echo $models[4]->getUploadUrl('img'); ?>');">
                <div class="post-anounce__info post-anounce__info--flex">
                    <?php if(count($models) > 5): ?>
                    <div class="post-anounce__text">
                        <div class="post-anounce__label">
                            еще
                        </div>
                        <?php echo count($models) - 5; ?> фото
                    </div>

                    <div class="post-anounce__ico">
                        <svg class="ico ico-arrow">
                            <use xlink:href="images/svg/symbol/sprite.svg#arrow"></use>
                        </svg>
                    </div>
                    <?php else: ?>
                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
                    <?php endif; ?>
                </div>
            </a>
    <?php endif; ?>
        </div>
    </div>
    <?php if(count($models) > 5): ?>
    <div class="post-anounce__more-wrapper">

        <div class="post-anounce__line">
            <?php foreach (array_slice($models, 5) as $model): ?>
            <a href="<?php echo $model->getUploadUrl('img'); ?>"
               class="post-anounce__item post-anounce__item-gallery"
               style="background-image: url('<?php echo $model->getUploadUrl('img'); ?>');">
                                <span class="post-anounce__ico post-anounce__ico--magnifier">
                                    <svg class="ico ico-search-round">
                                        <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                    </svg>
                                </span>
            </a>
            <?php endforeach; ?>
        </div><!-- /.post-anounce__line -->
    </div><!-- /.post-anounce__more-wrapper -->
    <?php endif; ?>

</div><!-- /.post-anounce -->
<?php endif; ?>