<?php
/* @var $models \app\models\Action[] */
use yii\helpers\Url;
?>
<div class="sale indent vertical">
    <h2 class="sale__title title-md">Акции</h2>

    <div class="sale__list">
        <?php for($i = 0; $i < 4; $i++): ?>
        <?php if(isset($models[$i])): ?>
                <div class="sale__item item">
                    <a href="<?php echo Url::to(['/organisation/view-action', 'id' => $models[$i]->id]) ?>" class="sale__img popup-with-form-ajax">
                        <img src="<?php echo $models[$i]->getImageUrl() ?>" alt="">
                        <span class="sale__label">Скидка - <?php echo $models[$i]->discount ?>%</span>
                    </a>
                    <div class="sale__info">
                        <a href="#form-sale" class="sale__item-title popup-with-form"><?php echo $models[$i]->name ?></a>
                    </div>
                </div>
        <?php endif; ?>
        <?php endfor; ?>
    </div><!-- /.sale__list -->

    <div class="sale__hidden">
        <div class="sale__list">
            <?php for($i = 4; $i < count($models); $i++): ?>
                <?php if(isset($models[$i])): ?>
                    <div class="sale__item item">
                        <a href="#form-sale" class="sale__img popup-with-form-ajax">
                            <img src="<?php echo $models[$i]->getImageUrl() ?>" alt="">
                            <span class="sale__label">Скидка - <?php echo $models[$i]->discount ?>%</span>
                        </a>
                        <div class="sale__info">
                            <a href="#form-sale" class="sale__item-title popup-with-form"><?php echo $models[$i]->name ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div><!-- /.sale__list -->
    </div>
    <?php if(count($models) > 4): ?>
    <a href="#" class="btn btn-trans sale__link-more">Показать еще...</a>
    <?php endif; ?>

</div><!-- /.sale -->