<?php

/**
 * @var $this \yii\base\View
 * @var $organisation Organisation
 * @var $agentForm \app\models\forms\AgentForm
 * @var $errorForm \app\models\forms\ErrorForm
 * @var $askForm \app\models\forms\AskForm
 */

use app\models\FsCity;
use app\models\Organisation;
use app\modules\likes\widgets\LikesWidget;
use app\widgets\Alert;
use kartik\editable\Editable;
use kartik\select2\Select2;
use yii\bootstrap\BootstrapAsset;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['/catalog']];
$this->params['breadcrumbs'][] = ['label' => $organisation->categories[0]->name, 'url' => ['/catalog', 'category_id' => $organisation->categories[0]->id]];
$this->params['breadcrumbs'][] = $organisation->name;
$this->params['modal'] = $organisation;
$this->params['address'] = $organisation->address;

\app\assets\MapsAsset::register($this);
BootstrapAsset::register($this);
$address = \app\models\OrganisationAdress::findOne(['organisation_id' => $organisation->id]);
$worktime = $organisation->worktime;
echo $this->render('_map', ['model' => $organisation]);
$dayOfWeek = date('N');
?>
<div class="intro">

    <div class="intro__wrapper intro__place place box">

        <div class="ymap-container">

            <div class="rate-like rate-like--oval">
                <?php $likes = LikesWidget::begin(['model' => $organisation]);?>
                    <?= $likes->like(true);?>
                    <?= $likes->dislike();?>
                <?php LikesWidget::end();?>
            </div>

            <div class="loader loader-default"></div>
            <div id="map-catalog" class="map"></div>
        </div>

        <div class="intro__left indent"></div><!-- /.intro__left -->

    

    </div><!-- /.intro__wrapper -->

</div><!-- /.intro -->

<div class="place__top indent">
    <div class="place__logo">
        <?= Html::img($organisation->getThumbUploadUrl('img'));?>
    </div>
    <div class="place__info">
        <h3 class="place__title title-md"><?= $organisation->name;?></h3>

        <div class="place__links">
            <a href="#" class="favorites">
                <i class="favorites-ico">
                    <svg class="ico ico-heart">
                        <use xlink:href="/images/svg/symbol/sprite.svg#heart"></use>
                    </svg>
                </i>
                <span class="favorites-text">В избранное</span>
            </a>

            <a href="#form-counter" class="counter-link popup-with-form">
                <span class="counter-link__ico"></span>
                <span class="counter-link__text">Получиться счетчик</span>
            </a>
        </div>

    </div>
</div>
<?php if($organisation->user_id == Yii::$app->user->id || (($agent = $organisation->getAgent()) && $agent->status == \app\models\Agent::STATUS_ACTIVE)) : ?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php if($organisation->status == Organisation::STATUS_REWORK) : ?>
        <div class="alert alert-danger"><?= $organisation->moderator_msg;?></div>
        <?php endif;?>
        <?= html::a('Редактировать', ['/organisation/update', 'id' => $organisation->clone_id ? $organisation->clone_id : $organisation->id], ['class' => 'btn btn-md btn-primary']);?>
    </div>
</div>
<?php elseif(Yii::$app->user->can('admin')) : ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= html::a('На публикацию', ['/admin/organisation/publish', 'id' => $organisation->id], ['class' => 'btn btn-md btn-primary', 'data-method' => 'post']);?>
        </div>
    </div>
    <?php $form = ActiveForm::begin([
        'action' => Url::to(['/admin/organisation/rework', 'id' => $organisation->id]),
    ]);?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?= $form->field($organisation, 'moderator_msg')->textarea();?>
            <?= Alert::widget();?>
            <?= Html::submitButton('Отправить на дороботку', ['class' => 'btn btn-md btn-warning']);?>
            <?php ActiveForm::end();?>
        </div>
    </div>
<?php endif;?>

<?= Breadcrumbs::widget([
    'options' => [
        'class' => 'breadcrumbs indent',
    ],
    'encodeLabels' => false,
    'itemTemplate' => '<li class="breadcrumbs__item">{link}</li>',
    'links' => $this->params['breadcrumbs'],
]);?>

<div class="place">
    <div class="place__descr indent">
        <div class="place__actions">
            <div class="place__actions-item">
                <?php echo $organisation->id; ?> — id страницы
            </div>
            <div class="place__actions-item">
                <?php echo $organisation->views_count; ?> — кол-во просмотров
            </div>
            <div class="place__actions-item">
                <?php echo $organisation->getCommentsCount() ?> — кол-во комментариев
            </div>
        </div>
        <p>
            <?= $organisation->desc;?>
        </p>
        <?php foreach ($organisation->additionallyDates as $dataModel) : ?>
        <?php $data = Json::decode($dataModel->data);?>
        <div class="service vertical-xs">
            <h4 class="text-center form-group"><?= $dataModel->form->title;?></h4>
            <?php foreach (Json::decode($dataModel->form->data)['rows'] as $row) : ?>
            <div class="service__wrapper">
                <?php foreach ($row['columns'] as $column) : ?>
                <div class="service__column">
                    <?php foreach ($column['elements'] as $element) : ?>
                    <div class="service__item">
                        <div class="service__ico">
                            <?php
                            $icon = isset($element['icon']) ? ' glyphicon glyphicon-' . $element['icon'] : '';
                            echo '<div class="ico ico-hookah' . $icon . '"></div>';
                            ?>
                            <!--<svg class="ico ico-hookah">
                                <use xlink:href="images/svg/symbol/sprite.svg#hookah"></use>
                            </svg>-->
                        </div>
                        <div class="service__info">
                            <div class="service__title"><?= $element['label'];?></div>
                            <div class="service__list">
                                <?php if($element['listData']) : ?>
                                    <?php foreach ((array)$data[$element['name']] as $item) : ?>
                                        <?php if(isset($element['listData'][$item])) : ?>
                                        <div class="service__element"><?= $element['listData'][$item];?></div>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php else : ?>
                                <div class="service__element"><?= $data[$element['name']] ?? '';?></div>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
                <?php endforeach;?>
            </div>
            <?php endforeach;?>
        </div>
        <?php endforeach;?>


        <div class="place__bottom">
            <div class="place__bottom-left">
                <a href="#form-question" class="btn btn-accent popup-with-form">Задать вопрос</a>
                <p class="text-xs">Остались вопросы? Обращайтесь мы вас поможем.</p>
            </div>
            <div class="place__bottom-right">
                <a href="#form-agent" class="place__bottom-link popup-with-form">
                                    <span class="place__bottom-link-ico">
                                        <svg class="ico ico-case">
                                            <use xlink:href="images/svg/symbol/sprite.svg#case"></use>
                                        </svg>
                                    </span>
                    <span class="place__bottom-link-text">Я представитель</span>
                </a>
                <a href="#form-error" class="place__bottom-link popup-with-form">
                                    <span class="place__bottom-link-ico">
                                        <svg class="ico ico-error">
                                            <use xlink:href="images/svg/symbol/sprite.svg#error"></use>
                                        </svg>
                                    </span>
                    <span class="place__bottom-link-text">Нашли ошибку</span>
                </a>
            </div>
        </div>
    </div>

    <div class="place__worktime-wrapper indent">
        <div class="place__worktime-list">
            <?php if($worktime) : ?>
            <div class="place__worktime<?= ($dayOfWeek == 0) ? ' active' : '';?>">
                <div class="place__worktime-day">Пн</div>
                <div class="place__worktime-time"><?= $worktime->mo_start;?> - <?= $worktime->mo_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->mo_br_start;?> - <?= $worktime->mo_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 1) ? ' active' : '';?>">
                <div class="place__worktime-day">Вт</div>
                <div class="place__worktime-time"><?= $worktime->tu_start;?> - <?= $worktime->tu_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->tu_br_start;?> - <?= $worktime->tu_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 2) ? ' active' : '';?>">
                <div class="place__worktime-day">Ср</div>
                <div class="place__worktime-time"><?= $worktime->we_start;?> - <?= $worktime->we_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->we_br_start;?> - <?= $worktime->we_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 3) ? ' active' : '';?>">
                <div class="place__worktime-day">Чт</div>
                <div class="place__worktime-time"><?= $worktime->th_start;?> - <?= $worktime->th_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->th_br_start;?> - <?= $worktime->th_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 4) ? ' active' : '';?>">
                <div class="place__worktime-day">Пт</div>
                <div class="place__worktime-time"><?= $worktime->fr_start;?> - <?= $worktime->fr_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->fr_br_start;?> - <?= $worktime->fr_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 5) ? ' active' : '';?>">
                <div class="place__worktime-day">Сб</div>
                <div class="place__worktime-time"><?= $worktime->sa_start;?> - <?= $worktime->sa_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->sa_br_start;?> - <?= $worktime->sa_br_end;?></div>
            </div>

            <div class="place__worktime<?= ($dayOfWeek == 6) ? ' active' : '';?>">
                <div class="place__worktime-day">Вс</div>
                <div class="place__worktime-time"><?= $worktime->su_start;?> - <?= $worktime->su_end;?></div>
                <div class="place__worktime-day">Перерыв</div>
                <div class="place__worktime-time"><?= $worktime->su_br_start;?> - <?= $worktime->su_br_end;?></div>
            </div>
            <?php endif;?>
        </div>

    </div>

</div>

<?php echo $this->render('_gallery_view', ['models' => $organisation->gallery]) ?>

<?php if(count($organisation->actions) > 0): ?>
    <?php echo $this->render('_action_view', ['models' => $organisation->actions]) ?>
<?php endif; ?>

<div class="menu menu-list">
    <div class="menu__wrapper indent vertical">
        <h2 class="menu__title title-md">Меню</h2>

        <div class="tabs tabs-menu" id="tabs-menu">
            <div class="menu__line">

                <ul class="menu__options tabs__list">
                    <li class="menu__option tabs__item ui-tabs-active">
                        <a class="tabs__link" href="#tabs-menu1">
                                            <span class="menu__option-title">
                                                Основное меню
                                            </span>
                            <span class="menu__option-count">
                                                52
                                            </span>
                        </a>
                    </li>
                    <li class="menu__option tabs__item">
                        <a class="tabs__link" href="#tabs-menu2">
                                            <span class="menu__option-title">
                                                Барное меню
                                            </span>
                            <span class="menu__option-count">
                                                25
                                            </span>
                        </a>
                    </li>
                    <li class="menu__option tabs__item">
                        <a class="tabs__link" href="#tabs-menu3">
                                            <span class="menu__option-title">
                                                Все меню
                                            </span>
                            <span class="menu__option-count">
                                                77
                                            </span>
                        </a>
                    </li>
                </ul><!-- /.menu__options -->

                <div class="menu__search">
                    <form action="/" class="search">
                        <div class="search__input-box">
                            <input class="search__input" type="search"
                                   placeholder="Поиск блюд в меню...">
                        </div>
                        <button type="submit" class="search__link">
                            <svg class="ico ico-search">
                                <use xlink:href="images/svg/symbol/sprite.svg#search"></use>
                            </svg>
                        </button>
                    </form>
                </div>
            </div>

            <div class="tabs__container">

                <div id="tabs-menu1" class="tabs__content">

                    <div class="menu__body">
                        <h3 class="menu__subtitle title-xs">
                            Холодные закуски
                        </h3>
                        <div class="menu__list">

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/1.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                        <span class="menu__plus">
                                                            <svg class="ico ico-plus-count">
                                                                <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                </use>
                                                            </svg>
                                                        </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                            <svg class="ico ico-minus">
                                                                <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/2.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/3.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/4.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                        </div><!-- /.menu__list -->

                        <div class="menu__hidden">
                            <div class="menu__list">

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/1.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/2.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/3.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/4.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                            </div><!-- /.menu__list -->
                        </div>
                        <a href="#" class="btn btn-trans menu__link-more">Показать еще...</a>
                    </div><!-- /.menu__body -->

                </div><!-- /.tabs__content -->

                <div id="tabs-menu2" class="tabs__content">

                    <div class="menu__body">
                        <h3 class="menu__subtitle title-xs">
                            Горячие закуски
                        </h3>
                        <div class="menu__list">

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/1.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/2.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/3.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/4.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                        </div><!-- /.menu__list -->

                        <div class="menu__hidden">
                            <div class="menu__list">

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/1.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/2.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/3.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/4.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                                <span class="menu__plus">
                                                                    <svg class="ico ico-plus-count">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                    <svg class="ico ico-minus">
                                                                        <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                            </div><!-- /.menu__list -->
                        </div>

                        <a href="#" class="btn btn-trans menu__link-more">Показать еще...</a>
                    </div><!-- /.menu__body -->

                </div><!-- /.tabs__content -->

                <div id="tabs-menu3" class="tabs__content">

                    <div class="menu__body">
                        <h3 class="menu__subtitle title-xs">
                            Все меню
                        </h3>
                        <div class="menu__list">

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/1.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/2.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/3.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                            <div class="menu__item-wrapper">
                                <a href="#form-menu" class="menu__item popup-with-form">

                                    <div class="menu__section">
                                        <div class="menu__img popup-with-form">
                                            <img src="images/recipe/4.jpg" alt="">
                                            <div class="menu__ico">
                                                <svg class="ico ico-plus-count">
                                                    <use
                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>
                                        <div class="menu__info">
                                            <h4 class="menu__item-title">
                                                Тартар из лосося
                                            </h4>
                                            <p class="menu__item-text">
                                                Филе лосося, рубленное с пряными специями, икрой летучей
                                                рыбы и
                                                японским
                                                майонезом.
                                                На
                                                тостах и с соусом «Гуакамоле»
                                            </p>
                                            <div class="menu__item-actions">
                                                <div class="menu__item-price">
                                                    695 руб.
                                                </div>
                                                <div class="menu__item-weight">
                                                    130/60/30гр.
                                                </div>
                                            </div><!-- /.menu__item-actions -->

                                            <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                399 руб. (45—1580 руб.)
                                            </p>
                                        </div><!-- /.menu__info -->
                                    </div><!-- /.menu__section -->

                                    <div class="menu__bottom">
                                        <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="menu__item-quant">52</span>
                                            <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                        </div>
                                        <div class="menu__item-btn btn btn-accent">Заказать</div>
                                    </div><!-- /.menu__bottom -->

                                </a><!-- /.menu__item -->
                            </div>

                        </div><!-- /.menu__list -->

                        <div class="menu__hidden">
                            <div class="menu__list">

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/1.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/2.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/3.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                                <div class="menu__item-wrapper">
                                    <a href="#form-menu" class="menu__item popup-with-form">

                                        <div class="menu__section">
                                            <div class="menu__img popup-with-form">
                                                <img src="images/recipe/4.jpg" alt="">
                                                <div class="menu__ico">
                                                    <svg class="ico ico-plus-count">
                                                        <use
                                                                xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="menu__info">
                                                <h4 class="menu__item-title">
                                                    Тартар из лосося
                                                </h4>
                                                <p class="menu__item-text">
                                                    Филе лосося, рубленное с пряными специями, икрой летучей
                                                    рыбы и
                                                    японским
                                                    майонезом.
                                                    На
                                                    тостах и с соусом «Гуакамоле»
                                                </p>
                                                <div class="menu__item-actions">
                                                    <div class="menu__item-price">
                                                        695 руб.
                                                    </div>
                                                    <div class="menu__item-weight">
                                                        130/60/30гр.
                                                    </div>
                                                </div><!-- /.menu__item-actions -->

                                                <p class="menu__item-subtext">Ср. цена в других 640 местах
                                                    399 руб. (45—1580 руб.)
                                                </p>
                                            </div><!-- /.menu__info -->
                                        </div><!-- /.menu__section -->

                                        <div class="menu__bottom">
                                            <div class="menu__count">
                                                            <span class="menu__plus">
                                                                <svg class="ico ico-plus-count">
                                                                    <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#plus-count">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                                <span class="menu__item-quant">52</span>
                                                <span class="menu__minus">
                                                                <svg class="ico ico-minus">
                                                                    <use xlink:href="images/svg/symbol/sprite.svg#minus">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            </div>
                                            <div class="menu__item-btn btn btn-accent">Заказать</div>
                                        </div><!-- /.menu__bottom -->

                                    </a><!-- /.menu__item -->
                                </div>

                            </div><!-- /.menu__list -->
                        </div>

                        <a href="#" class="btn btn-trans menu__link-more">Показать еще...</a>
                    </div><!-- /.menu__body -->

                </div><!-- /.tabs__content -->

            </div><!-- /.tabs__container -->

        </div><!-- /.tabs -->
    </div>

</div><!-- /.menu -->

<div class="experts indent vertical">

    <h2 class="experts__title title-md">Эксперты</h2>

    <div class="experts__list">
        <div class="experts__item">
            <div class="experts__header">
                <div class="experts__img">
                    <div class="experts__img-wrapper">
                        <img src="images/photo/1.jpg" alt="">
                    </div>
                </div>
                <div class="experts__name">
                    Dmitriy Cherepanov
                </div>
            </div>

            <div class="experts__section">
                <a href="#" class="experts__place">Кинотеатр Салют</a>
                <div class="experts__rate rate">
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                </div>
                <p class="experts__text text">Всем привет! Посетил вчера х/ф "Багровый
                    пик"
                    на 20:30,
                    забавно... Фильм комментировать не буду, как известно на вкус и
                    цвет,
                    товарищей
                    нет...</p>
            </div>
        </div><!-- /.experts__item -->

        <div class="experts__item">
            <div class="experts__header">
                <div class="experts__img">
                    <div class="experts__img-wrapper">
                        <img src="images/photo/2.jpg" alt="">
                    </div>
                </div>
                <div class="experts__name">
                    Юлия Владиславовна
                </div>
            </div>

            <div class="experts__section">
                <a href="#" class="experts__place">Yandex.ru</a>
                <div class="experts__rate rate">
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star rate__ico">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                </div>
                <p class="experts__text text">На сегодняшний день рекламная кампания
                    "Простите нас, конкуренты" была высоко оценена рекламным сообществом
                    и
                    получила несколько наград.</p>
            </div>
        </div><!-- /.experts__item -->

        <div class="experts__item experts__item-vip">
            <div class="experts__header">
                <div class="experts__img">
                    <div class="experts__img-wrapper">
                        <img src="images/photo/3.jpg" alt="">
                    </div>
                </div>
                <div class="experts__name">
                    TuryKcs25
                </div>
            </div>

            <div class="experts__section">
                <a href="#" class="experts__place">AUDI центр</a>
                <div class="experts__rate rate">
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                </div>
                <p class="experts__text text">Сервис по продаже автомобилей. Один из
                    самых
                    популярных в городе. Продают не только автомобили но и лодки, корм
                    для
                    собак и кошек ;)</p>
            </div>
        </div><!-- /.experts__item -->

        <div class="experts__item">
            <div class="experts__header">
                <div class="experts__img">
                    <div class="experts__img-wrapper">
                        <img src="images/photo/4.jpg" alt="">
                    </div>
                </div>
                <div class="experts__name">
                    Colambus
                </div>
            </div>

            <div class="experts__section">
                <a href="#" class="experts__place">МОТИВ</a>
                <div class="experts__rate">
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                    <i class="ico ico-star ico-star--empty rate__ico">
                        <svg class="ico ico-star">
                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                        </svg>
                    </i>
                </div>
                <p class="experts__text text">Молодежный парламент поселения Рязановское
                    и
                    25 детей из Детского сада "Ивушка" посадили на территории детского
                    сада
                    елки, орешник и дуб. </p>
            </div>
        </div><!-- /.experts__item -->
    </div>

</div><!-- /.experts -->
<div class="comment comment-indent">

    <div class="comment__top indent">
        <h3 class="comment__title title-md">Отзывы</h3>

        <div class="rate-like rate-like--row">
            <div class="rate-like__positive rate-like__item">
                                <span class="rate-like__ico">
                                    <svg class="ico ico-like-up">
                                        <use xlink:href="images/svg/symbol/sprite.svg#like-up"></use>
                                    </svg>
                                </span>
                <span class="rate-like__index">215</span>
            </div>
            <div class="rate-like__negative rate-like__item">
                                <span class="rate-like__ico">
                                    <svg class="ico ico-like-down">
                                        <use xlink:href="images/svg/symbol/sprite.svg#like-down"></use>
                                    </svg>
                                </span>
                <span class="rate-like__index">62</span>
            </div>
        </div><!-- /.rate-like -->
    </div><!-- /.comment__top -->


    <?php echo \app\components\Comment::widget([
        'model' => $organisation,
        'commentView' => '@app/views/comments/organisation',
        'maxLevel' => 2,
        'relatedTo' => 'Организации'
    ]); ?>

</div><!-- /.comment comment-indent -->

<div class="page-additional-mobile page-mobile"></div>

<div class="articles articles--line">
    <div class="indent vertical-xs">
        <h3 class="title-sm">Похожие организации</h3>
    </div>

    <div class="articles__list articles__list--row">
        <a href="#" class="articles__item articles__item--place"
           style="background-image: url('images/sights/1.jpg');">
            <div class="articles__hidden-top">
                <div class="articles__title">
                    Автосервис «Кволити Моторс» на Шмитовском <br>
                    <div class="articles__reviews">
                        Отзывов: 23
                    </div>
                </div>

            </div>
            <div class="articles__hidden-bottom">
                <div class="rate-reviews">
                    <div class="rate-reviews__positive rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-positive">
                                                <use xlink:href="images/svg/symbol/sprite.svg#positive"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">21</span>
                    </div>
                    <div class="rate-reviews__negative rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-negative">
                                                <use xlink:href="images/svg/symbol/sprite.svg#negative"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">671</span>
                    </div>

                </div><!-- /.rate-reviews -->
            </div><!-- /.articles__hidden-bottom -->
        </a>
        <a href="#" class="articles__item articles__item--place"
           style="background-image: url('images/sights/2.jpg');">
            <div class="articles__hidden-top">
                <div class="articles__title">
                    Автосервис «Кволити Моторс» на Шмитовском <br>
                    <div class="articles__reviews">
                        Отзывов: 23
                    </div>
                </div>

            </div>
            <div class="articles__hidden-bottom">
                <div class="rate-reviews">
                    <div class="rate-reviews__positive rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-positive">
                                                <use xlink:href="images/svg/symbol/sprite.svg#positive"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">21</span>
                    </div>
                    <div class="rate-reviews__negative rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-negative">
                                                <use xlink:href="images/svg/symbol/sprite.svg#negative"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">671</span>
                    </div>

                </div><!-- /.rate-reviews -->
            </div><!-- /.articles__hidden-bottom -->
        </a>
        <a href="#" class="articles__item articles__item--place"
           style="background-image: url('images/sights/1.jpg');">
            <div class="articles__hidden-top">
                <div class="articles__title">
                    Автосервис «Кволити Моторс» на Шмитовском <br>
                    <div class="articles__reviews">
                        Отзывов: 23
                    </div>
                </div>

            </div>
            <div class="articles__hidden-bottom">
                <div class="rate-reviews">
                    <div class="rate-reviews__positive rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-positive">
                                                <use xlink:href="images/svg/symbol/sprite.svg#positive"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">21</span>
                    </div>
                    <div class="rate-reviews__negative rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-negative">
                                                <use xlink:href="images/svg/symbol/sprite.svg#negative"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">671</span>
                    </div>

                </div><!-- /.rate-reviews -->
            </div><!-- /.articles__hidden-bottom -->
        </a>
        <a href="#" class="articles__item articles__item--place"
           style="background-image: url('images/sights/2.jpg');">
            <div class="articles__hidden-top">
                <div class="articles__title">
                    Автосервис «Кволити Моторс» на Шмитовском <br>
                    <div class="articles__reviews">
                        Отзывов: 23
                    </div>
                </div>

            </div>
            <div class="articles__hidden-bottom">
                <div class="rate-reviews">
                    <div class="rate-reviews__positive rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-positive">
                                                <use xlink:href="images/svg/symbol/sprite.svg#positive"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">21</span>
                    </div>
                    <div class="rate-reviews__negative rate-reviews__item">
                                        <span class="rate-reviews__ico">
                                            <svg class="ico ico-negative">
                                                <use xlink:href="images/svg/symbol/sprite.svg#negative"></use>
                                            </svg>
                                        </span>
                        <span class="rate-reviews__index">671</span>
                    </div>

                </div><!-- /.rate-reviews -->
            </div><!-- /.articles__hidden-bottom -->
        </a>
    </div><!-- /.articles__list -->
</div><!-- /.articles -->

<?php echo $this->render('popups/agent', ['model' => $agentForm, 'organisation' => $organisation]) ?>

<?php echo $this->render('popups/error', ['model' => $errorForm]) ?>
<?php echo $this->render('popups/ask', ['model' => $askForm]) ?>


<div class="form-pup form-pup--middle form-counter white-popup-block mfp-hide" id="form-counter">

    <div class="counter-form">

        <div class="counter-form__top">
            <h3 class="counter-form__title title-inside">Счетчик для сайта</h3>
            <div class="counter-form__img">
                <img src="images/counter-img.png" alt="">
            </div>
            <div class="counter-form__code">
                <div class="counter-form__ico"></div>
                <code>
                    &lt;a href="http://domain.ru/company" target="_blank" title="Мы на Garago.ru"&gt;
                    &lt;img src="http://domain.ru/rating/g1246.gif"/&gt;
                    &lt;/a&gt;
                </code>

            </div>
        </div>

        <div class="counter-form__section">

            <div class="counter-form__carousel owl-carousel">
                <div class="counter-form__carousel-item" data-index="0">

                    <h5 class="counter-form__subtitle">Как установить счетчик?</h5>
                    <p class="counter-form__text">Первым делом, скопируйте приведенный выше HTML код.</p>

                </div>
                <div class="counter-form__carousel-item" data-index="1">
                    <div class="counter-form__carousel-item">

                        <h5 class="counter-form__subtitle">Как установить счетчик?</h5>
                        <p class="counter-form__text">Первым делом, скопируйте приведенный выше HTML код.</p>

                    </div>
                </div>
                <div class="counter-form__carousel-item" data-index="2">
                    <div class="counter-form__carousel-item">

                        <h5 class="counter-form__subtitle">Как установить счетчик?</h5>
                        <p class="counter-form__text">Первым делом, скопируйте приведенный выше HTML код.</p>

                    </div>
                </div>
                <div class="counter-form__carousel-item" data-index="43">
                    <div class="counter-form__carousel-item">

                        <h5 class="counter-form__subtitle">Как установить счетчик?</h5>
                        <p class="counter-form__text">Первым делом, скопируйте приведенный выше HTML код.</p>

                    </div>
                </div>
            </div>

            <div class="counter-form__buttons">
                <button class="counter-form__btn" data-index="0">1</button>
                <button class="counter-form__btn" data-index="1">2</button>
                <button class="counter-form__btn" data-index="2">3</button>
                <button class="counter-form__btn" data-index="3">4</button>
            </div>
        </div><!-- /.counter-form__section -->

    </div><!-- /.counter-form -->

</div><!-- /.form-counter -->

