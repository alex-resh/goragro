<?php
/**
 * @var $this \yii\web\View
 * @var $form ActiveForm
 * @var $model \app\models\Organisation
 * @var $worktime \app\models\OrganisationWorktime
 * @var $address OrganisationAdress
 * @var $gallery \app\models\OrganisationGallery
 * @var $gallery_models \app\models\OrganisationGallery[]
 * @var $action_models \app\models\Action[]
 * @var $organisationForm \app\models\forms\OrganisationForm
 * @var $image \app\models\Image
 * @var $additionallyData mixed
 * @var $isAdmin bool
 */

use app\models\Category;
use app\models\FsCity;
use app\models\OrganisationAdress;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\OrganisationFormAsset;

$this->title = 'Добавление организации';

OrganisationFormAsset::register($this);
?>
<?php $form = ActiveForm::begin([
    'options' => ['class' => 'form form-request w100', 'enctype' => 'multipart/form-data']
]) ?>
    <div class="public-company bg">

        <div class="public-company__data">

            <div class="public-company__header request-header indent-lg vertical">
                <h3 class="request-header__title title-post">Публикация компании</h3>
                <div class="request-header__descr">
                    <svg class="ico" width="41px" height="41px">
                        <path fill-rule="evenodd" fill="rgb(112, 187, 57)"
                              d="M20.500,41.000 C9.178,41.000 -0.000,31.822 -0.000,20.500 C-0.000,9.178 9.178,-0.000 20.500,-0.000 C31.822,-0.000 41.000,9.178 41.000,20.500 C41.000,31.822 31.822,41.000 20.500,41.000 ZM12.000,38.029 C14.572,39.279 17.448,40.000 20.500,40.000 C23.951,40.000 27.188,39.095 30.000,37.522 L30.000,19.000 L25.000,19.000 L25.000,18.000 L15.000,18.000 L15.000,17.000 L25.000,17.000 L25.000,14.000 L12.000,14.000 L12.000,38.029 ZM20.500,1.000 C9.730,1.000 1.000,9.730 1.000,20.500 C1.000,27.818 5.037,34.186 11.000,37.522 L11.000,13.000 L25.000,13.000 L31.000,19.000 L31.000,36.914 C36.407,33.448 40.000,27.400 40.000,20.500 C40.000,9.730 31.270,1.000 20.500,1.000 ZM33.000,31.251 L33.000,29.638 C34.878,27.074 36.000,23.922 36.000,20.500 C36.000,11.940 29.060,5.000 20.500,5.000 C11.940,5.000 5.000,11.940 5.000,20.500 C5.000,24.491 6.522,28.118 9.000,30.866 L9.000,32.319 C5.920,29.322 4.000,25.138 4.000,20.500 C4.000,11.387 11.387,4.000 20.500,4.000 C29.613,4.000 37.000,11.387 37.000,20.500 C37.000,24.611 35.487,28.362 33.000,31.251 ZM27.000,22.000 L15.000,22.000 L15.000,21.000 L27.000,21.000 L27.000,22.000 ZM27.000,26.000 L15.000,26.000 L15.000,25.000 L27.000,25.000 L27.000,26.000 ZM27.000,30.000 L15.000,30.000 L15.000,29.000 L27.000,29.000 L27.000,30.000 ZM27.000,34.000 L15.000,34.000 L15.000,33.000 L27.000,33.000 L27.000,34.000 Z" />
                    </svg>
                    <p class="text-md">На этой странице вы можете добавить свою компанию в наш список. Это
                        позволит вам получить именно то что вам нужно от нашего сайта. Garago.ru -
                        интернет-ресурс созданный для простого предоставления и понимания информации о
                        компаниях.
                    </p>
                </div>
            </div>

            <div class="indent-lg vertical pt0">
                <div class="public-company__section">
                    <h3 class="section-subtitle">Основные данные</h3>

                    <div class="form__basic">
                        <div class="form__basic-left">
                            <div class="form__row">
                                <div class="form__row-col w100">
                                    <label class="form__label">
                                                        <span class="form__label-title">
                                                            Заголовок
                                                            <span class="form__star">*</span>
                                                        </span>
                                    </label>

                                    <div class="form-question">
                                        <a href="#" class="form__question js-form-question">?</a>
                                        <div class="help-warning">
                                            Это позволит вам получить именно то, что вам нужно!
                                        </div>
                                    </div>

                                </div>
                                <?= Html::activeTextInput($model, 'name', ['class' => 'form__input form__field']) ?>
                                <?= Html::error($model, 'name', ['class' => 'text-danger']);?>
                            </div><!-- /.form__row -->

                            <div class="form__row">
                                <div class="form__row-col w100">
                                    <label class="form__label">
                                                        <span class="form__label-title">
                                                            Описание
                                                            <span class="form__star">*</span>
                                                        </span>
                                    </label>
                                    <div class="form-question">
                                        <a href="#" class="form__question js-form-question">?</a>
                                        <div class="help-warning">
                                            Это позволит вам получить именно то, что вам нужно!
                                        </div>
                                    </div>
                                </div>
                                <?= Html::activeTextarea($model, 'desc', ['class' => 'form__input form__textarea'])?>
                                <?= Html::error($model, 'desc', ['class' => 'text-danger']);?>
                            </div><!-- /.form__row -->
                        </div>
                        <div class="form__basic-right">
                            <div class="logo">
                                <div class="logo__img">
                                    <span>Логотип компании</span>
                                    <?php if($model->img) : ?>
                                        <?= Html::img($model->getThumbUploadUrl('img'), ['class' => 'logo__picture', 'style' => ['opacity' => $model->img ? 1 : 0]]);?>
                                    <?php else : ?>
                                        <img src="" class="logo__picture" />
                                    <?php endif;?>
                                </div>
                                <button class="logo__btn btn btn-accent js-load-avatar"
                                        type="button">Загрузить
                                    лого</button>
                                <?php echo Html::activeFileInput($model, 'img', ['class' => 'logo__file-input']) ?>
                            </div>
                        </div>

                    </div>
                </div><!-- /.public-company__section -->

                <div class="public-company__section">
                    <div class="form__row">
                        <div class="form__row-col w100">
                            <label class="form__label">
                                                <span class="form__label-title">
                                                    Выберите категорию
                                                    <span class="form__star">*</span>
                                                </span>
                            </label>
                            <div class="form-question">
                                <a href="#" class="form__question js-form-question">?</a>
                                <div class="help-warning">
                                    Это позволит вам получить именно то, что вам нужно!
                                </div>
                            </div>
                        </div>
                        <?= Select2::widget([
                            'model' => $organisationForm,
                            'attribute' => 'categories',
                            'size' => Select2::LARGE,
                            'data' => Category::find()->select('name')->indexBy('id')->column(),
                            'options' => [
                                'multiple' => true,
                                'class' => 'js-category',
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                        ]);?>
                        <?= Html::error($organisationForm, 'categories', ['class' => 'text-danger']);?>
                    </div><!-- /.form__row -->
                </div><!-- /.public-company__section -->

                <div class="public-company__section">

                    <div class="form__row">
                        <div class="form__row-col w100">
                                            <span class="form__label-title">
                                                Город
                                                <span class="form__star">*</span>
                                            </span>
                            <div class="form-question">
                                <a href="#" class="form__question js-form-question">?</a>
                                <div class="help-warning">
                                    Это позволит вам получить именно то, что вам нужно!
                                </div>
                            </div>
                        </div>
                        <?= Select2::widget([
                            'model' => $address,
                            'attribute' => 'city_id',
                            'size' => Select2::LARGE,
                            'data' => FsCity::find()->select('name')->orderBy('name')->indexBy('id')->column(),
                            'options' => [
                                'placeholder' => 'Выберите город',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]);?>
                        <?= Html::error($address, 'city_id', ['class' => 'text-danger']);?>
                    </div><!-- /.form__row -->

                    <?= $this->render('address', [
                        'model' => $address,
                    ]);?>
                </div><!-- /.public-company__section -->
            </div><!-- /.indent-lg -->

        </div>

        <div class="public-company__data">

            <div class="indent-lg vertical pt0">
                <div class="consent-company__checkbox checkbox-inner">
                    <input class="checkbox__input" id="check20" type="checkbox" name="checkbox">
                    <label class="checkbox__wrapper" for="check20">
                        <span class="checkbox__custom"></span>
                        <span class="checkbox__label">
                                            <span class="checkbox__text">Я согласен с <a href="#">правилами
                                                    публикации</a> и <a href="#">правилами сайта</a></span>
                                        </span>
                    </label>
                </div>
                <div class="consent-company__buttons">
                    <?php echo Html::submitInput('Отправить запрос', ['class' => 'consent-company__btn consent-company__sumbit']) ?>
                    <a href="<?= Url::to();?>" class="consent-company__btn consent-company__cancel">Отменить</a>
                </div>
            </div>
        </div><!-- /.public-company__data -->
    </div><!-- /.request -->
<?php ActiveForm::end() ?>