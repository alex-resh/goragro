<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\OrganisationWorktime
 */

$this->registerJs(<<<JS
    $('body').on('click', 'label.checkbox__wrapper', function() {
        $(this).not('[for]').prev(':checkbox').click();
    });
    $('body').on('click', '.js-weekdays', function() {
        if(!$(this).is('.active')){
            $(this).closest('.work-time__element').find('.work-time__days :checkbox').slice(0, 5).not(':checked').click().end().end().slice(5, 7).filter(':checked').click();
        }
    });
    $('body').on('click', '.js-weekends', function() {
        if(!$(this).is('.active')){
            $(this).closest('.work-time__element').find('.work-time__days :checkbox').slice(5, 7).not(':checked').click().end().end().slice(0, 5).filter(':checked').click();
        }
    });
    $('body').on('click', '.js-daily', function() {
        if(!$(this).is('.active')){
            $(this).closest('.work-time__element').find('.work-time__days :checkbox').not(':checked').click();            
        }
    });
    $('body').on('change', '.work-time__days :checkbox', function() {
        if(!$(this).is(':checked')){
            var index = $('.checkbox-inner').has(this).index();
            $(this).closest('.work-time__element').find((index < 5 ? '.js-weekdays' : '.js-weekends') + ', .js-daily').removeClass('active');
        }
    });
    $('body').on('click', '.js-round-the-clock', function() {
        $(this).closest('.work-time__element').find('.js-work-start').text('0:00').end().find('.js-work-end').text('24:00');
    });
    $('body').on('click', '.js-work-start-list .select__item, .js-work-end-list .select__item', function() {
        var parent = $(this).parent();
        var text = $(this).text();
        if((parent.is('.js-work-start-list') && text != '0:00') || (parent.is('.js-work-end-list') && text != '24:00')){
            $(this).closest('.work-time__element').find('.js-round-the-clock').removeClass('active');
        }
    });
    $('body').on('change', '.js-nonstop-worktime', function() {
        if($(this).is(':checked')){
            $(this).closest('.work-time__element').find('.js-br-start, .js-br-end').text('');
        }
    });
    $('body').on('click', '.js-br-start-list .select__item, .js-br-end-list .select__item', function() {
        $(this).closest('.work-time__element').find('.js-nonstop-worktime:checked').click();
    });
    $('form').has('.work-time__element').on('submit', function() {
        var form = $(this);
        var res = [];
        form.find('.work-time__element').each(function() {
            var el = $(this);
            var days = [];
            el.find('.checkbox-inner').has(':checked').each(function() {
                days.push($(this).index());
            });            
            res.push({
                days:days,
                workStart: $('.js-work-start', el).text(),
                workEnd: $('.js-work-end', el).text(),
                breakStart: $('.js-br-start', el).text(),
                breakEnd: $('.js-br-end', el).text()
            });
        });
        $('.js-work-time-data').val(JSON.stringify(res));
    });
    $('.work-time__element--added .js-work-time-btn span').on('click', function() {
        $(this).closest('.work-time__element--added').remove();
        return false;
    });
JS
);

use yii\helpers\Html;
?>
<div class="work-time">
    <?= Html::hiddenInput('work_time_data', null, ['class' => 'js-work-time-data']);?>
    <?php if($model->data) : ?>
    <?php foreach ($model->data as $i => $item) : ?>
    <div class="work-time__element<?= $i ? ' work-time__element--added' : '';?>">
        <div class="work-time__wrapper">
            <div class="work-time__header">
                <div class="work-time__title">
                    Рабочие дни
                </div>
                <button class="work-time__button js-work-time-btn">
                    <span></span>
                </button>
            </div>
            <div class="work-time__section">
                <div class="work-time__days">
                    <div class="work-time__days-name">
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(0, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">пн</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(1, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">вт</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(2, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">ср</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(3, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">чт</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(4, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">пт</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(5, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">сб</span>
                                                                    </span>
                            </label>
                        </div>
                        <div class="checkbox checkbox-inner">
                            <input class="checkbox__input"<?= in_array(6, $item['days']) ? ' checked="checked"' : '';?>
                                   type="checkbox" name="work-day1">
                            <label class="checkbox__wrapper">
                                <span class="checkbox__custom"></span>
                                <span class="checkbox__label">
                                                                        <span class="checkbox__text">вс</span>
                                                                    </span>
                            </label>
                        </div>
                    </div><!-- /.work-time__days-name -->

                    <div class="work-time__tips">
                        <div class="work-time__tips-item js-work-time-tips js-weekdays">
                            будни
                        </div>
                        <div class="work-time__tips-item js-work-time-tips js-weekends">
                            выходные
                        </div>
                        <div class="work-time__tips-item js-work-time-tips js-daily">
                            ежедневно
                        </div>
                    </div>
                </div><!-- /.work-time__days -->

                <div class="work-time__data">
                    <div class="work-time__elem">
                        <div class="work-time__data-row">
                            <div class="work-time__data-column">
                                <div class="work-time__data-item">
                                    <div class="work-time__data-text work-time__data-text--from">Время работы с
                                    </div>
                                    <div
                                            class="work-time__select form__select select">
                                        <div class="select__header">
                                            <span class="select__label js-work-start"><?= $item['workStart'];?></span>
                                            <svg class="ico ico-arrow-bottom">
                                                <use
                                                        xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                </use>
                                            </svg>
                                        </div><!-- /.select__header -->

                                        <div class="select__list js-work-start-list">
                                            <div class="select__item">0:00</div>
                                            <div class="select__item">1:00</div>
                                            <div class="select__item">2:00</div>
                                            <div class="select__item">3:00</div>
                                            <div class="select__item">4:00</div>
                                            <div class="select__item">5:00</div>
                                            <div class="select__item">6:00</div>
                                            <div class="select__item">7:00</div>
                                            <div class="select__item">8:00</div>
                                            <div class="select__item">9:00</div>
                                            <div class="select__item">10:00</div>
                                            <div class="select__item">11:00</div>
                                            <div class="select__item">12:00</div>
                                            <div class="select__item">13:00</div>
                                            <div class="select__item">14:00</div>
                                            <div class="select__item">15:00</div>
                                            <div class="select__item">16:00</div>
                                            <div class="select__item">17:00</div>
                                            <div class="select__item">18:00</div>
                                            <div class="select__item">19:00</div>
                                            <div class="select__item">20:00</div>
                                            <div class="select__item">21:00</div>
                                            <div class="select__item">22:00</div>
                                            <div class="select__item">23:00</div>
                                            <div class="select__item">24:00</div>
                                        </div><!-- /.select__list -->
                                    </div><!-- /.select -->
                                </div>
                                <div class="work-time__data-item">
                                    <div class="work-time__data-text work-time__data-text--to">до</div>
                                    <div
                                            class="work-time__select form__select select">
                                        <div class="select__header">
                                            <span class="select__label js-work-end"><?= $item['workEnd'];?></span>
                                            <svg class="ico ico-arrow-bottom">
                                                <use
                                                        xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                </use>
                                            </svg>
                                        </div><!-- /.select__header -->

                                        <div class="select__list js-work-end-list">
                                            <div class="select__item">0:00</div>
                                            <div class="select__item">1:00</div>
                                            <div class="select__item">2:00</div>
                                            <div class="select__item">3:00</div>
                                            <div class="select__item">4:00</div>
                                            <div class="select__item">5:00</div>
                                            <div class="select__item">6:00</div>
                                            <div class="select__item">7:00</div>
                                            <div class="select__item">8:00</div>
                                            <div class="select__item">9:00</div>
                                            <div class="select__item">10:00</div>
                                            <div class="select__item">11:00</div>
                                            <div class="select__item">12:00</div>
                                            <div class="select__item">13:00</div>
                                            <div class="select__item">14:00</div>
                                            <div class="select__item">15:00</div>
                                            <div class="select__item">16:00</div>
                                            <div class="select__item">17:00</div>
                                            <div class="select__item">18:00</div>
                                            <div class="select__item">19:00</div>
                                            <div class="select__item">20:00</div>
                                            <div class="select__item">21:00</div>
                                            <div class="select__item">22:00</div>
                                            <div class="select__item">23:00</div>
                                            <div class="select__item">24:00</div>
                                        </div><!-- /.select__list -->
                                    </div><!-- /.select -->
                                </div>
                            </div><!-- /.work-time__data--column -->
                            <div
                                    class="work-time__data-column work-time__data-column--clock">
                                <div class="work-time__tips">
                                    <div
                                            class="work-time__tips-item js-work-time-tips js-round-the-clock">
                                        круглосуточно
                                    </div>
                                </div>
                            </div><!-- /.work-time__data--column -->
                        </div><!-- /.work-time__data--row -->

                    </div><!-- /.work-time__elem -->

                    <div class="work-time__elem">
                        <div class="work-time__data-row">
                            <div class="work-time__data-column">
                                <div class="work-time__data-item">
                                    <div class="work-time__data-text work-time__data-text--from">Перерыв с
                                    </div>
                                    <div
                                            class="work-time__select form__select select">
                                        <div class="select__header">
                                            <span class="select__label js-br-start"><?= $item['breakStart'];?></span>
                                            <svg class="ico ico-arrow-bottom">
                                                <use
                                                        xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                </use>
                                            </svg>
                                        </div><!-- /.select__header -->

                                        <div class="select__list js-br-start-list">
                                            <div class="select__item">0:00</div>
                                            <div class="select__item">1:00</div>
                                            <div class="select__item">2:00</div>
                                            <div class="select__item">3:00</div>
                                            <div class="select__item">4:00</div>
                                            <div class="select__item">5:00</div>
                                            <div class="select__item">6:00</div>
                                            <div class="select__item">7:00</div>
                                            <div class="select__item">8:00</div>
                                            <div class="select__item">9:00</div>
                                            <div class="select__item">10:00</div>
                                            <div class="select__item">11:00</div>
                                            <div class="select__item">12:00</div>
                                            <div class="select__item">13:00</div>
                                            <div class="select__item">14:00</div>
                                            <div class="select__item">15:00</div>
                                            <div class="select__item">16:00</div>
                                            <div class="select__item">17:00</div>
                                            <div class="select__item">18:00</div>
                                            <div class="select__item">19:00</div>
                                            <div class="select__item">20:00</div>
                                            <div class="select__item">21:00</div>
                                            <div class="select__item">22:00</div>
                                            <div class="select__item">23:00</div>
                                            <div class="select__item">24:00</div>
                                        </div><!-- /.select__list -->
                                    </div><!-- /.select -->
                                </div>
                                <div class="work-time__data-item">
                                    <div class="work-time__data-text work-time__data-text--to">до</div>
                                    <div
                                            class="work-time__select form__select select">
                                        <div class="select__header">
                                            <span class="select__label js-br-end"><?= $item['breakEnd'];?></span>
                                            <svg class="ico ico-arrow-bottom">
                                                <use
                                                        xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                </use>
                                            </svg>
                                        </div><!-- /.select__header -->

                                        <div class="select__list js-br-end-list">
                                            <div class="select__item">0:00</div>
                                            <div class="select__item">1:00</div>
                                            <div class="select__item">2:00</div>
                                            <div class="select__item">3:00</div>
                                            <div class="select__item">4:00</div>
                                            <div class="select__item">5:00</div>
                                            <div class="select__item">6:00</div>
                                            <div class="select__item">7:00</div>
                                            <div class="select__item">8:00</div>
                                            <div class="select__item">9:00</div>
                                            <div class="select__item">10:00</div>
                                            <div class="select__item">11:00</div>
                                            <div class="select__item">12:00</div>
                                            <div class="select__item">13:00</div>
                                            <div class="select__item">14:00</div>
                                            <div class="select__item">15:00</div>
                                            <div class="select__item">16:00</div>
                                            <div class="select__item">17:00</div>
                                            <div class="select__item">18:00</div>
                                            <div class="select__item">19:00</div>
                                            <div class="select__item">20:00</div>
                                            <div class="select__item">21:00</div>
                                            <div class="select__item">22:00</div>
                                            <div class="select__item">23:00</div>
                                            <div class="select__item">24:00</div>
                                        </div><!-- /.select__list -->
                                    </div><!-- /.select -->
                                </div>

                                <div class="work-time__data-item">
                                    <div class="checkbox checkbox-inner">
                                        <input class="checkbox__input js-nonstop-worktime" type="checkbox"
                                               name="nonstop_worktime">
                                        <label class="checkbox__wrapper">
                                            <span class="checkbox__custom"></span>
                                            <span class="checkbox__label">
                                                                                    <span class="checkbox__text">без
                                                                                        перерыва</span>
                                                                                </span>
                                        </label>
                                    </div><!-- /.checkbox -->
                                </div>
                            </div><!-- /.work-time__data-column -->
                        </div><!-- /.work-time__data-row -->


                    </div><!-- /.work-time__elem -->


                </div>
            </div>
        </div><!-- /.work-time__wrapper -->
    </div><!-- /.work-time__element -->
    <?php endforeach;?>
    <?php else : ?>
        <div class="work-time__element">
            <div class="work-time__wrapper">
                <div class="work-time__header">
                    <div class="work-time__title">
                        Рабочие дни
                    </div>
                    <button class="work-time__button js-work-time-btn">
                        <span></span>
                    </button>
                </div>
                <div class="work-time__section">
                    <div class="work-time__days">
                        <div class="work-time__days-name">
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">пн</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">вт</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">ср</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">чт</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">пт</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">сб</span>
                                                                    </span>
                                </label>
                            </div>
                            <div class="checkbox checkbox-inner">
                                <input class="checkbox__input"
                                       type="checkbox" name="work-day1">
                                <label class="checkbox__wrapper">
                                    <span class="checkbox__custom"></span>
                                    <span class="checkbox__label">
                                                                        <span class="checkbox__text">вс</span>
                                                                    </span>
                                </label>
                            </div>
                        </div><!-- /.work-time__days-name -->

                        <div class="work-time__tips">
                            <div class="work-time__tips-item js-work-time-tips js-weekdays">
                                будни
                            </div>
                            <div class="work-time__tips-item js-work-time-tips js-weekends">
                                выходные
                            </div>
                            <div class="work-time__tips-item js-work-time-tips js-daily">
                                ежедневно
                            </div>
                        </div>
                    </div><!-- /.work-time__days -->

                    <div class="work-time__data">
                        <div class="work-time__elem">
                            <div class="work-time__data-row">
                                <div class="work-time__data-column">
                                    <div class="work-time__data-item">
                                        <div class="work-time__data-text work-time__data-text--from">Время работы с
                                        </div>
                                        <div
                                                class="work-time__select form__select select">
                                            <div class="select__header">
                                                <span class="select__label js-work-start">0:00</span>
                                                <svg class="ico ico-arrow-bottom">
                                                    <use
                                                            xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                    </use>
                                                </svg>
                                            </div><!-- /.select__header -->

                                            <div class="select__list js-work-start-list">
                                                <div class="select__item">0:00</div>
                                                <div class="select__item">1:00</div>
                                                <div class="select__item">2:00</div>
                                                <div class="select__item">3:00</div>
                                                <div class="select__item">4:00</div>
                                                <div class="select__item">5:00</div>
                                                <div class="select__item">6:00</div>
                                                <div class="select__item">7:00</div>
                                                <div class="select__item">8:00</div>
                                                <div class="select__item">9:00</div>
                                                <div class="select__item">10:00</div>
                                                <div class="select__item">11:00</div>
                                                <div class="select__item">12:00</div>
                                                <div class="select__item">13:00</div>
                                                <div class="select__item">14:00</div>
                                                <div class="select__item">15:00</div>
                                                <div class="select__item">16:00</div>
                                                <div class="select__item">17:00</div>
                                                <div class="select__item">18:00</div>
                                                <div class="select__item">19:00</div>
                                                <div class="select__item">20:00</div>
                                                <div class="select__item">21:00</div>
                                                <div class="select__item">22:00</div>
                                                <div class="select__item">23:00</div>
                                                <div class="select__item">24:00</div>
                                            </div><!-- /.select__list -->
                                        </div><!-- /.select -->
                                    </div>
                                    <div class="work-time__data-item">
                                        <div class="work-time__data-text work-time__data-text--to">до</div>
                                        <div
                                                class="work-time__select form__select select">
                                            <div class="select__header">
                                                <span class="select__label js-work-end">24:00</span>
                                                <svg class="ico ico-arrow-bottom">
                                                    <use
                                                            xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                    </use>
                                                </svg>
                                            </div><!-- /.select__header -->

                                            <div class="select__list js-work-end-list">
                                                <div class="select__item">0:00</div>
                                                <div class="select__item">1:00</div>
                                                <div class="select__item">2:00</div>
                                                <div class="select__item">3:00</div>
                                                <div class="select__item">4:00</div>
                                                <div class="select__item">5:00</div>
                                                <div class="select__item">6:00</div>
                                                <div class="select__item">7:00</div>
                                                <div class="select__item">8:00</div>
                                                <div class="select__item">9:00</div>
                                                <div class="select__item">10:00</div>
                                                <div class="select__item">11:00</div>
                                                <div class="select__item">12:00</div>
                                                <div class="select__item">13:00</div>
                                                <div class="select__item">14:00</div>
                                                <div class="select__item">15:00</div>
                                                <div class="select__item">16:00</div>
                                                <div class="select__item">17:00</div>
                                                <div class="select__item">18:00</div>
                                                <div class="select__item">19:00</div>
                                                <div class="select__item">20:00</div>
                                                <div class="select__item">21:00</div>
                                                <div class="select__item">22:00</div>
                                                <div class="select__item">23:00</div>
                                                <div class="select__item">24:00</div>
                                            </div><!-- /.select__list -->
                                        </div><!-- /.select -->
                                    </div>
                                </div><!-- /.work-time__data--column -->
                                <div
                                        class="work-time__data-column work-time__data-column--clock">
                                    <div class="work-time__tips">
                                        <div
                                                class="work-time__tips-item js-work-time-tips js-round-the-clock">
                                            круглосуточно
                                        </div>
                                    </div>
                                </div><!-- /.work-time__data--column -->
                            </div><!-- /.work-time__data--row -->

                        </div><!-- /.work-time__elem -->

                        <div class="work-time__elem">
                            <div class="work-time__data-row">
                                <div class="work-time__data-column">
                                    <div class="work-time__data-item">
                                        <div class="work-time__data-text work-time__data-text--from">Перерыв с
                                        </div>
                                        <div
                                                class="work-time__select form__select select">
                                            <div class="select__header">
                                                <span class="select__label js-br-start">12:00</span>
                                                <svg class="ico ico-arrow-bottom">
                                                    <use
                                                            xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                    </use>
                                                </svg>
                                            </div><!-- /.select__header -->

                                            <div class="select__list js-br-start-list">
                                                <div class="select__item">0:00</div>
                                                <div class="select__item">1:00</div>
                                                <div class="select__item">2:00</div>
                                                <div class="select__item">3:00</div>
                                                <div class="select__item">4:00</div>
                                                <div class="select__item">5:00</div>
                                                <div class="select__item">6:00</div>
                                                <div class="select__item">7:00</div>
                                                <div class="select__item">8:00</div>
                                                <div class="select__item">9:00</div>
                                                <div class="select__item">10:00</div>
                                                <div class="select__item">11:00</div>
                                                <div class="select__item">12:00</div>
                                                <div class="select__item">13:00</div>
                                                <div class="select__item">14:00</div>
                                                <div class="select__item">15:00</div>
                                                <div class="select__item">16:00</div>
                                                <div class="select__item">17:00</div>
                                                <div class="select__item">18:00</div>
                                                <div class="select__item">19:00</div>
                                                <div class="select__item">20:00</div>
                                                <div class="select__item">21:00</div>
                                                <div class="select__item">22:00</div>
                                                <div class="select__item">23:00</div>
                                                <div class="select__item">24:00</div>
                                            </div><!-- /.select__list -->
                                        </div><!-- /.select -->
                                    </div>
                                    <div class="work-time__data-item">
                                        <div class="work-time__data-text work-time__data-text--to">до</div>
                                        <div
                                                class="work-time__select form__select select">
                                            <div class="select__header">
                                                <span class="select__label js-br-end">13:00</span>
                                                <svg class="ico ico-arrow-bottom">
                                                    <use
                                                            xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom">
                                                    </use>
                                                </svg>
                                            </div><!-- /.select__header -->

                                            <div class="select__list js-br-end-list">
                                                <div class="select__item">0:00</div>
                                                <div class="select__item">1:00</div>
                                                <div class="select__item">2:00</div>
                                                <div class="select__item">3:00</div>
                                                <div class="select__item">4:00</div>
                                                <div class="select__item">5:00</div>
                                                <div class="select__item">6:00</div>
                                                <div class="select__item">7:00</div>
                                                <div class="select__item">8:00</div>
                                                <div class="select__item">9:00</div>
                                                <div class="select__item">10:00</div>
                                                <div class="select__item">11:00</div>
                                                <div class="select__item">12:00</div>
                                                <div class="select__item">13:00</div>
                                                <div class="select__item">14:00</div>
                                                <div class="select__item">15:00</div>
                                                <div class="select__item">16:00</div>
                                                <div class="select__item">17:00</div>
                                                <div class="select__item">18:00</div>
                                                <div class="select__item">19:00</div>
                                                <div class="select__item">20:00</div>
                                                <div class="select__item">21:00</div>
                                                <div class="select__item">22:00</div>
                                                <div class="select__item">23:00</div>
                                                <div class="select__item">24:00</div>
                                            </div><!-- /.select__list -->
                                        </div><!-- /.select -->
                                    </div>

                                    <div class="work-time__data-item">
                                        <div class="checkbox checkbox-inner">
                                            <input class="checkbox__input js-nonstop-worktime" type="checkbox"
                                                   name="nonstop_worktime">
                                            <label class="checkbox__wrapper">
                                                <span class="checkbox__custom"></span>
                                                <span class="checkbox__label">
                                                                                    <span class="checkbox__text">без
                                                                                        перерыва</span>
                                                                                </span>
                                            </label>
                                        </div><!-- /.checkbox -->
                                    </div>
                                </div><!-- /.work-time__data-column -->
                            </div><!-- /.work-time__data-row -->


                        </div><!-- /.work-time__elem -->


                    </div>
                </div>
            </div><!-- /.work-time__wrapper -->
        </div><!-- /.work-time__element -->
    <?php endif;?>
</div><!-- /.work-time -->