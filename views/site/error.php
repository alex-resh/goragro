<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="not-found">
    <div class="not-found__wrapper">
        <a href="/" class="not-found__logo">
            <img src="images/logo-white.svg" alt="Garago">
        </a>
        <div class="not-found__title"><?php echo $exception->statusCode ?></div>
        <p class="not-found__text">Истина где-то рядом, продолжайте искать.</p>
        <a href="/" class="not-found__link">Вернуться на главную</a>
        <p class="not-found__copyright">© 2012 - 2018 Garago.ru</p>
    </div>

</div><!-- /.not-found -->