<?php



/* @var $this \yii\web\View */
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image:type" content="image/png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon-72x72.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">

    <style>
        body {
            opacity: 0;
        }
    </style>
</head>

<body class="">

<div class="offline fullscreen" style="background-image: url('/images/background/city_bg.jpg');">

    <div class="loader">
        <div class="loader__inner">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>

    </div>

    <header class="header">
        <div class="header__wrapper box-xs">

            <div class="header__top">
                <div class="header__left">
                    <a href="#" class="logo header__logo">
                        <img src="/images/logo-white.svg" alt="Garago">
                    </a>
                    <div class="header__city">
                        <div class="header__city-current">
                            <svg class="ico ico-target">
                                <use xlink:href="/images/svg/symbol/sprite.svg#target"></use>
                            </svg>
                            Санкт-Петербург
                        </div>

                    </div>
                </div>

                <div class="header__right">
                    <nav class="mnu-top">
                        <button class="mnu-top__button hamburger hamburger--collapse js-menu-open">
                                <span class="mnu-top__button-wrapper">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                    <span class="mnu-top__text">меню</span>
                                </span>
                        </button>
                        <div class="mnu-top__wrapper">
                            <button class="mnu-top__button hamburger hamburger--collapse is-active js-menu-close">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                            </button>
                            <ul class="mnu-top__list">
                                <li class="mnu-top__item"><a href="#" class="mnu-top__link">Каталог</a></li>
                                <li class="mnu-top__item"><a href="#" class="mnu-top__link">Новости</a></li>
                                <li class="mnu-top__item"><a href="#" class="mnu-top__link">Блоги</a></li>
                                <li class="mnu-top__item"><a href="#" class="mnu-top__link">Рецепты</a></li>
                                <li class="mnu-top__item"><a href="#" class="mnu-top__link">Юмор</a></li>
                            </ul>
                        </div>
                    </nav>

                    <div class="header__signup header__signup--offline header__signup-dekstop">
                        <div class="header__signup-buttons">
                            <a href="/user/login" class="header__registration">Регистрация</a>
                            <a href="/user/login" class="header__sign btn btn-trans">Войти</a>
                        </div>

                        <div class="user">
                            <div class="user__header">
                                <div class="user__data">
                                    <div class="user__avatar"><img src="/images/avatar.jpg" alt="avatar"></div>
                                    <div class="user__nickname">Nickname</div>
                                </div>
                                <div class="user__info">
                                    <div class="user__num">5</div>
                                    <div class="user__ico">
                                        <svg class="ico ico-arrow-bottom">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>

                            <div class="user__dropdown">
                                <div class="user__dropdown-section">
                                    <div class="user__dropdown-avatar">
                                        <img src="/images/avatar.jpg" alt="avatar">
                                    </div>
                                    <div class="user__dropdown-menu">
                                        <a href="#" class="user__dropdown-menu-item">Сообщения - 12</a>
                                        <a href="#" class="user__dropdown-menu-item">Добавить запись</a>
                                        <a href="#" class="user__dropdown-menu-item">Настройки</a>
                                    </div>
                                </div>
                                <a href="#" class="user__dropdown-bottom">
                                    <span class="user__dropdown-exit">Выход</span>
                                </a>
                            </div><!-- /.user__dropdown -->

                        </div><!-- /.user -->

                    </div><!-- /.header__signup -->
                </div><!-- /.header__right -->
            </div><!-- /.header__top -->

            <div class="header__bottom"></div>


        </div><!-- /.header__wrapper -->
    </header><!-- /.header -->

    <div class="offline__info">

        <p class="offline__text">
            <?php echo \app\models\Settings::get('off_about') ?>
        </p>
<!--        <div class="offline__time">-->
<!--            68 часов 26 минут-->
<!--        </div>-->
<!--        <p class="offline__subtext">-->
<!--            осталось до открытия-->
<!--        </p>-->
    </div>

    <div class="offline__bottom">
        <p class="offline__text-xs">...а пока Вы можете</p>
        <div class="offline__buttons">

            <a href="#" class="offline__btn">
                    <span class="offline__btn-ico">
                        <svg class="ico ico-build">
                            <use xlink:href="/images/svg/symbol/sprite.svg#build"></use>
                        </svg>
                    </span>
                <span class="offline__btn-text">Добавить фирму</span>
            </a>

            <a href="#" class="offline__btn">
                    <span class="offline__btn-ico">
                        <svg class="ico ico-message">
                            <use xlink:href="/images/svg/symbol/sprite.svg#message"></use>
                        </svg>
                    </span>
                <span class="offline__btn-text">Пообщаться</span>
            </a>

            <a href="#" class="offline__btn">
                    <span class="offline__btn-ico">
                        <svg class="ico ico-table">
                            <use xlink:href="/images/svg/symbol/sprite.svg#table"></use>
                        </svg>
                    </span>
                <span class="offline__btn-text">Ничего не делать</span>
            </a>

            <a href="#" class="offline__btn">
                    <span class="offline__btn-ico">
                        <svg class="ico ico-add">
                            <use xlink:href="/images/svg/symbol/sprite.svg#add"></use>
                        </svg>
                    </span>
                <span class="offline__btn-text">Найти друга</span>
            </a>

            <a href="#" class="offline__btn">
                    <span class="offline__btn-ico">
                        <svg class="ico ico-email">
                            <use xlink:href="/images/svg/symbol/sprite.svg#email"></use>
                        </svg>
                    </span>
                <span class="offline__btn-text">Проверить почту</span>
            </a>
        </div>
    </div>

    <div class="offline__footer">
        <div class="box-xs">
            <div class="offline__footer-left">
                <div class="offline__footer-copyright">
                    © 2012 - 2016 Garago.ru™
                </div>
                <a href="#" class="offline__footer-about">О проекте</a>
            </div>
            <div class="offline__footer-right">

                <div class="socsety socsety--round offline__socsety">

                    <p class="offline__socsety-text">вы найдете нас в</p>

                    <div class="socsety__list">
                        <a href="#" class="socsety__item vk">
                            <svg class="ico ico-vk">
                                <use xlink:href="/images/svg/symbol/sprite.svg#vk"></use>
                            </svg>
                        </a>
                        <a href="#" class="socsety__item tw">
                            <svg class="ico ico-tw">
                                <use xlink:href="/images/svg/symbol/sprite.svg#tw"></use>
                            </svg>
                        </a>
                        <a href="#" class="socsety__item fc">
                            <svg class="ico ico-fc">
                                <use xlink:href="/images/svg/symbol/sprite.svg#fc"></use>
                            </svg>
                        </a>
                        <a href="#" class="socsety__item ok">
                            <svg class="ico ico-ok">
                                <use xlink:href="/images/svg/symbol/sprite.svg#ok"></use>
                            </svg>
                        </a>
                        <a href="#" class="socsety__item mail">
                            <svg class="ico ico-mail">
                                <use xlink:href="/images/svg/symbol/sprite.svg#mail"></use>
                            </svg>
                        </a>
                        <a href="#" class="socsety__item instagram">
                            <svg class="ico ico-instagram" width="13" height="15">
                                <use xlink:href="/images/svg/symbol/sprite.svg#instagram"></use>
                            </svg>
                        </a>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<!-- /.wrapper -->

<form class="form-pup form-pup-select white-popup-block mfp-hide" id="select-sity" name="select-sity" action="#"
      method="post">
    <h2 class="form-pup__title">Выбран город Москва</h2>

    <div class="form-pup__search">
        <input type="text" class="form-pup__input">
        <input type="submit" class="form-pup__btn btn btn-trans" value="Найти">
    </div>

    <div class="form__body">

        <div class="city">
            <div class="city__list">
                <div class="city__column">
                    <a href="#" class="city__item">Архангельск</a>
                    <a href="#" class="city__item">Астрахань</a>
                    <a href="#" class="city__item">Барнаул</a>
                    <a href="#" class="city__item">Белгород</a>
                    <a href="#" class="city__item">Бийск</a>
                    <a href="#" class="city__item">Благовещенск</a>
                    <a href="#" class="city__item">Братск</a>
                    <a href="#" class="city__item">Брянск</a>
                    <a href="#" class="city__item">Великий Новгород</a>
                    <a href="#" class="city__item">Владивосток</a>
                    <a href="#" class="city__item">Владимир</a>
                    <a href="#" class="city__item">Волгоград</a>
                    <a href="#" class="city__item">Вологда</a>
                    <a href="#" class="city__item">Воронеж</a>
                    <a href="#" class="city__item"><b>Екатеринбург</b></a>
                    <a href="#" class="city__item">Иваново</a>

                </div>

                <div class="city__column">
                    <a href="#" class="city__item">Ижевск</a>
                    <a href="#" class="city__item">Иркутск</a>
                    <a href="#" class="city__item">Йошкар-Ола</a>
                    <a href="#" class="city__item"><b>Казань</b></a>
                    <a href="#" class="city__item">Калининград</a>
                    <a href="#" class="city__item">Калуга</a>
                    <a href="#" class="city__item">Кемерово</a>
                    <a href="#" class="city__item">Киров</a>
                    <a href="#" class="city__item">Комсомольск-на-Амуре</a>
                    <a href="#" class="city__item">Кострома</a>
                    <a href="#" class="city__item">Краснодар</a>
                    <a href="#" class="city__item">Красноярск</a>
                    <a href="#" class="city__item">Курган</a>
                    <a href="#" class="city__item">Курск</a>
                    <a href="#" class="city__item">Липецк</a>
                    <a href="#" class="city__item">Магнитогорск</a>
                </div>
                <div class="city__column">
                    <a href="#" class="city__item"><b>Москва</b></a>
                    <a href="#" class="city__item">Мурманск</a>
                    <a href="#" class="city__item">Набережные Челны</a>
                    <a href="#" class="city__item">Нижневартовск</a>
                    <a href="#" class="city__item">Нижний Новгород</a>
                    <a href="#" class="city__item">Нижний Тагил</a>
                    <a href="#" class="city__item">Новокузнецк</a>
                    <a href="#" class="city__item">Новороссийск</a>
                    <a href="#" class="city__item">Новосибирск</a>
                    <a href="#" class="city__item">Омск</a>
                    <a href="#" class="city__item">Оренбург</a>
                    <a href="#" class="city__item">Орёл</a>
                    <a href="#" class="city__item">Пенза</a>
                    <a href="#" class="city__item">Пермь</a>
                    <a href="#" class="city__item">Петрозаводск</a>
                    <a href="#" class="city__item">Псков</a>
                </div>

                <div class="city__column">
                    <a href="#" class="city__item">Ростов-на-Дону</a>
                    <a href="#" class="city__item">Рязань</a>
                    <a href="#" class="city__item"><b>Санкт-Петербург</b></a>
                    <a href="#" class="city__item"><b>Самара</b></a>
                    <a href="#" class="city__item">Саранск</a>
                    <a href="#" class="city__item">Саратов</a>
                    <a href="#" class="city__item">Смоленск</a>
                    <a href="#" class="city__item"><b>Сочи</b></a>
                    <a href="#" class="city__item">Ставрополь</a>
                    <a href="#" class="city__item">Старый Оскол</a>
                    <a href="#" class="city__item">Стерлитамак</a>
                    <a href="#" class="city__item">Сургут</a>
                    <a href="#" class="city__item">Сыктывкар</a>
                    <a href="#" class="city__item">Тамбов</a>
                    <a href="#" class="city__item">Тверь</a>
                    <a href="#" class="city__item">Тольятти</a>
                </div>
                <div class="city__column">
                    <a href="#" class="city__item">Томск</a>
                    <a href="#" class="city__item">Тула</a>
                    <a href="#" class="city__item">Тюмень</a>
                    <a href="#" class="city__item">Улан-Удэ</a>
                    <a href="#" class="city__item">Ульяновск</a>
                    <a href="#" class="city__item">Уфа</a>
                    <a href="#" class="city__item">Хабаровск</a>
                    <a href="#" class="city__item">Чебоксары</a>
                    <a href="#" class="city__item">Челябинск</a>
                    <a href="#" class="city__item">Чита</a>
                    <a href="#" class="city__item">Якутск</a>
                    <a href="#" class="city__item">Ярославль</a>
                </div>
            </div>
        </div>
    </div>
</form>

<link rel="stylesheet" href="/css/library.css">
<link rel="stylesheet" href="/css/style.css">
<script src="/js/libs.min.js"></script>
<script src="/js/common.js"></script>
</body>

</html>
