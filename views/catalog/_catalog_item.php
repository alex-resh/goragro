<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Organisation
 */

use app\modules\likes\widgets\LikesWidget;
use yii\helpers\Url;

?>
<div class="catalog__item indent">
    <div class="catalog__item-left">

        <div class="catalog__item-rate">
            <div class="rate-like">
                <?php $likes = LikesWidget::begin(['model' => $model]);?>
                <?= $likes->like(true);?>
                <?= $likes->dislike();?>
                <?php LikesWidget::end();?>
            </div><!-- /.rate-like -->
        </div>

        <div class="catalog__item-info">
            <div class="catalog__item-info-header">
                <a href="<?php echo Url::toRoute(['organisation/index', 'id' => $model->id]) ?>">
                    <h4 class="catalog__item-title"><?php echo $model->name; ?></h4>
                </a>
                <div class="favorites">
                    <i class="favorites-ico">
                        <svg class="ico ico-heart">
                            <use xlink:href="/images/svg/symbol/sprite.svg#heart"></use>
                        </svg>
                    </i>
                    <span class="favorites-text">В избранное</span>
                </div>
            </div>

            <div class="catalog__item-info-list">
                <div class="catalog__item-info-line">
                    <i class="catalog__item-ico">
                        <svg class="ico ico-location">
                            <use xlink:href="/images/svg/symbol/sprite.svg#location"></use>
                        </svg>
                    </i>
                    <div class="catalog__item-text">
                        <?php echo $model->address?$model->address->getToString():''; ?>
                    </div>
                </div>
                <div class="catalog__item-info-line">
                    <i class="catalog__item-ico">
                        <svg class="ico ico-keyboard">
                            <use xlink:href="/images/svg/symbol/sprite.svg#keyboard"></use>
                        </svg>

                    </i>
                    <div class="catalog__item-text">8 495 961-88-44</div>
                </div>
                <div class="catalog__item-info-line">
                    <i class="catalog__item-ico">
                        <svg class="ico ico-clock">
                            <use xlink:href="/images/svg/symbol/sprite.svg#clock"></use>
                        </svg>
                    </i>
                    <div class="catalog__item-text">с <?= $model->worktime->today[0];?> до <?= $model->worktime->today[1];?><span><?= $model->worktime->today[4];?></span>
                    </div>
                </div>
                <?php if($model->worktime->today[2] && $model->worktime->today[3]) : ?>
                <div class="catalog__item-info-line">
                    <div class="catalog__item-text">Перерыв с <?= $model->worktime->today[2];?> до <?= $model->worktime->today[3];?></div>
                </div>
                <?php endif;?>
                <div class="catalog__item-info-line">
                    <i class="catalog__item-ico">
                        <svg class="ico ico-arrow-link">
                            <use xlink:href="/images/svg/symbol/sprite.svg#arrow-link"></use>
                        </svg>
                    </i>
                    <div class="catalog__item-text catalog__item-link">Перейти на сайт</div>
                </div>
            </div>
        </div><!-- /.catalog__item-info -->
    </div>

    <div class="catalog__item-img catalog__item-img-dekstop">
        <img src="<?php echo $model->getThumbUploadUrl('img', 'preview') ?>" alt="">
    </div>
</div>
