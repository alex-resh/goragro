<?php

use yii\bootstrap\BootstrapAsset;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $category \app\models\Category || false
 * @var $filters \app\models\Additionally[]
 * @var $searchModel \app\models\search\OrganisationSearch
 * @var $datas \app\models\AdditionallyData[]
 */
$this->params['breadcrumbs'][] = 'Каталог';
$this->params['category'] = $category;

use yii\widgets\Block;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
BootstrapAsset::register($this);
?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]);?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_catalog_item',
        ]);?>
    </div>
</div>

<form class="form-pup form-pup-select white-popup-block mfp-hide" id="section-category" name="select-sity"
      action="#" method="post">
    <h2 class="form-pup__title">
        <?php if($category) : ?>
        Выбрана категория <?php echo $category->name; ?>
        <?php else : ?>
        Категория не выбрана
        <?php endif;?>
    </h2>

    <div class="form-pup__search">
        <input type="text" class="form-pup__input">
        <input type="submit" class="form-pup__btn btn btn-trans" value="Найти">
    </div>

    <div class="form__body">

        <div class="city">
            <div class="city__list">
                <div class="city__column">
                <?php foreach (\app\models\Category::getOrgWithCount() as $cat): ?>
                    <a href="<?php echo Url::toRoute(['catalog/index', 'category_id' =>$cat->id]) ?>" class="city__item"><?php echo '(' . $cat->c . ') ' . $cat->name ?></a>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</form>
<?php Block::begin([
    'id' => '_sidebar_catalog',
]);?>
<?= $this->render('../layouts/_sidebar_catalog', [
    'category' => $category,
    'filters' => $filters,
    'searchModel' => $searchModel,
    'datas' => $datas
]);?>
<?php Block::end();?>
