<?php
/* @var $model \app\models\Sight */
/* @var $this \yii\web\View */

\app\assets\MapsAsset::register($this);
$count_photos = count($model->gallery);
$this->registerJs(
<<<JS
ymaps.ready(init);
        var myMap7,
            myPlacemark7;

        // Инициализация карты
        function init() {

            myMap7 = new ymaps.Map("map-sights1", {
                center: [{$model->lat}, {$model->lon}],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark7 = new ymaps.Placemark(
                [{$model->lat}, {$model->lon}], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    '{$model->title}' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">{$model->getAddressStr()}</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="{$model->getMainPhoto()}" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">{$count_photos} фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap7.geoObjects.add(myPlacemark7);
        }
JS
);
