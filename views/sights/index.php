<?php
/* @var $model \app\models\Sight */
/* @var $this \yii\web\View */


use app\modules\likes\widgets\LikesWidget;
use yii\helpers\Url;

$this->params['title'] = 'Достопримечательности';
$this->params['name'] = $model->title;
echo $this->render('_map', ['model' => $model]);
?>
<div class="post indent">
    <div class="post__rate">
        <div class="rate-like">
            <?php $likes = LikesWidget::begin(['model' => $model]);?>
            <?= $likes->like(true);?>
            <?= $likes->dislike();?>
            <?php LikesWidget::end();?>
        </div>
    </div><!-- /.post__rate -->

    <div class="post__content">
        <h3 class="post__title title-post"><?php echo $model->title ?></h3>

        <div class="post__actions post-actions">
            <div class="post-actions__left">
                <a href="#" class="post-actions__comments">
                                <span class="post-actions__comments-ico">
                                    <svg class="ico ico-comments"><use
                                            xlink:href="images/svg/symbol/sprite.svg#comments"></use></svg>
                                </span>
                    <span class="post-actions__comments-count"><?php echo $model->getCommentsCount(); ?></span>
                </a>
                <a href="#" class="post-actions__view">
                                <span class="post-actions__view-ico">
                                    <svg class="ico ico-view"><use
                                            xlink:href="images/svg/symbol/sprite.svg#view"></use></svg>
                                </span>
                    <span class="post-actions__view-count"><?php echo $model->views_count; ?></span>
                </a>
            </div>
            <div class="post-actions__right">
                <a href="#" class="favorites">
                    <i class="favorites-ico">
                        <svg class="ico ico-heart">
                            <use xlink:href="images/svg/symbol/sprite.svg#heart"></use>
                        </svg>
                    </i>
                    <span class="favorites-text">В избранное</span>
                </a>
            </div>
        </div><!-- /.post__actions -->

        <div class="post__img">
            <img src="<?php echo $model->getMainPhoto() ?>" alt="">
        </div>

        <?php echo $model->text; ?>

    </div><!-- /.post__content -->

    <div class="post__autor autor">

        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__avatar">
            <div class="autor__avatar-wrapper">
                <img src="<?php echo $model->user->profile->getThumbUploadUrl('img', 'small') ?>" alt="">
            </div>
        </a>
        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__name"><?php echo $model->user->profile->name?$model->user->profile->name:$model->user->username ?></a>
        <div class="autor__date">
            <div class="autor__date-day"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y') ?></div>
            <div class="autor__date-time"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:H:i') ?></div>
        </div>
    </div>
</div><!-- /.post -->

<div class="post-location indent">
    <div class="post-location__wrapper">
        <div class="post-location__ico">
            <svg class="ico ico-location">
                <use xlink:href="images/svg/symbol/sprite.svg#location"></use>
            </svg>
        </div>
        <div class="post-location__info">
            <div class="post-location__info-line"><?php echo $model->getAddressStr(); ?></div>
            <div class="post-location__info-line">GPS <?php echo $model->lon>0?'N':'S'; ?> <?php echo abs($model->lon); ?> <?php echo $model->lat>0?'E':'W'; ?> <?php echo abs($model->lat); ?></div>
        </div>
    </div>

</div>

<div class="post-anounce post-anounce--inside post-anounce--sights gallery-list">

    <div class="post-anounce__line">
        <div class="post-anounce__column post-anounce__column--big">
            <div class="post-anounce__item">
                <div class="ymap-container">
                    <div id="map-sights1" class="map"></div>
                </div>
            </div>
        </div>

        <div class="post-anounce__column">
            <?php if(isset($model->gallery[0])): ?>
            <a href="<?php echo $model->gallery[0]->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item-gallery"
               style="background-image: url('<?php echo $model->gallery[0]->getThumbUploadUrl('img', 'preview') ?>');">
                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                            <svg class="ico ico-search-round">
                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                            </svg>
                        </span>
            </a>
            <?php endif; ?>
            <?php if(isset($model->gallery[1])): ?>
                <a href="<?php echo $model->gallery[1]->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('<?php echo $model->gallery[1]->getThumbUploadUrl('img', 'preview') ?>');">
                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                            <svg class="ico ico-search-round">
                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                            </svg>
                        </span>
                </a>
            <?php endif; ?>
        </div>

        <div class="post-anounce__column">
            <?php if(isset($model->gallery[2])): ?>
                <a href="<?php echo $model->gallery[2]->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('<?php echo $model->gallery[2]->getThumbUploadUrl('img', 'preview') ?>');">
                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                            <svg class="ico ico-search-round">
                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                            </svg>
                        </span>
                </a>
            <?php endif; ?>

            <?php if(isset($model->gallery[3])): ?>
                <?php if(count($model->gallery) > 4): ?>
                    <a href="<?php echo $model->gallery[3]->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item--more"
                       style="background-image: url('<?php echo $model->gallery[3]->getUploadUrl('img', 'preview') ?>');">
                        <div class="post-anounce__info post-anounce__info--flex">

                            <div class="post-anounce__text">
                                <div class="post-anounce__label">
                                    еще фото и видео
                                </div>
                                всего <?php echo count($model->gallery) - 4; ?>
                            </div>
                        </div>
                    </a>
                <?php else: ?>
                    <a href="<?php echo $model->gallery[3]->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item-gallery"
                       style="background-image: url('<?php echo $model->gallery[3]->getThumbUploadUrl('img', 'preview') ?>');">
                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                            <svg class="ico ico-search-round">
                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                            </svg>
                        </span>
                    </a>
                <?php endif; ?>
            <?php endif; ?>

        </div>
    </div>

    <div class="post-anounce__more-wrapper">

        <div class="post-anounce__line">
            <?php foreach (array_slice($model->gallery, 4) as $item): ?>
            <a href="<?php echo $item->getUploadUrl('img', 'preview') ?>" class="post-anounce__item post-anounce__item-gallery"
               style="background-image: url('<?php echo $item->getThumbUploadUrl('img', 'preview') ?>');">
                            <span class="post-anounce__ico post-anounce__ico--magnifier">
                                <svg class="ico ico-search-round">
                                    <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                </svg>
                            </span>
            </a>
            <?php endforeach; ?>
        </div><!-- /.post-anounce__line -->
    </div><!-- /.post-anounce__more-wrapper -->

</div><!-- /.post-anounce -->

<div class="post-navigate indent">

    <div class="soc-sety post-navigate__soc">
        <div class="addthis_inline_share_toolbox"></div>
    </div><!-- /.soc-sety -->

    <div class="post-nav">
        <div class="post-nav__arrows">
            <?php $prev = $model->getPrev(); ?>
            <?php if($prev): ?>
                <a href="<?php echo Url::toRoute(['/sights/index', 'id' => $prev->id]) ?>" class="post-nav__item prev">
                    <svg class="ico ico-arrow-big">
                        <use xlink:href="/images/svg/symbol/sprite.svg#arrow-left"></use>
                    </svg>
                </a>
            <?php endif; ?>
            <?php $next = $model->getNext(); ?>
            <?php if($next): ?>
                <a href="<?php echo Url::toRoute(['/sights/index', 'id' => $next->id]) ?>" class="post-nav__item next">
                    <svg class="ico ico-arrow-big">
                        <use xlink:href="/images/svg/symbol/sprite.svg#arrow-left"></use>
                    </svg>
                </a>
            <?php endif; ?>
        </div>
        <?php if($next): ?>
            <div class="post-nav__link">
                <span class="post-nav__label">далее: </span>
                <a href="<?php echo Url::toRoute(['/sights/index', 'id' => $next->id]) ?>" class="post-nav__link-title"><?php echo $next->title ?>...</a>
            </div>
        <?php endif; ?>
    </div>

</div>

<?php echo \app\components\Comment::widget([
    'model' => $model,
    'commentView' => '@app/views/comments/index',
    'maxLevel' => 2,
    'relatedTo' => 'Достопримечательности'
]); ?>

<div class="page-additional-mobile page-mobile"></div>
