<?php
/**
 * @var $form ActiveForm
 * @var $pasForm string
 */

use app\widgets\Alert;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="inputpage inputpage--signin">

    <div class="inputpage__left">

        <div class="signin">

            <div class="signin__wrapper">

                <a href="/" class="back-link">
                        <span class="back-link__ico">
                            <svg class="ico ico-arrow-left">
                                <use xlink:href="/images/svg/symbol/sprite.svg#arrow-left"></use>
                            </svg>
                        </span>
                    <span class="back-link__text">Перейти на главную</span>
                </a>

                <a href="#" class="signin__logo logo">
                    <img src="/images/logo-white.svg" alt="">
                </a>

                <?php $form = ActiveForm::begin([
                    'id' => 'form-signin',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                    'validateOnType' => false,
                    'validateOnChange' => false,
                    'options' => [
                        'class' => 'form form-signin form-input'
                    ]
                ]) ?>

                <div class="form__row">
                    <?php echo Html::activeTextInput($model, 'login', ['placeholder' => 'Логин', 'class' => 'form__input', 'required' => true]) ?>
                </div>

                <div class="form__row">
                    <?php echo Html::activePasswordInput($model, 'password', ['placeholder' => 'Пароль', 'class' => 'form__input', 'required' => true]) ?>
                </div>

                <div class="form__row form__row-col">
                    <!--<a href="#" class="signin__text">Восстановление пароля</a>-->
                    <?= Html::a( 'Восстановление пароля', ['/user/forgot-password'], ['class' => 'signin__text']);?>
                    <?php echo Html::submitInput('Войти', ['class' => 'signin__btn btn btn-accent']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <div class="socsety socsety--round socsety--big">


                    <div class="socsety__line">
                        <span class="socsety__line-hr"></span>
                        <span class="socsety__line-text">войти через</span>
                        <span class="socsety__line-hr"></span>
                    </div>
                    <div class="socsety__list">
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'vkontakte']) ?>" class="socsety__item vk">
                            <svg class="ico ico-vk">
                                <use xlink:href="/images/svg/symbol/sprite.svg#vk"></use>
                            </svg>
                        </a>
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'yandex']) ?>" class="socsety__item yandex">
                            <svg class="ico ico-yandex">
                                <use xlink:href="/images/svg/symbol/sprite.svg#yandex"></use>
                            </svg>
                        </a>
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'facebook']) ?>" class="socsety__item fc">
                            <svg class="ico ico-fc">
                                <use xlink:href="/images/svg/symbol/sprite.svg#fc"></use>
                            </svg>
                        </a>
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'odnoklassniki']) ?>" class="socsety__item ok">
                            <svg class="ico ico-ok">
                                <use xlink:href="/images/svg/symbol/sprite.svg#ok"></use>
                            </svg>
                        </a>
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'mailru']) ?>" class="socsety__item mail">
                            <svg class="ico ico-mail">
                                <use xlink:href="/images/svg/symbol/sprite.svg#mail"></use>
                            </svg>
                        </a>
                        <a href="<?php echo Url::toRoute(['/site/eauth', 'service' => 'instagram']) ?>" class="socsety__item instagram">
                            <svg class="ico ico-instagram" width="13" height="15">
                                <use xlink:href="/images/svg/symbol/sprite.svg#instagram"></use>
                            </svg>
                        </a>
                    </div>

                </div><!-- /.socsety -->
            </div><!-- /.signin__wrapper -->

        </div><!-- /.signin -->

    </div><!-- /.inputpage__left -->

    <div class="inputpage__right">
        <?= Alert::widget();?>
        <div class="form-gray__wrapper">
            <?= $pasForm;?>
        </div><!-- /.form-gray__wrapper -->

    </div><!-- /inputpage__right -->

</div><!-- /.inputpage__wrapper -->