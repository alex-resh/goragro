<?php

/**
 * @var $
 */

use yii\bootstrap\BootstrapAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

BootstrapAsset::register($this);
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-registration',
    'action' => \yii\helpers\Url::to(['registration/register']),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'form form-gray form-gray--registration form-registration'
    ]
]); ?>
<h2 class="form__title title-md">Регистрация</h2>
<div class="form__row">

    <label class="form__label">
                            <span class="form__label-title">
                                Логин
                                <span class="form__star">*</span>
                            </span>
    </label>

    <div class="form__row-col">
        <?php echo Html::activeTextInput($model, 'username', ['class' => 'form__input', 'required' => true, 'placeholder' => 'Логин']) ?>

        <div class="form-question">
            <a href="#" class="form__question js-form-question">?</a>
            <div class="help-warning">
                Это позволит вам получить именно то, что вам нужно!
            </div>
        </div>
    </div>
    <?= Html::error($model, 'username', ['class' => 'text-danger', 'style' => ['opacity' => 0.7, 'padding' => '3px 10px']]);?>

</div>
<div class="form__row">
    <label class="form__label">
                            <span class="form__label-title">
                                E-mail
                                <span class="form__star">*</span>
                            </span>
        <span class="form__tip">example@domain.ru</span>
    </label>

    <div class="form__row-col">
        <?php echo Html::activeTextInput($model, 'email', ['class' => 'form__input', 'required' => true, 'placeholder' => 'E-mail']) ?>


        <div class="form-question">
            <a href="#" class="form__question js-form-question">?</a>
            <div class="help-warning">
                Это позволит вам получить именно то, что вам нужно!
            </div>
        </div>
    </div>
    <?= Html::error($model, 'email', ['class' => 'text-danger', 'style' => ['opacity' => 0.7, 'padding' => '3px 10px']]);?>

</div>
<div class="form__row">
    <label class="form__label">
                            <span class="form__label-title">
                                Пароль
                                <span class="form__star">*</span>
                            </span>
    </label>

    <div class="form__row-col">
        <?php echo Html::activePasswordInput($model, 'password', ['class' => 'form__input', 'required' => true, 'placeholder' => '********']) ?>

        <div class="form-question">
            <a href="#" class="form__question js-form-question">?</a>
            <div class="help-warning">
                Это позволит вам получить именно то, что вам нужно!
            </div>
        </div>
    </div>
    <?= Html::error($model, 'password', ['class' => 'text-danger', 'style' => ['opacity' => 0.7, 'padding' => '3px 10px']]);?>

</div>

<div class="form__row">
    <input type="submit" class="signin__btn signin__registration btn btn-accent" value="Зарегистрироваться">
</div>

<?php ActiveForm::end() ?>
