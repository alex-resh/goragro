<?php

/**
 * @var $
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<h2 class="form__title title-md">Восстановление прароля</h2>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'form form-gray form-gray--registration form-registration'
    ]
]); ?>
<div class="form__row">
    <label class="form__label">
                            <span class="form__label-title">
                                E-mail
                                <span class="form__star">*</span>
                            </span>
        <span class="form__tip">example@domain.ru</span>
    </label>

    <div class="form__row-col">
        <?php echo Html::activeTextInput($model, 'email', ['class' => 'form__input', 'required' => true, 'placeholder' => 'E-mail']) ?>

        <div class="form-question">
            <a href="#" class="form__question js-form-question">?</a>
            <div class="help-warning">
                Это позволит вам получить именно то, что вам нужно!
            </div>
        </div>
    </div>

</div>

<div class="form__row">
    <input type="submit" class="signin__btn signin__registration btn btn-accent" value="Отправить запрос">
</div>

<?php ActiveForm::end() ?>
