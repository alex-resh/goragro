<?php
/* @var $this \yii\web\View */
/* @var $city \app\models\FsCity */
/* @var $orgsnisations \app\models\Organisation[] */
\app\assets\MapsAsset::register($this);

$coords = [];
$names = [];
$ids = [];
foreach ($orgsnisations as $organisation) {
    $coords[] = '[' . $organisation->address->lat . ',' . $organisation->address->lon . ']';
    $names[] = '"' . $organisation->name . '"';
    $ids[] = '"' . $organisation->id . '"';
}
$coords = implode(',', $coords);
$names = implode(',', $names);
$ids = implode(',', $ids);

$this->registerJs(
    <<<JS
ymaps.ready(function () {
    var myMap = new ymaps.Map('map-category', {
            center: [44.948237, 34.100318],
            zoom: 9,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),
        
        
        
        
        /**
         * Создадим кластеризатор, вызвав функцию-конструктор.
         * Список всех опций доступен в документации.
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer.xml#constructor-summary
         */
        clusterer = new ymaps.Clusterer({
            /**
             * Через кластеризатор можно указать только стили кластеров,
             * стили для меток нужно назначать каждой метке отдельно.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage.xml
             */
            preset: 'islands#invertedVioletClusterIcons',
            /**
             * Ставим true, если хотим кластеризовать только точки с одинаковыми координатами.
             */
            groupByCoordinates: false,
            /**
             * Опции кластеров указываем в кластеризаторе с префиксом "cluster".
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ClusterPlacemark.xml
             */
            clusterDisableClickZoom: true,
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false,
            clusterIcons: [{
                        href: 'images/icons/cluster.svg',
                        size: [32, 32],
                        offset: [-16, -32]
                    }]
        }),
        /**
         * Функция возвращает объект, содержащий данные метки.
         * Поле данных clusterCaption будет отображено в списке геообъектов в балуне кластера.
         * Поле balloonContentBody - источник данных для контента балуна.
         * Оба поля поддерживают HTML-разметку.
         * Список полей данных, которые используют стандартные макеты содержимого иконки метки
         * и балуна геообъектов, можно посмотреть в документации.
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObject.xml
         */
        getPointData = function (index) {
            return {
                balloonContentHeader: '',
                balloonContentBody: '',
                balloonContentFooter: '',
                clusterCaption: names[index],
                dataId: ids[index],
                openEmptyBalloon: true,
            };
        },
        /**
         * Функция возвращает объект, содержащий опции метки.
         * Все опции, которые поддерживают геообъекты, можно посмотреть в документации.
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObject.xml
         */
        getPointOptions = function () {
            return {
                iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker-full.svg',
                    iconImageSize: [22, 30],
                    iconImageOffset: [-11, -30],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false,
                    preset: 'islands#orangeIcon'
            };
        },
        points = [
            {$coords}
        ],
        names = [
            {$names}
        ],
        ids = [
            {$ids}
        ],
        geoObjects = [];

    /**
     * Данные передаются вторым параметром в конструктор метки, опции - третьим.
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark.xml#constructor-summary
     */
    for(var i = 0, len = points.length; i < len; i++) {
        geoObjects[i] = new ymaps.Placemark(points[i], getPointData(i), getPointOptions());
        geoObjects[i].events.add('click', function(event){
            var obj = event.get('target');
            obj.properties.set({
                    balloonContentBody: 'Загрузка...'
                });
            $.get('/organisation/ajax?id=' + obj.properties.get('dataId'), {}, function(result){
                obj.properties.set({
                    balloonContentBody: result
                });
            });
           
        });
    }
    

    /**
     * Можно менять опции кластеризатора после создания.
     */
    clusterer.options.set({
        gridSize: 80,
        clusterDisableClickZoom: true,
        //openBalloonOnClick: false
    });
    clusterer.events.add('click', function (e) {
                // получаем ссылку на объект, по которому кликнули
                var cluster = e.get('target');//.state.get('activeObject')
                    // включаем монитор слежения по переключению clusterCaption в балуне кластера
                    var stateMonitor = new ymaps.Monitor(cluster.state);
                    stateMonitor.add('activeObject', function(obj) {
                        obj.properties.set({
                            balloonContentBody: 'Загрузка...'
                        });
                        $.get('/organisation/ajax?id=' + obj.properties.get('dataId'), {}, function(result){
                            obj.properties.set({
                                balloonContentBody: result
                            });
                        });
                    });

            });
    /**
     * В кластеризатор можно добавить javascript-массив меток (не геоколлекцию) или одну метку.
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Clusterer.xml#add
     */
    clusterer.add(geoObjects);
    myMap.geoObjects.add(clusterer);

    /**
     * Спозиционируем карту так, чтобы на ней были видны все объекты.
     */

    myMap.setBounds(clusterer.getBounds(), {
        checkZoomRange: true
    });
    
    
});
JS
);

?>
<div class="ymap-container">
    <div id="map-category" class="map"></div>
</div>

<div class="fullmap__wrapper">

    <div class="search-map">
        <div class="search-map__top">
            <div class="search-map__count">
                <b><?php echo count($orgsnisations) ?></b> организаций на карте
            </div>
            <a href="<?php echo \yii\helpers\Url::to(['organisation/create']) ?>" class="btn btn-secondary">Добавить
                организацию</a>
        </div>
        <div class="search-map__section">
            <form action="#" id="search-map__form" name="search-map__form" class="search-map__form">

                <div class="search-map__input">
                    <input type="search" class="search-map__field" placeholder="Услуги, товары, организации">
                    <button type="submit" class="btn search-map__btn">
                        <i class="ico ico-search"></i>
                    </button>
                </div>

            </form>

            <div class="search-map__label">Выбрать категорию</div>

            <div class="search-map__list mCustomScrollbar _mCS_1">
                <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;"
                     tabindex="0">
                    <div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px;"
                         dir="ltr">

                        <?php foreach (\app\models\Category::getOrgWithCount() as $category): ?>
                            <div class="search-map__item">

                                <div class="search-map__header">
                                    <svg class="ico ico-arrow-bottom">
                                        <use xlink:href="#arrow-bottom"></use>
                                    </svg>
                                    <a href="<?php echo \yii\helpers\Url::to(['maps/index', 'category_id' => $category->id]) ?>"><?php echo $category->name ?> <?php echo $category->c ?></a>
                                </div><!-- /.search-map__header -->

                            </div><!-- /.search-map__item -->
                        <?php endforeach; ?>


                    </div>
                    <div id="mCSB_1_scrollbar_vertical"
                         class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical"
                         style="display: block;">
                        <div class="mCSB_draggerContainer">
                            <div id="mCSB_1_dragger_vertical" class="mCSB_dragger"
                                 style="position: absolute; min-height: 12px; display: block; height: 43px; max-height: 165px; top: 0px;">
                                <div class="mCSB_dragger_bar" style="line-height: 12px;"></div>
                            </div>
                            <div class="mCSB_draggerRail"></div>
                        </div>
                    </div>
                </div>
            </div><!-- /.search-map__list -->

        </div><!-- /.search-map__section -->
    </div><!-- /.search-map -->

</div>