<?php
/** @var $dataProvider \yii\data\ActiveDataProvider  */
/** @var $searchModel \app\models\search\NewsSearch */
/** @var $this \yii\web\View */
use yii\widgets\ListView;
use yii\widgets\LinkPager;
use yii\helpers\Url;

?>

<div class="inside__top indent">
    <h2 class="inside__title">Новости</h2>
</div>

<ul class="breadcrumbs indent">
    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Главная</a></li>
    <li class="breadcrumbs__item breadcrumbs__item--active">Новости</li>
</ul>

<div class="post-anounce post-anounce--news">

    <div class="post-anounce__line">
        <div class="post-anounce__column post-anounce__column--big">
            <?php if(isset($news[0])): ?>
            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[0]->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $news[0]->getThumbUploadUrl('img', 'preview') ?>');">
                <div class="post-anounce__info">
                    <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($news[0]->created_at, 'php: d/m Y') ?></div>
                    <span class="post-anounce__title"><?php echo $news[0]->title ?></span>
                </div>
            </a>
            <?php endif; ?>
        </div>
        <div class="post-anounce__column">
            <?php if(isset($news[1])): ?>
            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[1]->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $news[1]->getThumbUploadUrl('img', 'preview') ?>');">
                <div class="post-anounce__info">
                    <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($news[1]->created_at, 'php: d/m Y') ?></div>
                    <span class="post-anounce__title"><?php echo $news[1]->title ?></span>
                </div>
            </a>
            <?php endif; ?>
            <?php if(isset($news[2])): ?>
            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[2]->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $news[2]->getThumbUploadUrl('img', 'preview') ?>');">
                <div class="post-anounce__info">
                    <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($news[2]->created_at, 'php: d/m Y') ?></div>
                    <span class="post-anounce__title"><?php echo $news[2]->title ?></span>
                </div>
            </a>
            <?php endif; ?>
        </div>
        <div class="post-anounce__column">
            <?php if(isset($news[3])): ?>
            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[3]->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $news[3]->getThumbUploadUrl('img', 'preview') ?>');">
                <div class="post-anounce__info">
                    <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($news[3]->created_at, 'php: d/m Y') ?></div>
                    <span class="post-anounce__title"><?php echo $news[3]->title ?></span>
                </div>
            </a>
            <?php endif; ?>
            <?php if(isset($news[4])): ?>
            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[4]->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $news[4]->getThumbUploadUrl('img', 'preview') ?>');">
                <div class="post-anounce__info">
                    <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($news[4]->created_at, 'php: d/m Y') ?></div>
                    <span class="post-anounce__title"><?php echo $news[4]->title ?></span>
                </div>
            </a>
            <?php endif; ?>
        </div>
    </div>

</div>

<div class="news-wrapper indent">
    <h2 class="title-md">Не менее интересные новости</h2>

    <div class="news-list">
      <?php  echo ListView::widget([
        'dataProvider' => $dataProvider,
          'itemView' => '_item',
          'layout' => '{items}',
          ]);
        ?>


    </div><!-- /.news-list -->
    <div class="pagination">
        <div class="pagination__container indent">
            <?php echo LinkPager::widget([
                'pagination' => $dataProvider->pagination,
                'options' => ['class'=>'pagination__list'],
                'linkOptions' => [
                    'class' => 'pagination__link',
                ],
                'linkContainerOptions' => [
                    'class' => 'pagination__item',
                ],
                'activePageCssClass' => 'pagination__item--active',
                'prevPageCssClass' =>  'pagination__prev',
                'nextPageCssClass' => 'pagination__next',
                'prevPageLabel' => 'Предыдущая',
                'nextPageLabel' => 'Следующая',
                'maxButtonCount' => 5,
            ]) ?>
        </div>
    </div>
</div><!-- /.news-wrapper -->

