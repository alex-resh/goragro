
<a href="<?php echo \yii\helpers\Url::toRoute(['news/view', 'id' => $model->id]) ?>" class="news">
            <div class="news__container"></div>
            <div class="news__img">
                <img src="<?php echo $model->getThumbUploadUrl('img', 'preview') ?>" alt="">
                <span class="news__label"><?php echo $model->category->name ?></span>
</div>
<div class="news__info">
    <p class="news__date"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php: d/m Y') ?></p>
    <h5 class="news__title"><?php echo $model->title ?></h5>
    <p class="news__text text">
        <?php echo mb_substr($model->text, 0, 300) ?>
    </p>
</div>
</a><!-- /.news -->