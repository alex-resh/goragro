<?php
/* @var $model \app\models\News */
/* @var $news \app\models\News[] */
use yii\helpers\Url;
use app\modules\likes\widgets\LikesWidget;
?>
    <div class="inside__top indent">
        <h2 class="inside__title">Новости</h2>
    </div>
<div class="post indent">
    <div class="post__rate">
        <?php $likes = LikesWidget::begin(['model' => $model]) ?>
        <div class="rate-like">
            <?php echo $likes->like(true) ?>
            <?php echo $likes->dislike() ?>
        </div>
        <?php LikesWidget::end() ?>
    </div><!-- /.post__rate -->

    <div class="post__content">
        <a href="#" class="post__title"><?php echo $model->title; ?></a>
        <div class="post__actions post-actions">
            <div class="post-actions__left">
                <a href="#" class="post-actions__comments">
                                <span class="post-actions__comments-ico">
                                    <svg class="ico ico-comments"><use xlink:href="/images/svg/symbol/sprite.svg#comments"></use></svg>
                                </span>
                    <span class="post-actions__comments-count"><?php echo $model->getCommentsCount() ?></span>
                </a>
                <a href="#" class="post-actions__view">
                                <span class="post-actions__view-ico">
                                    <svg class="ico ico-view"><use xlink:href="/images/svg/symbol/sprite.svg#view"></use></svg>
                                </span>
                    <span class="post-actions__view-count"><?php echo $model->views_count ?></span>
                </a>
            </div>
            <div class="post-actions__right">
                <a href="#" class="favorites">
                    <i class="favorites-ico">
                        <svg class="ico ico-heart">
                            <use xlink:href="/images/svg/symbol/sprite.svg#heart"></use>
                        </svg>
                    </i>
                    <span class="favorites-text">В избранное</span>
                </a>
            </div>
        </div><!-- /.post__actions -->

        <div class="post__img">
            <img src="<?php echo $model->getThumbUploadUrl('img', 'preview') ?>" alt="">
        </div>

        <?php echo $model->text; ?>

        <div class="post__tag tag">
            <?php foreach ($model->getTagsList() as $tag): ?>
            <a href="<?php echo Url::toRoute(['/news', 'tag' => $tag]) ?>" class="tag__item"><?php echo $tag; ?></a>
            <?php endforeach; ?>
        </div>
    </div><!-- /.post__content -->

    <div class="post__autor autor">

        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__avatar">
            <div class="autor__avatar-wrapper">
                <img src="<?php echo $model->user->profile->getThumbUploadUrl('img', 'small') ?>" alt="">
            </div>
        </a>
        <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $model->user->id]) ?>" class="autor__name"><?php echo $model->user->profile->name?$model->user->profile->name:$model->user->username ?></a>
        <div class="autor__date">
            <div class="autor__date-day"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y') ?></div>
            <div class="autor__date-time"><?php echo Yii::$app->formatter->asDate($model->created_at, 'php:H:i') ?></div>
        </div>
    </div>
</div>
<div class="post-navigate indent">

    <div class="soc-sety post-navigate__soc">

        <div class="addthis_inline_share_toolbox"></div>
    </div><!-- /.soc-sety -->

    <div class="post-nav">
        <div class="post-nav__arrows">
            <?php $prev = $model->getPrev(); ?>
            <?php if($prev): ?>
            <a href="<?php echo Url::toRoute(['/news/view', 'id' => $prev->id]) ?>" class="post-nav__item prev">
                <svg class="ico ico-arrow-big">
                    <use xlink:href="/images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </a>
            <?php endif; ?>
            <?php $next = $model->getNext(); ?>
            <?php if($next): ?>
            <a href="<?php echo Url::toRoute(['/news/view', 'id' => $next->id]) ?>" class="post-nav__item next">
                <svg class="ico ico-arrow-big">
                    <use xlink:href="/images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </a>
            <?php endif; ?>
        </div>
        <?php if($next): ?>
        <div class="post-nav__link">
            <span class="post-nav__label">далее: </span>
            <a href="<?php echo Url::toRoute(['/news/view', 'id' => $next->id]) ?>" class="post-nav__link-title"><?php echo $next->title ?>...</a>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if($news): ?>
<div class="post-anounce post-anounce--news post-anounce--line indent">
    <div class="post-anounce__line">
        <?php foreach ($news as $item): ?>
        <a href="<?php echo Url::toRoute(['/news/view', 'id' => $item->id]) ?>" class="post-anounce__item" style="background-image: url('<?php echo $item->getThumbUploadUrl('img', 'preview') ?>');">
            <div class="post-anounce__info">
                <div class="post-anounce__date"><?php echo Yii::$app->formatter->asDate($item->created_at, 'php:d-m-Y, H:i') ?></div>
                <span class="post-anounce__title"><?php echo $item->title ?></span>
            </div>
        </a>
        <?php endforeach; ?>
    </div><!-- /.post-anounce__line -->

</div>
<?php endif; ?>
<?php echo \app\components\Comment::widget([
    'model' => $model,
    'commentView' => '@app/views/comments/index',
    'maxLevel' => 2,
    'relatedTo' => 'Новости'
]); ?>