<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\NewsCategory;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

$template = "{label}\n{input}\n{error}";

?>
<?php $form = ActiveForm::begin() ?>
<?php echo $form->field($model, 'title', [
    'template' => $template,
    'labelOptions' => [
        'class' => 'form__label'
    ],
    'options' =>[
        'class' => 'form__row'
    ]
])->textInput(['class' => 'form__input form__field']) ?>

<?php echo $form->field($model, 'category_id', [
    'template' => $template,
    'labelOptions' => [
        'class' => 'form__label'
    ],
    'options' =>[
        'class' => 'form__row'
    ]
])->dropDownList(ArrayHelper::map(NewsCategory::find()->all(), 'id', 'name'), [
    'prompt' => '--Выбрать--',
    'class' => 'select select_full'
]) ?>

<?php echo $form->field($model, 'text', [
    'template' => $template,
    'labelOptions' => [
        'class' => 'form__label'
    ],
    'options' =>[
        'class' => 'form__row'
    ]
])->widget(\dosamigos\tinymce\TinyMce::class) ?>
<?php if (!$model->isNewRecord): ?>
    <?php echo Html::img($model->getThumbUploadUrl('img', 'preview')) ?>
<?php endif; ?>
<?php echo $form->field($model, 'img',['template' => $template,
    'labelOptions' => [
        'class' => 'form__label'
    ],
    'options' =>[
        'class' => 'form__row'
    ]
])->fileInput() ?>
    <div class="form-group">
        <?php echo Html::submitInput('Отправить запрос', ['class' => 'consent-company__btn consent-company__sumbit']) ?>
    </div>

<?php ActiveForm::end(); ?>