<?php
use yii\helpers\Html;

$weather = Yii::$app->weather->getWeatherToday(Yii::$app->city->city->name_en);
?>
<div class="intro">

    <div class="intro__wrapper intro__weather-wrapper box">

        <div class="intro__left indent">
            <div class="intro__weather-info weather-info">
                <div class="weather-info__title">
                    Погода в <?php echo Yii::$app->city->city->name_ru_pred; ?>
                </div>
                <div class="weather-info__current">Сейчас <?php echo Yii::$app->formatter->asDate(time(), 'php:H:i')?></div>
                <div class="weather-info__data">
                    <div class="weather-info__data-info">
                        <div class="weather-info__data-ico">
                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass(Yii::$app->weather->getPrognosToday(Yii::$app->city->city->name_en), 'day')]);?>
                        </div>
                        <?php echo $weather->temp_max ?>°
                    </div>
                    <div class="weather-info__data-text"><?php echo Yii::$app->weather->getCondition($weather->condition) ?></div>
                </div>
            </div>
        </div><!-- /.intro__left -->

        <div class="intro__right">

            <div class="weather-table">
                <div class="weather-table__icons">
                    <svg class="ico ico-geo">
                        <use xlink:href="\images/svg/symbol/sprite.svg#geo"></use>
                    </svg>
                    <svg class="ico ico-wave">
                        <use xlink:href="\images/svg/symbol/sprite.svg#wave"></use>
                    </svg>

                </div>
                <div class="weather-table__wrapper">
                    <div class="weather-table__line">
                        <div class="weather-table__key">Влажность</div>
                        <div class="weather-table__val"><?php echo $weather->humidity ?>%</div>
                    </div>
                    <div class="weather-table__line">
                        <div class="weather-table__key">Давление</div>
                        <div class="weather-table__val"><?php echo $weather->pressure_mm ?> мм</div>
                    </div>
                    <div class="weather-table__line">
                        <div class="weather-table__key">Ветер <?php echo Yii::$app->weather->getWind($weather->wind_dir) ?></div>
                        <div class="weather-table__val"><?php echo $weather->wind_speed ?> м/с</div>
                    </div>
                    <?php if($water = Yii::$app->weather->getWater(Yii::$app->city->city->name_en)): ?>
                    <div class="weather-table__line">
                        <div class="weather-table__key">Вода</div>
                        <div class="weather-table__val"><?php echo $water ?>°</div>
                    </div>
                    <?php endif; ?>
                    <div class="weather-table__line">
                        <div class="weather-table__key">Восход</div>
                        <div class="weather-table__val"><?php echo Yii::$app->weather->getSun(Yii::$app->city->city->name_en)['start'] ?></div>
                    </div>
                    <div class="weather-table__line">
                        <div class="weather-table__key">Закат</div>
                        <div class="weather-table__val"><?php echo Yii::$app->weather->getSun(Yii::$app->city->city->name_en)['end'] ?></div>
                    </div>
                </div>

            </div>

        </div><!-- /.intro__right -->

    </div><!-- /.intro__wrapper -->

</div><!-- /.intro -->