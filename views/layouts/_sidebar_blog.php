<?php

use yii\helpers\Url;

?>
<div class="aside__top">
    <div class="inside__counter">
        <i class="inside__counter-ico">
            <i class="ico ico-scale"></i>
        </i>
        <p class="inside__counter-info"><b><?php echo \app\models\News::getCountByCity(null, \app\models\News::TYPE_BLOG)?></b> блогов на сайте</p>
    </div><!-- /.inside__counter -->

    <a href="#" class="back">
        <div class="back__link">Перейти на главную</div>
        <div class="back__ico">
            <svg class="ico ico-arrow-left">
                <use xlink:href="images/svg/symbol/sprite.svg#arrow-left"></use>
            </svg>
        </div>
    </a>
</div>

<div class="aside__body">
    <div class="top-autors">
        <h2 class="title-xs">Топ авторов</h2>

        <div class="autor-table">
            <?php foreach (\app\models\News::getTopAuthors() as $user): ?>
            <div class="autor-table__item">
                <div class="autor-table__img">
                    <img src="<?php echo $user->profile->getThumbUploadUrl('img', 'preview') ?>" alt="">
                </div>
                <a href="<?php echo \yii\helpers\Url::toRoute(['/profile', 'id' => $user->id]) ?>" class="autor-table__info"><?php echo $user->name ?></a>
            </div>
            <?php endforeach; ?>
        </div><!-- /.autor-table -->
    </div>
    <a href="<?php echo Url::toRoute('/blog/create') ?>" class="btn-add">
                    <span class="btn-add__ico">
                        <svg class="ico ico-plus">
                            <use xlink:href="images/svg/symbol/sprite.svg#plus"></use>
                        </svg>
                    </span>
        <span class="btn-add__text">Написать в блог</span>
    </a>

    <div class="app app--white">
        <div class="app__text">Наше приложение</div>
        <a href="#" class="app__apple app__item">
            <svg class="ico ico-apple">
                <use xlink:href="images/svg/symbol/sprite.svg#apple"></use>
            </svg>
        </a>
        <a href="#" class="app__android app__item">
            <svg class="ico ico-android">
                <use xlink:href="images/svg/symbol/sprite.svg#android"></use>
            </svg>
        </a>
    </div><!-- /.app -->

    <div class="best-comment">
        <h2 class="title-xs">Лучшие комментарии</h2>

        <div class="comment-aside">
            <?php foreach(\app\models\Comments::getBestCommentsForBlog() as $comment): ?>
            <div class="comment-aside__item">
                <div class="comment-aside__info">
                    <p><?php echo mb_substr($comment->content, 0, 46) ?>...</p>
                </div>
                <div class="comment-aside__bottom">
                    <a href="#" class="comment-aside__avatar">
                        <img src="<?php echo $comment->author->profile->getThumbUploadUrl('img', 'thumb') ?>" alt="">
                    </a>
                    <a href="#" class="comment-aside__name">
                        <?php echo $comment->getAuthorName(); ?>
                    </a>
                    <div class="comment-aside__date"><?php echo date('d.m', $comment->createdAt) ?> в <?php echo date('H.i', $comment->createdAt) ?></div>
                </div>
            </div><!-- /.comment-aside__item -->
            <?php endforeach; ?>

        </div>
    </div>

    <h2 class="title-xs">Популярные теги</h2>

    <div class="tag tag__aside">
        <?php foreach (\app\models\News::getTopTags() as $tag): ?>
            <a href="<?php echo Url::to(['blog/index', 'tag' => $tag->tag]) ?>" class="tag__item"><?php echo $tag->tag; ?></a>
        <?php endforeach; ?>
    </div>


        <?php echo \app\widgets\poll\Poll::widget() ?>


    <div class="viget">
        <script type="text/javascript" src="https://vk.com/js/api/openapi.js?167"></script>

        <!-- VK Widget -->
        <div id="vk_groups"></div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "240", height: "400"}, 175289947);
        </script>
    </div>
</div>
