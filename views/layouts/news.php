<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image:type" content="image/png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon-72x72.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo $this->render('_loader') ?>
<header class="header">
    <?php echo $this->render('_header') ?>
</header>
<main class="main inside">
    <div class="box d-flex">
        <div class="main-container">
            <?= $content ?>
            <?php echo $this->render('_footer')?>
        </div>
        <div class="aside">
            <?php echo $this->render('_sidebar_news')?>
        </div>
    </div>
</main>
<?php echo \app\widgets\city_popup\City_popup::widget() ?>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
