<?php

use yii\helpers\Url;

?>
<div class="intro">
    <div class="intro__wrapper intro__wrapper--catalog catalog box">
        <div class="intro__left indent">

            <div class="intro__bottom intro__catalog">

                <div class="intro__section">
                    <h1 class="title"><?php echo $this->params['category']?$this->params['category']->name:'Каталог' ?></h1>
                    <div class="intro__select">
                        <div class="intro__select-header">
                            <a href="#section-category" class="intro__select-text popup-with-form">Сменить раздел</a>
                            <span class="intro__select-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>

                <form action="#" class="form search search-big">
                    <div class="search__input-box">
                        <input class="search__input" type="search">
                    </div>
                    <div class="search__select select">
                        <a href="#select-sity" class="search__select-header select__header popup-with-form">
                            <div class="search__select-label select__label">
                                Москва
                            </div>
                            <div class="search__select-ico select__ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </a>
                        <div class="search__select-list select__list">
                            <div class="search__select-item select__item">Кировоград</div>
                            <div class="search__select-item select__item">Саратов</div>
                            <div class="search__select-item select__item">Севастополь</div>
                            <div class="search__select-item select__item">Симферополь</div>
                            <div class="search__select-item select__item">Киров</div>
                        </div>
                    </div>
                    <button type="submit" class="search__btn">
                            <span class="search__btn-ico">
                                <i class="ico ico-search"></i>
                            </span>
                        <span class="search__btn-text">Найти</span>
                    </button>
                </form>

            </div><!-- /.intro__bottom -->

        </div><!-- /.intro__left -->

        <div class="intro__right">

            <div class="catalog__wrapper">
                <div class="catalog__info">
                    <p class="catalog__text">Организаций в категории</p>
                    <p class="catalog__current"><?php echo $this->params['category']?$this->params['category']->name:('Каталог');  ?> <?php echo \app\models\Organisation::getCount($this->params['category']?$this->params['category']->id:false) ?></p>
                    <a href="<?php echo Url::toRoute(['organisation/create']) ?>" class="catalog__btn btn btn-secondary">Добавить организацию</a>
                </div>
            </div>

        </div><!-- /.intro__right -->
    </div><!-- /.intro__wrapper -->

</div><!-- /.intro -->