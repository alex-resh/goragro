<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Главная</title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta property="og:image:type" content="image/png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon-72x72.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
</head>

<body>

<div class="loader">
    <div class="loader__inner">
        <div class="sk-cube-grid">
            <div class="sk-cube sk-cube1"></div>
            <div class="sk-cube sk-cube2"></div>
            <div class="sk-cube sk-cube3"></div>
            <div class="sk-cube sk-cube4"></div>
            <div class="sk-cube sk-cube5"></div>
            <div class="sk-cube sk-cube6"></div>
            <div class="sk-cube sk-cube7"></div>
            <div class="sk-cube sk-cube8"></div>
            <div class="sk-cube sk-cube9"></div>
        </div>
    </div>
</div>
<?php echo $content ?>

<link rel="stylesheet" href="/css/library.css">
<link rel="stylesheet" href="/css/style.css">
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script src="/js/libs.min.js"></script>
<script src="/js/common.js"></script>
</body>
</html>