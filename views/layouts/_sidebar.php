<?php 


//$organisation = $this->params['modal'];
//$worktime = $organisation->worktime;
//
//$address = $organisation->address;

?>

<aside class="aside">

    <div class="aside__top">
        <div class="app">
            <div class="app__text">А у нас есть приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="/images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="/images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div>
    </div>

    <div class="aside__body">
        <?php echo \app\widgets\poll\Poll::widget() ?>
        <div class="viget">
            <script type="text/javascript" src="https://vk.com/js/api/openapi.js?167"></script>

            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "240", height: "400"}, 175289947);
            </script>
        </div>
        <?php echo \app\widgets\about\About::widget() ?>

</aside><!-- /.aside -->