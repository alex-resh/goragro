<?php 


$organisation = $this->params['modal']; 
$worktime = $organisation->worktime; 

$address = $organisation->address;

?>

<aside class="aside">

    <div class="aside__top">
        <div class="app">
            <div class="app__text">А у нас есть приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="/images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="/images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div>
    </div>
            <div class="intro__right place__data">

            <div class="place__data-line place__data-line--address">

                <div class="place__data-ico">
                    <svg class="ico ico-location">
                        <use xlink:href="/images/svg/symbol/sprite.svg#location"></use>
                    </svg>
                </div>
                <div class="place__data-info">
                    <p class="place__data-text h3">
                        <div><?= $address->getToString(); ?></div>
                    </p>
                </div>
            </div><!-- /.place__line -->

            <div class="place__data-line">
                <div class="place__data-ico">
                    <svg class="ico ico-keyboard">
                        <use xlink:href="/images/svg/symbol/sprite.svg#keyboard"></use>
                    </svg>
                </div>
                <div class="place__data-info">
                    <label class="place__data-label">телефоны</label>
                    <div class="place__data-data-info">
                        <?php foreach ((array)$organisation->address->phones as $i => $phone) : ?>
                        <p class="place__data-text"><b><?= $address->phones[$i];?></b> <span><?= $address->phoneType[$i];?></span></p>
                        <?php endforeach;?>
                    </div>
                </div>
            </div><!-- /.place__line -->

            <div class="place__data-line">
                <div class="place__data-ico">
                    <svg class="ico ico-clock">
                        <use xlink:href="images/svg/symbol/sprite.svg#clock"></use>
                    </svg>
                </div>
                <div class="place__data-info">
                    <label class="place__data-label">часы работы</label>
                    <div class="place__data-info">
                        <?php if($worktime) : ?>
                        <?php $dayOfWeek = date('w',time() - 60*60*24);?>
                        <?php $prefix = $worktime->days()[$dayOfWeek] . '_';?>
                        <?php if($worktime->{$prefix . 'start'}) : ?>
                            <p class="place__data-text"><b>с <?= $worktime->{$prefix . 'start'};?> до <?= $worktime->{$prefix . 'end'};?></b></p>
                            <label class="place_data-info">перерыв</label>
                            <p class="place__data-text"><b>c <?= $worktime->{$prefix . 'br_start'};?> до <?= $worktime->{$prefix . 'br_end'};?></b></p>
                        <?php else : ?>
                            <p class="place__data-text"><b>Выходной</b></p>
                        <?php endif;?>
                        <?php endif;?>
                    </div>
                </div>
            </div><!-- /.place__line -->

            <div class="place__data-line">
                <div class="place__data-ico">
                    <svg class="ico ico-arrow-link">
                        <use xlink:href="/images/svg/symbol/sprite.svg#arrow-link"></use>
                    </svg>
                </div>
                <div class="place__data-info">
                    <label class="place__data-label">сайт</label>
                    <div class="place__data-info">
                        <?php foreach ((array)$address->url as $i => $item) : ?>
                        <p class="place__data-text">
                            <b><a href="<?= $item;?>" class="place__data-link"><?= $item . ' ' . $address->urlType[$i];?></a></b>
                        </p>
                        <?php endforeach;?>
                    </div>
                </div>
            </div><!-- /.place__line -->

            <div class="soc-sety place__data-soc-sety">
                <a href="#" class="soc-sety__item vk">
                    <svg class="ico ico-vk">
                        <use xlink:href="/images/svg/symbol/sprite.svg#vk"></use>
                    </svg>
                </a>
                <a href="#" class="soc-sety__item tw">
                    <svg class="ico ico-tw">
                        <use xlink:href="/images/svg/symbol/sprite.svg#tw"></use>
                    </svg>
                </a>
                <a href="#" class="soc-sety__item fc">
                    <svg class="ico ico-fc">
                        <use xlink:href="/images/svg/symbol/sprite.svg#fc"></use>
                    </svg>
                </a>
                <a href="#" class="soc-sety__item ok">
                    <svg class="ico ico-ok">
                        <use xlink:href="/images/svg/symbol/sprite.svg#ok"></use>
                    </svg>
                </a>
                <a href="#" class="soc-sety__item mail">
                    <svg class="ico ico-mail">
                        <use xlink:href="/images/svg/symbol/sprite.svg#mail"></use>
                    </svg>
                </a>
                <a href="#" class="soc-sety__item instagram">
                    <svg class="ico ico-instagram" width="13" height="15">
                        <use xlink:href="/images/svg/symbol/sprite.svg#instagram"></use>
                    </svg>
                </a>
            </div>

        </div><!-- /.intro__right -->
    <div class="aside__body">
        <?php echo \app\widgets\poll\Poll::widget() ?>
        <div class="viget">
            <script type="text/javascript" src="https://vk.com/js/api/openapi.js?167"></script>

            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "240", height: "400"}, 175289947);
            </script>
        </div>
        <?php echo \app\widgets\about\About::widget() ?>

</aside><!-- /.aside -->