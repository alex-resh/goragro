<?php
use yii\helpers\Url;
use app\models\News;
?>
<aside class="aside">
    <div class="aside__top">
        <div class="inside__counter">
            <i class="inside__counter-ico">
                <i class="ico ico-scale"></i>
            </i>
            <p class="inside__counter-info"><b><?php echo News::getCountByCity() ?></b> новостей на сайте</p>
        </div><!-- /.inside__counter -->

        <a href="#" class="back">
            <div class="back__link">Вернуться в каталог</div>
            <div class="back__ico">
                <svg class="ico ico-arrow-left">
                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </div>
        </a>
    </div>
    <div class="aside__body">
        <?php echo \app\widgets\newscategory\NewsCategory::widget(); ?>

        <a href="<?php echo Url::toRoute('/news/create') ?>" class="btn-add">
                    <span class="btn-add__ico">
                        <svg class="ico ico-plus">
                            <use xlink:href="images/svg/symbol/sprite.svg#plus"></use>
                        </svg>
                    </span>
            <span class="btn-add__text">Предложить новость</span>
        </a>

        <div class="app app--white">
            <div class="app__text">Наше приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div><!-- /.app -->


        <div class="post-table">
            <h3 class="post-table__title title-xs">Рекомендуемые новости</h3>

            <div class="post-table__list">
                <?php foreach (News::find()->andWhere(['status' => 1, 'type' => News::TYPE_NEWS])->orderBy(['created_at' => SORT_DESC])->limit(5)->all() as $item): ?>
                    <div class="post-table__item">
                        <a href="<?php echo Url::to(['/news/view', 'id' => $item->id]) ?>" class="post-table__img">
                            <img src="<?php echo $item->getThumbUploadUrl('img', 'news_thumb') ?>" alt="">
                        </a>
                        <div class="post-table__info">
                            <a href="<?php echo Url::to(['/news/view', 'id' => $item->id]) ?>" class="post-table__link">
                                <?php echo $item->title ?>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div><!-- /.post-table__list -->
        </div>

        <div class="poll mt">
        <?php echo \app\widgets\poll\Poll::widget() ?>
        </div>

        <div class="viget">
            <script type="text/javascript" src="https://vk.com/js/api/openapi.js?167"></script>

            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "240", height: "400"}, 175289947);
            </script>
        </div>
    </div>
</aside><!-- /.aside -->