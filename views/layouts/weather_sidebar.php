<aside class="aside">
    <div class="aside__top">

        <a href="<?php echo \yii\helpers\Url::to('/site/index') ?>" class="back">
            <div class="back__link">Перейти на главную</div>
            <div class="back__ico">
                <svg class="ico ico-arrow-left">
                    <use xlink:href="\images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </div>
        </a>
    </div>
    <div class="aside__body">
        <div class="app app--white mt0">
            <div class="app__text">Наше приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="\images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="\images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div><!-- /.app -->
        <?php echo \app\widgets\poll\Poll::widget() ?>
        <div class="viget">
            <script type="text/javascript" src="https://vk.com/js/api/openapi.js?167"></script>

            <!-- VK Widget -->
            <div id="vk_groups"></div>
            <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 0, width: "240", height: "400"}, 175289947);
            </script>
        </div>
    </div>
</aside><!-- /.aside -->