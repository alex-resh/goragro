<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\bootstrap\BootstrapAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

BootstrapAsset::register($this);
AppAsset::register($this);
$this->registerMetaTag(['name' => 'description', 'content' => isset($this->description) ? Html::encode($this->description) : \app\models\Settings::get('seo_description')]);
$this->registerMetaTag(['name' => 'keywords', 'content' => isset($this->keywords) ? Html::encode($this->keywords) : \app\models\Settings::get('seo_keywords')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image:type" content="image/png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/favicon-72x72.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?php echo $this->title ? Html::encode($this->title) : \app\models\Settings::get('seo_title') ?></title>
    <?php $this->head() ?>
    <?php echo \app\models\Settings::get('seo_meta') ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo $this->render('_loader') ?>
<header class="header">
    <?php echo $this->render('_header') ?>
</header>
<?php if (Yii::$app->controller->id == 'site'): ?>
    <?php echo $this->render('_intro') ?>
<?php endif; ?>
<main class="main">
    <div class="box">
        <div class="main-container">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]); ?>
            <?= $content ?>
            <?php echo $this->render('_footer') ?>
        </div>
        <div class="aside">
            <?php echo $this->render('sidebar_org') ?>
        </div>
    </div>
</main>
<?php echo \app\widgets\city_popup\City_popup::widget() ?>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
