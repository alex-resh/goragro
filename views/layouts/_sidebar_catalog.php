<?php
/**
 * @var $this \yii\web\View
 * @var $category Category
 * @var $filters \app\models\Additionally[]
 * @var $searchModel \app\models\search\OrganisationSearch
 * @var $datas \app\models\AdditionallyData[]
 */

use app\assets\AdditionallyFieldAsset;
use app\models\AdditionallyData;
use app\models\Category;
use app\models\forms\ElForm;
use app\models\OrganisationAdress;
use app\models\OrganisationWorktime;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

AdditionallyFieldAsset::register($this);
?>
<aside class="aside">
    <div class="aside__top">

        <a href="#" class="back">
            <div class="back__link">Вернуться в каталог</div>
            <div class="back__ico">
                <svg class="ico ico-arrow-left">
                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </div>
        </a>
    </div>
    <div class="aside__body">

        <div class="table-data">
            <?php foreach (Category::getOrgWithCount() as $cat) : ?>
                <a href="<?= Url::to(['/catalog', 'category_id' => $cat->id]);?>" class="table-data-line<?= (isset($category->id) && ($category->id == $cat->id)) ? ' active' : '';?>">
                    <span class="table-data-name"><?= $cat->name;?></span>
                    <span class="table-data-val"><?= $cat->c;?></span>
                </a>
            <?php endforeach;?>

        </div><!-- /.table-data -->

        <div class="app app--white">
            <div class="app__text">Наше приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div><!-- /.app -->

        <?php ActiveForm::begin([
            'method' => 'get',
            'action' => Url::to($category ? ['/catalog', 'category_id' => $category->id] : ['/catalog']),
        ]);?>
        <div class="filter">
            <div class="filter__body">
                <h2 class="filter__title title-xs">Подобрать</h2>
                <div class="filter__list">
                    <?php foreach ($filters as $filter) : ?>
                    <h5><?= $filter->title;?></h5>
                    <?php foreach (Json::decode($filter->data)['rows'] as $row) : ?>
                        <?php foreach ($row['columns'] as $column) : ?>
                            <?php foreach ($column['elements'] as $element) : ?>
                            <div class="filter__item">
                                <div class="filter__item-header">
                                    <div class="filter__item-link"><?= $element['label'];?></div>
                                    <div class="filter__item-ico">
                                        <div class="ico ico-<?= $element['icon'];?>"></div>
                                    </div>
                                </div>
                                <div class="filter__item-section js-field">
                                    <?= ElForm::filter($element, $datas[$filter->id] ?? new AdditionallyData(['form_id' => $filter->id]));?>
                                </div>
                            </div>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                    <?php endforeach;?>

                    <div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Наличие сайта</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">
                            <div class="filter__item-visible">
                                <?= Html::activeRadioList($searchModel, 'site_availability', ['' => 'Не имеет значения', '1' => 'Есть', '0' => 'Нет'], ['class' => 'js-field']);?>
                            </div><!-- /.filter__item-visible -->
                        </div>
                    </div><!-- /.filter__item -->
                    <div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Район</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">
                            <div class="filter__item-visible">
                                <?= Html::activeCheckboxList($searchModel, 'region', OrganisationAdress::regionList(Yii::$app->city->city->id), ['class' => 'js-field']);?>
                            </div><!-- /.filter__item-visible -->
                        </div>
                    </div><!-- /.filter__item -->
                    <div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Время работы</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">
                            <div class="filter__item-visible">
                                <?= Html::activeRadioList($searchModel, 'wt_filter', OrganisationWorktime::listForFilter(), ['class' => 'js-field']);?>
                            </div><!-- /.filter__item-visible -->
                        </div>
                    </div><!-- /.filter__item -->
                    <!--<div class="filter__item filter__item--price">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Средний счет, руб</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">

                            <form action="#" class="filter__form">
                                <div class="form-price">
                                    <div class="form-price__input-wrapper">
                                        <span class="form-price__input-text">от</span>
                                        <input type="text" class="form-price__input form-price__min" value="2500"/>
                                    </div>
                                    <div class="form-price__input-wrapper">
                                        <span class="form-price__input-text">до</span>
                                        <input type="text" class="form-price__input form-price__max" value="9220"/>
                                    </div>
                                </div>
                                <button class="filter__form-button js-filter-price-button btn btn-accent">Применить</button>
                            </form>

                            <div class="ui-slider ui-slider-dekstop">
                                <div class="ui-slider__min">2100</div>
                                <div class="ui-slider__max">12600</div>
                            </div>

                        </div>
                    </div>--><!-- /.filter__item -->

                    <!--<div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Виды ресторанов</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">

                            <div class="filter__item-visible">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox1" type="checkbox" name="checkbox"
                                           checked>
                                    <label class="checkbox__wrapper" for="checkbox1">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Русский</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox2" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox2">
                                        <span class="checkbox__custom"></span>
                                        <span class="radio__label">
                                                <span class="radio__text">Итальянский</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox3" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox3">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Китайский</span>
                                            </span>
                                    </label>
                                </div>
                            </div>

                            <div class="filter__item-hidden">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox11" type="checkbox"
                                           name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox11">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                    <span class="checkbox__text">Русский</span>
                                                </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox12" type="checkbox"
                                           name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox12">
                                        <span class="checkbox__custom"></span>
                                        <span class="radio__label">
                                                    <span class="radio__text">Итальянский</span>
                                                </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox13" type="checkbox"
                                           name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox13">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                    <span class="checkbox__text">Китайский</span>
                                                </span>
                                    </label>
                                </div>
                            </div>

                            <a href="#" class="filter__item-more js-more">
                                <span class="filter__item-more-text">Смотреть все </span>
                                <span class="filter__item-more-count">(25)</span>
                            </a>
                        </div>
                    </div>-->

                    <!--<div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Кухня</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">

                            <div class="filter__item-visible">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox4" type="checkbox" name="checkbox"
                                           checked>
                                    <label class="checkbox__wrapper" for="checkbox4">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Русская</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox5" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox5">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Украинская</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox6" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox6">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Китайская</span>
                                            </span>
                                    </label>
                                </div>
                            </div>

                            <div class="filter__item-hidden">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox14" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox14">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Русская</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox15" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox15">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Украинская</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox16" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox16">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Китайская</span>
                                            </span>
                                    </label>
                                </div>
                            </div>

                            <a href="#" class="filter__item-more js-more">
                                <span class="filter__item-more-text">Смотреть все </span>
                                <span class="filter__item-more-count">(25)</span>
                            </a>
                        </div>
                    </div>-->

                    <!--<div class="filter__item">
                        <div class="filter__item-header">
                            <div class="filter__item-link">Район</div>
                            <div class="filter__item-ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="filter__item-section">
                            <div class="filter__item-visible">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox7" type="checkbox" name="checkbox"
                                           checked>
                                    <label class="checkbox__wrapper" for="checkbox7">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Хамовники</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox8" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox8">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Арбат</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio radio">
                                    <input class="checkbox__input" id="checkbox9" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox9">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Якиманка</span>
                                            </span>
                                    </label>
                                </div>
                            </div>

                            <div class="filter__item-hidden">
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox17" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox17">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Хамовники</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio checkbox">
                                    <input class="checkbox__input" id="checkbox18" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox18">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Арбат</span>
                                            </span>
                                    </label>
                                </div>
                                <div class="filter__item-radio radio">
                                    <input class="checkbox__input" id="checkbox19" type="checkbox" name="checkbox">
                                    <label class="checkbox__wrapper" for="checkbox19">
                                        <span class="checkbox__custom"></span>
                                        <span class="checkbox__label">
                                                <span class="checkbox__text">Якиманка</span>
                                            </span>
                                    </label>
                                </div>
                            </div>

                            <a href="#" class="filter__item-more js-more">
                                <span class="filter__item-more-text">Смотреть все </span>
                                <span class="filter__item-more-count">(25)</span>
                            </a>
                        </div>
                    </div>--><!-- /.filter__item -->
                </div><!-- /.filter__list -->
            </div>
            <div class="filter__bottom">
                <!--<a href="#" class="filter__btn btn btn-accent">Подобрать</a>-->
                <?= Html::submitButton('Подобрать', ['class' => 'filter__btn btn btn-accent']);?>
                <a href="<?= Url::to($category ? ['/catalog', 'category_id' => $category->id] : ['/catalog']);?>" class="filter__link js-filter-reset">
                            <span class="filter__link-ico">
                                <svg class="ico ico-delete">
                                    <use xlink:href="images/svg/symbol/sprite.svg#delete"></use>
                                </svg>
                            </span>
                    <span class="filter__link-text">сбросить</span>
                </a>
            </div>

        </div><!-- /.filter -->
        <?php ActiveForm::end();?>

        <?php echo \app\widgets\poll\Poll::widget() ?>

        <div class="viget">
            <img src="images/viget.jpg" alt="">
        </div><!-- /.viget -->
    </div>
</aside>