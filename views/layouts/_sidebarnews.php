<aside class="aside">
    <div class="aside__top">
        <div class="inside__counter">
            <i class="inside__counter-ico">
                <i class="ico ico-scale"></i>
            </i>
            <p class="inside__counter-info"><b>16621</b> блогов на сайте</p>
        </div><!-- /.inside__counter -->

        <a href="#" class="back">
            <div class="back__link">Перейти на главную</div>
            <div class="back__ico">
                <svg class="ico ico-arrow-left">
                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-left"></use>
                </svg>
            </div>
        </a>
    </div>

    <div class="aside__body">
        <div class="inside__table-data table-data">
            <a href="#" class="table-data-line">
                <span class="table-data-name">Авторазборки</span>
                <span class="table-data-val">156</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Автосалоны</span>
                <span class="table-data-val">78</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Автомойки</span>
                <span class="table-data-val">5</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Сервисы, СТО </span>
                <span class="table-data-val">89</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Авторынки</span>
                <span class="table-data-val">823</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Автостоянки</span>
                <span class="table-data-val">27</span>
            </a>

            <a href="#" class="table-data-line">
                <span class="table-data-name">Гаражные комплексы</span>
                <span class="table-data-val">45</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Автошколы</span>
                <span class="table-data-val">62</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Договора, услуги</span>
                <span class="table-data-val">8</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Мотосалоны</span>
                <span class="table-data-val">9</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Мотошколы</span>
                <span class="table-data-val">51</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Такси</span>
                <span class="table-data-val">68</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Шиномонтажи</span>
                <span class="table-data-val">71</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Шины, диски</span>
                <span class="table-data-val">83</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Эвакуация</span>
                <span class="table-data-val">65</span>
            </a>
            <a href="#" class="table-data-line">
                <span class="table-data-name">Обзоры</span>
                <span class="table-data-val">31</span>
            </a>
        </div><!-- /.table-data -->

        <a href="#" class="btn-add">
                    <span class="btn-add__ico">
                        <svg class="ico ico-plus">
                            <use xlink:href="images/svg/symbol/sprite.svg#plus"></use>
                        </svg>
                    </span>
            <span class="btn-add__text">Предложить новость</span>
        </a>

        <div class="app app--white">
            <div class="app__text">Наше приложение</div>
            <a href="#" class="app__apple app__item">
                <svg class="ico ico-apple">
                    <use xlink:href="images/svg/symbol/sprite.svg#apple"></use>
                </svg>
            </a>
            <a href="#" class="app__android app__item">
                <svg class="ico ico-android">
                    <use xlink:href="images/svg/symbol/sprite.svg#android"></use>
                </svg>
            </a>
        </div><!-- /.app -->

        <div class="post-table">
            <h3 class="post-table__title title-xs">Рекомендуемые новости</h3>

            <div class="post-table__list">
                <div class="post-table__item">
                    <a href="#" class="post-table__img">
                        <img src="images/sights/1.jpg" alt="">
                    </a>
                    <div class="post-table__info">
                        <a href="#" class="post-table__link">Глава Twitter объявил об увольнении 8%
                            сотрудников</a>
                    </div>
                </div>
                <div class="post-table__item">
                    <a href="#" class="post-table__img">
                        <img src="images/sights/2.jpg" alt="">
                    </a>
                    <div class="post-table__info">
                        <a href="#" class="post-table__link">Внешний диск Seagate Seven удостоен награды iF
                            Design</a>
                    </div>
                </div>
                <div class="post-table__item">
                    <a href="#" class="post-table__img">
                        <img src="images/sights/1.jpg" alt="">
                    </a>
                    <div class="post-table__info">
                        <a href="#" class="post-table__link">Петербуржец попытался украсть жевательной
                            резинки</a>
                    </div>
                </div>
                <div class="post-table__item">
                    <a href="#" class="post-table__img">
                        <img src="images/sights/2.jpg" alt="">
                    </a>
                    <div class="post-table__info">
                        <a href="#" class="post-table__link">Марион Котийяр сыграет в экранизации "Assassin’s
                            Creed"</a>
                    </div>
                </div>
                <div class="post-table__item">
                    <a href="#" class="post-table__img">
                        <img src="images/sights/1.jpg" alt="">
                    </a>
                    <div class="post-table__info">
                        <a href="#" class="post-table__link">В РФ разрабатываются материалы для 3D-печати
                            домови</a>
                    </div>
                </div>
            </div><!-- /.post-table__list -->
        </div>

        <?php echo \app\widgets\poll\Poll::widget() ?>

        <div class="viget">
            <img src="images/viget.jpg" alt="">
        </div><!-- /.viget -->
    </div>
</aside><!-- /.aside -->