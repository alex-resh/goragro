<div class="header__wrapper box">
    <div class="header__top">
        <div class="header__left">
            <a href="/" class="logo header__logo">
                <img src="/images/logo.svg" alt="Garago">
            </a>
            <div class="header__city">
                <div class="header__city-current"><?php echo Yii::$app->city->city->name; ?></div>

                <a href="#select-sity" class="header__city-other popup-with-form">
                    <i class="header__city-other-ico">
                        <svg class="ico ico-target">
                            <use xlink:href="/images/svg/symbol/sprite.svg#target"></use>
                        </svg>
                    </i>
                    <span class="header__city-other-text">Другой город</span>
                </a>
            </div>

        </div>

        <div class="header__right">
            <nav class="mnu-top">
                <button class="mnu-top__button hamburger hamburger--collapse js-menu-open">
                        <span class="mnu-top__button-wrapper">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                            <span class="mnu-top__text">меню</span>
                        </span>
                </button>
                <div class="mnu-top__wrapper">
                    <button class="mnu-top__button hamburger hamburger--collapse is-active js-menu-close">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                    </button>
                    <ul class="mnu-top__list">
                        <li class="mnu-top__item"><a href="<?php echo \yii\helpers\Url::to(['catalog/index']) ?>" class="mnu-top__link">Каталог</a></li>
                        <li class="mnu-top__item"><a href="<?php echo \yii\helpers\Url::to(['news/index']) ?>" class="mnu-top__link">Новости</a></li>
                        <li class="mnu-top__item"><a href="<?php echo \yii\helpers\Url::to(['blog/index']) ?>" class="mnu-top__link">Блоги</a></li>
                        <?php /*
                        <li class="mnu-top__item"><a href="<?php echo \yii\helpers\Url::to(['blog/index']) ?>" class="mnu-top__link">Блоги</a></li>
                        <li class="mnu-top__item"><a href="#" class="mnu-top__link">Рецепты</a></li>
                        <li class="mnu-top__item"><a href="#" class="mnu-top__link">Юмор</a></li>
                         */ ?>
                    </ul>
                </div>
            </nav>


                <?php echo \app\widgets\username\Username::widget() ?>




        </div><!-- /.header__right -->
    </div><!-- /.header__top -->


    <div class="header__bottom"></div>

</div><!-- /.header__wrapper -->

