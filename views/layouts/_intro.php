<?php /* @var $this \yii\web\View */

use yii\helpers\Html;
$this->registerJsFile('https://api-maps.yandex.ru/2.1/?apikey=4e405466-de50-447a-9017-87f7c9741a9b&lang=ru_RU');
$this->registerJsFile('/js/intromap.js');
\app\assets\WeatherAsset::register($this);


//$cbrf_usd = \Yii::$app->CbRF->dynamic(['id' => 'R01235', 'date_from' => time() - 60*60*24]);
$cbrf_usd_now = \Yii::$app->CbRF->getRate(time(), 'USD');
$cbrf_usd_pred = \Yii::$app->CbRF->getRate(time() - 60*60*24, 'USD');
$cbrf_usd_delta = $cbrf_usd_now - $cbrf_usd_pred;

//$cbrf_eur = \Yii::$app->CbRF->dynamic(['id' => 'R01239', 'date_from' => time() - 60*60*24]);
$cbrf_eur_now = \Yii::$app->CbRF->getRate(time(), 'EUR');
$cbrf_eur_pred = \Yii::$app->CbRF->getRate(time() - 60*60*24, 'EUR');
$cbrf_eur_delta = $cbrf_eur_now - $cbrf_eur_pred;

if($cbrf_eur_delta > 0) {
    $cbrf_eur_delta = '+'.$cbrf_eur_delta;
}

if($cbrf_usd_delta > 0) {
    $cbrf_usd_delta = '+'.$cbrf_usd_delta;
}


?>
<div class="intro">
    <div class="intro__wrapper intro__main box" style="background-image: url('<?php echo Yii::$app->city->city->getThumbUploadUrl('img', 'intro_img') ?>')">
        <div class="intro__left">

            <div class="intro__top d-flex">
                <div class="intro__today intro__top-item">
                    <div class="intro__today-day">Сегодня: <b><?php echo Yii::$app->formatter->asDate(time(), 'php:l') ?></b></div>
                    <div class="intro__today-time"><?php echo Yii::$app->formatter->asDate(time(), 'php:j F') ?>,
                        <svg class="ico ico-clock">
                            <use xlink:href="/images/svg/symbol/sprite.svg#clock"></use>
                        </svg>
                        <?php echo Yii::$app->formatter->asDate(time(), 'php:H:i') ?>
                    </div>
                </div>
                <div class="intro__top-item">

                    <div class="intro__weather">
                        <span class="intro__weather-key">Сегодня</span>
                        <span class="intro__weather-index">
                            <span class="intro__weather-ico">
                                <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass(Yii::$app->weather->getPrognosToday(Yii::$app->city->city->name_en), 'day')]);?>
                            </span>
                            <span class="intro__weather-value"><b><?php echo Yii::$app->weather->getWeather(Yii::$app->city->city->name_en, 0) ?> °C</b></span>
                        </span>
                    </div>

                    <div class="intro__weather">
                        <span class="intro__weather-key">Завтра</span>
                        <span class="intro__weather-index">
                            <span class="intro__weather-ico">
                                <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass(Yii::$app->weather->getPrognos(Yii::$app->city->city->name_en)[1], 'day')]);?>
                            </span>
                            <span class="intro__weather-value"><b><?php echo Yii::$app->weather->getWeather(Yii::$app->city->city->name_en, 1) ?> °C</b></span>
                        </span>
                    </div>

                </div>
                <div class="intro__traffic-jam intro__top-item">
                    <div class="intro__traffic-jam-line">Пробки
                        <div class="intro__traffic-jam-ico"></div>
                        <b>5 баллов</b></div>
                    <div class="intro__traffic-jam-line">местами затруднения</div>
                </div>
                <div class="intro__rates intro__top-item">
                    <div class="intro__rates-line">
                            <span class="intro__rates-name">
                                usd
                            </span>
                        <div class="intro__rates-value">
                            <i class="intro__rates-ico">
                                <svg class="ico ico-down">
                                    <use xlink:href="/images/svg/symbol/sprite.svg#<?php echo $cbrf_usd_delta < 0?'up':'down' ?>"></use>
                                </svg>
                            </i>

                            <div class="intro__rates-rub">
                                <b><?php echo $cbrf_usd_now ?></b> <span class="intro__rates-difference"><?php //echo $cbrf_usd_delta ?></span>
                            </div>

                        </div>
                    </div>
                    <div class="intro__rates-line">
                            <span class="intro__rates-name">
                                eur
                            </span>
                        <div class="intro__rates-value">
                            <i class="intro__rates-ico">
                                <svg class="ico ico-up">
                                    <use xlink:href="/images/svg/symbol/sprite.svg#<?php echo $cbrf_eur_delta < 0?'up':'down' ?>"></use>
                                </svg>
                            </i>

                            <div class="intro__rates-rub">
                                <b><?php echo $cbrf_eur_now ?></b> <span class="intro__rates-difference"><?php //echo $cbrf_eur_delta ?></span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="intro__bottom indent">

                <h1 class="intro__title title-lg">Отзовик №1 в России</h1>

                <form action="#" class="form search search-big">
                    <div class="search__input-box">
                        <input class="search__input" type="search">
                    </div>
                    <div class="search__select select">
                        <a href="#select-sity" class="search__select-header select__header popup-with-form">
                            <div class="search__select-label select__label">
                                Москва
                            </div>
                            <div class="search__select-ico select__ico">
                                <svg class="ico ico-arrow-bottom">
                                    <use xlink:href="/images/svg/symbol/sprite.svg#arrow-bottom"></use>
                                </svg>
                            </div>
                        </a>
                        <div class="search__select-list select__list">
                            <div class="search__select-item select__item">Кировоград</div>
                            <div class="search__select-item select__item">Саратов</div>
                            <div class="search__select-item select__item">Севастополь</div>
                            <div class="search__select-item select__item">Симферополь</div>
                            <div class="search__select-item select__item">Киров</div>
                        </div>
                    </div>
                    <button type="submit" class="search__btn">
                        <span class="search__btn-ico">
                            <i class="ico ico-search"></i>
                        </span>
                        <span class="search__btn-text">Найти</span>
                    </button>
                </form>

                <div class="intro__link">
                    <a href="#" class="intro__link-item"><span
                            class="intro__link-text">Заказать услугу или товар</span>
                        <svg class="ico ico-question">
                            <use xlink:href="/images/svg/symbol/sprite.svg#question"></use>
                        </svg>
                    </a>
                    <a href="#" class="intro__link-item"><span class="intro__link-text">Что такое Garago?</span></a>
                </div>

            </div><!-- /.intro__bottom -->

        </div><!-- /.intro__left -->

        <div class="intro__right">

            <div class="intro__aside">

                <div class="intro__aside-top">
                    <div class="intro__organization">
                            <span class="intro__organization-ico">
                                <i class="ico ico-scale"></i>
                            </span>
                        <div class="intro__organization-index">
                            <span class="intro__organization-quan"><?php echo $org_count = \app\models\Organisation::getCount(); ?></span>
                            <p class="intro__organization-text">организаций в каталоге</p>
                        </div>
                        <a href="<?php echo \yii\helpers\Url::to(['/maps']) ?>" class="intro__organization-btn">показать на карте</a>
                    </div>
                </div>

                <div class="intro__aside-body">
                    <div class="intro__aside-seсtion">
                        <span class="intro__aside-seсtion-value"><?php echo \app\models\User::find()->count(); ?></span>
                        <span class="intro__aside-seсtion-text">пользователей</span>
                    </div>
                    <div class="intro__aside-seсtion">
                        <span class="intro__aside-seсtion-value">0</span>
                        <span class="intro__aside-seсtion-text">рецептов</span>
                    </div>
                    <div class="intro__aside-seсtion">
                        <span class="intro__aside-seсtion-value"><?php echo \app\models\News::find()->where(['city_id' => Yii::$app->city->city->id])->count() ?></span>
                        <span class="intro__aside-seсtion-text">новостей</span>
                    </div>
                </div>

                <div class="intro__aside-bottom">
                    <i class="intro__aside-link-ico">
                        <svg class="ico ico-foot">
                            <use xlink:href="/images/svg/symbol/sprite.svg#foot"></use>
                        </svg>
                    </i>
                    <div class="intro__aside-link-item">
                        <a href="#" class="intro__aside-link">Для компаний</a>
                    </div>
                    <div class="intro__aside-link-item">
                        <a href="<?php echo \yii\helpers\Url::toRoute(['organisation/create']) ?>" class="intro__aside-link">Добавить фирму</a>
                    </div>
                    <div class="intro__aside-link-item">
                        <a href="#" class="intro__aside-link">Для чего это нужно?</a>
                    </div>
                </div>

            </div>
        </div><!-- /.intro__right -->

    </div><!-- /.intro__wrapper -->

</div><!-- /.intro -->

<!--<div id="intromap" style="width: 600px; height: 400px" ></div>-->


