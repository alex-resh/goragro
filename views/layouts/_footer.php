<div class="footer__top">
    <div class="footer__wrapper box indent">

        <div class="footer__copyright">
            <a href="#" class="footer__logo">
                <img src="/images/logo_footer.svg" alt="Garago">
            </a>
            <p class="footer__copyright-text text">Сайт Garago.ru может содержать контент,
                запрещенный к просмотру
                лицам
                не достигшим 18 лет!</p>
            <a href="#" class="footer__design">
                                <span class="footer__design-ico">
                                    <svg class="ico ico-centroarts.svg"><use
                                                xlink:href="/images/svg/symbol/sprite.svg#centroarts"></use></svg>
                                </span>
                <span class="footer__design-name">
                                    Дизайн - Centroarts
                                </span>
            </a>
        </div>

        <div class="footer__mnu">
            <div class="footer__mnu-column">
                <ul class="footer__mnu-list">
                    <li class="footer__mnu-title">Пользователю</li>
                    <li class="footer__mnu-item"><a href="<?php echo \yii\helpers\Url::to('about/index') ?>"
                                                    class="footer__mnu-link">О проекте</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Контакты</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Правила</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Вакансии</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#"
                                                    class="footer__mnu-link">Сотрудничество</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Как
                            пользоваться сайтом?</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Реклама</a>
                    </li>
                </ul>
            </div>
            <div class="footer__mnu-column">
                <ul class="footer__mnu-list">
                    <li class="footer__mnu-title">Для компаний</li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Как добавить
                            фирму?</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Для чего это
                            нужно?</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Какие
                            преимущества?</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Аудитория
                            Garago</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Правила
                            размещения</a></li>
                </ul>
            </div>
            <div class="footer__mnu-column">
                <ul class="footer__mnu-list">
                    <li class="footer__mnu-title">Читай нас</li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Вконтакте</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#"
                                                    class="footer__mnu-link">Одноклассники</a></li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Твиттер</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Фейсбук</a>
                    </li>
                    <li class="footer__mnu-item"><a href="#" class="footer__mnu-link">Инстаграм</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="footer__bottom">

    <div class="partners">
        <a href="#" class="partners__item">
            <img src="/images/partners/interlux.png" alt="interlux">
        </a>
        <a href="#" class="partners__item">
            <img src="/images/partners/mutewatch.png" alt="mutewatch">
        </a>
        <a href="#" class="partners__item">
            <img src="/images/partners/eight.png" alt="eight">
        </a>
        <a href="#" class="partners__item">
            <img src="/images/partners/allstar.png" alt="allstar">
        </a>
        <a href="#" class="partners__item">
            <img src="/images/partners/bexter.png" alt="bexter">
        </a>
    </div>
    <div class="footer__counter">
        <div class="footer__counter-item"></div>
        <div class="footer__counter-item"></div>
        <div class="footer__counter-item"></div>
    </div>

</div>
<div class="footer__copyright">
    <?php echo \app\models\Settings::get('copyright') ?>
    <?php echo \app\models\Settings::get('counters') ?>
</div>
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ea2a0b7e6115584"></script>