<?php

use app\modules\likes\widgets\LikesWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2mod\editable\Editable;

/* @var $this \yii\web\View */
/* @var $model \yii2mod\comments\models\CommentModel */
/* @var $maxLevel null|integer comments max level */
?>
<div class="comment__item" data-comment-content-id="<?php echo $model->id; ?>" id="comment-<?php echo $model->id; ?>">
    <div class="comment__parent">
        <div class="comment__wrapper">

            <a href="#" class="comment__avatar">
                <div class="comment__avatar-wrapper">
                    <?php echo Html::img($model->getAvatar(), ['alt' => $model->getAuthorName()]); ?>
                </div>
            </a>
            <div class="comment__info">
                <a href="#add-new-comment" class="comment__reply js-comment-reply">
                    <svg class="ico ico-link">
                        <use xlink:href="images/svg/symbol/sprite.svg#link"></use>
                    </svg>
                </a>
                <div class="comment__section">
                    <p class="text">
                        <?php echo $model->getContent(); ?>
                    </p>

                </div>

                <div class="comment__header comment__header--bottom">

                    <div class="comment__header-bottom">
                        <div class="comment__left">
                            <a href="#" class="comment__name online"><?php echo $model->getAuthorName(); ?></a>
                            <span class="comment__date">от <?php echo Yii::$app->formatter->asDate($model->createdAt, 'php:d.m.Y') ?> в <?php echo Yii::$app->formatter->asDate($model->createdAt, 'php:H:i') ?></span>
                        </div>

                        <div class="comment__right">
                            <div class="comment__poll">
                                <span class="comment__poll-label">Полезен ли отзыв?</span>
                                <span class="comment__poll-answers">
                                    <?php $likes = LikesWidget::begin(['model' => $model]);?>
                                    <?= $likes->likeText();?>
                                    <?= $likes->dislikeText();?>
                                    <?php LikesWidget::end();?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="comment__count">
                        #<?php echo $model->rating; ?>
                    </div>

                </div>

            </div><!-- /.comment__info -->
        </div><!-- /.comment__wrapper -->
    </div><!-- /.comment__parent -->
</div><!-- /.comment__item -->