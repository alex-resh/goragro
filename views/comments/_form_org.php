<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2mod\comments\models\CommentModel */
/* @var $encryptedEntity string */
/* @var $formId string comment form id */

$this->registerJs('$("#form-comment_message").emojioneArea({
        hideSource: true
    });');

$this->registerJs(
    <<<JS
$(document).on('afterCreate', '#$formId', function (e) {
    alert('Комментарий появится после модерации');
    return true;
});
JS
);

?>
<div class="indent vertical add-new-comment" id="add-new-comment">
    <h2 class="comment__title title-md">Добавить комментарий</h2>

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => $formId,
            'class' => 'comment-box form form-comment',
        ],
        'action' => Url::to(['/comment/default/create', 'entity' => $encryptedEntity]),
        'validateOnChange' => false,
        'validateOnBlur' => false,
    ]); ?>

<div class="form__row">
    <div class="rate-company">

        <span class="rate-company__text">Моя оценка</span>
        <div class="rating-count rate-company__stars" data-stars="1">
            <svg height="20" width="18" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 13 12" class="star rating" data-rating="1">
                <path fill-rule="evenodd" fill="#dbdbdb"
                      d="M6.517 0l2.136 3.771 4.346.813-3.025 3.143.549 4.273-4.006-1.829L2.511 12l.549-4.273L.035 4.584l4.345-.813L6.517 0" />
            </svg>

            <svg height="20" width="18" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 13 12" class="star rating" data-rating="2">
                <path fill-rule="evenodd" fill="#dbdbdb"
                      d="M6.517 0l2.136 3.771 4.346.813-3.025 3.143.549 4.273-4.006-1.829L2.511 12l.549-4.273L.035 4.584l4.345-.813L6.517 0" />
            </svg>
            <svg height="20" width="18" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 13 12" class="star rating" data-rating="3">
                <path fill-rule="evenodd" fill="#dbdbdb"
                      d="M6.517 0l2.136 3.771 4.346.813-3.025 3.143.549 4.273-4.006-1.829L2.511 12l.549-4.273L.035 4.584l4.345-.813L6.517 0" />
            </svg>
            <svg height="20" width="18" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 13 12" class="star rating" data-rating="4">
                <path fill-rule="evenodd" fill="#dbdbdb"
                      d="M6.517 0l2.136 3.771 4.346.813-3.025 3.143.549 4.273-4.006-1.829L2.511 12l.549-4.273L.035 4.584l4.345-.813L6.517 0" />
            </svg>
            <svg height="20" width="18" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 13 12" class="star rating" data-rating="5">
                <path fill-rule="evenodd" fill="#dbdbdb"
                      d="M6.517 0l2.136 3.771 4.346.813-3.025 3.143.549 4.273-4.006-1.829L2.511 12l.549-4.273L.035 4.584l4.345-.813L6.517 0" />
            </svg>
        </div>
    </div>
    <div class="form-wysiwyg">
        <?php echo $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textarea(['class' => 'form__textarea','placeholder' => 'Текст комментария', 'rows' => 4, 'data' => ['comment' => 'content'], 'id' => 'form-comment_message']); ?>
        <?php echo $form->field($commentModel, 'rating')->label(false)->hiddenInput(['value' => '1']) ?>
        <?php echo $form->field($commentModel, 'relatedTo')->label(false)->hiddenInput(['value' => $relatedTo]) ?>
        <?php echo $form->field($commentModel, 'url')->label(false)->hiddenInput(['value' => Url::current()]) ?>
        <div class="form-wysiwyg__bottom">
            <div class="form-wysiwyg__links">
                <?php /*
                <a href="#" class="form-wysiwyg__link">
                    <svg class="ico ico-photo-gallery">
                        <use xlink:href="images/svg/symbol/sprite.svg#photo-gallery"></use>
                    </svg>
                    Добавить фото
                </a>
                <a href="#" class="form-wysiwyg__link">
                    <svg class="ico ico-video">
                        <use xlink:href="images/svg/symbol/sprite.svg#video"></use>
                    </svg>
                    Добавить видео
                </a>
                */ ?>
                <a href="#" class="form-wysiwyg__link js-add-smiles-link">
                    <svg class="ico ico-smile">
                        <use xlink:href="images/svg/symbol/sprite.svg#smile"></use>
                    </svg>
                    Добавить смайлы
                </a>
            </div>

        </div>
    </div>
</div>

    <?php echo $form->field($commentModel, 'parentId', ['template' => '{input}'])->hiddenInput(['data' => ['comment' => 'parent-id']]); ?>
    <div class="comment-box-partial">
        <div class="button-container show">
            <?php echo Html::a(Yii::t('yii2mod.comments', 'Click here to cancel reply.'), '#', ['id' => 'cancel-reply', 'class' => 'pull-right', 'data' => ['action' => 'cancel-reply']]); ?>
            <?php echo Html::submitButton('Отправить отзыв', ['class' => 'btn btn-accent']); ?>
        </div>
    </div>
    <?php $form->end(); ?>
</div>