<?php

use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2mod\comments\models\CommentModel */
/* @var $maxLevel null|integer comments max level */
/* @var $encryptedEntity string */
/* @var $pjaxContainerId string */
/* @var $formId string comment form id */
/* @var $commentDataProvider \yii\data\ArrayDataProvider */
/* @var $listViewConfig array */
/* @var $commentWrapperId string */
?>
<div class="comment comment-wrapper" id="<?php echo $commentWrapperId; ?>">
    <?php Pjax::begin(['enablePushState' => false, 'timeout' => 20000, 'id' => $pjaxContainerId]); ?>
    <?php if (!Yii::$app->user->isGuest) : ?>
        <?php echo $this->render('_form', [
            'commentModel' => $commentModel,
            'formId' => $formId,
            'encryptedEntity' => $encryptedEntity,
            'relatedTo' => $relatedTo
        ]); ?>
    <?php endif; ?>
    <?php echo ListView::widget(ArrayHelper::merge(
        [
            'dataProvider' => $commentDataProvider,
            'layout' => "{items}\n{pager}",
            'itemView' => '_list',
            'viewParams' => [
                'maxLevel' => $maxLevel,
            ],
            'options' => [
                'tag' => 'div',
                'class' => 'comment__list',
            ],
            'itemOptions' => [
                'tag' => 'div',
                'class' => 'comment__item'
            ],
        ],
        $listViewConfig
    )); ?>
    <?php Pjax::end(); ?>
</div>
