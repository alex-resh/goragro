<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $commentModel \yii2mod\comments\models\CommentModel */
/* @var $encryptedEntity string */
/* @var $formId string comment form id */

$this->registerJs('$("#form-comment_message").emojioneArea({
        hideSource: true
    });');

$this->registerJs(
<<<JS
$(document).on('afterCreate', '#$formId', function (e) {
    alert('Комментарий появится после модерации');
    return true;
});
JS
);

?>
<div class="comment-form-container">
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => $formId,
            'class' => 'comment-box form form-comment',
        ],
        'action' => Url::to(['/comment/default/create', 'entity' => $encryptedEntity]),
        'validateOnChange' => false,
        'validateOnBlur' => false,
    ]); ?>

<div class="form__row">
    <div class="form-wysiwyg">
        <?php echo $form->field($commentModel, 'content', ['template' => '{input}{error}'])->textarea(['class' => 'form__textarea','placeholder' => 'Текст комментария', 'rows' => 4, 'data' => ['comment' => 'content'], 'id' => 'form-comment_message']); ?>
        <div class="form-wysiwyg__bottom">
            <div class="form-wysiwyg__links">
                <?php /*
                <a href="#" class="form-wysiwyg__link">
                    <svg class="ico ico-photo-gallery">
                        <use xlink:href="images/svg/symbol/sprite.svg#photo-gallery"></use>
                    </svg>
                    Добавить фото
                </a>
                <a href="#" class="form-wysiwyg__link">
                    <svg class="ico ico-video">
                        <use xlink:href="images/svg/symbol/sprite.svg#video"></use>
                    </svg>
                    Добавить видео
                </a>
                */ ?>
                <a href="#" class="form-wysiwyg__link js-add-smiles-link">
                    <svg class="ico ico-smile">
                        <use xlink:href="images/svg/symbol/sprite.svg#smile"></use>
                    </svg>
                    Добавить смайлы
                </a>
            </div>

        </div>
    </div>
</div>

    <?php echo $form->field($commentModel, 'parentId', ['template' => '{input}'])->hiddenInput(['data' => ['comment' => 'parent-id']]); ?>
    <?php echo $form->field($commentModel, 'relatedTo')->label(false)->hiddenInput(['value' => $relatedTo]) ?>
    <?php echo $form->field($commentModel, 'url')->label(false)->hiddenInput(['value' => Url::current()]) ?>
    <div class="comment-box-partial">
        <div class="button-container show">
            <?php echo Html::a(Yii::t('yii2mod.comments', 'Click here to cancel reply.'), '#', ['id' => 'cancel-reply', 'class' => 'pull-right', 'data' => ['action' => 'cancel-reply']]); ?>
            <?php echo Html::submitButton('Отправить комментарий', ['class' => 'btn btn-accent comment-submit']); ?>
        </div>
    </div>
    <?php $form->end(); ?>
</div>