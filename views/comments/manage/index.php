<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\Pjax;
use app\components\Status;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \yii2mod\comments\models\search\CommentSearch */
/* @var $commentModel \yii2mod\comments\models\CommentModel */

$this->title = Yii::t('yii2mod.comments', 'Comments Management');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">

    <h1><?php echo Html::encode($this->title); ?></h1>
    <?php Pjax::begin(['timeout' => 10000]); ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'content',
                //'contentOptions' => ['style' => 'max-width: 350px;'],
                'value' => function ($model) {
                    return $model->content;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'createdBy',
                'value' => function ($model) {
                    return $model->getAuthorName();
                },
                'label' => 'Автор',
                'filter' => $commentModel::getAuthors(),
                'filterInputOptions' => ['prompt' => Yii::t('yii2mod.comments', 'Select Author'), 'class' => 'form-control'],
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Status::getLabel($model->status);
                },
                'filter' => Status::listData(),
                'filterInputOptions' => ['prompt' => Yii::t('yii2mod.comments', 'Select Status'), 'class' => 'form-control'],
            ],
            [
                'attribute' => 'relatedTo',
                'filter' => \app\models\Comments::getRelatedTo(),
                'filterInputOptions' => ['prompt' => 'Выберите сущность', 'class' => 'form-control'],
            ],
            [
                'attribute' => 'createdAt',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->createdAt);
                },
                'filter' => false,
            ],
            [
                'header' => 'Actions',
                'class' => 'yii\grid\ActionColumn',
                'template' => '{approve} {view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $title = Yii::t('yii2mod.comments', 'View');
                        $options = [
                            'title' => $title,
                            'aria-label' => $title,
                            'data-pjax' => '0',
                            'target' => '_blank',
                        ];
                        $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']);
                        $url = $model->getViewUrl();

                        if (!empty($url)) {
                            return Html::a($icon, $url, $options);
                        }

                        return null;
                    },
                    'approve' => function ($url, $model, $key) {
                        if($model->status != Status::APPROVED) {
                            $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-check']);
                            $options = [
                                'title' => 'Одобрить',
                                'aria-label' => 'Одобрить',
                            ];
                            return Html::a($icon, $url, $options);
                        }
                    }
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
