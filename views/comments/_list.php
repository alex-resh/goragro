<?php

use app\modules\likes\widgets\LikesWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2mod\editable\Editable;

/* @var $this \yii\web\View */
/* @var $model \yii2mod\comments\models\CommentModel */
/* @var $maxLevel null|integer comments max level */
?>
<div class="comment__wrapper comment-content comment_status_<?php echo $model->status ?>" data-comment-content-id="<?php echo $model->id; ?>" id="comment-<?php echo $model->id; ?>">
    <a href="#" class="comment__avatar">
        <div class="comment__avatar-wrapper">
            <?php echo Html::img($model->getAvatar(), ['alt' => $model->getAuthorName()]); ?>
        </div>
    </a>
    <div class="comment__info">
        <div class="comment__header">
            <a href="#" class="comment__name online"><?php echo $model->getAuthorName(); ?></a>
            <span class="comment__date">от <?php echo Yii::$app->formatter->asDate($model->createdAt, 'php:d.m.Y') ?> в <?php echo Yii::$app->formatter->asDate($model->createdAt, 'php:H:i') ?></span>
        </div>
        <div class="comment__section">
            <p class="text">
                <?php if (Yii::$app->getModule('comment')->enableInlineEdit && Yii::$app->getUser()->can('admin')): ?>
                    <?php echo Editable::widget([
                        'model' => $model,
                        'attribute' => 'content',
                        'url' => Url::to(['/comment/default/quick-edit']),
                        'options' => [
                            'id' => 'editable-comment-' . $model->id,
                        ],
                    ]); ?>
                <?php else: ?>
                    <?php echo $model->getContent(); ?>
                <?php endif; ?>
            </p>

            <div class="rate-like rate-like--row">
                <?php $likes = LikesWidget::begin(['model' => $model]);?>
                <?= $likes->like(true);?>
                <?= $likes->dislike();?>
                <?php LikesWidget::end();?>
            </div><!-- /.rate-like -->
        </div>
        <div class="comment__bottom">
            <?php if (!Yii::$app->user->isGuest && ($model->level < $maxLevel || is_null($maxLevel))) : ?>
                <?php echo Html::a(Yii::t('yii2mod.comments', 'Reply'), '#', ['class' => 'comment__link reply reply-comment-btn', 'data' => ['action' => 'reply', 'comment-id' => $model->id]]); ?>
            <?php endif; ?>
            <a href="#" class="comment__link">цитировать</a>
            <a href="#" class="comment__link">пожаловаться</a>
        </div>
    </div>
</div>
<?php if ($model->hasChildren()) : ?>
    <div class="comments__children">
        <?php foreach ($model->getChildren() as $children) : ?>
            <?php echo $this->render('_list', ['model' => $children, 'maxLevel' => $maxLevel]); ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
