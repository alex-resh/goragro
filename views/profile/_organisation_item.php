<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Organisation
 */
use yii\helpers\Html;?>
<div class="row panel panel-default">
    <div class="col-md-2">
        <?= Html::img($model->getThumbUploadUrl('img'));?>
    </div>
    <div class="col-md-6">
        <div>
            <?= Html::a(Html::encode($model->name), ['/organisation/index', 'id' => $model->id]);?>
        </div>
        <div>
            <?= Html::encode($model->address->toString);?>
        </div>
        <div>
            <?= implode(', ', $model->getCategories()->select('name')->column());?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="text-<?= $model->getStatusClass();?>">
            <?= $model->getStatusName();?>
        </div>
        <div>
            <?= Html::a('Редактировать', ['/organisation/update', 'id' => $model->clone_id > 0 ? $model->clone_id : $model->id]);?>
        </div>
    </div>
</div>
