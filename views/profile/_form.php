<?php
/* @var \app\models\User $user */
/* @var \app\models\Profile $profile */
use yii\helpers\Html;
use yii\helpers\Url;
//$profile = $user->profile;
?>
<?php echo Html::beginForm([Url::toRoute(['profile/index'])], 'post', [
        'class' => 'form-pup form-edit white-popup-block mfp-hide',
        'id' => 'form-edit',
        'enctype' => 'multipart/form-data'
]) ?>
    <h2 class="form__title title-inside">Редактирование профиля</h2>

    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">
                    Имя пользователя
                    <span class="form__star">*</span>
                </span>
        </label>
        <?php echo Html::activeTextInput($profile, 'name', ['required' => true, 'class' => 'form__input', 'placeholder' => '']) ?>
    </div>
    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">Место жительства
                    <span class="form__star">*</span>
                </span>
        </label>
        <?php echo Html::activeTextInput($profile, 'location', ['required' => true, 'class' => 'form__input', 'placeholder' => '']) ?>
    </div>
    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">Электронная почта
                    <span class="form__star">*</span>
                </span>
        </label>
        <?php echo Html::activeTextInput($user, 'email', ['required' => true, 'class' => 'form__input', 'placeholder' => '']) ?>
    </div>
    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">
                    Телефон
                </span>
        </label>
        <?php echo Html::activeTextInput($profile, 'phone', ['required' => true, 'class' => 'form__input', 'placeholder' => '']) ?>
    </div>
    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">
                    Обращение
                    <span class="form__star">*</span>
                </span>
        </label>
        <?php echo Html::activeTextInput($profile, 'name_prefix', ['required' => true, 'class' => 'form__input', 'placeholder' => 'На Вы пожалуйста...']) ?>
    </div>
    <div class="form__row">
        <label class="form__label">
                <span class="form__label-title">
                    Кратко о себе
                </span>
        </label>
        <?php echo Html::activeTextarea($profile, 'about', ['class' => 'form__input form__textarea', 'placeholder' => 'Ваш вопрос']) ?>
    </div>

    <div class="form__row">

        <label class="form__label">
                <span class="form__label-title">
                    Аватар
                </span>
        </label>

        <div class="file-upload file-upload--user">

            <div class="file-upload__avatar">
                <div class="file-upload__avatar-wrapper">
                    <img src="<?php echo $profile->getThumbUploadUrl('img', 'form_img') ?>" alt="">
                </div>
            </div>

            <label class="file-upload__btn">Выберите файл</label>
            <?php echo Html::activeFileInput($profile, 'img') ?>
            <div class="form-upload__info">
                <p class="file-upload__text"> ... или перетащите мышкой</p>
                <p class="file-upload__formats">форматы jpg, gif, png</p>
            </div>

        </div>

    </div>

    <div class="form__row form__row--checkeds">

        <div class="form__row-column">
            <div class="radio">
                <input class="radio__input" id="radio20" type="radio" name="radio">
                <label class="radio__wrapper" for="radio20">
                    <span class="radio__custom"></span><!-- кругляшок с ::before при checked-->
                    <span class="radio__label">
                            <span class="radio__text">Интересует стоимость</span>
                        </span>
                </label>
            </div>
            <div class="radio">
                <input class="radio__input" id="radio21" type="radio" name="radio" checked>
                <label class="radio__wrapper" for="radio21">
                    <span class="radio__custom"></span>
                    <span class="radio__label">
                            <span class="radio__text">Интересует вакансия</span>
                        </span>
                </label>
            </div>
            <div class="radio">
                <input class="radio__input" id="radio22" type="radio" name="radio">
                <label class="radio__wrapper" for="radio22">
                    <span class="radio__custom"></span>
                    <span class="radio__label">
                            <span class="radio__text">Интересует журналистика</span>
                        </span>
                </label>
            </div>
            <div class="radio">
                <input class="radio__input" id="radio23" type="radio" name="radio">
                <label class="radio__wrapper" for="radio23">
                    <span class="radio__custom"></span>
                    <span class="radio__label">
                            <span class="radio__text">Интересует статья на сайте</span>
                        </span>
                </label>

            </div>
        </div>

        <div class="form__row-column">
            <div class="checkbox">
                <input class="checkbox__input" id="check20" type="checkbox" name="checkbox">
                <label class="checkbox__wrapper" for="check20">
                    <span class="checkbox__custom"></span>
                    <span class="checkbox__label">
                            <span class="checkbox__text">Интересует стоимость</span>
                        </span>
                </label>
            </div>
            <div class="checkbox">
                <input class="checkbox__input" id="check21" type="checkbox" name="checkbox" checked>
                <label class="checkbox__wrapper" for="check21">
                    <span class="checkbox__custom"></span>
                    <span class="checkbox__label">
                            <span class="checkbox__text">Интересует вакансия</span>
                        </span>
                </label>
            </div>
            <div class="checkbox">
                <input class="checkbox__input" id="check22" type="checkbox" name="checkbox">
                <label class="checkbox__wrapper" for="check22">
                    <span class="checkbox__custom"></span>
                    <span class="checkbox__label">
                            <span class="checkbox__text">Интересует журналистика</span>
                        </span>
                </label>
            </div>
            <div class="checkbox">
                <input class="checkbox__input" id="check23" type="checkbox" name="checkbox">
                <label class="checkbox__wrapper" for="check23">
                    <span class="checkbox__custom"></span>
                    <span class="checkbox__label">
                            <span class="checkbox__text">Интересует статья на сайте</span>
                        </span>
                </label>
            </div>

        </div><!-- /.form__row-column -->

    </div><!-- /.form__row-col -->

    <div class="form__row">
        <input type="submit" class="form__btn btn btn-accent" value="Сохранить">
        <button class="form__btn btn btn-accent btn-gray">Отменить</button>
    </div>
<?php echo Html::endForm(); ?>