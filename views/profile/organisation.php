<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
$this->title = 'Мои оргинизации';
$this->params['breadcrumbs'][] = ['label' => 'Профайл', 'url' => ['/profile']];
$this->params['breadcrumbs'][] = $this->title;

use app\models\Organisation;
use app\models\search\OrganisationSearch;
use yii\bootstrap\Tabs;
use yii\widgets\ListView;
?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h3><?= $this->title;?></h3>
        <?= Tabs::widget([
            'items' => [
                [
                    'label' => 'Опубликованные (' .
                        (new OrganisationSearch())->searchForProfile([])->query->andWhere(['>', 'clone_id', 0])
                        ->count() . ')',
                    'url' => ['/profile/organisation'],
                    'active' => Yii::$app->controller->action->uniqueId == '/profile/organisation',
                ],
                [
                    'label' => 'Новые (' .
                        (new OrganisationSearch())->searchForProfile([])->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_NEW])
                        ->count() . ')',
                    'url' => ['/profile/organisation-new'],
                    'active' => Yii::$app->controller->action->uniqueId == '/profile/organisation-new',
                ],
                [
                    'label' => 'На модерации (' .
                        (new OrganisationSearch())->searchForProfile([])->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_MODERATED])
                        ->count() . ')',
                    'url' => ['/profile/organisation-moderated'],
                    'active' => Yii::$app->controller->action->uniqueId == '/profile/organisation-moderated',
                ],
                [
                    'label' => 'На доработке (' .
                        (new OrganisationSearch())->searchForProfile([])->query->andWhere([Organisation::tableName() . '.status' => Organisation::STATUS_REWORK])
                        ->count() . ')',
                    'url' => ['/profile/organisation-reworked'],
                    'active' => Yii::$app->controller->action->uniqueId == '/profile/organisation-reworked',
                ],
            ],
        ]);?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_organisation_item',
        ]);?>
    </div>
</div>
