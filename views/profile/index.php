<?php
/* @var $this \yii\web\View */
/* @var $user \app\models\User|null */
/* @var $self boolean */

use app\models\Organisation;
use app\models\search\OrganisationSearch;
use yii\helpers\Html; ?>
<div class="userview">

    <div class="userview__data">

        <div class="userview__header indent">

            <div class="userview__img">
                <img src="<?php echo $profile->getThumbUploadUrl('img', 'thumb') ?>" alt="">
            </div><!-- /.userview__img -->

            <div class="userview__info">

                <div class="userview__header-left">

                    <div class="userview__header-top">
                        <div class="userview__name <?php echo $user->isOnline()?'username_online': 'username_offline' ?>">
                            <span class="userview__online"></span>
                            <?php echo $user->name; ?>
                        </div>
                        <?php if ($self): ?>
                        <a href="#form-edit" class="userview__link popup-with-form">
                                            <span class="userview__link-ico">
                                                <svg class="ico ico-error">
                                                    <use xlink:href="images/svg/symbol/sprite.svg#error"></use>
                                                </svg>
                                            </span>
                            <span class="userview__link-text">Редактировать</span>
                        </a>
                        <?php endif; ?>
                    </div>

                    <div class="userview__header-bottom">
                        <div class="userview__rate">
                            <p class="userview__rate-text">
                                Рейтинг на сайте
                            </p>

                            <div class="userview__rate-list">
                                <div class="userview__rate-item userview__rate-positive">
                                    <div class="userview__circle circle" id="circle1">
                                        <svg class="ico ico-plus">
                                            <use xlink:href="images/svg/symbol/sprite.svg#plus"></use>
                                        </svg>
                                    </div>
                                    <div class="userview__rate-count">60%</div>
                                </div><!-- /.userview__rate-item -->

                                <div class="userview__rate-item  userview__rate-negative">
                                    <div class="userview__circle circle" id="circle2">
                                        <svg class="ico ico-minus">
                                            <use xlink:href="images/svg/symbol/sprite.svg#minus"></use>
                                        </svg>
                                    </div>
                                    <div class="userview__rate-count">40%</div>
                                </div><!-- /.userview__rate-item -->
                            </div>


                        </div><!-- /.userview__rate -->
                    </div><!-- /.userview__header-bottom -->

                </div><!-- /.userview__header-left -->

                <div class="userview__header-right">

                    <div class="userview__header-links">
                        <?php if (!$self): ?>
                        <a href="#" class="userview__link">
                                            <span class="userview__link-ico">
                                                <svg class="ico ico-dialog">
                                                    <use xlink:href="images/svg/symbol/sprite.svg#dialog"></use>
                                                </svg>
                                            </span>
                            <span class="userview__link-text">Отправить сообщение</span>
                        </a>

                        <a href="#" class="userview__link">
                                            <span class="userview__link-ico">
                                                <svg class="ico ico-friend">
                                                    <use xlink:href="images/svg/symbol/sprite.svg#friend"></use>
                                                </svg>
                                            </span>
                            <span class="userview__link-text">Добавить в друзья</span>
                        </a>
                        <?php endif; ?>
                    </div><!-- /.userview__header-links -->

                </div><!-- /.userview__header-right -->

            </div><!-- /.userview__info -->

        </div><!-- /.userview__header -->

        <div class="userview__section indent">

            <div class="userview__stats stats">
                <div class="stats__line">
                    <div class="stats__item">
                        <div class="stats__ico">
                            <svg class="ico ico-notebook">
                                <use xlink:href="images/svg/symbol/sprite.svg#notebook"></use>
                            </svg>
                        </div>
                        <div class="stats__info">
                            <div class="stats__label">на сайте</div>
                            <div class="stats__count">246 дня</div>
                        </div>
                    </div>
                    <div class="stats__item">
                        <div class="stats__ico">
                            <svg class="ico ico-raiting">
                                <use xlink:href="images/svg/symbol/sprite.svg#raiting"></use>
                            </svg>
                        </div>
                        <div class="stats__info">
                            <div class="stats__label">рейтинг</div>
                            <div class="stats__count">5766</div>
                        </div>
                    </div>
                    <div class="stats__item">
                        <div class="stats__ico stats__ico--heart">
                            <svg class="ico ico-heart">
                                <use xlink:href="images/svg/symbol/sprite.svg#heart"></use>
                            </svg>
                        </div>
                        <div class="stats__info">
                            <div class="stats__label">спасибо</div>
                            <div class="stats__count">621</div>
                        </div>
                    </div>
                    <div class="stats__item">
                        <div class="stats__ico">
                            <svg class="ico ico-reviews">
                                <use xlink:href="images/svg/symbol/sprite.svg#reviews"></use>
                            </svg>
                        </div>
                        <div class="stats__info">
                            <div class="stats__label">отзывы</div>
                            <div class="stats__count">5766</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="userview__table">
                <div class="userview__table-item">
                    <div class="userview__row">
                        <div class="userview__table-key">Место жительства</div>
                        <div class="userview__table-value"><?php echo $profile->location ?></div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Зарегистрирован</div>
                        <div class="userview__table-value"><?php echo date('d F Y h:i', $user->created_at) ?></div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Последняя активность</div>
                        <div class="userview__table-value"><?php echo date('d F Y h:i', $user->last_action_time) ?></div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Группа</div>
                        <div class="userview__table-value">Администраторы</div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">E-mail</div>
                        <a href="mailto:garago@mail.ru"
                           class="userview__table-value link"><?php echo $user->email ?></a>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Телефон</div>
                        <a href="tel:79816667712" class="userview__table-value link"><?php echo $profile->phone ?></a>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Соцсети</div>
                        <div class="userview__table-value">
                            <div class="socsety userview__socsety">

                                <div class="socsety__list">
                                    <a href="#" class="socsety__item vk">
                                        <svg class="ico ico-vk">
                                            <use xlink:href="images/svg/symbol/sprite.svg#vk"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item tw">
                                        <svg class="ico ico-tw">
                                            <use xlink:href="images/svg/symbol/sprite.svg#tw"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item fc">
                                        <svg class="ico ico-fc">
                                            <use xlink:href="images/svg/symbol/sprite.svg#fc"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item ok">
                                        <svg class="ico ico-ok">
                                            <use xlink:href="images/svg/symbol/sprite.svg#ok"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item mail">
                                        <svg class="ico ico-mail">
                                            <use xlink:href="images/svg/symbol/sprite.svg#mail"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item instagram">
                                        <svg class="ico ico-instagram" width="13" height="13">
                                            <use xlink:href="images/svg/symbol/sprite.svg#instagram"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item youtube-text">
                                        <svg class="ico ico-youtube-text" width="17" height="17">
                                            <use xlink:href="images/svg/symbol/sprite.svg#youtube-text"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="socsety__item youtube">
                                        <svg class="ico ico-youtube" width="17" height="17">
                                            <use xlink:href="images/svg/symbol/sprite.svg#youtube"></use>
                                        </svg>
                                    </a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div><!-- /.userview__table-item -->

                <div class="userview__table-item">
                    <div class="userview__row">
                        <div class="userview__table-key">Публикации</div>
                        <div class="userview__table-value">
                            <span class="userview__table-count">21</span>
                            <a href="#" class="userview__table-link">все публикации</a>
                        </div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Комментарии</div>
                        <div class="userview__table-value">
                            <span class="userview__table-count">267</span>
                            <a href="#" class="userview__table-link">все комментарии</a>
                        </div>
                    </div>
                    <div class="userview__row">
                        <div class="userview__table-key">Организации</div>
                        <div class="userview__table-value">
                            <?php $count = (new OrganisationSearch())->searchForProfile([])->query
                            ->andWhere(['or', ['!=', Organisation::tableName() . '.status', Organisation::STATUS_PUBLISHED], ['>', 'clone_id', 0]])->count();?>
                            <span class="userview__table-count"><?= $count;?></span>
                            <?= Html::a('все организациии', ['/profile/organisation'], ['class' => 'userview__table-link']);?>
                        </div>
                    </div>
                </div><!-- /.userview__table-item -->

                <div class="userview__table-item">
                    <div class="userview__row">
                        <div class="userview__table-key">О себе</div>
                        <div class="userview__table-value">
                            <p class="userview__text">
                                <?php echo $profile->about ?>
                            </p>
                        </div>
                    </div>
                </div><!-- /.userview__table-item -->

            </div><!-- /.userview__table -->

        </div><!-- /.userview__section -->
    </div>

    <div class="post-anounce post-anounce--gallery gallery-list">

        <div class="post-anounce__header">
            <div class="post-anounce__line">
                <a href="images/news/anounce/1.jpg"
                   class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('images/news/anounce/1.jpg');">
                                    <span class="post-anounce__ico post-anounce__ico--magnifier">
                                        <svg class="ico ico-search-round">
                                            <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                        </svg>
                                    </span>
                </a>
                <a href="images/news/anounce/2.jpg"
                   class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('images/news/anounce/2.jpg');">
                                    <span class="post-anounce__ico post-anounce__ico--magnifier">
                                        <svg class="ico ico-search-round">
                                            <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                        </svg>
                                    </span>
                </a>
                <a href="images/news/anounce/3.jpg"
                   class="post-anounce__item post-anounce__item-gallery"
                   style="background-image: url('images/news/anounce/3.jpg');">
                                    <span class="post-anounce__ico post-anounce__ico--magnifier">
                                        <svg class="ico ico-search-round">
                                            <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                        </svg>
                                    </span>
                </a>
                <a href="images/news/anounce/5.jpg" class="post-anounce__item post-anounce__item--more"
                   style="background-image: url('images/news/anounce/4.jpg');">
                    <div class="post-anounce__info post-anounce__info--flex">

                        <div class="post-anounce__text">
                            <div class="post-anounce__label">
                                Еще фото
                            </div>
                            всего 256
                        </div>

                        <div class="post-anounce__ico">
                            <svg class="ico ico-arrow">
                                <use xlink:href="images/svg/symbol/sprite.svg#arrow"></use>
                            </svg>
                        </div>

                    </div>
                </a>
            </div><!-- /.post-anounce__line -->
        </div>

        <div class="post-anounce__section">
            <div class="post-anounce__more-wrapper">
                <div class="post-anounce__line">
                    <a href="images/news/anounce/2.jpg"
                       class="post-anounce__item post-anounce__item-gallery"
                       style="background-image: url('images/news/anounce/2.jpg');">
                                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                                            <svg class="ico ico-search-round">
                                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                            </svg>
                                        </span>
                    </a>
                    <a href="images/news/anounce/1.jpg"
                       class="post-anounce__item post-anounce__item-gallery"
                       style="background-image: url('images/news/anounce/1.jpg');">
                                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                                            <svg class="ico ico-search-round">
                                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                            </svg>
                                        </span>
                    </a>
                    <a href="images/news/anounce/4.jpg"
                       class="post-anounce__item post-anounce__item-gallery"
                       style="background-image: url('images/news/anounce/4.jpg');">
                                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                                            <svg class="ico ico-search-round">
                                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                            </svg>
                                        </span>
                    </a>
                    <a href="images/news/anounce/3.jpg"
                       class="post-anounce__item post-anounce__item-gallery"
                       style="background-image: url('images/news/anounce/3.jpg');">
                                        <span class="post-anounce__ico post-anounce__ico--magnifier">
                                            <svg class="ico ico-search-round">
                                                <use xlink:href="images/svg/symbol/sprite.svg#search-round"></use>
                                            </svg>
                                        </span>
                    </a>
                </div>
            </div>
        </div>

    </div><!-- /.post-anounce -->

    <div class="tabs tabs-userview-request" id="tabs-userview-request">
        <ul class="tabs__list indent">
            <li class="tabs__item ui-tabs-active">
                <a class="tabs__link" href="#tabs-userview-request1">
                    <span class="userview__request-label">Новые запросы</span>
                    <span class="userview__request-count">6</span>
                </a>
            </li>
            <li class="tabs__item">
                <a class="tabs__link" href="#tabs-userview-request2">История запросов</a>
            </li>
            <li class="tabs__item">
                <a class="tabs__link" href="#tabs-userview-request3">
                    <span class="userview__request-label">Отзывы</span>
                    <span class="userview__request-count">3</span>
                </a>
            </li>
            <li class="tabs__item">
                <a class="tabs__link" href="#tabs-userview-request4">
                    <span class="userview__request-label">Комментарии</span>
                    <span class="userview__request-count">25</span>
                </a>
            </li>
        </ul>

        <div class="tabs__container">
            <div id="tabs-userview-request1" class="tabs__content">
                <div class="userview__request request">
                    <div class="request__list">

                        <div class="request__top">
                            <div class="request__item">
                                <div class="request__ico">
                                    <svg class="ico ico-bold">
                                        <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                                    </svg>
                                </div>
                                <div class="request__info">
                                    <div class="request__info-left">

                                        <div class="request__img">
                                            <img src="images/catalog/comma.jpg" alt="">
                                        </div>

                                        <div class="request__descr">
                                            <div class="request__info-top">
                                                <a href="#" class="request__title title-inside">АТЦ
                                                    Новая
                                                    Рига</a>
                                                <div class="request__star">
                                                    <svg class="ico ico-star">
                                                        <use
                                                            xlink:href="images/svg/symbol/sprite.svg#star">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>

                                            <div class="request__info-table">
                                                <div class="request__info-column">
                                                    <div class="request__info-line request__info-line-address">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-location">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#location">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">109153, г.
                                                                            Москва, Лермонтовский пр-кт, д. 21 А109153, г.
                                                                            Москва, Лермонтовский пр-кт, д. 21 А</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-keyboard">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">8 495
                                                                            961-88-44</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->

                                                <div class="request__info-column">
                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-rate">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#rate">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">Место в
                                                                            рейтинге 15</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-bid">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#bid">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">Выполнил заявок
                                                                            126</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->
                                            </div><!-- /.request__info-table -->

                                        </div><!-- /.request__descr -->

                                    </div><!-- /.request__info-left -->

                                    <div class="request__info-right">
                                        <div class="rate-like rate-like--row">
                                            <div class="rate-like__positive rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-up">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">215</span>
                                            </div>
                                            <div class="rate-like__negative rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-down">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">62</span>
                                            </div>
                                        </div>
                                    </div><!-- /.request__info-right -->
                                </div><!-- /.request__info -->
                            </div><!-- /.request__item -->

                            <div class="request__item">
                                <div class="request__ico">
                                    <svg class="ico ico-bold">
                                        <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                                    </svg>
                                </div>
                                <div class="request__info">
                                    <div class="request__info-left">

                                        <div class="request__img">
                                            <img src="images/catalog/comma.jpg" alt="">
                                        </div>

                                        <div class="request__descr">
                                            <div class="request__info-top">
                                                <a href="#" class="request__title title-inside">АТЦ
                                                    Новая
                                                    Рига</a>
                                                <div class="request__star">
                                                    <svg class="ico ico-star">
                                                        <use
                                                            xlink:href="images/svg/symbol/sprite.svg#star">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>

                                            <div class="request__info-table">
                                                <div class="request__info-column">
                                                    <div class="request__info-line request__info-line-address">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-location">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#location">
                                                                                </use>
                                                                            </svg>

                                                                        </span>
                                                        <span class="request__info-text">109153, г.
                                                                            Москва, Лермонтовский пр-кт, д. 21 А</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-keyboard">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">8 495
                                                                            961-88-44</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->

                                                <div class="request__info-column">
                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-location">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#location">
                                                                                </use>
                                                                            </svg>

                                                                        </span>
                                                        <span class="request__info-text">Место в
                                                                            рейтинге 15</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-keyboard">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">Выполнил заявок
                                                                            126</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->
                                            </div><!-- /.request__info-table -->

                                        </div><!-- /.request__descr -->

                                    </div><!-- /.request__info-left -->

                                    <div class="request__info-right">
                                        <div class="rate-like rate-like--row">
                                            <div class="rate-like__positive rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-up">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">215</span>
                                            </div>
                                            <div class="rate-like__negative rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-down">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">62</span>
                                            </div>
                                        </div>
                                    </div><!-- /.request__info-right -->
                                </div><!-- /.request__info -->
                            </div><!-- /.request__item -->

                            <div class="request__item">
                                <div class="request__ico">
                                    <svg class="ico ico-bold">
                                        <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                                    </svg>
                                </div>
                                <div class="request__info">
                                    <div class="request__info-left">

                                        <div class="request__img">
                                            <img src="images/catalog/comma.jpg" alt="">
                                        </div>

                                        <div class="request__descr">
                                            <div class="request__info-top">
                                                <a href="#" class="request__title title-inside">АТЦ
                                                    Новая
                                                    Рига</a>
                                                <div class="request__star">
                                                    <svg class="ico ico-star">
                                                        <use
                                                            xlink:href="images/svg/symbol/sprite.svg#star">
                                                        </use>
                                                    </svg>
                                                </div>
                                            </div>

                                            <div class="request__info-table">
                                                <div class="request__info-column">
                                                    <div class="request__info-line request__info-line-address">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-location">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#location">
                                                                                </use>
                                                                            </svg>

                                                                        </span>
                                                        <span class="request__info-text">109153, г.
                                                                            Москва, Лермонтовский пр-кт, д. 21 А</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-keyboard">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">8 495
                                                                            961-88-44</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->

                                                <div class="request__info-column">
                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-location">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#location">
                                                                                </use>
                                                                            </svg>

                                                                        </span>
                                                        <span class="request__info-text">Место в
                                                                            рейтинге 15</span>
                                                    </div>

                                                    <div class="request__info-line">
                                                                        <span class="request__info-ico">
                                                                            <svg class="ico ico-keyboard">
                                                                                <use
                                                                                    xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                                </use>
                                                                            </svg>
                                                                        </span>
                                                        <span class="request__info-text">Выполнил заявок
                                                                            126</span>
                                                    </div>
                                                </div><!-- /.request__info-column -->
                                            </div><!-- /.request__info-table -->

                                        </div><!-- /.request__descr -->

                                    </div><!-- /.request__info-left -->

                                    <div class="request__info-right">
                                        <div class="rate-like rate-like--row">
                                            <div class="rate-like__positive rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-up">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">215</span>
                                            </div>
                                            <div class="rate-like__negative rate-like__item">
                                                                <span class="rate-like__ico">
                                                                    <svg class="ico ico-like-down">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="rate-like__index">62</span>
                                            </div>
                                        </div>
                                    </div><!-- /.request__info-right -->
                                </div><!-- /.request__info -->
                            </div><!-- /.request__item -->
                        </div>

                        <div class="request__item">
                            <div class="request__ico">
                                <svg class="ico ico-bold">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                                </svg>
                            </div>
                            <div class="request__info">
                                <div class="request__info-left">

                                    <div class="request__img">
                                        <img src="images/catalog/comma.jpg" alt="">
                                    </div>

                                    <div class="request__descr">
                                        <div class="request__info-top">
                                            <a href="#" class="request__title title-inside">АТЦ Новая
                                                Рига</a>
                                            <div class="request__star">
                                                <svg class="ico ico-star">
                                                    <use xlink:href="images/svg/symbol/sprite.svg#star">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>

                                        <div class="request__info-table">
                                            <div class="request__info-column">
                                                <div class="request__info-line request__info-line-address">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-location">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#location">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request__info-text">109153, г. Москва,
                                                                        Лермонтовский пр-кт, д. 21 А</span>
                                                </div>

                                                <div class="request__info-line">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-keyboard">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request__info-text">8 495
                                                                        961-88-44</span>
                                                </div>
                                            </div><!-- /.request__info-column -->

                                            <div class="request__info-column">
                                                <div class="catalog__item-info-line">
                                                    <i class="catalog__item-ico">
                                                        <svg class="ico ico-clock">
                                                            <use
                                                                xlink:href="images/svg/symbol/sprite.svg#clock">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                    <div class="catalog__item-text">с 8:00 до
                                                        19:00<span>Работает сейчас</span>
                                                    </div>
                                                </div>

                                                <div class="catalog__item-info-line">
                                                    <i class="catalog__item-ico">
                                                        <svg class="ico ico-arrow-link">
                                                            <use
                                                                xlink:href="images/svg/symbol/sprite.svg#arrow-link">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                    <a href="#" class="catalog__item-text catalog__item-link">Перейти на сайт</a>
                                                </div>
                                            </div><!-- /.request__info-column -->
                                        </div><!-- /.request__info-table -->

                                    </div><!-- /.request__descr -->

                                </div><!-- /.request__info-left -->

                                <div class="request__info-right">

                                    <div class="rate-like rate-like--row">
                                        <div class="rate-like__positive rate-like__item">
                                                            <span class="rate-like__ico">
                                                                <svg class="ico ico-like-up">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="rate-like__index">215</span>
                                        </div>
                                        <div class="rate-like__negative rate-like__item">
                                                            <span class="rate-like__ico">
                                                                <svg class="ico ico-like-down">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="rate-like__index">62</span>
                                        </div>
                                    </div>

                                    <div class="request__menu request-menu">

                                        <div class="request-menu__list">
                                            <a href="#" class="request-menu__item">
                                                                <span class="request-menu__item-ico">
                                                                    <svg class="ico ico-note">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#note">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span
                                                    class="request-menu__item-text">Редактировать</span>
                                            </a>

                                            <a href="#form-access-company"
                                               class="request-menu__item popup-with-form">
                                                                <span class="request-menu__item-ico">
                                                                    <svg class="ico ico-access">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#access">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="request-menu__item-text">Доступ к
                                                                    фирме</span>
                                            </a>


                                            <a href="#"
                                               class="request-menu__item request-menu__item--more">
                                                                <span class="request-menu__item-ico">
                                                                    <svg class="ico ico-more">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#more">
                                                                        </use>
                                                                    </svg>
                                                                </span>
                                                <span class="request-menu__item-text">Еще...</span>
                                            </a>
                                        </div>

                                        <div class="request-menu__term">до 20 марта 2019</div>

                                        <div class="request-menu__dropdown">
                                            <div class="request-menu__dropdown-top">
                                                <a href="#form-raise-ad"
                                                   class="request-menu__item request-menu__dropdown-item popup-with-form">
                                                                    <span class="request-menu__item-ico">
                                                                        <svg class="ico ico-top">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#top">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request-menu__item-text">Поднять</span>
                                                </a>
                                                <a href="#form-fix-ad"
                                                   class="request-menu__item request-menu__dropdown-item popup-with-form">
                                                                    <span class="request-menu__item-ico">
                                                                        <svg class="ico ico-lock">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#lock">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span
                                                        class="request-menu__item-text">Закрепить</span>
                                                </a>
                                                <a href="#form-highlight-ad"
                                                   class="request-menu__item request-menu__dropdown-item popup-with-form">
                                                                    <span class="request-menu__item-ico">
                                                                        <svg class="ico ico-distinguish">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#distinguish">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span
                                                        class="request-menu__item-text">Выделить</span>
                                                </a>
                                            </div>
                                            <div class="request-menu__dropdown-bottom">
                                                <a href="#" class="request-menu__item">
                                                                    <span class="request-menu__item-ico">
                                                                        <svg class="ico ico-close">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#close">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request-menu__item-text">Удалить</span>
                                                </a>

                                            </div>

                                        </div>
                                    </div>

                                </div><!-- /.request__info-right -->
                            </div><!-- /.request__info -->
                        </div><!-- /.request__item -->

                        <div class="request__item">
                            <div class="request__ico">
                                <svg class="ico ico-bold">
                                    <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                                </svg>
                            </div>
                            <div class="request__info">
                                <div class="request__info-left">

                                    <div class="request__img">
                                        <img src="images/catalog/comma.jpg" alt="">
                                    </div>

                                    <div class="request__descr">
                                        <div class="request__info-top">
                                            <a href="#" class="request__title title-inside">АТЦ Новая
                                                Рига</a>
                                            <div class="request__star">
                                                <svg class="ico ico-star">
                                                    <use xlink:href="images/svg/symbol/sprite.svg#star">
                                                    </use>
                                                </svg>
                                            </div>
                                        </div>

                                        <div class="request__info-table">
                                            <div class="request__info-column">
                                                <div class="request__info-line request__info-line-address">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-location">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#location">
                                                                            </use>
                                                                        </svg>

                                                                    </span>
                                                    <span class="request__info-text">109153, г. Москва,
                                                                        Лермонтовский пр-кт, д. 21 А</span>
                                                </div>

                                                <div class="request__info-line">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-keyboard">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request__info-text">8 495
                                                                        961-88-44</span>
                                                </div>
                                            </div><!-- /.request__info-column -->

                                            <div class="request__info-column">
                                                <div class="request__info-line">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-location">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#location">
                                                                            </use>
                                                                        </svg>

                                                                    </span>
                                                    <span class="request__info-text">Место в рейтинге
                                                                        15</span>
                                                </div>

                                                <div class="request__info-line">
                                                                    <span class="request__info-ico">
                                                                        <svg class="ico ico-keyboard">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                    <span class="request__info-text">Выполнил заявок
                                                                        126</span>
                                                </div>
                                            </div><!-- /.request__info-column -->
                                        </div><!-- /.request__info-table -->

                                    </div><!-- /.request__descr -->

                                </div><!-- /.request__info-left -->

                                <div class="request__info-right">
                                    <div class="rate-like rate-like--row">
                                        <div class="rate-like__positive rate-like__item">
                                                            <span class="rate-like__ico">
                                                                <svg class="ico ico-like-up">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="rate-like__index">215</span>
                                        </div>
                                        <div class="rate-like__negative rate-like__item">
                                                            <span class="rate-like__ico">
                                                                <svg class="ico ico-like-down">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="rate-like__index">62</span>
                                        </div>
                                    </div>
                                </div><!-- /.request__info-right -->
                            </div><!-- /.request__info -->
                        </div><!-- /.request__item -->

                    </div><!-- /.request__list -->
                </div><!-- /.userview__request -->
            </div>

            <div id="tabs-userview-request2" class="tabs__content">

                <div class="request__item">
                    <div class="request__ico">
                        <svg class="ico ico-bold">
                            <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                        </svg>
                    </div>
                    <div class="request__info">
                        <div class="request__info-left">

                            <div class="request__img">
                                <img src="images/catalog/comma.jpg" alt="">
                            </div>

                            <div class="request__descr">
                                <div class="request__info-top">
                                    <a href="#" class="request__title title-inside">АТЦ Новая Рига</a>
                                    <div class="request__star">
                                        <svg class="ico ico-star">
                                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="request__info-table">
                                    <div class="request__info-column">
                                        <div class="request__info-line request__info-line-address">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">109153, г. Москва,
                                                                Лермонтовский пр-кт, д. 21 А</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">8 495 961-88-44</span>
                                        </div>
                                    </div><!-- /.request__info-column -->

                                    <div class="request__info-column">
                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">Место в рейтинге 15</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">Выполнил заявок 126</span>
                                        </div>
                                    </div><!-- /.request__info-column -->
                                </div><!-- /.request__info-table -->

                            </div><!-- /.request__descr -->

                        </div><!-- /.request__info-left -->

                        <div class="request__info-right">
                            <div class="rate-like rate-like--row">
                                <div class="rate-like__positive rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-up">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">215</span>
                                </div>
                                <div class="rate-like__negative rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-down">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">62</span>
                                </div>
                            </div>
                        </div><!-- /.request__info-right -->
                    </div><!-- /.request__info -->
                </div><!-- /.request__item -->

                <div class="request__item">
                    <div class="request__ico">
                        <svg class="ico ico-bold">
                            <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                        </svg>
                    </div>
                    <div class="request__info">
                        <div class="request__info-left">

                            <div class="request__img">
                                <img src="images/catalog/comma.jpg" alt="">
                            </div>

                            <div class="request__descr">
                                <div class="request__info-top">
                                    <a href="#" class="request__title title-inside">АТЦ Новая Рига</a>
                                    <div class="request__star">
                                        <svg class="ico ico-star">
                                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="request__info-table">
                                    <div class="request__info-column">
                                        <div class="request__info-line request__info-line-address">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">109153, г. Москва,
                                                                Лермонтовский пр-кт, д. 21 А</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">8 495 961-88-44</span>
                                        </div>
                                    </div><!-- /.request__info-column -->

                                    <div class="request__info-column">
                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">Место в рейтинге 15</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">Выполнил заявок 126</span>
                                        </div>
                                    </div><!-- /.request__info-column -->
                                </div><!-- /.request__info-table -->

                            </div><!-- /.request__descr -->

                        </div><!-- /.request__info-left -->

                        <div class="request__info-right">
                            <div class="rate-like rate-like--row">
                                <div class="rate-like__positive rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-up">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">215</span>
                                </div>
                                <div class="rate-like__negative rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-down">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">62</span>
                                </div>
                            </div>
                        </div><!-- /.request__info-right -->
                    </div><!-- /.request__info -->
                </div><!-- /.request__item -->

                <div class="request__item">
                    <div class="request__ico">
                        <svg class="ico ico-bold">
                            <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                        </svg>
                    </div>
                    <div class="request__info">
                        <div class="request__info-left">

                            <div class="request__img">
                                <img src="images/catalog/comma.jpg" alt="">
                            </div>

                            <div class="request__descr">
                                <div class="request__info-top">
                                    <a href="#" class="request__title title-inside">АТЦ Новая Рига</a>
                                    <div class="request__star">
                                        <svg class="ico ico-star">
                                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="request__info-table">
                                    <div class="request__info-column">
                                        <div class="request__info-line request__info-line-address">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">109153, г. Москва,
                                                                Лермонтовский пр-кт, д. 21 А</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">8 495 961-88-44</span>
                                        </div>
                                    </div><!-- /.request__info-column -->

                                    <div class="request__info-column">
                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">Место в рейтинге 15</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">Выполнил заявок 126</span>
                                        </div>
                                    </div><!-- /.request__info-column -->
                                </div><!-- /.request__info-table -->

                            </div><!-- /.request__descr -->

                        </div><!-- /.request__info-left -->

                        <div class="request__info-right">
                            <div class="rate-like rate-like--row">
                                <div class="rate-like__positive rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-up">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">215</span>
                                </div>
                                <div class="rate-like__negative rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-down">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">62</span>
                                </div>
                            </div>
                        </div><!-- /.request__info-right -->
                    </div><!-- /.request__info -->
                </div><!-- /.request__item -->

                <div class="request__item">
                    <div class="request__ico">
                        <svg class="ico ico-bold">
                            <use xlink:href="images/svg/symbol/sprite.svg#arrow-bold"></use>
                        </svg>
                    </div>
                    <div class="request__info">
                        <div class="request__info-left">

                            <div class="request__img">
                                <img src="images/catalog/comma.jpg" alt="">
                            </div>

                            <div class="request__descr">
                                <div class="request__info-top">
                                    <a href="#" class="request__title title-inside">АТЦ Новая Рига</a>
                                    <div class="request__star">
                                        <svg class="ico ico-star">
                                            <use xlink:href="images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </div>
                                </div>

                                <div class="request__info-table">
                                    <div class="request__info-column">
                                        <div class="request__info-line request__info-line-address">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">109153, г. Москва,
                                                                Лермонтовский пр-кт, д. 21 А</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">8 495 961-88-44</span>
                                        </div>
                                    </div><!-- /.request__info-column -->

                                    <div class="request__info-column">
                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-location">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#location">
                                                                    </use>
                                                                </svg>

                                                            </span>
                                            <span class="request__info-text">Место в рейтинге 15</span>
                                        </div>

                                        <div class="request__info-line">
                                                            <span class="request__info-ico">
                                                                <svg class="ico ico-keyboard">
                                                                    <use
                                                                        xlink:href="images/svg/symbol/sprite.svg#keyboard">
                                                                    </use>
                                                                </svg>
                                                            </span>
                                            <span class="request__info-text">Выполнил заявок 126</span>
                                        </div>
                                    </div><!-- /.request__info-column -->
                                </div><!-- /.request__info-table -->

                            </div><!-- /.request__descr -->

                        </div><!-- /.request__info-left -->

                        <div class="request__info-right">
                            <div class="rate-like rate-like--row">
                                <div class="rate-like__positive rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-up">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">215</span>
                                </div>
                                <div class="rate-like__negative rate-like__item">
                                                    <span class="rate-like__ico">
                                                        <svg class="ico ico-like-down">
                                                            <use xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                            </use>
                                                        </svg>
                                                    </span>
                                    <span class="rate-like__index">62</span>
                                </div>
                            </div>
                        </div><!-- /.request__info-right -->
                    </div><!-- /.request__info -->
                </div><!-- /.request__item -->
            </div>

            <div id="tabs-userview-request3" class="tabs__content">
                <div class="feedback-user">

                    <div class="feedback-user__item">
                        <div class="feedback-user__wrapper">

                            <div class="feedback-user__avatar">
                                <div class="feedback-user__avatar-wrapper">
                                    <img src="images/avatar.jpg" alt="Аватар">
                                </div>
                            </div>

                            <div class="feedback-user__info">
                                <div class="feedback-user__header">

                                    <div class="rate">
                                        <div class="rate__like--alone rate__like--positive">

                                            <div class="rate-like">
                                                <div class="rate-like__positive rate-like__item active">
                                                                    <span class="rate-like__ico">
                                                                        <svg class="ico ico-like-up">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                </div>
                                            </div>

                                            <span class="feedback-user__span">Отзыв</span>

                                        </div>
                                    </div><!-- /.rate -->

                                    <div class="feedback-user__header-text">
                                        о компании
                                        <span class="feedback-user__span"><a href="#">Traveler`s
                                                                Coffee</a></span>
                                    </div>
                                </div>
                                <div class="feedback-user__section">
                                    <div class="feedback-user__section-top">
                                        <div class="feedback-user__left">
                                            <span class="feedback-user__name">Alexey K</span>
                                            <span class="feedback-user__date"> от 12.02.2017 в
                                                                11:52</span>
                                        </div>
                                        <div class="feedback-user__right">
                                                            <span class="feedback-user__label">написал 2 отзыва об этом
                                                                месте</span>
                                        </div>

                                    </div>
                                    <div class="feedback-user__section-text text">
                                        <p>«Пошли кофе пить», - сказал мне мой товарищ по приятному
                                            времяпрепровождению. Не долго думая, мы двинули в сторону
                                            Городской
                                            Кулинарии TORTA на Тарской, 13а.</p>

                                        <p>Небольшое помещение, два человека персонала, сотрудники
                                            ближайших
                                            офисов,
                                            обедающие и просто употребляющие кофе и мы. Я брал латте с
                                            халвой,
                                            эклер
                                            и потом еще молочный коктейль. Здесь удовлетворение по двум
                                            пунктам
                                            из
                                            трех. Эклер был жесткий и не очень свежий и в целом не очень
                                            вкусный. А
                                            вот напитки просто прекрасные. И кофе с халвой, и молочный
                                            коктейли
                                            на 5
                                            баллов. Сотрудницы добродушные и улыбчивые. Температура в
                                            помещении
                                            комфортная. Сиденья удобные.</p>
                                    </div>
                                    <div class="feedback-user__section-bottom">

                                        <a href="#" class="feedback-user__link reply">
                                            <svg class="ico ico-dialog">
                                                <use xlink:href="images/svg/symbol/sprite.svg#dialog">
                                                </use>
                                            </svg>
                                            <span class="feedback-user__link-text">
                                                                Комментариев 0
                                                            </span>
                                        </a>

                                    </div><!-- /.feedback-user__section-bottom -->
                                </div>
                            </div>

                            <div class="feedback-user__rate">
                                <div class="rate-like rate-like--row">
                                    <div class="rate-like__positive rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-up">
                                                                <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">215</span>
                                    </div>
                                    <div class="rate-like__negative rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-down">
                                                                <use
                                                                    xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">62</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feedback-user__item">
                        <div class="feedback-user__wrapper">

                            <div class="feedback-user__avatar">
                                <div class="feedback-user__avatar-wrapper">
                                    <img src="images/avatar.jpg" alt="Аватар">
                                </div>
                            </div>

                            <div class="feedback-user__info">
                                <div class="feedback-user__header">

                                    <div class="rate">
                                        <div class="rate__like--alone rate__like--negative">

                                            <div class="rate-like">
                                                <div class="rate-like__negative rate-like__item active">
                                                                    <span class="rate-like__ico">
                                                                        <svg class="ico ico-like-down">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                </div>
                                            </div>

                                            <span class="feedback-user__span">Отзыв</span>

                                        </div>
                                    </div><!-- /.rate -->

                                    <div class="feedback-user__header-text">

                                        о компании
                                        <span class="feedback-user__span"><a href="#">Traveler`s
                                                                Coffee</a></span>
                                    </div>
                                </div>

                                <div class="feedback-user__section">

                                    <div class="feedback-user__section-top">
                                        <div class="feedback-user__left">
                                            <span class="feedback-user__name">Alexey K</span>
                                            <span class="feedback-user__date"> от 12.02.2017 в
                                                                11:52</span>
                                        </div>
                                        <div class="feedback-user__right">
                                                            <span class="feedback-user__label">написал 2 отзыва об этом
                                                                месте</span>
                                        </div>

                                    </div>

                                    <div class="feedback-user__gallery gallery-list post-anounce">

                                        <div class="feedback-user__gallery-list">
                                            <a href="images/recipe/1.jpg"
                                               class="feedback-user__gallery-item post-anounce__item-gallery"
                                               style="background-image: url('images/recipe/1.jpg');">

                                                                <span
                                                                    class="post-anounce__ico post-anounce__ico--magnifier">
                                                                    <svg class="ico ico-search-round">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#search-round">
                                                                        </use>
                                                                    </svg>
                                                                </span>

                                            </a>
                                            <a href="images/recipe/2.jpg"
                                               class="feedback-user__gallery-item post-anounce__item-gallery"
                                               style="background-image: url('images/recipe/2.jpg');">

                                                                <span
                                                                    class="post-anounce__ico post-anounce__ico--magnifier">
                                                                    <svg class="ico ico-search-round">
                                                                        <use
                                                                            xlink:href="images/svg/symbol/sprite.svg#search-round">
                                                                        </use>
                                                                    </svg>
                                                                </span>

                                            </a>
                                            <a href="images/recipe/3.jpg"
                                               class="feedback-user__gallery-item feedback-user__gallery-more"
                                               style="background-image: url('images/recipe/3.jpg');">

                                                                <span class="feedback-user__gallery-info">Еще 2
                                                                    фото</span>
                                            </a>
                                        </div><!-- /.feedback-user__gallery-list -->

                                        <div class="feedback-user__gallery-hidden">
                                            <div class="feedback-user__gallery-list">
                                                <a href="images/recipe/1.jpg"
                                                   class="feedback-user__gallery-item post-anounce__item-gallery"
                                                   style="background-image: url('images/recipe/1.jpg');">

                                                                    <span
                                                                        class="post-anounce__ico post-anounce__ico--magnifier">
                                                                        <svg class="ico ico-search-round">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#search-round">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                </a>
                                                <a href="images/recipe/2.jpg"
                                                   class="feedback-user__gallery-item post-anounce__item-gallery"
                                                   style="background-image: url('images/recipe/2.jpg');">

                                                                    <span
                                                                        class="post-anounce__ico post-anounce__ico--magnifier">
                                                                        <svg class="ico ico-search-round">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#search-round">
                                                                            </use>
                                                                        </svg>
                                                                    </span>

                                                </a>
                                            </div><!-- /.feedback-user__gallery-list -->
                                        </div><!-- /.feedback-user__gallery-hidden -->

                                    </div><!-- /.feedback-user__gallery -->

                                    <div class="feedback-user__section-text text">
                                        <p>«Пошли кофе пить», - сказал мне мой товарищ по приятному
                                            времяпрепровождению. Не долго думая, мы двинули в сторону
                                            Городской
                                            Кулинарии TORTA на Тарской, 13а.</p>

                                        <p>Небольшое помещение, два человека персонала, сотрудники
                                            ближайших
                                            офисов,
                                            обедающие и просто употребляющие кофе и мы. Я брал латте с
                                            халвой,
                                            эклер
                                            и потом еще молочный коктейль. Здесь удовлетворение по двум
                                            пунктам
                                            из
                                            трех. Эклер был жесткий и не очень свежий и в целом не очень
                                            вкусный. А
                                            вот напитки просто прекрасные. И кофе с халвой, и молочный
                                            коктейли
                                            на 5
                                            баллов. Сотрудницы добродушные и улыбчивые. Температура в
                                            помещении
                                            комфортная. Сиденья удобные.</p>
                                    </div>
                                    <div class="feedback-user__section-bottom">

                                        <a href="#" class="feedback-user__link reply">
                                            <svg class="ico ico-dialog">
                                                <use xlink:href="images/svg/symbol/sprite.svg#dialog">
                                                </use>
                                            </svg>
                                            <span class="feedback-user__link-text">
                                                                Комментариев 0
                                                            </span>
                                        </a>

                                    </div><!-- /.feedback-user__section-bottom -->
                                </div>
                            </div>

                            <div class="feedback-user__rate">
                                <div class="rate-like rate-like--row">
                                    <div class="rate-like__positive rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-up">
                                                                <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">215</span>
                                    </div>
                                    <div class="rate-like__negative rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-down">
                                                                <use
                                                                    xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">62</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="feedback-user__item">
                        <div class="feedback-user__wrapper">

                            <div class="feedback-user__avatar">
                                <div class="feedback-user__avatar-wrapper">
                                    <img src="images/avatar.jpg" alt="Аватар">
                                </div>
                            </div>

                            <div class="feedback-user__info">
                                <div class="feedback-user__header">

                                    <div class="rate">
                                        <div class="rate__like--alone rate__like--positive">

                                            <div class="rate-like">
                                                <div class="rate-like__positive rate-like__item active">
                                                                    <span class="rate-like__ico">
                                                                        <svg class="ico ico-like-up">
                                                                            <use
                                                                                xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                            </use>
                                                                        </svg>
                                                                    </span>
                                                </div>
                                            </div>

                                            <span class="feedback-user__span">Отзыв</span>

                                        </div>
                                    </div><!-- /.rate -->

                                    <div class="feedback-user__header-text">
                                        о компании
                                        <span class="feedback-user__span"><a href="#">Traveler`s
                                                                Coffee</a></span>
                                    </div>
                                </div>
                                <div class="feedback-user__section">
                                    <div class="feedback-user__section-top">
                                        <div class="feedback-user__left">
                                            <span class="feedback-user__name">Alexey K</span>
                                            <span class="feedback-user__date"> от 12.02.2017 в
                                                                11:52</span>
                                        </div>
                                        <div class="feedback-user__right">
                                                            <span class="feedback-user__label">написал 2 отзыва об этом
                                                                месте</span>
                                        </div>

                                    </div>
                                    <div class="feedback-user__section-text text">
                                        <p>«Пошли кофе пить», - сказал мне мой товарищ по приятному
                                            времяпрепровождению. Не долго думая, мы двинули в сторону
                                            Городской
                                            Кулинарии TORTA на Тарской, 13а.</p>

                                        <p>Небольшое помещение, два человека персонала, сотрудники
                                            ближайших
                                            офисов,
                                            обедающие и просто употребляющие кофе и мы. Я брал латте с
                                            халвой,
                                            эклер
                                            и потом еще молочный коктейль. Здесь удовлетворение по двум
                                            пунктам
                                            из
                                            трех. Эклер был жесткий и не очень свежий и в целом не очень
                                            вкусный. А
                                            вот напитки просто прекрасные. И кофе с халвой, и молочный
                                            коктейли
                                            на 5
                                            баллов. Сотрудницы добродушные и улыбчивые. Температура в
                                            помещении
                                            комфортная. Сиденья удобные.</p>
                                    </div>
                                    <div class="feedback-user__section-bottom">

                                        <a href="#" class="feedback-user__link reply">
                                            <svg class="ico ico-dialog">
                                                <use xlink:href="images/svg/symbol/sprite.svg#dialog">
                                                </use>
                                            </svg>
                                            <span class="feedback-user__link-text">
                                                                Комментариев 0
                                                            </span>
                                        </a>

                                    </div><!-- /.feedback-user__section-bottom -->
                                </div>
                            </div>

                            <div class="feedback-user__rate">
                                <div class="rate-like rate-like--row">
                                    <div class="rate-like__positive rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-up">
                                                                <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">215</span>
                                    </div>
                                    <div class="rate-like__negative rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-down">
                                                                <use
                                                                    xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">62</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.feedback-user -->
            </div>

            <div id="tabs-userview-request4" class="tabs__content">
                <div class="feedback-user">

                    <div class="feedback-user__item">
                        <div class="feedback-user__wrapper">

                            <div class="feedback-user__avatar">
                                <div class="feedback-user__avatar-wrapper">
                                    <img src="images/avatar.jpg" alt="Аватар">
                                </div>
                            </div>

                            <div class="feedback-user__info">
                                <div class="feedback-user__header">

                                    <div class="feedback-user__line">
                                        <span class="feedback-user__arrow">→</span>
                                        <span class="feedback-user__span">Комментарии</span>
                                        на
                                        <span class="feedback-user__span"><a href="#">отзыв</a></span>
                                        о компании
                                        <span class="feedback-user__span"><a href="#">Traveler`s
                                                                Coffee</a></span>
                                    </div>

                                </div>

                                <div class="feedback-user__section">
                                    <div class="feedback-user__section-top">
                                        <div class="feedback-user__left">
                                            <span class="feedback-user__name">Alexey K</span>
                                            <span class="feedback-user__date"> от 12.02.2017 в
                                                                11:52</span>
                                        </div>
                                        <div class="feedback-user__right">
                                                            <span class="feedback-user__label">написал 2 отзыва об этом
                                                                месте</span>
                                        </div>

                                    </div>
                                    <div class="feedback-user__section-text text">
                                        <p>«Пошли кофе пить», - сказал мне мой товарищ по приятному
                                            времяпрепровождению. Не долго думая, мы двинули в сторону
                                            Городской
                                            Кулинарии TORTA на Тарской, 13а.</p>

                                        <p>Небольшое помещение, два человека персонала, сотрудники
                                            ближайших
                                            офисов,
                                            обедающие и просто употребляющие кофе и мы. Я брал латте с
                                            халвой,
                                            эклер
                                            и потом еще молочный коктейль. Здесь удовлетворение по двум
                                            пунктам
                                            из
                                            трех. Эклер был жесткий и не очень свежий и в целом не очень
                                            вкусный. А
                                            вот напитки просто прекрасные. И кофе с халвой, и молочный
                                            коктейли
                                            на 5
                                            баллов. Сотрудницы добродушные и улыбчивые. Температура в
                                            помещении
                                            комфортная. Сиденья удобные.</p>
                                    </div>
                                    <div class="feedback-user__section-bottom">

                                        <a href="#" class="feedback-user__link reply">
                                                            <span class="feedback-user__link-text">
                                                                ответить
                                                            </span>
                                        </a>

                                        <a href="#" class="feedback-user__link">
                                                            <span class="feedback-user__link-text">
                                                                цитировать
                                                            </span>
                                        </a>

                                        <a href="#" class="feedback-user__link">
                                                            <span class="feedback-user__link-text">
                                                                пожаловаться
                                                            </span>
                                        </a>

                                    </div><!-- /.feedback-user__section-bottom -->
                                </div><!-- /.feedback-user__section -->
                            </div><!-- /.feedback-user__info -->

                            <div class="feedback-user__rate">
                                <div class="rate-like rate-like--row">
                                    <div class="rate-like__positive rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-up">
                                                                <use xlink:href="images/svg/symbol/sprite.svg#like-up">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">215</span>
                                    </div>
                                    <div class="rate-like__negative rate-like__item">
                                                        <span class="rate-like__ico">
                                                            <svg class="ico ico-like-down">
                                                                <use
                                                                    xlink:href="images/svg/symbol/sprite.svg#like-down">
                                                                </use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-like__index">62</span>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.feedback-user__wrapper -->
                    </div><!-- /.feedback-user__item -->

                </div><!-- /.feedback-user -->

            </div><!-- /.tabs__content -->
        </div><!-- /. tabs__container -->
    </div><!-- /.tabs-userview-request -->

</div>
<?php if($self): ?>
<?php echo $this->render('_form', ['user' => $user, 'profile' => $profile]); ?>
<?php endif; ?>
