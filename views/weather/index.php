<?php
/* @var $this \yii\base\View */
/* @var $weather Array */
/* @var $today Object */

use app\assets\WeatherAsset;
use yii\helpers\Html;

WeatherAsset::register($this);
?>

<div class="weather-list">
    <div class="weather">
        <div class="weather__body" style="display:block;">
            <div class="indent bg">
                <table class="weather__table adaptive-table indent">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Давление</th>
                        <th>Влажность</th>
                        <th>Ветер</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Ночь
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->night->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'night')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->night->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->night->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->night->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->night->wind_dir) ?> <?php echo $today->parts->night->wind_speed ?> м/с</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Утро
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->morning->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'morning')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->morning->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->morning->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->morning->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->morning->wind_dir) ?> <?php echo $today->parts->morning->wind_speed ?> м/с</td>
                    </tr>

                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    День
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->day->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'day')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->day->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->day->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->day->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->day->wind_dir) ?> <?php echo $today->parts->day->wind_speed ?> м/с</td>
                    </tr>

                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Вечер
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->evening->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'evening')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->evening->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->evening->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->evening->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->evening->wind_dir) ?> <?php echo $today->parts->evening->wind_speed ?> м/с</td>
                    </tr>
                    </tbody>
                </table>
                <div class="weather__bottom">
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->humidity ?>%</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Влажность</div>
                            </div>
                        </div>
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->pressure_mm ?> мм</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Давление</div>
                            </div>
                        </div>
                    </div>
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->wind_speed ?> м/с</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Ветер <?php echo Yii::$app->weather->getWind($today->parts->day_short->wind_dir) ?></div>
                            </div>
                        </div>
                        <?php if($water = Yii::$app->weather->getWater(Yii::$app->city->city->name_en)): ?>
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $water ?>°</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Вода</div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->sunrise ?></div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Рассвет</div>
                            </div>
                        </div>
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->sunset ?></div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Закат</div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.weather__bottom -->
            </div><!-- /.indent bg -->
        </div><!-- /.weather__body -->
    </div><!-- /.weather -->



    <?php foreach ($weather as $i => $today): ?>
    <div class="weather">
        <div class="weather__header indent">

            <div class="weather__header-left">
                <div class="weather__day">
                    <div class="weather__day-number"><?php echo date('d', $today->date_ts) ?></div>
                    <div class="weather__day-info">
                        <div class="weather__day-info-line"><b><?php echo Yii::$app->formatter->asDate($today->date_ts, 'MMMM') ?></b></div>
                        <div class="weather__day-info-line"><?php echo Yii::$app->formatter->asDate($today->date_ts, 'EEEE') ?></div>
                    </div>
                </div>

                <div class="weather__info weather__info--big">
                    <div class="weather__degree"><?php echo $today->parts->day->temp_max ?>°</div>
                    <div class="weather__text-wrapper">
                        <div class="weather__text-ico">
                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'day')]);?>
                        </div>
                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->day->condition) ?></div>
                    </div>
                </div>
            </div>

            <div class="weather__header-right">
                <div class="weather__ico">
                    <svg class="ico ico-arrow-bottom">
                        <use xlink:href="\images/svg/symbol/sprite.svg#arrow-bottom"></use>
                    </svg>
                </div>
            </div>

        </div>
        <div class="weather__body" style="display:block;">
            <div class="indent bg">
                <table class="weather__table adaptive-table indent">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Давление</th>
                        <th>Влажность</th>
                        <th>Ветер</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Ночь
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->night->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'night')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->night->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->night->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->night->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->night->wind_dir) ?> <?php echo $today->parts->night->wind_speed ?> м/с</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Утро
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->morning->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'morning')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->morning->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->morning->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->morning->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->morning->wind_dir) ?> <?php echo $today->parts->morning->wind_speed ?> м/с</td>
                    </tr>

                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    День
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->day->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'day')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->day->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->day->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->day->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->day->wind_dir) ?> <?php echo $today->parts->day->wind_speed ?> м/с</td>
                    </tr>

                    <tr>
                        <td>
                            <div class="weather__line">
                                <div class="weather__timeday">
                                    Вечер
                                </div>
                                <div class="weather__info">
                                    <div class="weather__degree"><?php echo $today->parts->evening->temp_max ?>°</div>
                                    <div class="weather__text-wrapper">
                                        <div class="weather__text-ico">
                                            <?= Html::tag('div', '', ['class' => Yii::$app->weather->getIconClass($today, 'evening')]);?>
                                        </div>
                                        <div class="weather__text"><?php echo Yii::$app->weather->getCondition($today->parts->evening->condition) ?></div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td data-table="Давление"><?php echo $today->parts->evening->pressure_mm ?> мм</td>
                        <td data-table="Влажность"><?php echo $today->parts->evening->humidity ?>%</td>
                        <td data-table="Ветер"><?php echo Yii::$app->weather->getWind($today->parts->evening->wind_dir) ?> <?php echo $today->parts->evening->wind_speed ?> м/с</td>
                    </tr>
                    </tbody>
                </table>
                <div class="weather__bottom">
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->humidity ?>%</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Влажность</div>
                            </div>
                        </div>
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->pressure_mm ?> мм</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Давление</div>
                            </div>
                        </div>
                    </div>
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->parts->day_short->wind_speed ?> м/с</div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Ветер <?php echo Yii::$app->weather->getWind($today->parts->day_short->wind_dir) ?></div>
                            </div>
                        </div>
                        <?php if($water = Yii::$app->weather->getWater(Yii::$app->city->city->name_en, $i + 1)): ?>
                            <div class="weather__data">
                                <div class="weather__data-value">
                                    <div class="weather__data-item"><?php echo $water ?>°</div>
                                </div>
                                <div class="weather__data-key">
                                    <div class="weather__data-item">Вода</div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="weather__column">
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->sunrise ?></div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Рассвет</div>
                            </div>
                        </div>
                        <div class="weather__data">
                            <div class="weather__data-value">
                                <div class="weather__data-item"><?php echo $today->sunset ?></div>
                            </div>
                            <div class="weather__data-key">
                                <div class="weather__data-item">Закат</div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.weather__bottom -->
            </div><!-- /.indent bg -->
        </div><!-- /.weather__body -->
    </div>
    <?php endforeach; ?>
</div>