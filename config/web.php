<?php

use app\models\Profile;
use app\models\Settings;
use yii\swiftmailer\Mailer;
use yii\web\Application;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

//$server_name = 'waymi.ru';
$server_name_arr = explode('.', $_SERVER['SERVER_NAME']);
$server_name = $server_name_arr[count($server_name_arr) - 2] . '.' . $server_name_arr[count($server_name_arr) - 1];
//$server_schema = $_SERVER['REQUEST_SCHEME'] . '://';
$server_schema = 'http://';

$config = [
    'id' => 'basic',
    'language' => 'ru',
    'timezone' => 'Europe/Moscow',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'lastuseraction',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'on beforeAction' => function(){
        $offline = Settings::get('site_off');
        if($offline
            && Yii::$app->controller->module->id != 'admin'
            && Yii::$app->requestedRoute != 'user/security/login'
            && Yii::$app->requestedRoute != 'site/offline'){
            Yii::$app->controller->redirect(['/site/offline']);
            return false;
        }
    },
    'components' => [
        'lastuseraction' => [
            'class' => \app\components\UserLastAction::class
        ],
        'request' => [
            'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'WLyjM5WFI9QgHS43i985pbnH3UVKAygh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'domain' =>  '.' . $server_name,
                'name' => '_identity'
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'class' => \app\components\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                $server_schema . '<city:\w+>.' . $server_name . '/' => 'site/index',
                $server_schema . '<city:\w+>.' . $server_name . '/<module:admin|user|autoparser|rbac|comment|likes>' => '<module>',
                $server_schema . '<city:\w+>.' . $server_name . '/<controller>' => '<controller>/index',
                $server_schema . '<city:\w+>.' . $server_name . '/<controller>/<action>' => '<controller>/<action>',
                $server_schema . '<city:\w+>.' . $server_name . '/<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<module:admin|user|autoparser|rbac|comment|likes|likes|poll>' => '<module>/default/index',
                '<controller>' => '<controller>/index',
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],

        'i18n' => [
            'translations' => [
                'yii2mod.rbac' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/rbac/messages',
                ],
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
                'yii2mod.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/comments/messages',
                ],
                'survey' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages/survey',
                ],
            ],
        ],

        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                    '@onmotion/survey/views' => '@app/views/poll',
                    '@yii2mod/comments/views' => '@app/views/comments'
                ],
            ],
        ],
        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
                'google' => [
                    // register your app here: https://code.google.com/apis/console/
                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'Google',
                ],
                'twitter' => [
                    // register your app here: https://dev.twitter.com/apps/new
                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
                    'key' => '...',
                    'secret' => '...',
                ],
                'yandex' => [
                    // register your app here: https://oauth.yandex.ru/client/my
                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
                    'clientId' => '27379c619009465893fe3e23d3dfb31d',
                    'clientSecret' => '00bd62d31c2a40d5b5154f5202c8c66f',
                    'title' => 'Yandex',
                ],
                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                    'clientId' => '192559921956928',
                    'clientSecret' => '3be74393d5a3b67a761eec8475ec1f56',
                ],
                'yahoo' => [
                    'class' => 'nodge\eauth\services\YahooOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                ],
                'linkedin' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
                    'key' => '...',
                    'secret' => '...',
                    'title' => 'LinkedIn (OAuth1)',
                ],
                'linkedin_oauth2' => [
                    // register your app here: https://www.linkedin.com/secure/developer
                    'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                    'title' => 'LinkedIn (OAuth2)',
                ],
                'github' => [
                    // register your app here: https://github.com/settings/applications
                    'class' => 'nodge\eauth\services\GitHubOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'live' => [
                    // register your app here: https://account.live.com/developers/applications/index
                    'class' => 'nodge\eauth\services\LiveOAuth2Service',
                    'clientId' => '...',
                    'clientSecret' => '...',
                ],
                'steam' => [
                    'class' => 'nodge\eauth\services\SteamOpenIDService',
                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
                    'apiKey' => '...', // Optional. You can get it here: https://steamcommunity.com/dev/apikey
                ],
                'instagram' => [
                    // register your app here: https://instagram.com/developer/register/
                    'class' => 'nodge\eauth\services\InstagramOAuth2Service',
                    'clientId' => '1491824347652815',
                    'clientSecret' => 'f375a8b5bc5622cfad2941896aeb6aef',
                ],
                'vkontakte' => [
                    // register your app here: https://vk.com/editapp?act=create&site=1
                    'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
                    'clientId' => '7325259',
                    'clientSecret' => 'JxYdEODNIW4GSPU7YjRC',
                ],
                'mailru' => [
                    // register your app here: http://api.mail.ru/sites/my/add
                    'class' => 'nodge\eauth\services\MailruOAuth2Service',
                    'clientId' => '769046',
                    'clientSecret' => '474b35665669abdc195254ca621af1db',
                ],
                'odnoklassniki' => [
                    // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
                    // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
                    'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
                    'clientId' => '512000410270',
                    'clientSecret' => '34E09FBBB4031575727D4507',
                    'clientPublic' => 'CPJDELJGDIHBABABA',
                    'title' => 'Odnoklas.',
                ],
            ],
        ],
        'CbRF' => [
            'class' => 'microinginer\CbRFRates\CBRF',
            'defaultCurrency' => "USD",
            'cached' => true,
            'cacheDuration' => 60*60*24
        ],
        'weather' => [
            'class' => 'app\components\Weather'
        ],
        'maps' => [
            'class' => 'app\components\Maps'
        ],

        'session' => [
            'class' => \yii\web\DbSession::class,
            'cookieParams' => [
                'domain' =>  '.' . $server_name,
            ]
        ],
        'city' => [
            'class' => \app\components\City::class
        ],
        'geocoder' => [
            'class' => \app\components\Geocoder::class,
        ],
        'assetManager' => [
//            'linkAssets' => true,
            'bundles' => [
                \yii\web\JqueryAsset::class => [
                    'js' => [
                        '/js/libs.min.js'
                    ]
                ]
            ]
        ],
        'reCaptcha' => [
            'class' => \himiklab\yii2\recaptcha\ReCaptchaConfig::class,
            'siteKeyV2' => function(){
                return Settings::get('recaptcha_site_key');
            },
            'secretV2' => function(){
                return Settings::get('recaptcha_secret_key');
            }
        ],
        'affiche' => [
            'class' => 'app\components\Affiche',
        ],
    ],
    'params' => $params,

    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'adminPermission' => 'admin',
            'controllerMap' => [
                'security' => \app\controllers\user\SecurityController::class
            ],
            'enableFlashMessages' => false,
            'enableConfirmation' => false,
            'layout' => '@app/modules/admin/views/layouts/admin',
            'urlRules' => [
                '<id:\d+>'                               => 'profile/show',
                '<action:(login|logout|auth|forgot-password|register)>'           => 'security/<action>',
                '<action:recover>/<id:\d+>/<code:[A-Za-z0-9_-]+>' => 'security/reset',
                '<action:(register|resend)>'             => 'registration/<action>',
                'confirm/<id:\d+>/<code:[A-Za-z0-9_-]+>' => 'registration/confirm',
                'forgot'                                 => 'recovery/request',
                'recover-origin/<id:\d+>/<code:[A-Za-z0-9_-]+>' => 'recovery/reset',
                'settings/<action:\w+>'                  => 'settings/<action>'
            ],
        ],
//        'autoparser' => [
//            'class' => 'deka6pb\yii2-autoparser\Module',
//            'consumers' => $params['consumers'],
//            'providers' => $params['providers'],
//        ],
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            'layout' => '@app/modules/admin/views/layouts/admin'
        ],

        'admin' => [
            'class' => \app\modules\admin\Module::class
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
            'layout' => '@app/modules/admin/views/layouts/admin',
            'commentModelClass' => \app\models\Comments::class,
            'controllerMap' => [
                'manage' => \app\modules\admin\controllers\ManageController::class
            ]
        ],
        'likes' => [
            'class' => \app\modules\likes\Module::class
        ],
        'poll' => [
            'class' => \onmotion\survey\Module::class,
            'params' => [
                'uploadsUrl' => '/uploads/survey/',
                'uploadsPath' => '@web/upload/survey/'
            ],
            'controllerNamespace' => 'onmotion\survey\controllers',
            'layout' => '@app/modules/admin/views/layouts/admin',
            'as access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        //'actions' => ['*'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ]
                ],
            ]
        ],
        'survey' => [
            'class' => \onmotion\survey\Module::class,
            'params' => [
                'uploadsUrl' => '/uploads/survey/',
                'uploadsPath' => '@web/upload/survey/'
            ],
            'controllerNamespace' => 'onmotion\survey\widgetControllers',
        ]
    ],
];
Yii::$classMap[\dektrium\user\models\Token::class] = __DIR__ . '/../components/Token.php';

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*.*.*.*', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*', '::1'],
    ];
}

return $config;
