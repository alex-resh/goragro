<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'providers' => [
        'provider1' => [
            'class' => 'path to provider',
            'count' => 1,
            'homepage' => 'website',
            'on' => false
        ],
    ],
];

