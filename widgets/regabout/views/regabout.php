<div class="form__row">
    <label class="form__label">
        <span class="form__label-title">О себе</span>
    </label>
    <textarea class="form__input form__textarea" name="form_message"></textarea>
</div>

<div class="form__row form__row--checkeds">

    <div class="form__row-column">
        <div class="radio">
            <input class="radio__input" id="radio20" type="radio" name="radio">
            <label class="radio__wrapper" for="radio20">
                <span class="radio__custom"></span><!-- кругляшок с ::before при checked-->
                <span class="radio__label">
                                        <span class="radio__text">Интересует стоимость</span>
                                    </span>
            </label>
        </div>
        <div class="radio">
            <input class="radio__input" id="radio21" type="radio" name="radio" checked>
            <label class="radio__wrapper" for="radio21">
                <span class="radio__custom"></span>
                <span class="radio__label">
                                        <span class="radio__text">Интересует вакансия</span>
                                    </span>
            </label>
        </div>
        <div class="radio">
            <input class="radio__input" id="radio22" type="radio" name="radio">
            <label class="radio__wrapper" for="radio22">
                <span class="radio__custom"></span>
                <span class="radio__label">
                                        <span class="radio__text">Интересует журналистика</span>
                                    </span>
            </label>
        </div>
        <div class="radio">
            <input class="radio__input" id="radio23" type="radio" name="radio">
            <label class="radio__wrapper" for="radio23">
                <span class="radio__custom"></span>
                <span class="radio__label">
                                        <span class="radio__text">Интересует статья на сайте</span>
                                    </span>
            </label>

        </div>
    </div><!-- /.form__row-column -->

    <div class="form__row-column">

        <div class="checkbox">
            <input class="checkbox__input" id="check20" type="checkbox" name="checkbox">
            <label class="checkbox__wrapper" for="check20">
                <span class="checkbox__custom"></span>
                <span class="checkbox__label">
                                        <span class="checkbox__text">Интересует стоимость</span>
                                    </span>
            </label>
        </div>
        <div class="checkbox">
            <input class="checkbox__input" id="check21" type="checkbox" name="checkbox" checked>
            <label class="checkbox__wrapper" for="check21">
                <span class="checkbox__custom"></span>
                <span class="checkbox__label">
                                        <span class="checkbox__text">Интересует вакансия</span>
                                    </span>
            </label>
        </div>
        <div class="checkbox">
            <input class="checkbox__input" id="check22" type="checkbox" name="checkbox">
            <label class="checkbox__wrapper" for="check22">
                <span class="checkbox__custom"></span>
                <span class="checkbox__label">
                                        <span class="checkbox__text">Интересует журналистика</span>
                                    </span>
            </label>
        </div>
        <div class="checkbox">
            <input class="checkbox__input" id="check23" type="checkbox" name="checkbox">
            <label class="checkbox__wrapper" for="check23">
                <span class="checkbox__custom"></span>
                <span class="checkbox__label">
                                        <span class="checkbox__text">Интересует статья на сайте</span>
                                    </span>
            </label>
        </div>

    </div><!-- /.form__row-col -->

</div><!-- /.form__row -->

<div class="form__row">

    <div class="file-upload">
        <label class="file-upload__btn">Выберите файл</label>
        <input type="file" name="file">
        <div class="form-upload__info">
            <p class="file-upload__text"> ... или перетащите мышкой</p>
            <p class="file-upload__formats">форматы jpg, gif, png</p>
        </div>
    </div>

</div>

<div class="form__row">
    <input type="submit" class="signin__btn signin__registration btn btn-accent" value="Зарегистрироваться">
</div>


