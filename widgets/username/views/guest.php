<div class="header__signup header__signup--offline header__signup-dekstop">
    <div class="header__signup-buttons">
         <a href="<?php echo \yii\helpers\Url::to(['/user/registration/register']) ?>" class="header__registration">Регистрация</a>
         <a href="<?php echo \yii\helpers\Url::to(['/user/security/login']) ?>" class="header__sign btn btn-trans">Войти</a>
    </div>
</div>