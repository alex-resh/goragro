<?php
use yii\helpers\Url;
?>
<div class="header__signup header__signup--online header__signup-dekstop">
    <div class="user">
                    <div class="user__header">
                        <div class="user__data">
                            <div class="user__avatar"><img src="<?php echo Yii::$app->user->identity->profile->getThumbUploadUrl('img', 'small') ?>" alt="avatar"></div>
                            <div class="user__nickname"><?php echo Yii::$app->user->identity->profile->name? Yii::$app->user->identity->profile->name : Yii::$app->user->identity->username ?></div>
</div>
<div class="user__info">
    <div class="user__num">5</div>
    <div class="user__ico">
        <svg class="ico ico-arrow-bottom">
            <use xlink:href="images/svg/symbol/sprite.svg#arrow-bottom"></use>
        </svg>
    </div>
</div>
</div>

<div class="user__dropdown">
    <div class="user__dropdown-section">
        <div class="user__dropdown-avatar">
            <img src="<?php echo Yii::$app->user->identity->profile->getThumbUploadUrl('img', 'thumb') ?>" alt="avatar">
        </div>
        <div class="user__dropdown-menu">
            <a href="#" class="user__dropdown-menu-item">Сообщения - 12</a>
            <a href="<?php echo Url::toRoute('/blog/create') ?>" class="user__dropdown-menu-item">Добавить запись</a>
            <a href="<?php echo Url::to('/profile') ?>" class="user__dropdown-menu-item">Настройки</a>
        </div>
    </div>
    <a href="<?php echo \yii\helpers\Url::to(['site/logout']) ?>" class="user__dropdown-bottom">
        <span class="user__dropdown-exit">Выход</span>
    </a>
</div><!-- /.user__dropdown -->

</div><!-- /.user -->
</div><!-- /.header__signup -->
