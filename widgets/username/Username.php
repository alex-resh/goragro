<?php

namespace app\widgets\username;

use yii\base\Widget;

class Username extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->render('guest');
        } else {
            return $this->render('user');
        }
    }
}
