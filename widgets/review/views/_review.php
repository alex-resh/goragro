<div class="reviews experts indent">

    <div class="reviews__top">

        <h2 class="reviews__title title-md">Отзывы</h2>

        <div class="tabs tabs-reviews" id="tabs-reviews">
            <ul class="tabs__list">
                <li class="tabs__item ui-tabs-active">
                    <a class="tabs__link" href="#tabs1">Новые</a>
                </li>
                <li class="tabs__item">
                    <a class="tabs__link" href="#tabs2">Лучшие</a>
                </li>
            </ul>

            <div class="tabs__container">
                <div id="tabs1" class="tabs__content">
                    <div class="reviews__experts experts__list">
                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Dmitriy Cherepanov
                                </div>
                            </div>

                            <div class="experts__section">
                                <a href="#" class="experts__place">Кинотеатр Салют</a>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Всем привет! Посетил вчера х/ф "Багровый
                                    пик"
                                    на 20:30,
                                    забавно... Фильм комментировать не буду, как известно на вкус и
                                    цвет,
                                    товарищей
                                    нет...</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Юлия Владиславовна
                                </div>
                            </div>

                            <div class="experts__section">
                                <a href="#" class="experts__place">Yandex.ru</a>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star rate__ico">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">На сегодняшний день рекламная кампания
                                    "Простите нас, конкуренты" была высоко оценена рекламным сообществом
                                    и
                                    получила несколько наград.</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item experts__item-vip item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    TuryKcs25
                                </div>
                            </div>

                            <div class="experts__section">
                                <a href="#" class="experts__place">AUDI центр</a>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Сервис по продаже автомобилей. Один из
                                    самых
                                    популярных в городе. Продают не только автомобили но и лодки, корм
                                    для
                                    собак и кошек ;)</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Colambus
                                </div>
                            </div>

                            <div class="experts__section">
                                <a href="#" class="experts__place">МОТИВ</a>
                                <div class="experts__rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Молодежный парламент поселения Рязановское
                                    и
                                    25 детей из Детского сада "Ивушка" посадили на территории детского
                                    сада
                                    елки, орешник и дуб. </p>
                            </div>
                        </div><!-- /.experts__item -->
                    </div>
                </div>
                <div id="tabs2" class="tabs__content">
                    <div class="reviews__experts experts__list">
                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Лучшие Dmitriy Cherepanov
                                </div>
                            </div>

                            <div class="experts__section">
                                <a href="#" class="experts__place">Кинотеатр Салют</a>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Всем привет! Посетил вчера х/ф "Багровый
                                    пик"
                                    на 20:30,
                                    забавно... Фильм комментировать не буду, как известно на вкус и
                                    цвет,
                                    товарищей
                                    нет...</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Лучшие Юлия Владиславовна
                                </div>
                            </div>

                            <div class="experts__section">
                                <div class="experts__place">Yandex.ru</div>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star rate__ico">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">На сегодняшний день рекламная кампания
                                    "Простите нас, конкуренты" была высоко оценена рекламным сообществом
                                    и
                                    получила несколько наград.</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item experts__item-vip item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Лучшие TuryKcs25
                                </div>
                            </div>

                            <div class="experts__section">
                                <div class="experts__place">AUDI центр</div>
                                <div class="experts__rate rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Сервис по продаже автомобилей. Один из
                                    самых
                                    популярных в городе. Продают не только автомобили но и лодки, корм
                                    для
                                    собак и кошек ;)</p>
                            </div>
                        </div><!-- /.experts__item -->

                        <div class="experts__item item">
                            <div class="experts__header">
                                <div class="experts__img">
                                    <div class="experts__img-wrapper">
                                        <img src="/images/photo/4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="experts__name">
                                    Лучшие Colambus
                                </div>
                            </div>

                            <div class="experts__section">
                                <div class="experts__place">МОТИВ</div>
                                <div class="experts__rate">
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                    <i class="ico ico-star ico-star--empty rate__ico">
                                        <svg class="ico ico-star">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                        </svg>
                                    </i>
                                </div>
                                <p class="experts__text text">Молодежный парламент поселения Рязановское
                                    и
                                    25 детей из Детского сада "Ивушка" посадили на территории детского
                                    сада
                                    елки, орешник и дуб. </p>
                            </div>
                        </div><!-- /.experts__item -->
                    </div>
                </div>
            </div><!-- /.tabs__container -->

        </div><!-- /.tabs -->

        <a href="#" class="reviews__add btn-add">
                            <span class="btn-add__ico">
                                    <svg class="ico ico-plus">
                                        <use xlink:href="/images/svg/symbol/sprite.svg#plus"></use>
                                    </svg>
                            </span>
            <span class="btn-add__text">Написать отзыв</span>
        </a>

    </div><!-- /.reviews__top -->

</div><!-- /.reviews -->