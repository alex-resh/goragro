<?php

namespace app\widgets\review;

use yii\base\Widget;

class Review extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        return $this->render('_review');
    }
}