<div class="organization indent">

    <div class="organization__header organization__header-flex">
        <h2 class="organization__title title-md">В нашем городе</h2>
        <a href="#" class="organization__link">
            <span class="organization__link-img"></span>
            <span class="organization__link-text">Каталог</span>
        </a>
    </div><!-- /.organization__header -->

    <div class="organization__body">
        <div class="organization__list">
            <div class="organization__column table-data">
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Авто, мото</span>
                    <span class="table-data-val">14</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Государство</span>
                    <span class="table-data-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Безопасность, охрана</span>
                    <span class="table-data-val">25</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Бизнес, финансы</span>
                    <span class="table-data-val">13</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Дизайн и интерьер</span>
                    <span class="table-data-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Животные и растения</span>
                    <span class="table-data-val">8</span>
                </a>
            </div><!-- /.organization__column -->

            <div class="organization__column table-data">
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Компьютеры, интернет, IT</span>
                    <span class="table-data-val">15</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Красота, стиль, мода</span>
                    <span class="table-data-val">52</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Культура, искусство, досуг</span>
                    <span class="table-data-val">26</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Кафе, бары, рестораны</span>
                    <span class="table-data-val">14</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Лесное хозяйство</span>
                    <span class="table-data-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Медицина и здоровье</span>
                    <span class="table-data-val">67</span>
                </a>
            </div><!-- /.organization__column -->

            <div class="organization__column table-data">
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Недвижимость</span>
                    <span class="table-data-val">2</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Образование, карьера</span>
                    <span class="table-data-val">7</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Обзоры</span>
                    <span class="table-data-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Промышленность</span>
                    <span class="table-data-val">82</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Реклама, полиграфия</span>
                    <span class="table-data-val">14</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Спорте</span>
                    <span class="table-data-val">25</span>
                </a>
            </div><!-- /.organization__column -->

            <div class="organization__column table-data">
                <a href="#" class="table-data-line">
                    <span class="table-data-name">СМИ</span>
                    <span class="table-data-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Строительство, ремонт</span>
                    <span class="table-data-val">25</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Топливо и энергетика</span>
                    <span class="table-data-val">13</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-info-name">Торговля, магазины</span>
                    <span class="table-info-val">5</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Транспорт и логистика</span>
                    <span class="table-data-val">8</span>
                </a>
                <a href="#" class="table-data-line">
                    <span class="table-data-name">Туризм, гостиничный бизнес</span>
                    <span class="table-data-val">15</span>
                </a>
            </div><!-- /.organization__column -->
        </div>
    </div><!-- /.organization__body -->

</div><!-- /.organization -->