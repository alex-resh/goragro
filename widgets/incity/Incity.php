<?php

namespace app\widgets\incity;

use yii\base\Widget;

class Incity extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        return $this->render(incity);
    }
}