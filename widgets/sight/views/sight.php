<?php
/* @var $sights \app\models\Sight[] */

use yii\helpers\Url;

?>
<div class="place">
    <div class="tabs tabs-place" id="tabs-place">
        <ul class="tabs__list indent">
            <li class="tabs__item ui-tabs-active">
                <a class="tabs__link" href="#tabs-1">Рекомендуемые организации</a>
            </li>
            <li class="tabs__item">
                <a class="tabs__link" href="#tabs-2">Достопримечательности</a>
            </li>
        </ul>
        <div class="tabs__container">
            <div id="tabs-1" class="tabs__content">
                <div class="place-view">
                    <div class="place-view__list">

                        <a href="#" class="place-view__item item"
                           style="background-image: url('images/organization/1.jpg');">

                            <div class="place-view__content">
                                <div class="place-view__info">
                                    <i class="place-view__ico">
                                        <svg class="ico ico-star-big">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star-big"></use>
                                        </svg>
                                    </i>
                                    <h4 class="place-view__title">Тушинский ЗАГС</h4>
                                </div>
                            </div>

                            <div class="place-view__hidden">
                                <div class="place-view__hidden-left">
                                    <div class="place-view__reviews">
                                        <span class="place-view__reviews-index">23</span> отзыва
                                    </div>
                                </div>
                                <div class="place-view__hidden-right">
                                    <div class="place-view__rate rate">
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star ico-star--empty rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p class="place-view__text">На сайте есть представитель фирмы</p>
                                </div>
                            </div><!-- /.place-view__hidden -->

                        </a><!-- /.place-view__item -->

                        <a href="#" class="place-view__item item"
                           style="background-image: url('images/organization/2.jpg');">
                            <div class="place-view__content">
                                <div class="place-view__info">
                                    <i class="place-view__ico">
                                        <svg class="ico ico-star-big">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star-big"></use>
                                        </svg>
                                    </i>
                                    <h4 class="place-view__title">Царицынский ЗАГС</h4>
                                </div>
                            </div>

                            <div class="place-view__hidden">
                                <div class="place-view__hidden-left">
                                    <div class="place-view__reviews">
                                        <span class="place-view__reviews-index">23</span> отзыва
                                    </div>
                                </div>
                                <div class="place-view__hidden-right">
                                    <div class="place-view__rate rate">
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star ico-star--empty rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p class="place-view__text">На сайте есть представитель фирмы</p>
                                </div>
                            </div><!-- /.place-view__hidden -->
                        </a><!-- /.place-view__item -->

                        <a href="#" class="place-view__item item"
                           style="background-image: url('images/organization/1.jpg');">
                            <div class="place-view__content">
                                <div class="place-view__info">
                                    <i class="place-view__ico">
                                        <svg class="ico ico-star-big">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star-big"></use>
                                        </svg>
                                    </i>
                                    <h4 class="place-view__title">Черёмушкинский ЗАГС</h4>
                                </div>
                            </div>

                            <div class="place-view__hidden">
                                <div class="place-view__hidden-left">
                                    <div class="place-view__reviews">
                                        <span class="place-view__reviews-index">23</span> отзыва
                                    </div>
                                </div>
                                <div class="place-view__hidden-right">
                                    <div class="place-view__rate rate">
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star ico-star--empty rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p class="place-view__text">На сайте есть представитель фирмы</p>
                                </div>
                            </div><!-- /.place-view__hidden -->
                        </a><!-- /.place-view__item -->

                        <a href="#" class="place-view__item item"
                           style="background-image: url('images/organization/3.jpg');">
                            <div class="place-view__content">
                                <div class="place-view__info">
                                    <i class="place-view__ico">
                                        <svg class="ico ico-star-big">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star-big"></use>
                                        </svg>
                                    </i>
                                    <h4 class="place-view__title">ООО "Слипад"</h4>
                                </div>
                            </div>

                            <div class="place-view__hidden">
                                <div class="place-view__hidden-left">
                                    <div class="place-view__reviews">
                                        <span class="place-view__reviews-index">23</span> отзыва
                                    </div>
                                </div>
                                <div class="place-view__hidden-right">
                                    <div class="place-view__rate rate">
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star ico-star--empty rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p class="place-view__text">На сайте есть представитель фирмы</p>
                                </div>
                            </div><!-- /.place-view__hidden -->
                        </a><!-- /.place-view__item -->

                    </div><!-- /.place-view__list -->
                </div><!-- /.place-view -->
            </div><!-- /.tabs__content -->

            <div id="tabs-2" class="tabs__content">
                <div class="place-view">
                    <div class="place-view__list">
                        <?php foreach ($sights as $sight): ?>
                        <a href="<?php echo Url::to(['sights/index', 'id' => $sight->id]) ?>" class="place-view__item item"
                           style="background-image: url('<?php echo $sight->getMainPhoto() ?>');">

                            <div class="place-view__content">
                                <div class="place-view__info">
                                    <i class="place-view__ico">
                                        <svg class="ico ico-star-big">
                                            <use xlink:href="/images/svg/symbol/sprite.svg#star-big"></use>
                                        </svg>
                                    </i>
                                    <h4 class="place-view__title"><?php echo $sight->title ?></h4>
                                </div>
                            </div>

                            <div class="place-view__hidden">
                                <div class="place-view__hidden-left">
                                    <div class="place-view__reviews">
                                        <span class="place-view__reviews-index">23</span> отзыва
                                    </div>
                                </div>
                                <div class="place-view__hidden-right">
                                    <div class="place-view__rate rate">
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                        <i class="ico ico-star ico-star--empty rate__ico">
                                            <svg class="ico ico-star">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#star"></use>
                                            </svg>
                                        </i>
                                    </div>
                                    <p class="place-view__text">На сайте есть представитель фирмы</p>
                                </div>
                            </div><!-- /.place-view__hidden -->

                        </a><!-- /.place-view__item -->
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div><!-- /.tabs__container -->
    </div><!-- /.tabs -->
</div><!-- /.place -->