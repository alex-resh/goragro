<?php

namespace app\widgets\sight;

use yii\base\Widget;
use yii\db\Expression;

class Sight extends Widget
{
    public function run()
    {
        $sights = \app\models\Sight::find()->limit(4)->orderBy(new Expression('RAND()'))->all();
        return $this->render('sight', ['sights' => $sights]);
    }
}