<?php


namespace app\widgets\newscategory;

use app\models\News;
use yii\base\Widget;
use yii\db\Expression;

class NewsCategory extends Widget
{

    public function run()
    {
        $res = \app\models\NewsCategory::find()->all();
        $out = [];
        foreach ($res as $category){
            $count = $category->countNewsByCity();
            if($count > 0){
                $out[] = [
                    'name' => $category->name,
                    'count' => $count,
                    'id' => $category->id
                ];
            }
        }
        return $this->render('newscategory', ['out' => $out]);
    }

}