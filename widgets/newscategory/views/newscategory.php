<?php
/* @var $this \yii\web\View */
/* @var $out array */
use yii\helpers\Url;
?>
<div class="inside__table-data table-data">
    <?php foreach ($out as $item): ?>
    <a href="<?php echo Url::toRoute(['/news', 'cat_id' => $item['id']]) ?>" class="table-data-line">
        <span class="table-data-name"><?php echo $item['name'] ?></span>
        <span class="table-data-val"><?php echo $item['count'] ?></span>
    </a>
    <?php endforeach; ?>
</div><!-- /.table-data -->
