<?php

use yii\helpers\Url;

?>
<div class="organization indent">

    <div class="organization__header">
        <h2 class="organization__title title-md">Организации в <?php echo Yii::$app->city->city->name_ru_pred ?></h2>
        <div class="organization__index">
            <i class="organization__index-ico">
                <svg class="ico ico-building">
                    <use xlink:href="/images/svg/symbol/sprite.svg#building"></use>
                </svg>
            </i>
            <span class="organization__index-info">
                                <span class="organization__index-text">Всего организаций</span>
                                <span class="organization__index-value"><?php echo \app\models\Organisation::getCount() ?></span>
                            </span>

        </div>
    </div><!-- /.organization__header -->

    <div class="organization__body">
        <div class="organization__list">
            <?php $i = 0; ?>
            <?php foreach ($categories as $category): ?>
                <?php if ($i == 0): ?>
                    <div class="organization__column table-data">
                <?php endif; ?>
                <a href="<?php echo Url::toRoute(['catalog/index', 'category_id' => $category->id]) ?>" class="table-data-line">
                    <span class="table-data-name"><?php echo $category->name ?></span>
                    <span class="table-data-val"><?php echo $category->c ?></span>
                </a>
                <?php $i++; ?>
                <?php if ($i == 5): $i = 0; ?>
                    </div>
                <?php endif; ?>
                <?php
//                $i++;
            endforeach;
            ?>
            <?php if($i > 0): ?>
            </div>
            <?php endif; ?>
        </div>
    </div><!-- /.organization__body -->

    <div class="organization__bottom">
        <a href="<?php echo \yii\helpers\Url::to(['/maps']) ?>" class="btn btn-map">
            <span class="btn-map__ico"></span>
            <span class="btn-map__text">Показать организации на карте</span>
        </a>
    </div><!-- /.organization__bottom -->

</div><!-- /.organization -->