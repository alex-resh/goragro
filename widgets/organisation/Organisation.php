<?php

namespace app\widgets\organisation;

use app\models\Category;
use yii\base\Widget;

class Organisation extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        $categories = Category::getOrgWithCount();
    return $this->render('organisation', ['categories' => $categories]);
    }
}