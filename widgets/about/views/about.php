<div class="about-project">
    <h2 class="poll__title title-xs">О проекте Gorago</h2>

    <div class="owl-carousel video-carousel">
        <div class="item-video" data-merge="1">
            <a class="owl-video" href="https://youtu.be/xjYxg5rKK2g"></a>
        </div>
        <div class="item-video" data-merge="2">
            <a class="owl-video" href="https://www.youtube.com/watch?v=JZZ3y30leUM"></a>
        </div>
        <div class="item-video" data-merge="3">
            <a class="owl-video" href="https://youtu.be/xjYxg5rKK2g"></a>
        </div>
        <div class="item-video" data-merge="4">
            <a class="owl-video" href="https://www.youtube.com/watch?v=JZZ3y30leUM"></a>
        </div>
    </div>
    <p class="text mb">Мы рады приветствовать всех, кто зашёл на наш информационно-развлекательный
        портал. Здесь вы можете оставить отзывы о компаниях и магазинах, поликлиниках и школах, товарах
        и услугах – словом, обо всём, что заинтересовало лично вас и может быть полезно и интересно
        другим посетителям нашего сайта. Любая из организаций, размещённых здесь, имеет свою
        персональную страницу с контактными данными, так что наш ресурс можно рассматривать также и как
        справочник, где возможно интерактивное общение.</p>

    <p class="text">Если какого-либо предприятия нет в нашей базе, а вы хотите поделиться впечатлениями
        о нём, можете, заполнив специальную форму, внести его в наш реестр. Прежде чем оставлять отзывы
        о фирмах и о товарах, ознакомьтесь с нашими правилами. Мы не практикуем жёсткую модерацию, но
        это не значит, что здесь можно флудить и спамить, употреблять нецензурную лексику и оскорблять
        кого бы то ни было. </p>
</div>


</div>