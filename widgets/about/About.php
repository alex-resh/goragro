<?php

namespace app\widgets\about;

use yii\base\Widget;

class About extends Widget
{
    public function run()
    {
        return $this->render('about');
    }
}