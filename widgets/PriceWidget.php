<?php


namespace app\widgets;


use yii\helpers\Html;
use yii\jui\SliderInput;

class PriceWidget extends SliderInput
{
    public $defaultClientOptions = [
        'range' => true,
        'min' => 0,
        'max' => 100,
        'step' => 1,
    ];
    public $defaultClientEvents = [
        'slide' => <<<JS
            function(e, ui){
                var parent = $(ui.handle).parent();
                $('.form-price__min', parent.prev()).val(ui.values[0]);
                $('.form-price__max', parent.prev()).val(ui.values[1]);
                parent.next('input').val('~between~' + ui.values[0] + '~' + ui.values[1]);
            }
JS
        ,
        'create' => <<<JS
            function (e, ui) {
                let tmp = $(e.target).next('input').val().split('~');
                if(tmp.length == 4){
                    $(e.target).slider('values', [tmp[2], tmp[3]]);
                    $(e.target).prev().find('.form-price__min').val(tmp[2]).end().find('.form-price__max').val(tmp[3]);
                }
            }
JS
        ,
    ];

    public function init()
    {
        parent::init();
        $this->clientOptions = array_merge($this->defaultClientOptions, $this->clientOptions);
        $this->clientEvents = array_merge($this->defaultClientEvents, $this->clientEvents);
        $this->clientOptions['values'] = $this->clientOptions['values'] ?? [$this->clientOptions['min'], $this->clientOptions['max']];
    }

    public function run()
    {
        $value0 = $this->clientOptions['values'][0];
        $value1 = $this->clientOptions['values'][1];
        echo <<<HTML
            <div class="form-price">
                <div class="form-price__input-wrapper">
                    <span class="form-price__input-text">от</span>
                    <input type="text" class="form-price__input form-price__min" value="$value0"/>
                </div>
                <div class="form-price__input-wrapper">
                    <span class="form-price__input-text">до</span>
                    <input type="text" class="form-price__input form-price__max" value="$value1"/>
                </div>
            </div>
HTML;
        echo Html::tag('div', '<div class="ui-slider__min">' . $this->clientOptions['min'] . '</div><div class="ui-slider__max">' . $this->clientOptions['max'] . '</div>', $this->containerOptions);

        if ($this->hasModel()) {
            echo Html::activeHiddenInput($this->model, $this->attribute, $this->options);
            $this->clientOptions['value'] = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            echo Html::hiddenInput($this->name, $this->value, $this->options);
            $this->clientOptions['value'] = $this->value;
        }

        if (!isset($this->clientEvents['slide'])) {
            $this->clientEvents['slide'] = 'function (event, ui) {
                jQuery("#' . $this->options['id'] . '").val(ui.value);
            }';
        }

        $this->registerWidget('slider', $this->containerOptions['id']);
    }
}