<?php
    use yii\helpers\Url;
?>
<form class="form-pup form-pup-select white-popup-block mfp-hide" id="select-sity" name="select-sity"
      action="#" method="post">
    <h2 class="form-pup__title">Выбран город <?php echo Yii::$app->city->city->name ?></h2>

    <div class="form-pup__search">
        <input type="text" class="form-pup__input">
        <input type="submit" class="form-pup__btn btn btn-trans" value="Найти">
    </div>

    <div class="form__body">

        <div class="city">
            <div class="city__list">
                <div class="city__column">
                   <?php foreach ($cities as $city):?>

                       <a href="<?php echo Url::toRoute(['site/index', 'city' => $city->name_en]) ?>" class="city__item"><?php if ($city->bold == 1):?>
                           <b><?php echo  $city->name ?></b>
                           <?php else:?>
                               <?php echo  $city->name ?>
                           <?php endif;?>

                       </a>
                   <?php endforeach;?>



                </div>

            </div>
        </div>
    </div>
</form>