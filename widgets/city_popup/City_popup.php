<?php

namespace app\widgets\city_popup;
use app\models\FsCity;
use yii\base\Widget;

class City_popup extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        $cities = FsCity::find()->where(['in_popup' => 1])->orderBy(['name' => SORT_ASC])->all();

            return $this->render('city_popup', ['cities' => $cities]);

    }
}