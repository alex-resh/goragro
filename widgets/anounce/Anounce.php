<?php

namespace app\widgets\anounce;

use app\models\News;
use yii\base\Widget;

class Anounce extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        $news = News::find()->andWhere(['type' => News::TYPE_NEWS])->orderBy(['created_at' => SORT_DESC])->limit(3)->all();
        $blogs = News::find()->andWhere(['type' => News::TYPE_BLOG])->orderBy(['created_at' => SORT_DESC])->limit(3)->all();
        return $this->render('anounce',['news' => $news, 'blogs' => $blogs]);
    }
}