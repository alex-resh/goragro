<?php
/* @var $news \app\models\News[] */

use yii\helpers\Url;

?>
<div class="anounce indent">

    <div class="anounce__item">
        <h2 class="anounce__title title-md">Новости</h2>
        <?php if (!empty($news)): ?>
            <div class="anounce__line anounce__line--active">
                <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[0]->id]) ?>" class="anounce__img">
                    <span class="anounce__label"><?php echo $news[0]->category->name ?></span>
                    <img src="<?php echo $news[0]->getThumbUploadUrl('img', 'preview') ?>" alt="">
                </a>
                <div class="anounce__info">
                    <div class="anounce__date"><?php echo Yii::$app->formatter->asDate($news[0]->created_at, 'php: d/m Y') ?></div>
                    <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[0]->id]) ?>"
                       class="anounce__line-title"><?php echo $news[0]->title ?></a>
                    <p class="anounce__text text"><?php echo mb_substr(strip_tags($news[0]->text), 0, 100) ?></p>
                </div>
            </div>

            <?php if (isset($news[1])): ?>
                <div class="anounce__line">
                    <div class="anounce__line-left">
                        <div class="anounce__key"><?php echo Yii::$app->formatter->asDate($news[1]->created_at, 'php: d/m Y') ?></div>
                    </div>

                    <div class="anounce__line-right">
                        <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[1]->id]) ?>"
                           class="anounce__line-title text"><?php echo mb_substr(strip_tags($news[1]->text), 0, 100) ?></a>
                    </div>
                </div>
                <?php if (isset($news[2])): ?>
                    <div class="anounce__line">
                        <div class="anounce__line-left">
                            <div class="anounce__key"><?php echo Yii::$app->formatter->asDate($news[2]->created_at, 'php: d/m Y') ?></div>
                        </div>

                        <div class="anounce__line-right">
                            <a href="<?php echo Url::toRoute(['news/view', 'id' => $news[2]->id]) ?>"
                               class="anounce__line-title text"><?php echo mb_substr(strip_tags($news[2]->text), 0, 100) ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div><!-- /.anounce__item -->

    <div class="anounce__item">
        <h2 class="anounce__title title-md">Сегодня в блогах</h2>

        <?php if (!empty($blogs)): ?>
            <div class="anounce__line anounce__line--active">
                <a href="<?php echo Url::toRoute(['blog/view', 'id' => $blogs[0]->id]) ?>" class="anounce__img">
                    <img src="<?php echo $blogs[0]->getThumbUploadUrl('img', 'preview') ?>" alt="">
                </a>
                <div class="anounce__info">
                    <div class="anounce__date"><?php echo Yii::$app->formatter->asDate($blogs[0]->created_at, 'php: d/m Y') ?></div>
                    <a href="<?php echo Url::toRoute(['blog/view', 'id' => $blogs[0]->id]) ?>"
                       class="anounce__line-title"><?php echo $blogs[0]->title ?></a>
                    <p class="anounce__text text"><?php echo mb_substr(strip_tags($blogs[0]->text), 0, 100) ?></p>
                </div>
            </div>

            <?php if (isset($blogs[1])): ?>
                <div class="anounce__line">
                    <div class="anounce__line-left">
                        <div class="anounce__key"><?php echo Yii::$app->formatter->asDate($blogs[1]->created_at, 'php: d/m Y') ?></div>
                    </div>

                    <div class="anounce__line-right">
                        <a href="<?php echo Url::toRoute(['blog/view', 'id' => $blogs[1]->id]) ?>"
                           class="anounce__line-title text"><?php echo mb_substr(strip_tags($blogs[1]->text), 0, 100) ?></a>
                    </div>
                </div>
                <?php if (isset($blogs[2])): ?>
                    <div class="anounce__line">
                        <div class="anounce__line-left">
                            <div class="anounce__key"><?php echo Yii::$app->formatter->asDate($blogs[2]->created_at, 'php: d/m Y') ?></div>
                        </div>

                        <div class="anounce__line-right">
                            <a href="<?php echo Url::toRoute(['blog/view', 'id' => $blogs[2]->id]) ?>"
                               class="anounce__line-title text"><?php echo mb_substr(strip_tags($blogs[2]->text), 0, 100) ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>

    </div><!-- /.anounce__item -->

</div><!-- /.anounce -->