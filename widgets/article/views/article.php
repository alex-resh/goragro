<div class="articles">

    <div class="tabs tabs-articles" id="tabs-articles">
        <ul class="tabs__list indent">
            <li class="tabs__item ui-tabs-active">
                <a class="tabs__link" href="#tabs-articles1">Кулинарные рецепты</a>
            </li>
            <li class="tabs__item">
                <a class="tabs__link" href="#tabs-articles2">Афиша</a>
            </li>
        </ul>

        <div class="tabs__container">
            <div id="tabs-articles1" class="tabs__content">

                <div class="articles__section">
                    <div class="articles__list articles__list-carousel owl-carousel">
                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/1.jpg');">

                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->

                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/2.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/3.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/4.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/1.jpg');">

                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->

                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/2.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/3.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/4.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/1.jpg');">

                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->

                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/2.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/3.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/4.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Блины на воде</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->
                    </div><!-- /.articles__list -->
                </div><!-- /.articles__section -->

                <div class="articles__bottom indent">
                    <a href="#" class="articles__link">
                        <span class="articles__link-text">Все рецепты</span>
                        <span class="articles__link-ico">
                                            <svg class="ico ico-arrow">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#arrow"></use>
                                            </svg>
                                        </span>
                    </a>
                </div><!-- /.articles__bottom -->

            </div><!-- /.tabs__content -->

            <div id="tabs-articles2" class="tabs__content">
                <div class="articles__section">
                    <div class="articles__list owl-carousel">
                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/1.jpg');">

                            <div class="articles__hidden-top">
                                <div class="articles__title">Аленький цветочек</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->

                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/4.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Фред, Фредди и Фредерих</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/2.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Анна Каренина</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/3.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Qeen</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                        <a href="#" class="articles__item"
                           style="background-image: url('images/recipe/4.jpg');">
                            <div class="articles__hidden-top">
                                <div class="articles__title">Фред, Фредди и Фредерих</div>
                            </div>
                            <div class="articles__hidden-bottom">
                                <div class="rate-reviews">
                                    <div class="rate-reviews__positive rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-positive"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#positive"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">21</span>
                                    </div>
                                    <div class="rate-reviews__negative rate-reviews__item">
                                                        <span class="rate-reviews__ico">
                                                            <svg class="ico ico-negative"><use
                                                                    xlink:href="/images/svg/symbol/sprite.svg#negative"></use>
                                                            </svg>
                                                        </span>
                                        <span class="rate-reviews__index">671</span>
                                    </div>

                                </div><!-- /.rate-reviews -->
                            </div><!-- /.articles__hidden-bottom -->
                        </a><!-- /.articles__item -->

                    </div><!-- /.articles__list -->
                </div><!-- /.articles__section -->

                <div class="articles__bottom indent">
                    <a href="#" class="articles__link">
                        <span class="articles__link-text">Вся афиша</span>
                        <span class="articles__link-ico">
                                            <svg class="ico ico-arrow">
                                                <use xlink:href="/images/svg/symbol/sprite.svg#arrow"></use>
                                            </svg>
                                        </span>
                    </a>
                </div><!-- /.articles__bottom -->
            </div>
        </div><!-- /.tabs__container -->

    </div><!-- /.tabs -->

</div><!-- /.articles -->