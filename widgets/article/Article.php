<?php

namespace app\widgets\article;

use yii\base\Widget;

class Article extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        return $this->render('article');
    }
}