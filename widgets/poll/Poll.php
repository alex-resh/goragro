<?php

namespace app\widgets\poll;

use onmotion\survey\models\Survey;
use yii\base\Widget;

class Poll extends Widget
{
    public function run()
    {
        $poll = Survey::find()->where('(survey_expired_at > NOW() OR survey_expired_at IS NULL) AND survey_is_closed=0')->one();
        if($poll) {
            return $this->render('poll', ['survey_id' => $poll->survey_id]);
        }
    }
}
