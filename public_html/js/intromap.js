ymaps.ready(function () {
    var introMap = new ymaps.Map('intromap', {
            center: [44.948237, 34.100318],
            zoom: 10,
            behaviors: ['default', 'scrollZoom']
        }, {
            searchControlProvider: 'yandex#search'
        });
    // Создадим провайдер пробок "Сейчас" с включенным слоем инфоточек.
    var actualProvider = new ymaps.traffic.provider.Actual({}, { infoLayerShown: true });
    // И затем добавим его на карту.
    actualProvider.setMap(introMap);
    alert(actualProvider.state.get('level'));
});
