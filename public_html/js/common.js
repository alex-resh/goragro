var owl_carousel = false,
    map = false,
    tabs_place = false,
    tabs_reviews = false,
    tabs_articles = false,
    articles__list = false,
    video_carousel = false,
    slider_ui = false,
    tabs_recipes = false,
    category_recipes_list = false,
    news_list = false;

window.onload = function () {
    //setTimeout(function () {
    $(".loader").fadeOut(500);
    //}, 1000);
};

$(function () {

    owl_carousel = $("div").is(".owl-carousel"),
        owl_theme = $("div").is(".owl-theme"),

        map_catalog = $("div").is("#map-catalog"),
        map_category = $("div").is("#map-category"),
        map_currency_1 = $("div").is("#map-currency1"),
        map_currency_2 = $("div").is("#map-currency1"),
        map_currency_3 = $("div").is("#map-currency1"),
        map_currency_4 = $("div").is("#map-currency1"),
        map_sights_1 = $("div").is("#map-sights1"),
        map_currency_objects = $("div").is("#map-currency-objects"),
        map_request = $("div").is("#map-request"),

        tabs_place = $("div").is("#tabs-place"),
        tabs_reviews = $("div").is("#tabs-reviews"),
        tabs_articles = $("div").is("#tabs-articles"),
        articles_list = $("div").is("#tabs-articles"),
        video_carousel = $("div").is(".video-carousel"),
        tabs_recipes = $("div").is("#tabs-recipes"),
        category_recipes_list = $("div").is(".category-recipes__list"),
        news_list = $("div").is(".news-list"),
        ui_slider_dekstop = $("div").is(".ui-slider-dekstop"),
        ui_slider_mobile = $("div").is(".ui-slider-mobile"),
        tabs_menu = $("div").is(".tabs-menu"),
        tabs_userview_request = $("div").is("#tabs-userview-request"),
        counter_carousel = $("div").is(".counter-form__carousel");

    //стандартная карусель на внутренних страницах
    if (owl_theme) {
        $(".owl-theme").owlCarousel({
            items: 1,
            loop: false,
            dots: true,
            nav: false,
            dotsContainer: '.owl__dots'
        });
    }

    //кастомизированные кнопки на слайдере на внутренней странице
    $('.owl__next').click(function (el) {
        el.preventDefault();
        $('.owl-theme').trigger('next.owl.carousel');
    });
    $('.owl__prev').click(function (el) {
        el.preventDefault();
        $('.owl-theme').trigger('prev.owl.carousel', [300]);
    });

    if (counter_carousel) {
        $('.counter-form__carousel').owlCarousel({
            items: 1,
            loop: true,
            dots: false,
            nav: false,
            autoHeight: false,
            autoWidth: false
        });

        $('.counter-form__carousel').on("changed.owl.carousel", function (e) {
            $(".counter-form__btn").each(function () {
                $(this).removeClass("active");
            });
            var t = $(".counter-form__carousel").find(".owl-item").eq(e.item.index).find(".counter-form__carousel-item").attr("data-index");
            $(".counter-form__btn").eq(t).addClass("active");

            $(".counter-form__btn").trigger("to.owl.carousel", [t, 300, true]);
            $(".counter-form__btn").find(".owl-item").eq(t).find("img");
        });

        $(".counter-form__carousel-item").eq(0).addClass("active");

        //переключение слайдера при нажатии на кнопки навигации
        $(".counter-form__btn").click(function () {
            $(".counter-form__btn").removeClass("active");
            var e = $(this).attr("data-index");
            $(".counter-form__carousel").trigger("to.owl.carousel", [e, 300, true]);
            $(this).addClass("active");
        });
    }

    function saleListSlider() {
        if( $(window).width() < 1200 ) {
            $(".sale__list").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
            //
        } else {
            $(".sale__list").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }

    function menuListSlider() {
        if( $(window).width() < 768 ) {
            $(".menu__list").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    575:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".menu__list").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }

    function postAnounceSlider() {
        if( $(window).width() < 768 ) {
            $(".post-anounce__more-wrapper .post-anounce__line").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                responsive:{
                    449:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".post-anounce__more-wrapper .post-anounce__line").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }

    //появление слайдера на экране 1199 и меньше
    function pagesAdditionalMobile() {
        if( $(window).width() < 1200 ) {

            //если это главная страница
            if ($("body").hasClass("main-page")) {
                if ($(".page-additional-mobile").html() == "") {
                    $(".about-project .video-carousel").trigger('destroy.owl.carousel');
                    var about_project_html = $(".about-project").html();
                    var app_html = $(".app").html();

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<div class='page-mobile__item'>" + about_project_html + "</div>" +
                        "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                        "</div>");

                    $(".page-additional-mobile .title-xs").removeClass("title-xs").addClass("title-md");

                    $(".page-additional-mobile").find(".video-carousel").trigger('destroy.owl.carousel');
                    $(".page-additional-mobile").find(".video-carousel").owlCarousel({
                        items: 1,
                        merge: true,
                        loop: false,
                        video: true
                    });
                }
                //если это каталог элемент
            } else if ($("body").hasClass("catalog-item-page")) {
                var aside_soc_sety = $(".aside__soc-sety").html();
                var app_html = $(".app").html();

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item page-mobile__soc-sety'>" + aside_soc_sety + "</div>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "</div>");
                // $(".adaptive-box").each(function(){
                //     var html_adaptive_box = $(this).html();
                //     var class_adaptive_box = $(this).attr('class');
                //
                //     //var html_ = $(this).parent().clone().html();
                //     //.log(html_);
                //
                //     // if( $(".page-additional-mobile").html() == '' ) {
                //     //     $(".page-additional-mobile").html(html_);
                //     // }
                //
                //     if( $(".page-additional-mobile").html() == '' ) {
                //         $(".page-additional-mobile").append("<div class=" + class_adaptive_box + ">" + html_adaptive_box + "</div>");
                //         $(".page-additional-mobile *").each(function(){
                //             $(this).addClass( class_adaptive_box );
                //         });
                //     }
                // });
                //если это текстовая страница
            } else if ($("body").hasClass("text-page")) {
                var aside_table_data = $(".table-data").html();
                var app_html = $(".app").html();

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<h3 class='page-mobile__item page-mobile__title title-md'>Меню</h3>" +
                    "<div class='page-mobile__item page-mobile__table-data table-data'>" + aside_table_data + "</div>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "</div>");
                //если это страница просмотр пользователя
            } else if ($("body").hasClass("userview-page")) {
                var aside_table_data = $(".table-data").html();//меню
                var app_html = $(".app").html();//наше приложение
                var reward = $(".reward").html();//подписки
                var subscriptions = $(".subscriptions").html();//подписки
                var subscribers = $(".subscribers").html();//подписчики

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<h3 class='page-mobile__item page-mobile__title title-md'>Меню</h3>" +
                    "<div class='page-mobile__item page-mobile__table-data table-data'>" + aside_table_data + "</div>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "<div class='page-mobile__item subscription subscriptions'>" + subscriptions + "</div>" +
                    "<div class='page-mobile__item subscription subscribers'>" + subscribers + "</div>" +
                    "<div class='page-mobile__item reward'>" + reward + "</div>" +
                    "</div>");
                //если это страница список рецептов
            } else if ($("body").hasClass("recipes-page") || $("body").hasClass("recipe-item-page")) {
                var category_recipes_html = $(".category-recipes").html();
                var btn_add_html = $(".btn-add").html();
                var post_table_recipes_html = $(".post-table.post-table--recipes").html();
                var app_html = $(".app").html();//наше приложение

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item page-mobile__category-recipes category-recipes'>" + category_recipes_html + "</div>" +
                    "<a href='#' class='btn-add'>" + btn_add_html + "</a>" +
                    "<div class='page-mobile__item post-table post-table--recipes'>" + post_table_recipes_html + "</div>" +
                    "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                    "</div>");

                $(".page-additional-mobile .tabs-recipes").tabs();
                //если это страница результатов поиска
            } else if ($("body").hasClass("searchresult-page")) {
                var app_html = $(".app").html();//наше приложение

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "</div>");
            } else if ($("body").hasClass("blog-item-page") || $("body").hasClass("blog-list-page")) {
                var top_autors_html = $(".top-autors").html();
                var best_comment = $(".best-comment").html();
                var app_html = $(".app").html();//наше приложение

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item page-mobile__top-autors'>" + top_autors_html + "</div>" +
                    "<div class='page-mobile__item page-mobile__top-autors'>" + best_comment + "</div>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "</div>");
            } else if ($("body").hasClass("favorites-page")) {
                var app_html = $(".app").html();//наше приложение

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                    "</div>");
            } else if ( $("body").hasClass("news-page") ) {
                var btn_add_html = $(".btn-add").html();
                var table_data_html = $(".table-data").html();
                var app_html = $(".app").html();//наше приложение

                $(".page-additional-mobile").html
                ("<div class='indent vertical'>" +
                    "<div class='page-mobile__item table-data'>" + table_data_html + "</div>" +
                    "<div class='page-mobile__item'>"+
                        "<a href='#' class='btn-add'>" + btn_add_html + "</a>" +
                    "</div>"+
                    "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                    "</div>");
                } else if ( $("body").hasClass("news-item-page") ) {
                    var btn_add_html = $(".btn-add").html();
                    var table_data_html = $(".table-data").html();
                    var post_table_html = $(".post-table").html();
                    var app_html = $(".app").html();//наше приложение

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<div class='page-mobile__item table-data'>" + table_data_html + "</div>" +
                        "<div class='page-mobile__item'>"+
                            "<a href='#' class='btn-add'>" + btn_add_html + "</a>" +
                        "</div>"+
                        "<div class='page-mobile__item post-table'>" + post_table_html + "</div>" +
                        "</div>"+
                        "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                        "</div>");
                } else if( $("body").hasClass("sights-item-page") ) {
                    var btn_add_html = $(".btn-add").html();
                    var table_data_html = $(".table-data").html();
                    var post_table_html = $(".post-table").html();
                    var app_html = $(".app").html();//наше приложение

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<div class='page-mobile__item table-data'>" + table_data_html + "</div>" +
                        "<div class='page-mobile__item'>"+
                        "<a href='#' class='btn-add'>" + btn_add_html + "</a>" +
                        "</div>"+
                        "<div class='page-mobile__item post-table'>" + post_table_html + "</div>" +
                        "</div>"+
                        "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                        "</div>");
                }  else if( $("body").hasClass("afisha-kino-item-page") || $("body").hasClass("afisha-kino-list-page") || $("body").hasClass("afisha-theatre-item-page") || $("body").hasClass("afisha-theatre-list-page") ) {
                    var btn_add_html = $(".btn-add").html();
                    var table_data_html = $(".table-data").html();
                    var post_table_html = $(".post-table").html();
                    var app_html = $(".app").html();//наше приложение

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<div class='page-mobile__item table-data'>" + table_data_html + "</div>" +
                        "<div class='page-mobile__item'>"+
                        "<a href='#' class='btn-add'>" + btn_add_html + "</a>" +
                        "</div>"+
                        "<div class='page-mobile__item post-table post-table--afisha'>" + post_table_html + "</div>" +
                        "</div>"+
                        "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                        "</div>");
                } else if( $("body").hasClass("balance-page") ) {
                    var btn_add_html = $(".btn-add").html();
                    var table_data_html = $(".table-data").html();
                    var post_table_html = $(".post-table").html();
                    var app_html = $(".app").html();//наше приложение

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<h3 class='page-mobile__item page-mobile__title title-md'>Меню</h3>" +
                        "<div class='page-mobile__item table-data'>" + table_data_html + "</div>" +
                        "<div class='page-mobile__item page-mobile__app app'>" + app_html + "</div>" +
                        "</div>");
                } else if( $("body").hasClass("zapros-result-page") || $("body").hasClass("zapros-uslug-page") || $("body").hasClass("zapros-company-page") ) {
                    var aside_table_data = $(".table-data").html();
                    var app_html = $(".app").html();

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<h3 class='page-mobile__item page-mobile__title title-md'>Меню</h3>" +
                        "<div class='page-mobile__item page-mobile__table-data table-data'>" + aside_table_data + "</div>" +
                        "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>" +
                        "</div>");
                }  else {
                    var app_html = $(".app").html();//наше приложение

                    $(".page-additional-mobile").html
                    ("<div class='indent vertical'>" +
                        "<div class='page-mobile__item app page-mobile__app'>" + app_html + "</div>"+
                        "</div>");

                }
        } else {
            $(".page-additional-mobile").html("");
        }
    }

    var aside_table_data = $(".table-data").html();
    var app_html = $(".app").html();

    $(".page-additional-mobile").html
    ("<div class='indent vertical'>" +
        "<h3 class='page-mobile__title title-md'>Меню</h3>" +
        "<div class='page-mobile__table-data table-data'>" + aside_table_data + "</div>" +
        "<div class='app page-mobile__app'>" + app_html + "</div>" +
        "</div>");

    //показ фильтра в каталоге
    function catalogPageFilterShow() {
        if( $("body").hasClass("catalog-page") ) {
            if( $(window).width() < 1200 ) {
                var html_filter = $(".filter").html();

                $(".catalog-filter-mobile").html("");
                $(".catalog-filter-mobile").html("<div class='indent'><div class='filter'>" + html_filter + "</div></div>").css({"margin-top": "20px"});
            } else {
                $(".catalog-filter-mobile").html("").css({"margin-top": "0px"});
            }
        }
    }

    //рекомендуемые организации и отзывы
    function placeViewSlider() {
        if($(window).width() < 768) {
            $(".place-view__list").addClass("owl-carousel");
            $(".place-view__list").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });

            $(".experts__list").addClass("owl-carousel");
            $(".experts__list").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".place-view__list").trigger('destroy.owl.carousel').removeClass("owl-carousel");
            $(".experts__list").trigger('destroy.owl.carousel').removeClass("owl-carousel");;
        }
    }

    function articlesSlider() {
        if($(window).width() < 768) {
            $(".articles__list--row").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".articles__list--row").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }

    function afishaSlider() {
        if($(window).width() < 1199) {
            $(".products__list").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    768:{
                        items:3
                    },
                    575:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".products__list").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }


    //подключение слайдера о компании в aside
    function asideSliderConnect() {
        if($(window).width() > 1199) {
            if ( !$(".aside").find(".video-carousel").hasClass("owl-loaded") ) {
                $(".aside .video-carousel").owlCarousel({
                    items: 1,
                    merge: true,
                    loop: false,
                    video: true
                });
            }
        }
    }

    function showImgCatalogList() {
        if($(".catalog__list").length > 0) {
            if($(window).width() < 576) {
                $(".catalog__item").each(function () {
                    var html_img = $(this).find(".catalog__item-img").html();

                    $(this).find(".catalog__item-img-mobile").remove();
                    $(this).find(".catalog__item-title").after("<div class='catalog__item-img catalog__item-img-mobile'>"+ html_img + "</div>");
                });
            } else {
                $(".catalog__item-img-mobile").remove();
            }
        }
    }

    function galleryLoadPhoto() {
        if($(window).width() < 992) {
            $(".form-gallery__row").addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                }
            });
        } else {
            $(".form-gallery__row").removeClass("owl-carousel").trigger('destroy.owl.carousel');
        }
    }

    $(window).resize(function(){
        headerSignupShow();
        placeViewSlider();
        pagesAdditionalMobile();
        asideSliderConnect();
        postAnounceSlider();
        menuListSlider();
        articlesSlider();
        showImgCatalogList();
        galleryLoadPhoto();
        saleListSlider();
        masonryNewsList();
        tabsRecipes();
        afishaSlider();
    });

    //о проекте на главной
    pagesAdditionalMobile();

    //показ блока с авторизацией пользователя
    headerSignupShow();

    //рекомендуемые организации и отзывы
    placeViewSlider();

    //подключение слайдера о компании в aside
    asideSliderConnect();

    postAnounceSlider();

    menuListSlider();

    articlesSlider();

    showImgCatalogList();

    galleryLoadPhoto();

    saleListSlider();

    afishaSlider();

    if (tabs_place) {
        //при создании таба - обновляем слайдер
        if($(window).width() < 768) {
            $("#tabs-place").on("tabscreate", function (event, ui) {
                $(".place-view__list").trigger('refresh.owl.carousel');
            });
        }
        $("#tabs-place").tabs();

    }

    if (tabs_reviews) {
        //при создании таба - обновляем слайдер
        if($(window).width() < 768) {
            $("#tabs-reviews").on("tabscreate", function (event, ui) {
                $(".experts__list").trigger('refresh.owl.carousel');
            });
        }
        $("#tabs-reviews").tabs();
    }

    if (tabs_articles) {
        $("#tabs-articles").tabs();
    }

    function tabsRecipes() {
        if($(window).width() > 1199) {
            $(".aside #tabs-recipes").tabs();
        }
    }
    if (tabs_recipes) {
        tabsRecipes();
    }

    if (tabs_menu) {
        $("#tabs-menu").tabs();
    }

    if (tabs_userview_request) {
        $("#tabs-userview-request").tabs();
    }

    if (articles_list) {
        $('.articles__list').trigger('refresh.owl.carousel');
        $(".articles__list").owlCarousel({
            items: 4,
            loop: false,
            nav: false,
            dots: true,
            responsive: {
                //До 768
                768:{
                    items:4
                },
                575:{
                    items:3
                },
                399:{
                    items:2
                },
                0:{
                    items:1
                }
            }
        });
    }

    //круговой прогрессбар
    $('#circle1').circleProgress({
        value: 0.60,
        startAngle: -1.5,
        size: 22,
        fill: {
            gradient: ["#80bf51"]
        },
        emptyFill: '#43474b',
        thickness: '3'
    });

    $('#circle2').circleProgress({
        value: 0.40,
        startAngle: -1.5,
        size: 22,
        fill: {
            gradient: ["#9e2e54"]
        },
        emptyFill: '#43474b',
        thickness: '3'
    });

    //Открытие меню
    $(".js-menu-open").click(function(){
        $(this).next().toggleClass("open");
    });

    //Закрытие меню
    $(".js-menu-close").click(function(){
        $(".mnu-top__wrapper").toggleClass("open");
    });

    //ссылка на всплывающую подсказку
    $(".js-form-question").click(function(e){
        e.preventDefault();
    });

    function headerSignupShow() {
        if($(window).width() < 768) {
            var signup_html = $(".header__signup-dekstop").html();
            $(".header__signup-dekstop").hide();

            if( $("div").is(".offline") ) {
                $(".header__bottom").html("<div class='header__signup header__signup--offline'>"+ signup_html + "</div>");
            } else {
                $(".header__bottom").html("<div class='header__signup header__signup--online'>"+ signup_html + "</div>");
            }
        } else {
            $(".header__signup-dekstop").show();
            $(".header__bottom").html("");
        }
    }

    //скрыть / показать расписание сеанса
    $(".afisha__btn").click(function (el) {
        el.preventDefault();

        $(this).toggleClass("open");

        if ($(this).hasClass('open')) {
            $(this).next().slideDown();
            $(this).html("Скрыть расписание");
        } else {
            $(this).next().slideUp();
            $(this).html("Показать расписание");
        }
    });

    //афиша - выпадающее меню по дням
    $(".inside__link").click(function (el) {
        el.preventDefault();

        if ($(this).closest(".inside__afisha-toggle").find(".shedule-dropdown").hasClass("open")) {
            $(this).closest(".inside__afisha-toggle").find(".shedule-dropdown").removeClass("open");
            $(this).closest(".inside__flex").removeClass("open");
        } else {
            $(this).closest(".inside__afisha-toggle").find(".shedule-dropdown").addClass("open");
            $(this).closest(".inside__flex").addClass("open");
        }
    });

    $(".comment__view").click(function (el) {
        el.preventDefault();

        $(this).toggleClass("open");

        if ($(this).hasClass('open')) {
            $(this).find(".comment__view-text").html('скрыть');
        } else {
            $(this).find(".comment__view-text").html('показать');
        }

        $(this).closest(".comment__item").next().slideToggle();
    });

    //афиша выпадающее меню по дням - выбор дня
    $(".shedule-dropdown__line").click(function (el) {

        if ($(this).find(".shedule-dropdown__day").html() == '') {
            var html = $(this).find(".shedule-dropdown__number").html();

        } else {
            var html = $(this).find(".shedule-dropdown__day").html();
        }

        $(".shedule-dropdown__line").removeClass("active");
        $(this).addClass("active")
        $(".inside__link").html(html);

        if ($(".shedule-dropdown").hasClass("open")) {
            $(".shedule-dropdown, .inside__flex, .inside__link").removeClass("open");
        } else {
            $(".shedule-dropdown, .inside__flex, .inside__link").addClass("open");
        }
    });

    //рейтинг звезд
    $('.star.rating').click(function () {
        $(this).parent().attr('data-stars', $(this).data('rating'));
        $('#comments-rating').val($(this).data('rating'));
    });

    //всего пойдут - выпадающее меню
    $(".afisha-actions__item").click(function (el) {
        el.preventDefault();

        var afisha_actions_dropdown = $(this).closest(".afisha-actions__toggle").find(".afisha-actions__dropdown");

        if (afisha_actions_dropdown.hasClass("open")) {
            afisha_actions_dropdown.removeClass("open");
        } else {
            afisha_actions_dropdown.addClass("open");
        }
    });

    //в афише показ / скрытие полного описания
    $(".afisha-more").click(function (el) {
        el.preventDefault();

        $(this).closest(".afisha__text").find(".afisha-more__hidden").slideToggle().toggleClass("open");
        $(this).toggleClass("open");

        if ($(this).hasClass('open')) {
            $(this).find(".afisha-more__btn-text").html('Скрыть описание');
        } else {
            $(this).find(".afisha-more__btn-text").html('Раскрыть описание');
        }
    });

    //курсы валют банков - появление карты
    $(".currency-table__visible").click(function (el) {
        var currency_table_hidden = $(this).closest(".currency-table__row").find(".currency-table__hidden");

        $(".currency-table__hidden").slideUp();
        $(".currency-table__visible").removeClass("open");

        if (currency_table_hidden.is(":visible")) {
            currency_table_hidden.slideUp();
            $(this).removeClass("open");
        } else {
            currency_table_hidden.slideDown();
            $(this).addClass("open");
        }
    });

    //список категорий на карте - появление выпадающего меню
    $(".search-map__header").click(function (el) {

        var search_map_dropdown = $(this).closest(".search-map__item").find(".search-map__dropdown");

        $(".search-map__dropdown").not(search_map_dropdown).removeClass("open");
        $(".search-map__header").not($(this)).removeClass("open");

        if (search_map_dropdown.hasClass("open")) {
            $(this).removeClass("open");
            search_map_dropdown.removeClass("open");
        } else {
            search_map_dropdown.addClass("open");
            $(this).addClass("open");
        }
    });

    //галерея в запросах пользователя
    var feedback__user__text = $(".feedback-user__gallery-info").html();

    $(".feedback-user__gallery-more").click(function (el) {
        el.preventDefault();

        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
            $(this).closest(".feedback-user__gallery-list").next().slideDown();
            $(this).find(".feedback-user__gallery-info").html('Свернуть');
        } else {
            $(this).closest(".feedback-user__gallery-list").next().slideUp();
            $(this).find(".feedback-user__gallery-info").html(feedback__user__text);
        }
    });

    //галерея фото в каталоге
    var html = $(".post-anounce__text").html();

    $(".post-anounce__info--flex").click(function (el) {
        el.preventDefault();

        $(".post-anounce__more-wrapper").slideToggle('600');

        $(this).toggleClass("open");

        if ($(this).hasClass('open')) {
            $(this).find(".post-anounce__text").html('<div class="post-anounce__label">Свернуть</div>все');
        } else {
            $(this).find(".post-anounce__text").html(html);
        }

    });

    //смотреть все
    $(".sale__link-more").click(function (el) {
        el.preventDefault();
        $(".sale__hidden").slideToggle();

        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
            $(this).text('Свернуть все');
        } else {
            $(this).text('Показать еще...');
        }
    });

    $(".menu__link-more").click(function (el) {
        el.preventDefault();
        $(this).parent().find(".menu__hidden").slideToggle();

        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
            $(this).text('Свернуть все');
        } else {
            $(this).text('Показать еще...');
        }
    });

    //select
    $(document).on('click', '.select__header', function () {
        var select_list = $(this).closest(".select").find(".select__list");

        if (select_list.is(":visible")) {
            $(this).next().slideUp();
            $(this).removeClass("open");
        } else {
            $(this).next().slideDown();
            $(this).addClass("open");
        }
    });

    $(document).on('click', '.select__item', function (el) {
        el.preventDefault();
        var text = $(this).html(),
            select_list = $(this).closest(".select").find(".select__list"),
            select_evalution = $(this).closest(".select--evalution"),
            select_header = $(this).closest(".select").find(".select__header"),
            select_label = $(this).closest(".select").find(".select__label");

        if ($(this).closest(".select").hasClass("select--evalution")) {
            var text_evalution = $(this).html();
            select_evalution.find(".select__header-left").html(text_evalution);
        } else {
            select_label.html(text);
        }
        select_list.slideUp();
        select_header.removeClass("open");
    });

    //select с категориями блюд
    $(document).on('click', '.category-recipes__header', function (el) {
        //$(this).toggleClass("open");
        //$(".category-recipes__list-wrapper").toggleClass("open");

        if ($(".category-recipes__list-wrapper").hasClass("open")) {
            $(this).removeClass("open");
            $(".category-recipes__list-wrapper").removeClass("open");
        } else {
            $(this).addClass("open");
            $(".category-recipes__list-wrapper").addClass("open");
        }
    });

    //запись в поле select - текст выбранного варианта
    $(".category-recipes__item").click(function (el) {
        el.preventDefault();
        var text = $(this).html();

        $(".category-recipes__type").html(text);
        $(".category-recipes__list-wrapper").removeClass("open");
        $(".category-recipes__header").removeClass("open");
    });

    //scrollbar
    if (category_recipes_list) {

        $(".category-recipes__list").mCustomScrollbar({
            axis: "y",
            contentTouchScroll: 20,
            documentTouchScroll: true
        });
    }

    //каталог организаций на карте
    $(".search-map__list").mCustomScrollbar({
        axis: "y"
    });

    $(".search-map__dropdown").mCustomScrollbar({
        axis: "y"
    });

    //popup с видео
    $('.post-anounce__item--video').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });

    //popup форма
    $('.popup-with-form').magnificPopup({
        type: 'inline',
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        focus: '#name',
        callbacks: {
            open: function () {
                $(".counter-form__carousel").trigger('refresh.owl.carousel');
                $(".form-gallery__row").trigger('refresh.owl.carousel');
            }
        }
    });

    $('.popup-with-form-ajax').magnificPopup({
        type: 'ajax',
        mainClass: 'my-mfp-slide-bottom',
        focus: '#name',
        removalDelay: 300,
    })

    //Добавить фото
    $(".js-add-photo-pup, .js-add-video-pup").magnificPopup({
        type: 'inline',
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        focus: '#name',
        callbacks: {
            open: function () {
                $(".counter-form__carousel").trigger('refresh.owl.carousel');
            }
        }
    });

    //popup галерея
    $('.gallery-list').each(function () { // the containers for all your galleries
        $(this).magnificPopup({
            delegate: '.post-anounce__item-gallery',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">' +
            '<svg class="ico ico-delete">' +
            '<use xlink:href="/images/svg/symbol/sprite.svg#delete"></use>' +
            '</svg>' +
            '</button>',
            mainClass: 'mfp-with-zoom mfp-img-mobile',

            image: {
                verticalFit: true,
            },
            gallery: {
                enabled: true
            }
        });
    });

    //смайлы
    $("#form-comment_message").emojioneArea({
        hideSource: true
    });

    //Добавить смайлы
    $(".js-add-smiles-link").click(function(e) {
        e.preventDefault();
        $(".emojionearea-button").trigger("click");
        //$(".emojionearea").addClass("focused");
    });

    //погода
    $(".weather__header").click(function (el) {
        $(this).toggleClass("open");
        $(this).next().slideToggle();
    });

    //меню пользователя при авторизации
    $(document).on('click', '.user__header', function () {
        if ($(".user__dropdown").hasClass("open")) {
            $(this).next().removeClass("open");
            $(this).removeClass("open");
        } else {
            $(this).next().addClass("open");
            $(this).addClass("open");
        }

    });

    //фильтр в каталоге
    $(document).on("click", ".js-more", function (el) {
        el.preventDefault();
        $(this).parent().find(".filter__item-hidden").slideToggle();

        $(this).toggleClass('open');

        if ($(this).hasClass('open')) {
            $(this).find(".filter__item-more-text").text('Свернуть');
            $(this).find(".filter__item-more-count").hide();
        } else {
            $(this).find(".filter__item-more-text").text('Смотреть все');
            $(this).find(".filter__item-more-count").show();
        }
    });

    $(document).on("click", ".filter__item-header", function (el) {
        $(this).toggleClass("open");
        $(this).next().slideToggle();
    });

    //меню в запросах пользователя
    $(document).on("click", ".request-menu__item--more", function (el) {
        el.preventDefault();

        //$(this).closest(".request-menu").find(".request-menu__dropdown").toggleClass("open");
        if ($(this).closest(".request-menu").find(".request-menu__dropdown").hasClass("open")) {
            $(this).closest(".request-menu").find(".request-menu__dropdown").removeClass("open");
        } else {
            $(this).closest(".request-menu").find(".request-menu__dropdown").addClass("open");
        }
    });

    //сменить раздел
    $(document).on("click", ".intro__select-header", function (el) {
        el.preventDefault();

        $(this).toggleClass("open");
    });

    $(document).on("click", ".chat-dropdown__file", function (el) {
        el.preventDefault();

        $(".chat-dropdown__actions").toggleClass("open");
        $(".chat-dropdown__smiles").removeClass("open");
    });

    $(document).on("click", ".chat-dropdown__smile", function (el) {
        el.preventDefault();

        $(".chat-dropdown__actions").removeClass("open");
        $(".chat-dropdown__smiles").toggleClass("open");
    });

    $(document).on("click", ".chat-dropdown__close", function (el) {
        el.preventDefault();

        $(this).closest(".chat-dropdown").hide();
    });

    $(document).on("click", ".tags-choice__item", function (el) {
        var tags_select_value = $(this).html(),
            tags_list = $(this).closest(".tags-choice").find(".tags-choice__list");

        var tags_box = $('<div class="tags-choice__li tags-choice__li-active">' +
            '<span class="tags-choice__select">' + tags_select_value + '</span>' +
            '<a href="#" class="tags-choice__delete" onclick="inPut(this); return false;"></a>' +
            '</div>');

        $(this).closest(".tags-choice").find(".tags-choice__input input").val('');
        $(this).closest(".tags-choice").find(".tags-choice__ul").append(tags_box);

        //tags_list.slideUp();
    });

    $('.tags-choice__input input').keydown(function (event) {
        var val = $(this).val(),
            tags_list = $(this).closest(".tags-choice").find(".tags-choice__list");

        var tags_box = $('<div class="tags-choice__li tags-choice__li-active">' +
            '<span class="tags-choice__select">' + val + '</span>' +
            '<a href="#" class="tags-choice__delete" onclick="inPut(this); return false;"></a>' +
            '</div>');

        if ($(this).val() != '') {
            tags_list.slideDown();
        }

        if (event.keyCode == 13) {
            event.preventDefault();

            $(this).closest(".tags-choice").find(".tags-choice__ul").append(tags_box);
            $(this).val('');
            tags_list.slideUp();

            return false;
        }
    });

    $('.tags-choice__input input').keyup(function (event) {
        event.preventDefault();

        var val_input = $(this).parent(),
            val = $(this).val(),
            tags_list = $(this).closest(".tags-choice").find(".tags-choice__list");

        var tags_box = $('<div class="tags-choice__li tags-choice__li-active">' +
            '<span class="tags-choice__select">' + val + '</span>' +
            '<a href="#" class="tags-choice__delete" onclick="inPut(this); return false;"></a>' +
            '</div>');
    });

    $(".tags-choice__content").click(function (el) {
        var tags_list = $(this).closest(".tags-choice").find(".tags-choice__list");
        tags_list.slideToggle();
    });

    $(".chat-dropdown__search").click(function (el) {
        el.preventDefault();

        $(this).closest(".chat").hide().next().show();
    });

    $(".users-table__line").click(function (el) {
        el.preventDefault();

        $(this).closest(".chat").hide().prev().show();
    });

    $(".chat-users__more").click(function (el) {
        el.preventDefault();

        $(this).closest(".chat-users").find(".chat-users__list").slideToggle();
        $(".chat-users__toggle").show();
    });

    $(".chat-users__toggle").click(function (el) {
        el.preventDefault();

        $(this).closest(".chat-users").find(".chat-users__list").slideToggle();
        $(".chat-users__toggle").hide();
    });

    $('.form-request-submit').magnificPopup({
        type: 'inline',
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom'
    });

    $(".js-info-line-add").not('.js-add-field').click(function (el) {
        if ($(this).closest(".info-line").find(".info-line__input").val() != '') {
            var select_html = $(this).closest(".form__row").find(".info-line:first").html();
            $(this).closest(".form__row").append("<div class='info-line info-line--added'>" + select_html + "</div>");
        }
    });

    $('.js-comment-reply').click(function () {
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $('html,body').animate({
            scrollTop: destination
        }, 800);
        return false;
    });


    $(document).on('click', '.info-line--added .js-info-line-add', function () {
        $(this).closest(".info-line--added").remove();
    });

    $(document).on('click', '.js-cookie-panel', function (e) {
        e.preventDefault();
        $(this).closest(".cookie-panel").hide();
    });

    $('body').on('click', '.js-gallery-button', function (el) {
        $(this).closest(".form-gallery__img").find(".gallery-menu").slideToggle();
    });

    $(".form-gallery__img").mouseleave(function () {
        $(this).find(".gallery-menu").slideUp();
    });

    $(".js-added-info-btn-add").click(function (el) {
        el.preventDefault();
        var counter = $(this).data('count');
        if ($(this).closest(".added-info__element").find(".added-info__title").val() != '') {
            var obj = $(this).closest(".added-info__element")[0];
            obj = $(obj).clone();
            $(obj).find('[name]').each(function(i, e){
                var name = $(e).attr('name');
                name = name.replace(/\[0\]/, '[' + counter + ']');
                $(e).attr('name', name);
                $(e).attr('value', '');
                $(e).text('');
            });
            $(obj).find('.added-info__picture').attr('src', '');
            $(obj).find('.added-info__image-id').val('');
            var added_html = $(obj).html();
            $(this).closest(".added-info__item").append("<div class='added-info__element added-info__element--added'>" + added_html + "</div>");
            $(this).data('count', counter + 1);
        }
        $(this).closest('.added-info__item').trigger('addElement');
    });

    $('body').on('click', '.added-info__element--added .js-added-info-btn', function () {
        $(this).closest(".added-info__element").remove();
    });

    $(".js-work-time-btn").click(function (el) {
        el.preventDefault();
        var added_html = $(this).closest(".work-time__element").html();
        $(this).closest(".work-time").append("<div class='work-time__element work-time__element--added'>" + added_html + "</div>");
    });

    $(document).on('click', '.work-time__element--added .js-work-time-btn', function () {
        $(this).closest(".work-time__element--added").remove();
    });

    $(document).on('click', '.js-work-time-tips', function () {
        $(this).toggleClass("active").siblings().removeClass("active");
    });

    //нажатие по документу - все всплывающие окна сворачиваются
    $(document).mouseup(function (e) {
        var user__dropdown = $(".user__dropdown"),
            user__header = $(".user__header"),
            category_recipes__header = $(".category-recipes__header"),
            category_recipes__list = $(".category-recipes__list-wrapper"),
            select__header = $(".select__header"),
            select__list = $(".select__list"),
            afisha_actions_dropdown = $(".afisha-actions__dropdown"),
            afisha_actions_item = $(".afisha-actions__item"),
            shedule_dropdown = $(".shedule-dropdown"),
            inside_flex = $(".inside__flex"),
            intro_select_header = $(".intro__select-header"),
            search_map_dropdown = $(".search-map__dropdown"),
            search_map_header = $(".search-map__header"),
            tags_choice_list = $(".tags-choice__list"),
            tags_choice_content = $(".tags-choice__content"),
            request_menu_dropdown = $(".request-menu__dropdown"),
            request_menu_item_more = $(".request-menu__item--more");

        if (!user__dropdown.is(e.target) &&
            user__dropdown.has(e.target).length === 0 &&
            !user__header.is(e.target) &&
            user__header.has(e.target).length === 0 &&
            !category_recipes__header.is(e.target) &&
            category_recipes__header.has(e.target).length === 0 &&
            !category_recipes__list.is(e.target) &&
            category_recipes__list.has(e.target).length === 0 &&
            !select__header.is(e.target) &&
            select__header.has(e.target).length === 0 &&
            !select__list.is(e.target) &&
            select__list.has(e.target).length === 0 &&
            !afisha_actions_dropdown.is(e.target) &&
            afisha_actions_dropdown.has(e.target).length === 0 &&
            !afisha_actions_item.is(e.target) &&
            afisha_actions_item.has(e.target).length === 0 &&
            !shedule_dropdown.is(e.target) &&
            shedule_dropdown.has(e.target).length === 0 &&
            !inside_flex.is(e.target) &&
            inside_flex.has(e.target).length === 0 &&
            !search_map_dropdown.is(e.target) &&
            search_map_dropdown.has(e.target).length === 0 &&
            !search_map_header.is(e.target) &&
            search_map_header.has(e.target).length === 0 &&
            !tags_choice_list.is(e.target) &&
            tags_choice_list.has(e.target).length === 0 &&
            !tags_choice_content.is(e.target) &&
            tags_choice_content.has(e.target).length === 0 &&
            !request_menu_dropdown.is(e.target) &&
            request_menu_dropdown.has(e.target).length === 0 &&
            !request_menu_item_more.is(e.target) &&
            request_menu_item_more.has(e.target).length === 0) {
            user__dropdown.removeClass("open");
            user__header.removeClass("open");
            category_recipes__header.removeClass("open");
            category_recipes__list.removeClass("open");
            select__header.removeClass("open");
            select__list.slideUp().removeClass("open");
            afisha_actions_dropdown.removeClass("open");
            shedule_dropdown.removeClass("open");
            inside_flex.removeClass("open");
            intro_select_header.removeClass("open");
            search_map_dropdown.removeClass("open");
            search_map_header.removeClass("open");
            tags_choice_list.slideUp();
            request_menu_dropdown.removeClass("open");
        }
    });

    // слайдер ui
    // if (ui_slider_dekstop) {
    //     //декстоп фильтр цен
    //     $(".ui-slider-dekstop").slider({
    //         min: 2100,
    //         max: 12600,
    //         step: 1,
    //         values: [2500, 9220],
    //         range: true,
    //         stop: function (event, ui) {
    //             $(".form-price__min").val($(".ui-slider-dekstop").slider("values", 0));
    //             $(".form-price__max").val($(".ui-slider-dekstop").slider("values", 1));
    //         },
    //         slide: function (event, ui) {
    //             $(".form-price__min").val($(".ui-slider-dekstop").slider("values", 0));
    //             $(".form-price__max").val($(".ui-slider-dekstop").slider("values", 1));
    //         }
    //     });
    //
    //     $(".form-price__min").change(function () {
    //         var value1 = $(".form-price__min").val();
    //         var value2 = $(".form-price__max").val();
    //
    //         if (parseInt(value1) > parseInt(value2)) {
    //             value1 = value2;
    //             $(".form-price__min").val(value1);
    //         }
    //         $(".ui-slider-dekstop").slider("values", 0, value1);
    //     });
    //
    //     $(".form-price__max").change(function () {
    //         var value1 = $(".form-price__min").val();
    //         var value2 = $(".form-price__max").val();
    //
    //         if (value2 > 12600) {
    //             value2 = 12600;
    //             $(".form-price__max").val(12600);
    //         }
    //
    //         if (parseInt(value1) > parseInt(value2)) {
    //             value2 = value1;
    //             $(".form-price__max").val(value2);
    //         }
    //         $(".ui-slider-dekstop").slider("values", 1, value2);
    //     });
    // }

    // if (ui_slider_mobile) {
    //     //мобильный фильтр цен
    //     $(".ui-slider-mobile").slider({
    //         min: 2100,
    //         max: 12600,
    //         step: 1,
    //         values: [2500, 9220],
    //         range: true,
    //         stop: function (event, ui) {
    //             $(".form-price__min1").val($(".ui-slider-mobile").slider("values", 0));
    //             $(".form-price__max1").val($(".ui-slider-mobile").slider("values", 1));
    //         },
    //         slide: function (event, ui) {
    //             $(".form-price__min1").val($(".ui-slider-mobile").slider("values", 0));
    //             $(".form-price__max1").val($(".ui-slider-mobile").slider("values", 1));
    //         }
    //     }).draggable();
    //
    //     $(".form-price__min1").change(function () {
    //         var value11 = $(".form-price__min1").val();
    //         var value21 = $(".form-price__max1").val();
    //
    //         if (parseInt(value11) > parseInt(value21)) {
    //             value11 = value21;
    //             $(".form-price__min").val(value11);
    //         }
    //         $(".ui-slider-mobile").slider("values", 0, value11);
    //     });
    //
    //
    //     $(".form-price__max1").change(function () {
    //         var value11 = $(".form-price__min1").val();
    //         var value21 = $(".form-price__max1").val();
    //
    //         if (value21 > 12600) {
    //             value21 = 12600;
    //             $(".form-price__max1").val(12600);
    //         }
    //
    //         if (parseInt(value11) > parseInt(value21)) {
    //             value21 = value11;
    //             $(".form-price__max1").val(value21);
    //         }
    //         $(".ui-slider-mobile").slider("values", 1, value21);
    //     });
    //
    //     $(document).on("click", ".js-filter-price-button", function(e){
    //         e.preventDefault();
    //         var value11 = $(".form-price__min1").val();
    //         var value21 = $(".form-price__max1").val();
    //
    //         if (parseInt(value11) > parseInt(value21)) {
    //             value11 = value21;
    //             $(".form-price__min1").val(value1);
    //         }
    //
    //         if (value21 > 12600) {
    //             value21 = 12600;
    //             $(".form-price__max1").val(12600);
    //         }
    //
    //         if (parseInt(value11) > parseInt(value21)) {
    //             value21 = value11;
    //             $(".form-price__max1").val(value21);
    //         }
    //
    //         $(".ui-slider-mobile").slider("values", 1, value21);
    //         $(".ui-slider-mobile").slider("values", 0, value11);
    //     });
    // }

    //расположение новостей
    if (news_list) {
        masonryNewsList();
    }

    //список новостей
    function masonryNewsList() {
        if($(window).width() > 1199) {
            $('.news-list').removeClass("owl-carousel").trigger('destroy.owl.carousel');
            $('.news-list').masonry({
                // options
                itemSelector: '.news',
                columnWidth: '.news',
                percentPosition: true
            });
        } else {
            if( $('.news-list').masonry()) {
                $('.news-list').masonry( 'destroy' );
            }
            $('.news-list').addClass("owl-carousel").owlCarousel({
                loop:false,
                nav:false,
                dots: true,
                margin: 10,
                responsive:{
                    768:{
                        items:4
                    },
                    575:{
                        items:3
                    },
                    399:{
                        items:2
                    },
                    0:{
                        items:1
                    }
                    }
                });
            }
    }

    //карта в каталоге
    if (map_catalog == 'ololo') {
        ymaps.ready(init);
        var myMap,
            myPlacemark;

        // Инициализация карты
        function init() {

            myMap = new ymaps.Map("map-catalog", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );

            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap.geoObjects.add(myPlacemark);

            myMap.behaviors.disable('scrollZoom');
        }

    }



    if (map_currency_1) {
        ymaps.ready(init);
        var myMap3,
            myPlacemark3;

        // Инициализация карты
        function init() {

            myMap3 = new ymaps.Map("map-currency1", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark3 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap3.geoObjects.add(myPlacemark3);
        }

    }

    if (map_currency_2) {
        ymaps.ready(init);
        var myMap3,
            myPlacemark3;

        // Инициализация карты
        function init() {

            myMap4 = new ymaps.Map("map-currency2", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark4 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap4.geoObjects.add(myPlacemark4);
        }

    }

    if (map_currency_3) {
        ymaps.ready(init);
        var myMap5,
            myPlacemark5;

        // Инициализация карты
        function init() {

            myMap5 = new ymaps.Map("map-currency3", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark5 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap5.geoObjects.add(myPlacemark5);
        }

    }

    if (map_currency_4) {
        ymaps.ready(init);
        var myMap6,
            myPlacemark6;

        // Инициализация карты
        function init() {

            myMap6 = new ymaps.Map("map-currency4", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark6 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap6.geoObjects.add(myPlacemark6);
        }

    }


    if (map_currency_objects) {
        ymaps.ready(init);
        var myMap8,
            myPlacemark8;

        // Инициализация карты
        function init() {

            myMap8 = new ymaps.Map("map-currency-objects", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark8 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap8.geoObjects.add(myPlacemark8);
        }
    }

    if (map_request) {
        ymaps.ready(init);
        var myMap9,
            myPlacemark9;

        // Инициализация карты
        function init() {

            myMap9 = new ymaps.Map("map-request", {
                center: [44.496141, 34.162030],
                zoom: 17,
                controls: []
            });

            // 1 метка
            myPlacemark9 = new ymaps.Placemark(
                [44.496141, 34.162030], {
                    balloonContent: '<div class="event">' +

                    '<div class="event__info">' +
                    '<p class="event__title">' +
                    'Автосервис «Кволити Моторс» на Шмитовском' +
                    '</p>' +
                    '<div class="event__rate">' +
                    '<div class="rate">' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '<i class="ico ico-star ico-star--empty rate__ico">' +
                    '<svg class="ico ico-star">' +
                    '<use xlink:href="/images/svg/symbol/sprite.svg#star"></use>' +
                    '</svg>' +
                    '</i>' +
                    '</div>' +
                    '<div class="event__rate-text">5 оценок</div>' +
                    '</div>' +
                    '<div class="event__descr">' +
                    '<p class="event__text">ул. Малышева, 5</p>' +
                    '<p class="event__text">8 (800) 1000-200, пн-вс: 10:00-21:00</p>' +
                    '</div>' +
                    '</div>' +

                    '<div class="event__right">' +
                    '<div class="event__img">' +
                    '<img src="/images/recipe/1.jpg" alt="">' +
                    '</div>' +
                    '<a href="#" class="event__link">25 фото</a>' +
                    '</div>' +

                    '   </div>' +
                    '</div>'
                }, {
                    // Иконка кластера
                    iconLayout: 'default#image',
                    iconImageHref: 'images/icons/marker.svg',
                    iconImageSize: [38, 52],
                    iconImageOffset: [-19, -52],

                    // Балун
                    balloonImageSize: [497, 183],
                    balloonMinWidth: 497,
                    balloonOffset: [35, -40],
                    balloonAutoPanCheckZoomRange: false,
                    clusterDisableClickZoom: true,
                    hideIconOnBalloonOpen: false
                }
            );


            var myClusterer = new ymaps.Clusterer({
                clusterDisableClickZoom: true
            });
            myMap9.geoObjects.add(myPlacemark9);
        }
    }
});

function inPut(el) {
    el.closest('.tags-choice__li').remove();
}

$(function(){
    $('.save-btn').on('click', function(){
        $(this).attr('disabled', true);
    });

    $('#profile-img').on('change', function(e){
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.file-upload__avatar-wrapper img')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(this.files[0]);
        }
        // $('.file-upload__avatar-wrapper img').attr('src',$(this).val());
    })

});