<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Sight|null */
/* @var $form \yii\widgets\ActiveForm */
/* @var $gallery \app\models\SightGallery */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\NewsCategory;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;

$data = null;
if ($model->city) {
    $data = [$model->city_id => $model->city->name];
}

$uploadUrl = Url::to(['/admin/sight/gallery-upload']);
$deleteUrl = Url::to(['/admin/sight/gallery-delete']);
$field_name = Html::getInputName($gallery, 'img');
$field_name_singht = Html::getInputName($gallery, 'sight_id');
$this->registerJs(<<<JS
    var updateId = false;

    $('body').on('change', '.gallery-file-input', function(){
        
        var files = $('.gallery-file-input')[0].files;
        for (file_id in files){
            var file = files[file_id];
            if(file.size == undefined) continue;
            
            console.log(file);
            
            var fd = new FormData();

            if(updateId){
                var id = $(this).parents('.form-gallery__item-wrap').data('id');
                fd.append('id', updateId);
                var obj = $('.form-gallery__item-wrap[data-id='+updateId+']');
            }
            
            fd.append('$field_name',file);
            fd.append('$field_name_singht', 0);
            
        
            $.ajax({
                url: '$uploadUrl',
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response){
                    if(response != 0){
                        if(!updateId){
                            var gallery_item = $('.gallery-item:first').clone();
                            $(gallery_item).data('id', response.id);
                            $(gallery_item).attr('data-id', response.id);
                            $(gallery_item).find("img").attr("src",response.thumb);
                            $(gallery_item).find('.image-id').val(response.id);
                            $('.gallery-images').append(gallery_item);
                            gallery_item.show();
                        } else {
                            obj.find('img').attr('src', response.thumb);
                            updateId = false;
                            $('.gallery-file-input').attr('multiple', 'true');
                        }
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        }
    });

    $('body').on('click', '.delete-image', function(e){
        e.preventDefault();
        var obj = $(this).parents('.gallery-item');
        var id = $(this).parents('.gallery-item').data('id');
        var fd = new FormData();
        fd.append('id', id);
        $.ajax({
            url: '$deleteUrl',
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response){
                if(response != 0){
                    obj.remove();
                }else{
                    alert('file not uploaded');
                }
            },
        });
    });

    
JS
);
?>
<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]) ?>
<?php echo $form->field($model, 'title') ?>
<?php echo $form->field($model, 'city_id')->label('Город')->widget(Select2::class, [
    'data' => $data,
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 2,
        'ajax' => [
            'url' => Url::to(['news/city-list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(city) { return city.text; }'),
        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
    ]
]) ?>
<?php echo $form->field($model, 'address') ?>
<?php echo $form->field($model, 'text')->widget(\dosamigos\tinymce\TinyMce::class) ?>

    <div class="gallery">
        <div class="gallery-images">
            <div class="gallery-item" style="display: none" >
                <img src="">
                <?php echo $form->field($model, 'images[]', ['template' => '{input}'])->label(false)->hiddenInput(['id' => false, 'class' => 'image-id']) ?>
                <a href="#" class="delete-image">Удалить</a>
            </div>
            <?php foreach ($model->gallery as $item): ?>
                <div class="gallery-item" data-id="<?php echo $item->id; ?>">
                    <img src="<?php echo $item->getThumbUploadUrl('img', 'preview') ?>">
                    <?php echo $form->field($model, 'images[]', ['template' => '{input}'])->label(false)->hiddenInput(['id' => false, 'class' => 'image-id', 'value' => $item->id ]) ?>
                    <a href="#" class="delete-image">Удалить</a>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="gallery-input">
            Перетащите изображения сюда
            <input type="file" class="gallery-file-input" multiple />
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitInput('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
<?php ActiveForm::end(); ?>