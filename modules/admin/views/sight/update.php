<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Sight|null */
/* @var $form \yii\widgets\ActiveForm */
/* @var $gallery \app\models\SightGallery */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактировать достопримечательность';
?>
<h2><?php echo $this->title ?></h2>
<?php echo $this->render('_form', ['model' => $model, 'gallery' => $gallery]); ?>

