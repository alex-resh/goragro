<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\search\SightSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Достопримечательности';
?>
<h2><?php echo $this->title ?></h2>
<p>
    <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
</p>
<?php echo GridView::widget([
   'dataProvider' => $dataProvider,
   'columns' => [
       'id',
       'title',
       'city.name',
       [
           'class' => \yii\grid\ActionColumn::class,
           'template' => '{update} {delete}'
       ]
   ]
]);