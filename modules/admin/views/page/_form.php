<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\News|null */
/* @var $form \yii\widgets\ActiveForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin() ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'seo_url') ?>
<?php echo $form->field($model, 'text')->widget(\dosamigos\tinymce\TinyMce::class) ?>

<div class="form-group">
    <?php echo Html::submitInput('Создать', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

