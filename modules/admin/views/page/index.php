<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\search\PageSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
$this->title = 'Страницы';
?>
    <h2><?php echo $this->title ?></h2>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            //'text',
            'title',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => \yii\grid\ActionColumn::class
            ]
        ]
    ]
);