<?php

/**
 * @var $this \yii\web\View
 */
$this->title = 'Продукты';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Организации', 'url' => ['/admin/organisation']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>