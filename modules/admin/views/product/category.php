<?php

/**
 * @var $this \yii\web\View
 */

use app\models\Category;
use app\models\Product;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = 'Категории продуктов';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['/admin/product']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs(<<<JS
    $('.js-categories :checkbox').on('change', function() {
        var checkbox = $(this);
        var data = {
            class: checkbox.attr('name').replace('[]', ''),
            id: checkbox.val(),
            value: checkbox.is(':checked')
        };
        $.ajax({
            url: $('.js-categories').data('url'),
            method: 'post',
            data: data
        }).done(function(res) {
            console.log(res);
        }).fail(function(res) {
            console.log(res.responseText);
        });
    });
JS
);
?>
<h4><?= $this->title;?></h4>
<div class="row js-categories" data-url="<?= Url::to(['/admin/product/check-category']);?>">
    <?php foreach (Product::productTypeList() as $class => $type) : ?>
    <div class="col-md-3">
        <h5><?= $type;?></h5>
        <?= Html::checkboxList($class, Product::categoryIds($class), Category::find()->select('name')->indexBy('id')->column());?>
    </div>
    <?php endforeach;?>
</div>
