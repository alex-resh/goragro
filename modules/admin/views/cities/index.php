<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\search\FsCitySearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;

$this->title = 'Города';
?>
<h2><?php echo $this->title ?></h2>
<?php echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'name_en',
            [
                'attribute' => 'id_region',
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\FsRegion::find()->all(), 'id', 'name'),
                'value' => function($model){
                    return $model->region->name;
                }
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{update}'
            ]
        ],
        'filterModel' => $searchModel
    ]
);