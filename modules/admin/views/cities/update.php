<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\FsCity|null */
/* @var $form \yii\widgets\ActiveForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактировать город';
?>
<h2><?php echo $this->title ?></h2>
<?php $form = ActiveForm::begin([]) ?>
<?php echo $form->field($model, 'name') ?>
<?php echo $form->field($model, 'name_en') ?>
<?php echo $form->field($model, 'name_ru_rod') ?>
<?php echo $form->field($model, 'name_ru_dat') ?>
<?php echo $form->field($model, 'name_ru_vin') ?>
<?php echo $form->field($model, 'name_ru_tvar') ?>
<?php echo $form->field($model, 'name_ru_pred') ?>
<?php echo $form->field($model, 'in_popup')->checkbox() ?>
<?php echo $form->field($model, 'bold')->checkbox() ?>
<?php echo Html::img($model->getThumbUploadUrl('img', 'preview')) ?>
<?php echo $form->field($model, 'img')->fileInput() ?>

<div class="form-group">
    <?php echo Html::submitInput('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>

