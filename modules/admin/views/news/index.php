<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\search\NewsSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
$this->title = 'Новости';
?>
    <h2><?php echo $this->title ?></h2>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php echo GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            //'text',
            'title',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 0){
                        return Html::tag('span', 'На модерации', ['style' => 'color: red']);
                    } else {
                        return Html::tag('span', 'Опубликовано', ['style' => 'color: green']);
                    }
                },
                'format' => 'raw'
            ],
            [
                'class' => \yii\grid\ActionColumn::class,
                'template' => '{update} {delete}'
            ]
        ]
    ]
);