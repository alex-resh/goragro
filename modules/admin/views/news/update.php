<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\News|null */
/* @var $form \yii\widgets\ActiveForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Редактировать новость';
?>
<h2><?php echo $this->title ?></h2>
<?php echo $this->render('_form', ['model' => $model]); ?>

