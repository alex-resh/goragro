<?php
/* @var $this \yii\web\View */
/* @var $searchModel \app\models\search\AgentSearch */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Представители';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = $this->title;

?>
    <h3><?= $this->title; ?></h3>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        [
            'attribute' => 'organisation.name',
            'label' => 'Организация',
            'value' => function ($model, $key, $index, $column) {
                return Html::a($model->organisation->name, ['/organisation/index', 'id' => $model->organisation->id]);
            },
            'format' => 'raw'
        ],
        [
            'attribute' => 'user.name',
            'label' => 'Пользователь',
            'value' => function ($model, $key, $index, $column) {
                return Html::a($model->user->username, ['/profile/index', 'id' => $model->user->id]);
            },
            'format' => 'raw'
        ],
        'name',
        'email',
        'phone',
        [
            'attribute' => 'status',
            'format' => 'html',
            'filter' => $searchModel->statusList(),
            'value' => function ($model) {
                return Html::tag('div', $model->statusName, ['class' => 'col-md-12 btn btn-xs btn-' . $model->statusClass]);
            }
        ],
        [
            'class' => \yii\grid\ActionColumn::class,
            'template' => '{approve} {delete}',
            'buttons' => [
                'approve' => function ($url, $model, $key) {
                    return $model->status == \app\models\Agent::STATUS_NEW ? Html::a('Одобрить', $url, ['class' => 'btn btn-success']) : '';
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('Удалить', $url, ['class' => 'btn btn-danger', 'confirm' => 'Удалить агента?']);
                }
            ]
        ]
    ]
]);
