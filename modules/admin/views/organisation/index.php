<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \app\models\search\OrganisationSearch
 */
$this->title = 'Организации';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = $this->title;

use app\models\Organisation;
use yii\bootstrap\Tabs;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

?>
<h3><?= $this->title;?></h3>
<?= Tabs::widget([
    'items' => [
        [
            'label' => 'Опубликованные (' . Organisation::find()->where(['!=', 'clone_id', ''])->count() . ')',
            'url' => ['/admin/organisation'],
            'active' => Url::to() == Url::to(['/admin/organisation']),
        ],
        [
            'label' => 'Новые (' . Organisation::find()->where(['status' => Organisation::STATUS_NEW])->count() . ')',
            'url' => ['/admin/organisation/new'],
            'active' => Yii::$app->controller->action->uniqueId == 'admin/organisation/new',
        ],
        [
            'label' => 'На модерации (' . Organisation::find()->where(['status' => Organisation::STATUS_MODERATED])->count() . ')',
            'url' => ['/admin/organisation/moderated'],
            'active' => Yii::$app->controller->action->uniqueId == 'admin/organisation/moderated',
        ],
        [
            'label' => 'На дороботке (' . Organisation::find()->where(['status' => Organisation::STATUS_REWORK])->count() . ')',
            'url' => ['/admin/organisation/reworked'],
            'active' => Yii::$app->controller->action->uniqueId == 'admin/organisation/reworked',
        ],
    ],
]);?>
<?= Html::a('Создать организацию', ['/admin/organisation/create'], ['class' => 'btn btn-md btn-primary']);?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => SerialColumn::class],
        'id',
        Yii::$app->controller->action->uniqueId == 'admin/organisation/index' ?
        [
            'attribute' => 'clone_id',
            'label' => 'ID для редактирования',
        ] :
        [
            'attribute' => 'origin.id',
            'label' => 'ID опубликованной',
        ],
        [
            'attribute' => 'img',
            'format' => 'html',
            'value' => function($model){
                return Html::img($model->getThumbUploadUrl('img', 'org_img'));
            },
        ],
        'name',
        'desc',
        'moderator_msg',
        [
            'label' => 'Адрес',
            'format' => 'html',
            'value' => function($model){
                $html = '<div>' . $model->address->toString . '</div>';
                return $html;
            },
            'visible' => function($model){
                return $model->status != Organisation::STATUS_PUBLISHED;
            }
        ],
        'user.username',
        [
            'label' => 'Телефоны',
            'format' => 'ntext',
            'value' => function($model){
                $phones = [];
                foreach ((array)$model->address->phones as $i => $phone) {
                    $phones[] = $phone . ' ' . $model->address->phoneType[$i];
                }
                return implode("\n", $phones);
            }
        ],
        [
            'label' => 'Электронная почта',
            'format' => 'ntext',
            'value' => function($model){
                $emails = [];
                foreach ((array)$model->address->emails as $i => $email) {
                    $emails[] = $email . ' ' . $model->address->emailType[$i];
                }
                return implode("\n", $emails);
            }
        ],
        [
            'label' => 'Сайты',
            'format' => 'ntext',
            'value' => function($model){
                $urls = [];
                foreach ((array)$model->address->url as $i => $url) {
                    $urls[] = $url . ' ' . $model->address->urlType[$i];
                }
                return implode("\n", $urls);
            }
        ],
        [
            'class' => ActionColumn::class,
            'urlCreator' => function($action, $model, $key, $index){
                if($action != 'view' && $model->clone_id){
                    return Url::to(['/admin/organisation/' . $action, 'id' => $model->clone_id]);
                }
                return Url::to(['/admin/organisation/' . $action, 'id' => $model->id]);
            }
        ],
    ],
]);?>