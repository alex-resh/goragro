<?php

/**
 * @var $this \yii\web\View
 * @var $model app\models\forms\CommentSettingsForm
 */
$this->title = 'Настройки коментариев';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Коментарии', 'url' => ['/admin/comment']];
$this->params['breadcrumbs'][] = $this->title;

use yii\widgets\ActiveForm; ?>
<h4><?= $this->title;?></h4>
<?php $form = ActiveForm::begin();?>
<h5>Изображения к коментариям</h5>
<div class="form-group col-md-6">
    <?= $form->field($model, 'number', ['options' => ['class' => 'col-md-3']]);?>
    <?= $form->field($model, 'width', ['options' => ['class' => 'col-md-3']]);?>
    <?= $form->field($model, 'height', ['options' => ['class' => 'col-md-3']]);?>
</div>
<div class="clearfix"></div>
<?= $form->field($model, 'img_allowed')->checkbox();?>
<?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-md btn-success']);?>
<?php ActiveForm::end();?>
