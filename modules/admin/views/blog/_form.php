<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\NewsCategory;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
if($model->city){
    $data = [$model->city_id => $model->city->name];
} else {
    $data = [];
}

?>
<?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
]) ?>
<?php echo $form->field($model, 'title') ?>
<?php echo $form->field($model, 'seo_url') ?>
<?php // echo $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(NewsCategory::find()->all(), 'id', 'name'), ['prompt' => '--Выбрать--']) ?>
<?php echo $form->field($model, 'city')->widget(Select2::class, [
        'data' => $data,
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => Url::to(['city-list']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
        ]
]) ?>
<?php echo $form->field($model, 'text')->widget(\dosamigos\tinymce\TinyMce::class) ?>
<?php if(!$model->isNewRecord): ?>
    <?php echo Html::img($model->getThumbUploadUrl('img', 'preview')) ?>
<?php endif; ?>
<?php echo $form->field($model, 'img')->fileInput() ?>
<?php echo $form->field($model, 'tags')->widget(Select2::class, [
    'data' => ArrayHelper::map(\app\models\NewsTag::find()->all(), 'tag', 'tag'),
    'options' => ['multiple' => true],
    'pluginOptions' => [
        'tags' => true,
        'tokenSeparators' => [',', ' '],
        'maximumInputLength' => 10
    ],
]) ?>
<?php echo $form->field($model, 'status')->radioList([0 => 'На модерации', 1 => 'Опубликовано']) ?>
    <div class="form-group">
        <?php echo Html::submitInput('Сохранить', ['class' => 'btn btn-success save-btn']) ?>
    </div>
<?php ActiveForm::end(); ?>