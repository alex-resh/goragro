<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\grid\GridView;

$this->title = 'Рассылки';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны писем', 'url' => ['/admin/mail/template']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
]);?>
