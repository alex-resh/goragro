<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Шаблоны писем';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Рассылки', 'url' => ['/admin/mail/mailing']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>
<div class="form-group">
    <?= Html::a('Создать', ['create-template'], ['class' => 'btn btn-md btn-primary']);?>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
]);?>

