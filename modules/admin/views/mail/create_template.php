<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\MailingTemplate
 */

$this->title = 'Создать шаблон рассылки';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Рассылки', 'url' => ['/admin/mail/mailing']];
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны писем', 'url' => ['/admin/mail/template']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>
<?= $this->render('_template_form', [
    'model' => $model,
]);?>
