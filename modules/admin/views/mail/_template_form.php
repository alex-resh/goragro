<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\MailingTemplate
 */

use dosamigos\tinymce\TinyMce;
use yii\widgets\ActiveForm;

?>
<pre>
<?php print_r($model->attributes);?>
</pre>
<?php $form = ActiveForm::begin();?>
<?= $form->field($model, 'name');?>
<?= $form->field($model, 'description')->textarea();?>
<?= $form->field($model, 'from');?>
<?= $form->field($model, 'subject');?>
<?= $form->field($model, 'body')->widget(TinyMce::class, [
    'language' => 'ru',
]);?>
<?= $form->field($model, 'html')->checkbox();?>
<?php ActiveForm::end();?>
