<?php
/* @var $this \yii\web\View */
/* @var $model \app\models\Category */
$this->title = 'Редактировать категорию';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h4><?= $this->title;?></h4>
<?php echo $this->render('_form', ['model' => $model]);