<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\helpers\Html;

$this->title = 'Категории организаций';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = $this->title;

use yii\grid\GridView; ?>
<h4><?= $this->title;?></h4>
<div class="form-group">
    <?php echo Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
</div>
<?php echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'name',
        'url',
        'created_at:date',
        [
            'class' => \yii\grid\ActionColumn::class,
            'template' => '{update} {delete}',
        ]
    ]
]);?>
