<?php
/* @var $this \yii\web\View */
/* @var $form \yii\widgets\ActiveForm */
/* @var $model \app\models\Category|\yii\db\ActiveRecord */
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin() ?>
<?php echo $form->field($model, 'name') ?>
<?php echo $form->field($model, 'url') ?>
<?php  // echo $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map(Category::find()->all(), 'id', 'name'), ['prompt' => '--Выберите--']) ?>
<div class="form-group">
    <?php echo Html::submitInput('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
