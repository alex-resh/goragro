<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\AfficheCategory
 */

$this->title = 'Создать категорию афиш';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Афиши - Категории', 'url' => ['/admin/affiche/category']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>
<?= $this->render('_category_form', [
    'model' => $model,
]);?>