<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
$this->title = 'Афиши - Категории';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;

use yii\grid\GridView;
use yii\helpers\Html; ?>
<h4><?= $this->title;?></h4>
<div class="form-group">
    <?= Html::a('Создать категорию', ['/admin/affiche/category-create'], ['class' => 'btn btn-md btn-primary']);?>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
]);?>
