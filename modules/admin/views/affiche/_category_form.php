<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\AfficheCategory
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo '<pre>';
print_r($model->attributes);
echo '</pre>';
?>
<?php $form = ActiveForm::begin();?>
<?= $form->field($model, 'name');?>
<?= $form->field($model, 'description')->textarea();?>
<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-md btn-success']);?>
<?php ActiveForm::end();?>
