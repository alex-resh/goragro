<?php

use yii\helpers\Html;
use app\models\Settings;

?>
<div class="form-group">
    <label class="control-label">Title главнои страницы</label>
    <?php echo Html::textInput('seo_title', Settings::get('seo_title'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Keywords главнои страницы</label>
    <?php echo Html::textInput('seo_keywords', Settings::get('seo_keywords'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Description главнои страницы</label>
    <?php echo Html::textInput('seo_description', Settings::get('seo_description'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Произвольный код в head</label>
    <?php echo Html::textarea('seo_meta', Settings::get('seo_meta'), ['class' => 'form-control']) ?>
</div>
