<?php
/**
 * @var $this \yii\web\View
 * @var $images array
 */
use yii\helpers\Html;
use app\models\Settings;
use yii\bootstrap\Tabs;
?>
<h2>Настройки</h2>
<?php echo Html::beginForm(); ?>
<?php $this->beginBlock('settings_main') ?>
<div class="form-group">
    <label class="control-label">Название сайта</label>
    <?php echo Html::textInput('site_name', Settings::get('site_name'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Описание сайта</label>
    <?php echo Html::textInput('site_about', Settings::get('site_about'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Ключевые слова сайта</label>
    <?php echo Html::textInput('site_key', Settings::get('site_key'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Краткое название сайта</label>
    <?php echo Html::textInput('short_name', Settings::get('short_name'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Поделиться в соц.сетях</label>
    <?php echo Html::textarea('share', Settings::get('share'), ['class' => 'form-control']) ?>
</div>

<div class="form-group">
    <label class="control-label">Копирайт</label>
    <?php echo \dosamigos\tinymce\TinyMce::widget([
        'name' => 'copyright',
        'value' => Settings::get('copyright')
    ]) ?>
</div>

<div class="form-group">
    <label class="control-label">Выключить сайт</label>
    <?php echo Html::hiddenInput('site_off', '0') ?>
    <?php echo Html::checkbox('site_off', Settings::get('site_off')) ?>
</div>

<div class="form-group">
    <label class="control-label">Причина отключения сайта</label>
    <?php echo Html::textInput('off_about', Settings::get('off_about'), ['class' => 'form-control']) ?>
</div>
<?php $this->endBlock() ?>
<?php $this->beginBlock('settings_counters') ?>
<div class="form-group">
    <label class="control-label">Код счетчиков (metrika, google analitics и.т.д)</label>
    <?php echo Html::textarea('counters', Settings::get('counters'), ['class' => 'form-control']) ?>
</div>
<?php $this->endBlock() ?>

<?php echo Tabs::widget([
    'items' => [
        ['label' => 'Основные', 'content' => $this->blocks['settings_main']],
        ['label' => 'Счетчики', 'content' => $this->blocks['settings_counters']],
        ['label' => 'Изображения', 'content' => $this->render('_images', ['images' => $images])],
        ['label' => 'Ключи API', 'content' => $this->render('_api')],
        ['label' => 'SEO', 'content' => $this->render('_seo')]
        ]
]); ?>
<div class="form-group">
    <?php echo Html::submitInput('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
<?php echo Html::endForm(); ?>
