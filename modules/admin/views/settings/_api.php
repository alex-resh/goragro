<?php

use yii\helpers\Html;
use app\models\Settings;

?>
<div class="form-group">
    <label class="control-label">Яндекс.Погода</label>
    <?php echo Html::textInput('ya_weather', Settings::get('ya_weather'), ['class' => 'form-control']) ?>
</div>
<div class="form-group">
    <label class="control-label">Яндекс.Геокодер</label>
    <?php echo Html::textInput('ya_geocoder', Settings::get('ya_geocoder'), ['class' => 'form-control']) ?>
</div>
<div class="form-group">
    <label class="control-label">reCAPTCHA site key</label>
    <?php echo Html::textInput('recaptcha_site_key', Settings::get('recaptcha_site_key'), ['class' => 'form-control']) ?>
</div>
<div class="form-group">
    <label class="control-label">reCAPTCHA secret key</label>
    <?php echo Html::textInput('recaptcha_secret_key', Settings::get('recaptcha_secret_key'), ['class' => 'form-control']) ?>
</div>