<?php

/**
 * @var $this \yii\web\View
 * @var $images array
 */

use yii\helpers\Html;

?>
<div class="alert alert-warning">
    При изменении размеров будут перерисованы все ранее залытые изображения данного типа.
</div>
<?php foreach ($images as $title => $image) : ?>
    <?php $i = 1;?>
    <div class="form-group clearfix">
        <h4><?= $title;?></h4>
        <?php foreach ($image->fieldNames as $key => $value) : ?>
            <?php if($i % 2) :?>
                <div class="col-md-3 panel panel-info">
                <div><?= Html::label(preg_replace('`(^.*?\:|\:.*$)`', '', $key));?></div>
                <?= Html::label('ширина ' . Html::textInput($key, $value, ['class' => 'form-control']), null, ['class' => 'col-md-6']);?>
            <?php else : ?>
                <?= Html::label('высота ' . Html::textInput($key, $value, ['class' => 'form-control col-md-6']), null, ['class' => 'col-md-6']);?>
                </div>
            <?php endif;?>
            <?php $i++;?>
        <?php endforeach;?>
    </div>
<?php endforeach;?>