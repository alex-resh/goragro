<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

\app\assets\AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <script src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
</head>
<body style="background: #eee;">


<aside class="sidebar">
    <div id="leftside-navigation" class="nano">
        <ul class="nano-content">
            <li>
                <a href="/admin"><i class="fa fa-dashboard"></i><span>Админ. панель</span></a>
            </li>


            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa-cogs"></i><span>Публикации</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>

                    <li><a href="/admin/news/index">Новости</a>
                    </li>
                    <li><a href="/admin/blog/index">Блоги</a>
                    </li>
                    <li><a href="/admin/page/index">Страницы</a>
                    </li>
                    <li><a href="/admin/sight/index">Достопримечательности</a>
                    </li>
                </ul>
            </li>


            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa-table"></i><span>Категории</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>
                    <li><a href="/admin/newscategory/index">Категории новостей</a>
                    </li>
                    <li><a href="/admin/additionally">Дополнительные данные</a>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa fa-tasks"></i><span>Справочная</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>
                    <li><a href="/admin/organisation">Организации
                            (<?php echo \app\models\Organisation::getCountAdminMenu() ?>)</a>
                    </li>

                    <li><a href="/admin/product">Продускты</a>
                    </li>

                    <li><a href="/admin/product/category">Категории продуктов</a>
                    </li>

                    <li>
                        <a href="/admin/agents/index"><span>Представители (<?php echo \app\models\Agent::getCountNew() ?>)</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/comment/manage"><span>Отзывы (<?php echo \app\models\Comments::getCountNew(); ?>)</span></a>
                    </li>
                    <li><a href="/admin/org-category">Категории организаций</a>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa fa-tasks"></i><span>Пользователи</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>
                    <li class="sub-menu">
                        <a href="/user/admin/index"><span>Список</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/rbac/assignment/index"><span>Сопостоавления</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/rbac/role/index"><span>Роли</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/rbac/permission/index"><span>Разрешения</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/rbac/route/index"><span>Маршруты</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="/rbac/rule/index"><span>Правила</span></a>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa-bar-chart-o"></i><span>Коментарии</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>
                    <li><a href="/admin/comment/setting">Настроки</a>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="/admin/cities/index"><i class="fa fa-map-marker"></i><span>Города</span></a>

            </li>
            <li class="sub-menu">
                <a href="javascript:void(0);"><i class="fa fa-file"></i><span>Работа с почтой</span><i
                            class="arrow fa fa-angle-right pull-right"></i></a>
                <ul>
                    <li><a href="/admin/mail/mailing">Рассылки</a>
                    </li>
                    <li><a href="/admin/mail/template">Шаблоны писем</a>
                    </li>

                </ul>
            </li>
            <li class="sub-menu">
                <a href="/admin/settings/index"><i class="fa fa-envelope"></i><span>Настройки</span></a>
            </li>
            <li class="sub-menu">
                <a href="/poll"><i class="fa fa-envelope"></i><span>Голосование</span></a>
            </li>
        </ul>
    </div>
</aside>

<div class="container" style="width: auto; margin-left: 240px;">
    <div class="row">
      <div class="box-headtop">
        <p><?php if(!Yii::$app->user->isGuest) echo Yii::$app->user->identity->username; ?></p>
        <a href="#">Выход</a>
      </div>
        <div class="col-lg-12"><?= $content ?></div>
    </div>
</div>

<style>

    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,700');
@import url('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css');
body {
  color: #5D5F63;
  background: #293949;
  font-family: 'Open Sans', sans-serif;
  padding: 0;
  margin: 0;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
}
.sidebar-toggle {
  margin-left: -240px;
}
.sidebar {
  width: 240px;
  height: 100%;
  background: #293949;
  position: absolute;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  -ms-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
  z-index: 100;
}
.sidebar #leftside-navigation ul,
.sidebar #leftside-navigation ul ul {
  margin: -2px 0 0;
  padding: 0;
}
.sidebar #leftside-navigation ul li {
  list-style-type: none;
  border-bottom: 1px solid rgba(255,255,255,0.05);
}
.sidebar #leftside-navigation ul li.active > a {
  color: #1abc9c;
}
.sidebar #leftside-navigation ul li.active ul {
  display: block;
}
.sidebar #leftside-navigation ul li a {
  color: #aeb2b7;
  text-decoration: none;
  display: block;
  padding: 18px 0 18px 25px;
  font-size: 12px;
  outline: 0;
  -webkit-transition: all 200ms ease-in;
  -moz-transition: all 200ms ease-in;
  -o-transition: all 200ms ease-in;
  -ms-transition: all 200ms ease-in;
  transition: all 200ms ease-in;
}
.sidebar #leftside-navigation ul li a:hover {
  color: #1abc9c;
}
.sidebar #leftside-navigation ul li a span {
  display: inline-block;
}
.sidebar #leftside-navigation ul li a i {
  width: 20px;
}
.sidebar #leftside-navigation ul li a i .fa-angle-left,
.sidebar #leftside-navigation ul li a i .fa-angle-right {
  padding-top: 3px;
}
.sidebar #leftside-navigation ul ul {
  display: none;
}
.sidebar #leftside-navigation ul ul li {
  background: #23313f;
  margin-bottom: 0;
  margin-left: 0;
  margin-right: 0;
  border-bottom: none;
}
.sidebar #leftside-navigation ul ul li a {
  font-size: 12px;
  padding-top: 13px;
  padding-bottom: 13px;
  color: #aeb2b7;
}

.box-headtop{
    background-color: rgba(126, 188, 80, 0.87);
    height: 50px;
    display: flex;
    width: auto;
    justify-content: space-between;
    align-items: center;
    padding-left: 35px;
    padding-right: 35px;
      font-weight: 600;
  font-size: 18px;
  line-height: 35px;
  color: #ffffff;
  letter-spacing: -1.2px;
  transition: 0.5s;
}
.box-headtop a,.box-headtop p {
  margin: 0;
  color: #fff;
}
.box-headtop p,a:hover{
 
  text-decoration: none;
  color: #fff;
}
a{
  color: #6eb13f;
}
a:hover{
  color: #568c30;
  transition: 0.5s;

}

</style>

<script>
    $("#leftside-navigation .sub-menu > a").click(function(e) {
  $("#leftside-navigation ul ul").slideUp(), $(this).next().is(":visible") || $(this).next().slideDown(), e.stopPropagation()
})
</script>

<!-- <footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
    </div>
</footer> -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
