<?php

/**
 * @var $this View
 * @var $model \app\models\Additionally
 * @var $el_model ElForm
 */

use app\assets\AdditionallyFieldAsset;
use app\assets\AppAsset;
use app\models\forms\ElForm;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\bootstrap\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use app\assets\AdditionallyFormAsset;
AppAsset::register($this);
AdditionallyFormAsset::register($this);
AdditionallyFieldAsset::register($this);
?>
<div class="row">
    <div>
        <?php $form = ActiveForm::begin([
            'id' => 'additionallyForm',
        ]);?>
        <?= $form->field($model, 'title');?>
        <?= $form->field($model, 'desc')->textarea();?>
        <?= $form->field($model, 'catIds')->checkboxList((array)\app\models\Category::find()->select('name')->indexBy('id')->column())->label('Категории');?>
        <?= $form->field($model, 'data')->hiddenInput()->label(false);?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-md btn-success']);?>
        <h4>Предпросмотр</h4>
        <div class="form-group col-md-9">
            <div class="row preview additional-info">
                <?php if($model->data && $data = Json::decode($model->data)) : ?>
                    <?php
                    $dataJs = Json::encode($data);
                    $dataJs = str_replace('\\', '\\\\', $model->data);
                    ?>
                    <?php $this->registerJs(<<<JS
                var resData = JSON.parse('$dataJs');
JS
                        , View::POS_HEAD);?>
                    <h4><?= $model->title;?></h4>
                    <?php foreach($data['rows'] as $row) :?>
                        <div class="row additional-info__row">
                            <?php foreach ($row['columns'] as $column) :?>
                                <div class="col col-md-4 additional-info__column">
                                    <?php foreach ($column['elements'] as $el) :?>
                                        <div class="el form-group additional-info__section js-field"><span class="js-edit glyphicon glyphicon-edit" title="Редактировать"></span><span class="control js-delete" title="Удалить элемент">x</span>
                                            <?php if(isset($el['label']) && $el['el'] != 'radio' && $el['el'] != 'checkBox') :?>
                                                <h5 class="additional__title"><?= $el['label'];?></h5>
                                            <?php endif;?>
                                            <?= ElForm::field($el);?>
                                        </div>
                                    <?php endforeach;?>
                                    <span class="control js-add-el">Добавить элемент</span>
                                    <span class="control js-delete" title="Удалить колонку">x</span>
                                </div>
                            <?php endforeach;?>
                            <span class="control js-add-column">Добавить колонку</span>
                            <span class="control js-delete" title="Удалить строку">x</span>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
                <span class="control js-add-row">Добавить строку</span>
            </div>
        </div>
        <div class="col-md-3">
            <h4>Фильтры</h4>
            <?= $this->render('_filter_block', [
                'model' => $model,
            ]);?>
        </div>
        <?php ActiveForm::end();?>
    </div>
</div>

<?php Modal::begin([
    'id' => 'add-el-modal',
    'header' => 'Добавить элемент',
    'size' => Modal::SIZE_DEFAULT,
    'footer' => Html::button('Очистить', ['class' => 'js-el-reset btn btn-md btn-default']) . ' ' .
    Html::button('Сохранить', ['class' => 'js-el-form-submit btn btn-md btn-success']),
]);?>
<?php $form = ActiveForm::begin([
    'id' => 'el-form',
    'action' => ['/admin/additionally/create-el'],
]);
$el_model->options = '';
$el_model->listData = '';
$el_model->widgetOptions = '';
$el_model->filterOptions = '';
$el_model->filterListData = '';
$el_model->filterWidgetOptions = '';
?>
<div class="form-group">
    <div class="col-md-6">
        <?= $form->field($el_model, 'label', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Имя']])->label(false);?>
        <?= $form->field($el_model, 'icon')->widget(Select2::class, [
            'data' => ElForm::iconList(),
            'theme' => Select2::THEME_BOOTSTRAP,
            'options' => [
                'prompt' => 'Выберите иконку',
            ],
            'pluginOptions' => [
                'tags' => true,
                'templateResult' => new JsExpression(<<<JS
    function (item) {
        if(!item.id){
            return item.text;
        }
        return $('<span class="glyphicon glyphicon-' + item.text + '"> ' + item.text + ' </span>');
    }
JS
                ),
                'templateSelection' => new JsExpression(<<<JS
    function (item) {
        if(!item.id){
            return item.text;
        }
        return $('<span class="glyphicon glyphicon-' + item.text + '"> ' + item.text + ' </span>');
    }
JS
                ),
                'escapeMarkup' => new JsExpression('function(m) { return m; }'),
            ],
        ])->label(false);?>
    </div>
    <div class="col-md-6">
        <?= $form->field($el_model, 'name', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Атрибут']])->label(false);?>
        <?= $form->field($el_model, 'default_value', ['inputOptions' =>['class' => 'form-control', 'placeholder' => 'Значение поумолчанию']])->label(false);?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="form-group">
    <div class="panel panel-info col-md-6">
        <?= $form->field($el_model, 'el')->dropDownList($el_model->elList, ['class' => 'form-control', 'prompt' => 'Выберите элемент']);?>
        <?= $form->field($el_model, 'options', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Опции']])->textarea();?>
        <?= $form->field($el_model, 'listData', ['options' => ['class' => 'hidden'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Список']])->textarea();?>
        <?= $form->field($el_model, 'widgetOptions', ['options' => ['class' => 'hidden'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Опции Виджета']])->textarea();?>
    </div>
    <div class="panel panel-info col-md-6">
        <?= $form->field($el_model, 'filter')->dropDownList($el_model->elList, ['class' => 'form-control', 'prompt' => 'Выберите фильтр']);?>
        <?= $form->field($el_model, 'filterOptions', ['inputOptions' => ['class' => 'form-control', 'placeholder' => 'Опции']])->textarea();?>
        <?= $form->field($el_model, 'filterListData', ['options' => ['class' => 'hidden'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Список']])->textarea();?>
        <?= $form->field($el_model, 'filterWidgetOptions', ['options' => ['class' => 'hidden'], 'inputOptions' => ['class' => 'form-control', 'placeholder' => 'Опции Виджета']])->textarea();?>
    </div>
</div>
<div class="clearfix"></div>
<?php ActiveForm::end();?>
<?php Modal::end();?>
