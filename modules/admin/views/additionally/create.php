<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Additionally
 * @var $el_model \app\models\forms\ElForm
 */
$this->title = 'Создать форму';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = ['label' => 'Категории Организаций', 'url' => ['/admin/org-category']];
$this->params['breadcrumbs'][] = ['label' => 'Дополнительные данные', 'url' => ['/admin/additionally']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h4><?= $this->title;?></h4>
<?= $this->render('_form', [
    'model' => $model,
    'el_model' => $el_model,
]);?>
