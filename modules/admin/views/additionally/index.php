<?php

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
$this->title = 'Дополнительные данные';
$this->params['breadcrumbs'][] = ['label' => 'Админ-панель', 'url' => ['/admin/default']];
$this->params['breadcrumbs'][] = ['label' => 'Категории организаций', 'url' => ['/admin/org-category']];
$this->params['breadcrumbs'][] = $this->title;

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Json;

?>
<h4><?= $this->title;?></h4>
<div class="form-group">
    <?= Html::a('Создать форму', ['create'], ['class' => 'btn btn-md btn-primary']);?>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'title',
        'desc',
        'type',
        'status',
        ['class' => 'yii\grid\ActionColumn']
    ],
]);?>
