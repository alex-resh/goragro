<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\forms\ElForm
 * @var $html string
 * @var $filter string
 */
echo '<div class="el form-group additional-info__section js-field"><span class="js-edit glyphicon glyphicon-edit" title="Редактировать"></span> <span class="control js-delete" title="Удалить элемент">x</span>' .
    ((isset($model->label) && $model->el != 'radio' && $model->el != 'checkBox') ? '<h5 class="additional__title">' . $model->label . '</h5>' : '') .
    $html . '</div>' .
    '<div class="filter-el form-group">' .
    ((isset($model->label) && $model->el != 'radio' && $model->el != 'checkBox') ? '<h5 class="additional__title">' . $model->label . '</h5>' : '') .
    $filter . '</div>';