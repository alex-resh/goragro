<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Additionally
 */

use app\models\forms\ElForm;
use conquer\helpers\Json;

$data = Json::decode($model->data);
?>
<div class="filter-block">
    <div class="h5"><?= $model->title;?></div>
    <?php foreach ((array)$data['rows'] as $row) : ?>
        <div class="filter-row">
            <?php foreach ($row['columns'] as $column) : ?>
            <div class="filter-column">
                <?php foreach ($column['elements'] as $element) : ?>
                    <div class="filter-el form-group">
                        <?php if(isset($element['label']) && $element['el'] != 'radio' && $element['el'] != 'checkBox') :?>
                            <h5 class="additional__title"><?= $element['label'];?></h5>
                        <?php endif;?>
                        <?= ElForm::filter($element);?>
                    </div>
                <?php endforeach;?>
            </div>
            <?php endforeach;?>
        </div>
    <?php endforeach;?>
</div>
