(function($){
    var row = '<div class="row">' +
        '<span class="control js-add-column">Добавить колонку</span>' +
        '<span class="control js-delete" title="Удалить строку">x</span>' +
        '</div>';
    var column = '<div class="col col-md-4">' +
        '<span class="control js-add-el">Добавить элемент</span>' +
        '<span class="control js-delete" title="Удалить колонку">x</span> ' +
        '</div>';
    var filterRow = '<div class="filter-row"></div>';
    var filterColumn = '<div class="filter-column"></div>';
    var resData = window.resData ? window.resData : {rows: []};
    var prepData = function(data){
        var res = {};
        $(data).each(function (i, item) {
            if(i > 0){
                var name = item.name.replace(/^.*?\[(.*?)\]$/, '$1');
                var value = item.value;
                try {
                    value = JSON.parse(value);
                } catch (e) {
                }
                res[name] = value;
            }
        });
        return res;
    };
    $(function(){
        $('body').on('mouseover', '.control, .js-edit', function () {
            var outline = $(this).is('.js-delete') ? 'out-red' : 'out-green';
            var parent = $(this).parent()
            parent.addClass(outline);
            if(parent.is('.row')){
                var rowIndex;
                rowIndex = parent.prevAll('.row').length;
                $('.filter-row').eq(rowIndex).addClass(outline);
            } else if(parent.is('.col')){
                var colIndex = parent.prevAll('.col').length;
                var rowIndex = parent.parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).addClass(outline);
            } else if(parent.is('.el')) {
                var elIndex = parent.prevAll('.el').length;
                var colIndex = parent.parent().prevAll('.col').length;
                var rowIndex = parent.parent().parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).find('.filter-el').eq(elIndex).addClass(outline);
            }
        });

        $('body').on('mouseleave', '.control, .js-edit', function () {
            var parent = $(this).parent();
            parent.removeClass('out-red').removeClass('out-green');
            var rowIndex;
            if(parent.is('.row')){
                rowIndex = parent.prevAll('.row').length;
                $('.filter-row').eq(rowIndex).removeClass('out-red').removeClass('out-green');
            } else if(parent.is('.col')){
                var colIndex = parent.prevAll('.col').length;
                var rowIndex = parent.parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).removeClass('out-red').removeClass('out-green');
            } else if(parent.is('.el')){
                var elIndex = parent.prevAll('.el').length;
                var colIndex = parent.parent().prevAll('.col').length;
                var rowIndex = parent.parent().parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).find('.filter-el').eq(elIndex).removeClass('out-red').removeClass('out-green');
            }
        });

        $('body').on('click', '.js-add-row', function () {
            $(this).before(row);
            $('.filter-block').append(filterRow);
            resData.rows.push({columns: []});
        });
        $('body').on('click', '.js-delete', function () {
            var parent = $(this).parent();
            if(parent.is('.row')){
                var rowIndex = parent.prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).remove();
                resData.rows.splice(rowIndex, 1);
            }
            if(parent.is('.col')){
                var colIndex = parent.prevAll('.col').length;
                var rowIndex = parent.parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).remove();
                resData.rows[rowIndex].columns.splice(colIndex, 1);
            }
            if(parent.is('.el')){
                var elIndex = parent.prevAll('.el').length;
                var colIndex = parent.parent().prevAll('.col').length;
                var rowIndex = parent.parent().parent().prevAll('.row').length;
                $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).find('.filter-el').eq(elIndex).remove();
                resData.rows[rowIndex].columns[colIndex].elements.splice(elIndex, 1);
            }
            parent.remove();
        });

        $('body').on('click', '.js-edit', function(){
            var parent = $(this).parent();
            $('.priview .active').removeClass('active');
            $(this).addClass('active');
            var elIndex = parent.prevAll('.el').length;
            var colIndex = parent.parent().prevAll('.col').length;
            var rowIndex = parent.parent().parent().prevAll('.row').length;
            var data = resData.rows[rowIndex].columns[colIndex].elements[elIndex];
            for (item in data) {
                if(typeof data[item] != "string"){
                    data[item] = JSON.stringify(data[item]);
                }
                $('#add-el-modal [name="ElForm[' + item + ']"]').val(data[item]);
            }
            $('#add-el-modal').modal('show');
            $('#add-el-modal select').trigger('change');
        });

        $('body').on('click', '.js-add-column', function () {
            $(this).before(column);
            var rowIndex = $(this).parent().prevAll('.row').length;
            $('.filter-block .filter-row').eq(rowIndex).append(filterColumn);
            resData.rows[rowIndex].columns.push({elements: []});
        });

        $('body').on('click', '.js-add-el', function () {
            $('.priview .active').removeClass('active');
            $(this).addClass('active');
            $('#add-el-modal').modal('show');
        });
        $('body').on('click', '.js-el-reset', function () {
            $('#elform-icon').select2('val', '_');
            $('#add-el-modal .modal-body').find('input, textarea, select').val('');
        });

        $('body').on('click', '.js-el-form-submit', function () {
            var t = $(this);
            var modal = $('#add-el-modal');
            var form = $('form', modal);
            var data = form.serializeArray();
            $.ajax({
                url: form.attr('action'),
                method: 'post',
                data: data,
                dataType: 'json'
            }).done(function (res) {
                if(typeof res == 'string'){
                    var active = $('.js-add-el.active, .js-edit.active');
                    var colIndex = active.parent().prevAll('.col').length;
                    var rowIndex = active.parent().parent().prevAll('.row').length;
                    if(active.is('.js-add-el')){
                        active.before(res);
                        active.prevAll('.filter-el').appendTo($('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex));
                        resData.rows[rowIndex].columns[colIndex].elements.push(prepData(data));
                    } else {
                        var elIndex = active.parent().prevAll('.el').length;
                        colIndex = active.parent().parent().prevAll('.col').length;
                        rowIndex = active.parent().parent().parent().prevAll('.row').length;
                        active.parent('.el').replaceWith(res);
                        $('.filter-block .filter-row').eq(rowIndex).find('.filter-column').eq(colIndex).find('.filter-el').eq(elIndex).replaceWith($('.el + .filter-el'));
                        resData.rows[rowIndex].columns[colIndex].elements[elIndex] = prepData(data);
                    }
                    active.removeClass('active');
                    modal.modal('hide');
                } else {
                    form.yiiActiveForm('updateMessages', res, true);
                }
            }).fail(function (res) {
                console.log(res.responseText);
            });
        });
        $('#additionallyForm').on('submit', function () {
            var data = JSON.stringify(resData);
            data = data.replace(/((^|[^\\])\\([^\\]|$))/, '$2\\$3');
            $('#additionally-data').val(data);
        });
        $('select[name="ElForm[el]"]').on('change', function(){
            if(/list/i.test($(this).val())){
                $('.field-elform-listdata').removeClass('hidden');
                $('.field-elform-widgetoptions').addClass('hidden');
            } else if($(this).val() == 'widget'){
                $('.field-elform-listdata').addClass('hidden');
                $('.field-elform-widgetoptions').removeClass('hidden');
            } else {
                $('.field-elform-listdata').addClass('hidden');
                $('.field-elform-widgetoptions').addClass('hidden');
            }
        });
        $('select[name="ElForm[filter]"]').on('change', function(){
            if(/list/i.test($(this).val())){
                $('.field-elform-filterlistdata').removeClass('hidden');
                $('.field-elform-filterwidgetoptions').addClass('hidden');
            } else if($(this).val() == 'widget'){
                $('.field-elform-filterlistdata').addClass('hidden');
                $('.field-elform-filterwidgetoptions').removeClass('hidden');
            } else {
                $('.field-elform-filterlistdata').addClass('hidden');
                $('.field-elform-filterwidgetoptions').addClass('hidden');
            }
        });
    });
})(jQuery);