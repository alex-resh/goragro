<?php


namespace app\modules\admin\controllers;


use app\models\MailingTemplate;
use Yii;
use yii\web\Controller;
use app\models\search\MailingSearch;
use app\models\search\MailingTemplateSearch;

class MailController extends Controller
{
    public function actionTemplate()
    {
        $searchModel = new MailingTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('template', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMailing()
    {
        $searchModel = new MailingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('mailing', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreateTemplate()
    {
        $model = new MailingTemplate();
        return $this->render('create_template', [
            'model' => $model,
        ]);
    }
}