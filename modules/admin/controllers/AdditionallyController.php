<?php


namespace app\modules\admin\controllers;


use app\models\Additionally;
use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use app\models\search\AdditionallySearch;
use app\models\forms\ElForm;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdditionallyController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new AdditionallySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $el_model = new ElForm();
        $model = new Additionally();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/admin/additionally/index']);
        }
        return $this->render('create', [
            'model' => $model,
            'el_model' => $el_model,
        ]);
    }

    public function actionCreateEl()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ElForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            if(in_array($model->el, $model->getListElements())){
                $html = Html::{$model->el}($model->name, $model->default_value, $model->listData ? Json::decode($model->listData) : [], $model->options ? Json::decode($model->options) : []);
            } else if($model->el == 'widget'){
                $options = $model->widgetOptions ? Json::decode($model->widgetOptions, true) : [];
                $class = $options['class'];
                $options = array_merge($options, [
                    'name' => $model->name,
                ]);
                $html = $class::widget($options);
            } else {
                $html = Html::{$model->el}($model->name, $model->default_value, Json::decode($model->options));
            }
            if($model->default_value){
                try {
                    $model->default_value = Json::decode($model->default_value);
                } catch (\Exception $e) {}
            }
            if($model->options){
                try {
                    $model->options = Json::decode($model->options);
                } catch (\Exception $e) {}
            }
            if($model->listData){
                try {
                    $model->listData = Json::decode($model->listData);
                } catch (\Exception $e) {}
            }
            if($model->widgetOptions){
                try {
                    $model->widgetOptions = Json::decode($model->widgetOptions);
                } catch (\Exception $e) {}
            }
            if($model->filterOptions){
                try {
                    $model->filterOptions = Json::decode($model->filterOptions);
                } catch (\Exception $e) {}
            }
            if($model->filterListData){
                try {
                    $model->filterListData = Json::decode($model->filterListData);
                } catch (\Exception $e) {}
            }
            if($model->filterWidgetOptions){
                try {
                    $model->filterWidgetOptions = Json::decode($model->filterWidgetOptions);
                } catch (\Exception $e) {}
            }
            $html = ElForm::field($model->attributes);
            $filter = ElForm::filter($model->attributes);
            return $this->renderAjax('_additionally_element', [
                'model' => $model,
                'html' => $html,
                'filter' => $filter,
            ]);
        } else {
            return ActiveForm::validate($model);
        }
    }

    public function actionUpdate($id)
    {
        $el_model = new ElForm();
        $model = Additionally::findOne($id);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/admin/additionally/index']);
        }
        return $this->render('update', [
            'model' => $model,
            'el_model' => $el_model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Additionally::findOne($id);
        $model->delete();
        return $this->redirect(['/admin/additionally']);
    }
}