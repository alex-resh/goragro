<?php


namespace app\modules\admin\controllers;

use app\models\FsCity;
use app\models\News;
use app\models\search\NewsSearch;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

class NewsController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new News();
        $model->type = News::TYPE_NEWS;
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        $model->tags = $model->getTagsList();
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    private function getModel($id)
    {
        $model = News::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $model;
    }

    public function actionCityList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('id, name AS text')
                ->from('fs_city')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => FsCity::find($id)->name];
        }
        return $out;
    }

    public function actionDelete($id){
        $model = $this->getModel($id);
        $model->delete();
        return $this->redirect(['index']);
    }

}