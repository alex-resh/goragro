<?php


namespace app\modules\admin\controllers;


use app\components\Status;
use app\models\Comments;
use yii\web\NotFoundHttpException;

class ManageController extends \yii2mod\comments\controllers\ManageController
{

    public function actionApprove($id){
        $model = Comments::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException('Не найден');
        }
        $model->status = Status::APPROVED;
        $model->save();
        return $this->redirect(['index']);
    }

}