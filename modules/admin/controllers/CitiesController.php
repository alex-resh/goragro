<?php


namespace app\modules\admin\controllers;


use app\models\FsCity;
use app\models\search\FsCitySearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CitiesController extends Controller
{

    public function actionIndex(){
        $searchModel = new FsCitySearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionUpdate($id){
        $model = $this->getModel($id);
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            \Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(Url::to(['index']));
//            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    private function getModel($id){
        $model = FsCity::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $model;
    }

}