<?php


namespace app\modules\admin\controllers;


use app\models\CommentImage;
use app\models\FsCity;
use app\models\Image;
use app\models\News;
use app\models\Organisation;
use app\models\OrganisationGallery;
use app\models\Profile;
use app\models\Settings;
use app\models\SightGallery;
use Yii;
use yii\web\Controller;
use app\events\SettingsEvent;

class SettingsController extends Controller
{

    public function actionIndex(){
        $images = [
            'Аватар' => new Profile(),
            'Новости' => new News(),
            'Город' => new FsCity(),
            'Изображеня' => new Image(),
            'Организация' => new Organisation(),
            'Галерея организации' => new OrganisationGallery(),
            'Изображения в коментариях' => new CommentImage(),
//            'Ещё одна галерея' => new SightGallery(),
        ];
        $changed = [];
        if(Yii::$app->request->isPost){
            $post = Yii::$app->request->post();
            foreach ($post as $key => $item){
                if($key == '_csrf'){
                    continue;
                }
                $model = Settings::findOne(['key' => $key]);
                if(!$model){
                    $model = new Settings();
                    $model->key = $key;
                }
                $model->value = $item;
                if($model->isNewRecord || $model->value != $model->oldAttributes['value']){
                    $changed[$model->key] = $model->value;
                }
                $model->save();
            }
            if(count($changed)){
                $event = new SettingsEvent([
                    'attributes' => $changed,
                ]);
                foreach ($images as $image) {
                    Yii::$app->on(Settings::EVENT_CHANGE, [$image, 'afterChangeSettings']);
                }
                Yii::$app->trigger(Settings::EVENT_CHANGE, $event);
            }
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->refresh();
        }
        return $this->render('index', [
            'images' => $images,
        ]);
    }

}