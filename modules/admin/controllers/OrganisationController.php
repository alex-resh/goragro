<?php


namespace app\modules\admin\controllers;


use app\assets\AppAsset;
use app\models\Organisation;
use app\models\search\OrganisationAdminSearch;
use Yii;
use yii\web\Controller;
use yii2mod\rbac\filters\AccessControl;

class OrganisationController extends Controller
{
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
//                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $searchModel = new OrganisationAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['!=', 'clone_id', '']);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionNew()
    {
        $searchModel = new OrganisationAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['clone_id' => null, 'status' => Organisation::STATUS_NEW]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionModerated()
    {
        $searchModel = new OrganisationAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['clone_id' => null, 'status' => Organisation::STATUS_MODERATED]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionReworked()
    {
        $searchModel = new OrganisationAdminSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['clone_id' => null, 'status' => Organisation::STATUS_REWORK]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate()
    {
        $controller = Yii::$app->createController('/organisation')[0];
        $controller->layout = '@app/modules/admin/views/layouts/admin';
        AppAsset::register($controller->view);
        return $controller->actionCreate();
    }

    public function actionView($id)
    {
        $controller = Yii::$app->createController('/organisation')[0];
        $controller->layout = '@app/modules/admin/views/layouts/admin';
        AppAsset::register($controller->view);
        return $controller->actionIndex($id);
    }

    public function actionUpdate($id)
    {
        $controller = Yii::$app->createController('/organisation/update')[0];
        $controller->layout = '@app/modules/admin/views/layouts/admin';
        AppAsset::register($controller->view);
        return $controller->actionUpdate($id);
    }

    public function actionPublish($id)
    {
        $model = Organisation::findOne($id);
        $model->status = Organisation::STATUS_PUBLISHED;
        $model->save();
        return $this->redirect(['/admin/organisation']);
    }

    public function actionRework($id)
    {
        $model = Organisation::findOne($id);
        $model->scenario = 'rework';
        $model->status = Organisation::STATUS_REWORK;
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/admin/organisation']);
        }
        Yii::$app->session->addFlash('error', $model->getFirstError('moderator_msg'));
        return $this->redirect(['/admin/organisation/view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        $model = Organisation::findOne($id);
        if($model->delete()){
            Yii::$app->session->addFlash('success', 'Организация #' . $model->id . ' "' . $model->name . '" успешно удалена из базы данных');
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка при удалении организации #' . $model->id . ' "' . $model->name . '"');
        }
        return $this->redirect(['/admin/organisation']);
    }
}