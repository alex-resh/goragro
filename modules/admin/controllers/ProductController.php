<?php


namespace app\modules\admin\controllers;

use app\models\ProductCategory;
use Yii;
use yii\web\Controller;

class ProductController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

    public function actionCategory()
    {
        return $this->render('category');
    }

    public function actionCheckCategory()
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            $model = ProductCategory::findOne(['category_id' => $request->post('id'), 'product_class_name' => $request->post('class')]);
            if($request->post('value') == true && !$model){
                $model = new ProductCategory(['category_id' => $request->post('id'), 'product_class_name' => $request->post('class')]);
                if($model->save()){
                    return 'Ok';
                } else {
                    echo $model->category_id;
                    echo '<pre>';
                    print_r($model->errors);
                    echo '</pre>';
                }
            } elseif($request->post('value') == 'false' && $model){
                $model->delete();
            }
        }
    }
}