<?php


namespace app\modules\admin\controllers;


use app\models\OrganisationGallery;
use app\models\search\SightSearch;
use app\models\Sight;
use app\models\SightGallery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SightController extends Controller
{

    public function actionIndex(){
        $searchModel = new SightSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionCreate(){
        $model = new Sight();
        $gallery = new SightGallery();
        $model->user_id = \Yii::$app->user->id;
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            SightGallery::updateAll(['sight_id' => $model->id], ['id' => $model->images]);
            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model, 'gallery' => $gallery]);
    }

    public function actionUpdate($id){
        $model = Sight::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        if($model->load(\Yii::$app->request->post()) && $model->save()){
            SightGallery::updateAll(['sight_id' => $model->id], ['id' => $model->images]);
            return $this->redirect(['index']);
        }
        $gallery = new SightGallery();
        return $this->render('update', ['model' => $model, 'gallery' => $gallery]);
    }

    public function actionDelete($id){
        $model = Sight::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    public function actionGalleryUpload(){
        $id = \Yii::$app->request->post('id');

        if(!$id) {
            $model = new SightGallery();
        } else {
            $model = SightGallery::findOne($id);
        }

        if($model->load(\Yii::$app->request->post()) && $model->save()){
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'id' => $model->id,
                'thumb' => $model->getThumbUploadUrl('img', 'preview')
            ];
        }
    }

    public function actionGalleryDelete(){
        $id = \Yii::$app->request->post('id');
        $model = SightGallery::findOne($id);
        if($model){
            $model->delete();
            return '1';
        }
    }

}