<?php


namespace app\modules\admin\controllers;


use app\events\SettingsEvent;
use app\models\Settings;
use Cassandra\Set;
use Yii;
use yii\web\Controller;
use app\models\forms\CommentSettingsForm;
use app\models\CommentImage;

class CommentController extends Controller
{
    public $layout = '@app/modules/admin/views/layouts/admin';

    public function actionSetting()
    {
        $model = new CommentSettingsForm();
        $model->number = Settings::get('comment_img_number', $model->number);
        $model->width = Settings::get('app\models\CommentImage:thumb:width', $model->width);
        $model->height = Settings::get('app\models\CommentImage:thumb:height', $model->height);
        $model->img_allowed = Settings::get('comments_img_allowed', $model->img_allowed);
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            Settings::set('comment_img_number', $model->number);
            Settings::set('app\models\CommentImage:thumb:width', $model->width);
            Settings::set('app\models\CommentImage:thumb:height', $model->height);
            Settings::set('comment_img_allowed', $model->img_allowed);
            $event = new SettingsEvent([
                'attributes' => [
                    'app\models\CommentImage:thumb:width',
                    'app\models\CommentImage:thumb:height',
                ],
            ]);
            (new CommentImage())->afterChangeSettings($event);
            Yii::$app->session->addFlash('success', 'Настроки сохранены');
        }
        return $this->render('setting', [
            'model' => $model,
        ]);
    }
}