<?php


namespace app\modules\admin\controllers;


use app\models\Agent;
use app\models\search\AgentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class AgentsController extends Controller
{

    public function actionIndex(){
        $searchModel = new AgentSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionApprove($id){
        $model = Agent::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $model->status = Agent::STATUS_ACTIVE;
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionDelete($id){
        $model = Agent::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Страница не найдена');
        }
        $model->delete();
        return $this->redirect(['index']);
    }

}