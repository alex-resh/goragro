<?php


namespace app\modules\admin\controllers;


use app\models\Category;
use app\models\search\CategorySearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class OrgCategoryController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate(){
        $model = new Category();
        if($model->load(Yii::$app->request->post()) && $model->validate()){
//            if(!$model->parent_id){
//                //$model->makeRoot();
//                $model->parent_id = 1;
//            }
//            $parent = Category::findOne($model->parent_id);
//            $model->appendTo($parent);
            $model->save();

            return $this->redirect(['index']);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id){
        $model = Category::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Not found');
        }
        if($model->load(Yii::$app->request->post()) && $model->validate()){
//            if(!$model->parent_id){
//                $model->parent_id = 1;
//            }
//            $parent = Category::findOne($model->parent_id);
//            $model->appendTo($parent);
            $model->save();
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete($id){
        $model = Category::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('Not found');
        }

        $model->delete();

        return $this->redirect(['index']);
    }

}