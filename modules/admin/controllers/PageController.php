<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\models\search\PageSearch;
use app\models\Page;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]);
    }

    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->getModel($id);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['index']);
        }
        return $this->render('update', ['model' => $model]);
    }

    private function getModel($id)
    {
        $model = Page::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Страница не найдена');
        }
        return $model;
    }
}