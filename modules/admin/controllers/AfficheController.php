<?php


namespace app\modules\admin\controllers;


use app\models\AfficheCategory;
use Yii;
use yii\web\Controller;
use app\models\search\AfficheCategorySearch;

class AfficheController extends Controller
{
    public function actionIndex($id)
    {
        return $this->render('options');
    }

    public function actionCategory()
    {
        $searchModel = new AfficheCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('category', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCategoryCreate()
    {
        $model = new AfficheCategory();
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['/admin/affiche/category']);
        }
        return $this->render('category_create', [
            'model' => $model,
        ]);
    }
}