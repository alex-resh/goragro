"use strict";
(function($){
    $(function(){
        $('body').on('click', '.rate-like__item', function(){
            var t = $(this);
            $.ajax({
                url:t.data('url'),
                dataType: 'json',
                data:{
                    entity:t.data('entity'),
                    entity_id:t.data('id'),
                    value:t.is('.rate-like__positive') ? 1 : -1
                }
            }).done(function(res){
                $('.up-' + t.data('entity') + '-' + t.data('id')).text(res.likes);
                $('.down-' + t.data('entity') + '-' + t.data('id')).text(res.dislikes);
            });
        });
    });
})(jQuery);