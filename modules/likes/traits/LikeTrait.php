<?php


namespace app\modules\likes\traits;


use app\models\Likes;
use yii\db\ActiveQuery;

trait LikeTrait
{
    public function entityName()
    {
        return self::class;
    }
    public static function getBest()
    {
        /** @var ActiveQuery $query */
        $query = self::find()
            ->addSelect(['*', 'rating' => '`likes`-`dislikes`'])
            ->leftJoin(Likes::tableName(), ['likes.entity' => self::class])
            ->orderBy(['rating' => SORT_DESC]);
        return $query;
    }
}