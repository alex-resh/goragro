<?php


namespace app\modules\likes;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class LikeAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/likes/src';

    public $depends = [
        JqueryAsset::class,
    ];

    public $js = [
        'js/likes.js',
    ];
}