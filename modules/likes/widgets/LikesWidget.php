<?php


namespace app\modules\likes\widgets;


use yii\base\Widget;
use app\models\Likes;
use app\modules\likes\LikeAsset;
use yii\helpers\Url;

class LikesWidget extends Widget
{

    public $model;

    public $likes = 0;
    public $dislikes = 0;

    public $entity;

    public function init()
    {
        parent::init();
        $model = Likes::findOne(['entity' => $this->model->entityName(), 'entity_id' => $this->model->id]);
        if($model){
            $this->likes = $model->likes;
            $this->dislikes = $model->dislikes;
        }
        $arr = explode('\\', $this->model->entityName());
        $this->entity = strtolower($arr[count($arr) - 1]);
        return $this;
    }

    public function run()
    {
        LikeAsset::register($this->view);
    }

    public function like($big = false){
        $url = Url::to(['likes/default/index']);
        $class = $big ? 'rate-like__item--big' : '';
        $str = '<div class="rate-like__positive rate-like__item ' . $class . '" data-entity="' . $this->entity . '" data-id="' . $this->model->id . '" data-url="' . $url .'">
                            <span class="rate-like__ico">
                                <svg class="ico ico-like-up"><use xlink:href="/images/svg/symbol/sprite.svg#like-up"></use>
                                </svg>
                            </span>
                <span class="rate-like__index up-' . $this->entity . '-' . $this->model->id . '">'.$this->likes.'</span>
            </div>';
        return $str;
    }

    public function dislike(){
        $url = Url::to(['likes/default/index']);
        $str = '<div class="rate-like__negative rate-like__item" data-entity="' . $this->entity . '" data-id="' . $this->model->id . '" data-url="' . $url .'">
                            <span class="rate-like__ico">
                                <svg class="ico ico-like-down"><use xlink:href="/images/svg/symbol/sprite.svg#like-down"></use>
                                </svg>
                            </span>
                <span class="rate-like__index down-' . $this->entity . '-' . $this->model->id . '">'.$this->dislikes.'</span>
            </div>';
        return $str;
    }

    public function likeText(){
        $url = Url::to(['likes/default/index']);
        $str = '<span class="comment__poll-yes comment__poll-answer rate-like__item rate-like__positive" data-entity="' . $this->entity . '" data-id="' . $this->model->id . '" data-url="' . $url .'">Да&nbsp;<span class="rate-like__index up-' . $this->entity . '-' . $this->model->id . '">'.$this->likes.'</span></span>';
        return $str;
    }

    public function dislikeText(){
        $url = Url::to(['likes/default/index']);
        $str = '<span class="comment__poll-no comment__poll-answer rate-like__item" data-entity="' . $this->entity . '" data-id="' . $this->model->id . '" data-url="' . $url .'">Нет&nbsp;<span class="rate-like__index down-' . $this->entity . '-' . $this->model->id . '">'.$this->dislikes.'</span></span>';
        return $str;
    }

    public function getObjectClass($is_like){
        return ($is_like ? 'like' : 'dislike') . '-' . $this->entity . '-' . $this->model->id . '-count';
    }
}