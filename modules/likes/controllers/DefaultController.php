<?php


namespace app\modules\likes\controllers;


use app\models\Likes;
use app\models\LikesToUser;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($entity, $entity_id, $value){
        $model = Likes::find()->where(['entity' => 'app\\models\\' . ucfirst($entity), 'entity_id' => $entity_id])->one();
        $voite = false;
        if(!$model){
            $model = new Likes();
            $model->entity = 'app\\models\\' . ucfirst($entity);
            $model->entity_id = $entity_id;
            $model->likes = 0;
            $model->dislikes = 0;
            $like_to_user = new LikesToUser();
            $like_to_user->like_id = $model->id;
            $like_to_user->user_id = Yii::$app->user->id;
            $like_to_user->value = $value > 0 ? 1 : -1;
            $like_to_user->save();
        } else {
            /* @var \app\models\LikesToUser $voite */
            $voite = $model->getUsers(Yii::$app->user->id)->one();
        }

        if(!$voite) {
            if ($value > 0) {
                $model->likes += 1;
            } else {
                $model->dislikes += 1;
            }
            $model->save();
            $like_to_user = new LikesToUser();
            $like_to_user->like_id = $model->id;
            $like_to_user->user_id = Yii::$app->user->id;
            $like_to_user->value = $value > 0 ? 1 : -1;
            $like_to_user->save();
        } else {
            if($value > 0){
                if($voite->value > 0){
                    $model->likes -= 1;
                    $voite->delete();
                } else {
                    $model->likes += 1;
                    $model->dislikes -= 1;
                    $voite->value = 1;
                    $voite->save();
                }
            } else {
                if($voite->value > 0){
                    $model->likes -= 1;
                    $model->dislikes +=1;
                    $voite->value = -1;
                    $voite->save();
                } else {
                    $model->dislikes -= 1;
                    $voite->delete();
                }
            }
            $model->save();
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'likes' => $model->likes,
            'dislikes' => $model->dislikes,
        ];
    }

}